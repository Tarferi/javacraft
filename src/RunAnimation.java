//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:16
//


import SCSharp.Mpq;
import SCSharp.MpqDirectory;
import SCSharp.UI.GlobalResources;
import SCSharp.UI.Painter;
import SCSharp.UI.SpriteManager;

public class RunAnimation   
{
    int sprite_number = 146;
    int animation_type = 11;
    Mpq mpq;
    SCSharp.UI.Sprite sprite;
    Painter painter;
    public RunAnimation(Mpq mpq) throws Exception {
        this.mpq = mpq;
        GlobalResources globals = new GlobalResources(mpq);
        //		globals.Ready += GlobalsReady;
        globals.load();
        createWindow();
        Timer.DelaySeconds(5);
        globalsReady();
    }

    void createWindow() throws Exception {
        Video.WindowIcon();
        Video.WindowCaption = "animation viewer";
        Surface surf = Video.SetVideoModeWindow(320, 200);
        Mouse.ShowCursor = false;
        painter = new Painter(surf, 100);
        SpriteManager.AddToPainter(painter);
    }

    boolean die() throws Exception {
        sprite.RunScript(1);
        return false;
    }

    void globalsReady() throws Exception {
        Console.WriteLine("GlobalsReady");
        sprite = SpriteManager.CreateSprite(mpq, sprite_number, 0, 0);
        sprite.RunScript(animation_type);
    }

    //		Timer.DelaySeconds (3);
    //		GLib.Timeout.Add (10000, die);
    void quit(Object sender, QuitEventArgs e) throws Exception {
        Events.QuitApplication();
    }

    public static void main(String[] args) throws Exception {
        RunAnimation.Main(args);
    }

    public static void Main(String[] args) throws Exception {
        Mpq mpq = new MpqDirectory(args[0]);
        RunAnimation la = new RunAnimation(mpq);
        Events.Quit += Quit;
        Events.Run();
    }

}


