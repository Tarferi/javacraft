//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:16
//


import CS2JNet.System.LCC.Disposable;
import SCSharp.Util;

public class TGA   
{
    public static void writeTGA(String filename, byte[] image, uint width, uint height) throws Exception {
        FileStream fs = File.OpenWrite(filename);
        try
        {
            {
                fs.WriteByte(0);
                fs.WriteByte(0);
                // no colormap
                fs.WriteByte(2);
                // rgb
                Util.WriteWord(fs, 0);
                // first color map entry
                Util.WriteWord(fs, 0);
                // number of colors in palette
                fs.WriteByte(0);
                // number of bites per palette entry
                Console.WriteLine("width = {0}, height = {1}", (ushort)width, (ushort)height);
                Util.WriteWord(fs, 0);
                // image x origin
                Util.WriteWord(fs, 0);
                // image y origin
                Util.WriteWord(fs, (ushort)width);
                // width
                Util.WriteWord(fs, (ushort)height);
                // height
                fs.WriteByte(24);
                // bits per pixel
                fs.WriteByte(32);
                fs.Write(image, 0, image.Length);
                fs.Close();
            }
        }
        finally
        {
            if (fs != null)
                Disposable.mkDisposable(fs).dispose();
             
        }
    }

}


