//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:16
//


import SCSharp.Mpq;
import SCSharp.MpqArchive;

public class ListMpq   
{
    public static void main(String[] args) throws Exception {
        ListMpq.Main(args);
    }

    public static void Main(String[] args) throws Exception {
        if (args.Length != 1)
        {
            Console.WriteLine("usage:  lsmpq.exe <mpq-file>");
            Environment.Exit(0);
        }
         
        Mpq mpq = new MpqArchive(args[0]);
        StreamReader sr = new StreamReader(Assembly.GetExecutingAssembly().GetManifestResourceStream("list.txt"));
        String entry = new String();
        while ((entry = sr.ReadLine()) != null)
        {
            entry = entry.ToLower();
            Stream mpq_stream = mpq.getStreamForResource(entry);
            if (mpq_stream == null)
                continue;
             
            Console.WriteLine("{0} {1}", entry, mpq_stream.Length);
            mpq_stream.Dispose();
        }
    }

}


