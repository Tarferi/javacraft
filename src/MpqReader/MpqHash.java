//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:05
//

package MpqReader;


public class MpqHash   
{
    public MpqHash() {
    }

    public uint Name1 = new uint();
    public uint Name2 = new uint();
    public uint Locale = new uint();
    public uint BlockIndex = new uint();
    public static final uint Size = 16;
    public MpqHash(BinaryReader br) throws Exception {
        Name1 = br.ReadUInt32();
        Name2 = br.ReadUInt32();
        Locale = br.ReadUInt32();
        BlockIndex = br.ReadUInt32();
    }

    public boolean getIsValid() throws Exception {
        return Name1 != uint.MaxValue && Name2 != uint.MaxValue;
    }

}


