//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:05
//

package MpqReader;

import MpqReader.MpqBlock;
import MpqReader.MpqHash;
import MpqReader.MpqHeader;
import MpqReader.MpqStream;

//
// MpqArchive.cs
//
// Authors:
//		Foole (fooleau@gmail.com)
//
// (C) 2006 Foole (fooleau@gmail.com)
// Based on code from StormLib by Ladislav Zezula
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
public class MpqArchive  extends IDisposable 
{
    private Stream mStream = new Stream();
    private MpqHeader mHeader = new MpqHeader();
    private long mHeaderOffset = new long();
    private int mBlockSize = new int();
    private MpqHash[] mHashes = new MpqHash[]();
    private MpqBlock[] mBlocks = new MpqBlock[]();
    private static uint[] sStormBuffer = new uint[]();
    static {
        try
        {
            sStormBuffer = buildStormBuffer();
        }
        catch (Exception __dummyStaticConstructorCatchVar0)
        {
            throw new ExceptionInInitializerError(__dummyStaticConstructorCatchVar0);
        }
    
    }

    public MpqArchive(String Filename) throws Exception {
        mStream = File.Open(Filename, FileMode.Open, FileAccess.Read);
        init();
    }

    public MpqArchive(Stream SourceStream) throws Exception {
        mStream = SourceStream;
        init();
    }

    public void dispose() throws Exception {
        if (mStream != null)
            mStream.Close();
         
    }

    private void init() throws Exception {
        if (locateMpqHeader() == false)
            throw new Exception("Unable to find MPQ header");
         
        BinaryReader br = new BinaryReader(mStream);
        mBlockSize = 0x200 << mHeader.BlockSize;
        // Load hash table
        mStream.Seek(mHeader.HashTablePos, SeekOrigin.Begin);
        byte[] hashdata = br.ReadBytes((int)(mHeader.HashTableSize * MpqHash.Size));
        DecryptTable(hashdata, "(hash table)");
        BinaryReader br2 = new BinaryReader(new MemoryStream(hashdata));
        mHashes = new MpqHash[mHeader.HashTableSize];
        for (int i = 0;i < mHeader.HashTableSize;i++)
            mHashes[i] = new MpqHash(br2);
        // Load block table
        mStream.Seek(mHeader.BlockTablePos, SeekOrigin.Begin);
        byte[] blockdata = br.ReadBytes((int)(mHeader.BlockTableSize * MpqBlock.Size));
        DecryptTable(blockdata, "(block table)");
        br2 = new BinaryReader(new MemoryStream(blockdata));
        mBlocks = new MpqBlock[mHeader.BlockTableSize];
        for (int i = 0;i < mHeader.BlockTableSize;i++)
            mBlocks[i] = new MpqBlock(br2,(uint)mHeaderOffset);
    }

    private boolean locateMpqHeader() throws Exception {
        BinaryReader br = new BinaryReader(mStream);
        for (long i = 0;i < mStream.Length - MpqHeader.Size;i += 0x200)
        {
            // In .mpq files the header will be at the start of the file
            // In .exe files, it will be at a multiple of 0x200
            mStream.Seek(i, SeekOrigin.Begin);
            mHeader = new MpqHeader(br);
            if (mHeader.ID == MpqHeader.MpqId)
            {
                mHeaderOffset = i;
                mHeader.HashTablePos += (uint)mHeaderOffset;
                mHeader.BlockTablePos += (uint)mHeaderOffset;
                if (mHeader.DataOffset == 0x6d9e4b86)
                {
                    // then this is a protected archive
                    mHeader.DataOffset = (uint)(MpqHeader.Size + i);
                }
                 
                return true;
            }
             
        }
        return false;
    }

    public MpqStream openFile(String Filename) throws Exception {
        MpqHash hash = new MpqHash();
        MpqBlock block = new MpqBlock();
        hash = getHashEntry(Filename);
        uint blockindex = hash.BlockIndex;
        if (blockindex == uint.MaxValue)
            throw new FileNotFoundException("File not found: " + Filename);
         
        block = mBlocks[blockindex];
        return new MpqStream(this,block);
    }

    public boolean fileExists(String Filename) throws Exception {
        MpqHash hash = getHashEntry(Filename);
        return (hash.BlockIndex != uint.MaxValue);
    }

    public Stream getBaseStream() throws Exception {
        return mStream;
    }

    public int getBlockSize() throws Exception {
        return mBlockSize;
    }

    private MpqHash getHashEntry(String Filename) throws Exception {
        uint index = hashString(Filename,0);
        index &= mHeader.HashTableSize - 1;
        uint name1 = hashString(Filename,0x100);
        uint name2 = hashString(Filename,0x200);
        for (uint i = index;i < mHashes.Length;++i)
        {
            MpqHash hash = mHashes[i];
            if (hash.Name1 == name1 && hash.Name2 == name2)
                return hash;
             
        }
        MpqHash nullhash = new MpqHash();
        nullhash.BlockIndex = uint.MaxValue;
        return nullhash;
    }

    public static uint hashString(String Input, int Offset) throws Exception {
        uint seed1 = 0x7fed7fed;
        uint seed2 = 0xeeeeeeee;
        for (Object __dummyForeachVar0 : Input)
        {
            char c = (Character)__dummyForeachVar0;
            int val = (int)char.ToUpper(c);
            seed1 = sStormBuffer[Offset + val] ^ (seed1 + seed2);
            seed2 = (uint)val + seed1 + seed2 + (seed2 << 5) + 3;
        }
        return seed1;
    }

    // Used for Hash Tables and Block Tables
    public static void decryptTable(byte[] Data, String Key) throws Exception {
        DecryptBlock(Data, hashString(Key,0x300));
    }

    public static void decryptBlock(byte[] Data, uint Seed1) throws Exception {
        uint seed2 = 0xeeeeeeee;
        for (int i = 0;i < Data.Length - 3;i += 4)
        {
            // NB: If the block is not an even multiple of 4,
            // the remainder is not encrypted
            seed2 += sStormBuffer[0x400 + (Seed1 & 0xff)];
            uint result = BitConverter.ToUInt32(Data, i);
            result ^= (Seed1 + seed2);
            Seed1 = ((~Seed1 << 21) + 0x11111111) | (Seed1 >> 11);
            seed2 = result + seed2 + (seed2 << 5) + 3;
            if (BitConverter.IsLittleEndian)
            {
                Data[i + 0] = ((byte)(result & 0xff));
                Data[i + 1] = ((byte)((result >> 8) & 0xff));
                Data[i + 2] = ((byte)((result >> 16) & 0xff));
                Data[i + 3] = ((byte)((result >> 24) & 0xff));
            }
            else
            {
                Data[i + 3] = ((byte)(result & 0xff));
                Data[i + 2] = ((byte)((result >> 8) & 0xff));
                Data[i + 1] = ((byte)((result >> 16) & 0xff));
                Data[i + 0] = ((byte)((result >> 24) & 0xff));
            } 
        }
    }

    public static void decryptBlock(uint[] Data, uint Seed1) throws Exception {
        uint seed2 = 0xeeeeeeee;
        for (int i = 0;i < Data.Length;i++)
        {
            seed2 += sStormBuffer[0x400 + (Seed1 & 0xff)];
            uint result = Data[i];
            result ^= Seed1 + seed2;
            Seed1 = ((~Seed1 << 21) + 0x11111111) | (Seed1 >> 11);
            seed2 = result + seed2 + (seed2 << 5) + 3;
            Data[i] = result;
        }
    }

    // This function calculates the encryption key based on
    // some assumptions we can make about the headers for encrypted files
    public static uint detectFileSeed(uint[] Data, uint Decrypted) throws Exception {
        uint value0 = Data[0];
        uint value1 = Data[1];
        uint temp = (value0 ^ Decrypted) - 0xeeeeeeee;
        for (int i = 0;i < 0x100;i++)
        {
            uint seed1 = temp - sStormBuffer[0x400 + i];
            uint seed2 = 0xeeeeeeee + sStormBuffer[0x400 + (seed1 & 0xff)];
            uint result = value0 ^ (seed1 + seed2);
            if (result != Decrypted)
                continue;
             
            uint saveseed1 = seed1;
            // Test this result against the 2nd value
            seed1 = ((~seed1 << 21) + 0x11111111) | (seed1 >> 11);
            seed2 = result + seed2 + (seed2 << 5) + 3;
            seed2 += sStormBuffer[0x400 + (seed1 & 0xff)];
            result = value1 ^ (seed1 + seed2);
            if ((result & 0xffff0000) == 0)
                return saveseed1;
             
        }
        return 0;
    }

    private static uint[] buildStormBuffer() throws Exception {
        uint seed = 0x100001;
        uint[] result = new uint[0x500];
        for (uint index1 = 0;index1 < 0x100;index1++)
        {
            uint index2 = index1;
            for (int i = 0;i < 5;i++, index2 += 0x100)
            {
                seed = (seed * 125 + 3) % 0x2aaaab;
                uint temp = (seed & 0xffff) << 16;
                seed = (seed * 125 + 3) % 0x2aaaab;
                result[index2] = temp | (seed & 0xffff);
            }
        }
        return result;
    }

}


