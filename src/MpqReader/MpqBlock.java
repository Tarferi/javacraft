//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:05
//

package MpqReader;

import MpqReader.MpqFileFlags;

public class MpqBlock   
{
    public MpqBlock() {
    }

    public uint FilePos = new uint();
    public uint CompressedSize = new uint();
    public uint FileSize = new uint();
    public MpqFileFlags Flags = MpqFileFlags.Changed;
    public static final uint Size = 16;
    public MpqBlock(BinaryReader br, uint HeaderOffset) throws Exception {
        FilePos = br.ReadUInt32() + HeaderOffset;
        CompressedSize = br.ReadUInt32();
        FileSize = br.ReadUInt32();
        Flags = (MpqFileFlags)br.ReadUInt32();
    }

    public boolean getIsEncrypted() throws Exception {
        return (Flags & MpqFileFlags.Encrypted) != 0;
    }

    public boolean getIsCompressed() throws Exception {
        return (Flags & MpqFileFlags.Compressed) != 0;
    }

}


