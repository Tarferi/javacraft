//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:05
//

package MpqReader;

import MpqReader.MpqBlock;
import MpqReader.MpqFileFlags;
import MpqReader.MpqHuffman;
import MpqReader.MpqWavCompression;
import MpqReader.PKLibDecompress;

//
// MpqHuffman.cs
//
// Authors:
//		Foole (fooleau@gmail.com)
//
// (C) 2006 Foole (fooleau@gmail.com)
// Based on code from StormLib by Ladislav Zezula
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
/**
* A Stream based class for reading a file from an MPQ file
*/
public class MpqStream  extends Stream 
{
    private Stream mStream = new Stream();
    private int mBlockSize = new int();
    private MpqBlock mBlock = new MpqBlock();
    private uint[] mBlockPositions = new uint[]();
    private uint mSeed1 = new uint();
    private long mPosition = new long();
    private byte[] mCurrentData = new byte[]();
    private int mCurrentBlockIndex = -1;
    private MpqStream() throws Exception {
    }

    public MpqStream(MpqReader.MpqArchive File, MpqBlock Block) throws Exception {
        mBlock = Block;
        mStream = File.getBaseStream();
        mBlockSize = File.getBlockSize();
        if (mBlock.getIsCompressed())
            loadBlockPositions();
         
    }

    // Compressed files start with an array of offsets to make seeking possible
    private void loadBlockPositions() throws Exception {
        int blockposcount = (int)((mBlock.FileSize + mBlockSize - 1) / mBlockSize) + 1;
        mBlockPositions = new uint[blockposcount];
        synchronized (mStream)
        {
            {
                mStream.Seek(mBlock.FilePos, SeekOrigin.Begin);
                BinaryReader br = new BinaryReader(mStream);
                for (int i = 0;i < blockposcount;i++)
                    mBlockPositions[i] = br.ReadUInt32();
            }
        }
        uint blockpossize = (uint)blockposcount * 4;
        // StormLib takes this to mean the data is encrypted
        if (mBlockPositions[0] != blockpossize)
        {
            if (mSeed1 == 0)
            {
                mSeed1 = MpqReader.MpqArchive.detectFileSeed(mBlockPositions,blockpossize);
                if (mSeed1 == 0)
                    throw new Exception("Unable to determine encyption seed");
                 
            }
             
            MpqReader.MpqArchive.decryptBlock(mBlockPositions,mSeed1);
            mSeed1++;
        }
         
    }

    // Add 1 because the first block is the offset list
    private byte[] loadBlock(int BlockIndex, int ExpectedLength) throws Exception {
        uint offset = new uint();
        int toread = new int();
        if (mBlock.getIsCompressed())
        {
            offset = mBlockPositions[BlockIndex];
            toread = (int)(mBlockPositions[BlockIndex + 1] - offset);
        }
        else
        {
            offset = (uint)(BlockIndex * mBlockSize);
            toread = ExpectedLength;
        } 
        offset += mBlock.FilePos;
        byte[] data = new byte[toread];
        synchronized (mStream)
        {
            {
                mStream.Seek(offset, SeekOrigin.Begin);
                mStream.Read(data, 0, toread);
            }
        }
        if (mBlock.getIsEncrypted() && mBlock.FileSize > 3)
        {
            if (mSeed1 == 0)
                throw new Exception("Unable to determine encryption key");
             
            MpqReader.MpqArchive.DecryptBlock(data, (uint)(mSeed1 + BlockIndex));
        }
         
        if (mBlock.getIsCompressed() && data.Length != ExpectedLength)
        {
            if ((mBlock.Flags & MpqFileFlags.CompressedMulti) != 0)
                data = DecompressMulti(data, ExpectedLength);
            else
                data = PKDecompress(new MemoryStream(data), ExpectedLength); 
        }
         
        return data;
    }

    public boolean getCanRead() throws Exception {
        return true;
    }

    public boolean getCanSeek() throws Exception {
        return true;
    }

    public boolean getCanWrite() throws Exception {
        return false;
    }

    public long getLength() throws Exception {
        return mBlock.FileSize;
    }

    public long getPosition() throws Exception {
        return mPosition;
    }

    public void setPosition(long value) throws Exception {
        Seek(value, SeekOrigin.Begin);
    }

    public void flush() throws Exception {
    }

    // NOP
    public long seek(long Offset, SeekOrigin Origin) throws Exception {
        long target = new long();
        SeekOrigin __dummyScrutVar0 = Origin;
        if (__dummyScrutVar0.equals(SeekOrigin.Begin))
        {
            target = Offset;
        }
        else if (__dummyScrutVar0.equals(SeekOrigin.Current))
        {
            target = getPosition() + Offset;
        }
        else if (__dummyScrutVar0.equals(SeekOrigin.End))
        {
            target = getLength() + Offset;
        }
        else
        {
            throw new ArgumentException("Origin", "Invalid SeekOrigin");
        }   
        if (target < 0)
            throw new ArgumentOutOfRangeException("Attmpted to Seek before the beginning of the stream");
         
        if (target >= getLength())
            throw new ArgumentOutOfRangeException("Attmpted to Seek beyond the end of the stream");
         
        mPosition = target;
        return mPosition;
    }

    public void setLength(long Value) throws Exception {
        throw new NotSupportedException("SetLength is not supported");
    }

    public int read(byte[] Buffer, int Offset, int Count) throws Exception {
        int toread = Count;
        int readtotal = 0;
        while (toread > 0)
        {
            int read = ReadInternal(Buffer, Offset, toread);
            if (read == 0)
                break;
             
            readtotal += read;
            Offset += read;
            toread -= read;
        }
        return readtotal;
    }

    private int readInternal(byte[] Buffer, int Offset, int Count) throws Exception {
        bufferData();
        int localposition = (int)(mPosition % mBlockSize);
        int bytestocopy = Math.Min(mCurrentData.Length - localposition, Count);
        if (bytestocopy <= 0)
            return 0;
         
        Array.Copy(mCurrentData, localposition, Buffer, Offset, bytestocopy);
        mPosition += bytestocopy;
        return bytestocopy;
    }

    public int readByte() throws Exception {
        if (mPosition >= getLength())
            return -1;
         
        bufferData();
        int localposition = (int)(mPosition % mBlockSize);
        mPosition++;
        return mCurrentData[localposition];
    }

    private void bufferData() throws Exception {
        int requiredblock = (int)(mPosition / mBlockSize);
        if (requiredblock != mCurrentBlockIndex)
        {
            int expectedlength = (int)Math.Min(getLength() - (requiredblock * mBlockSize), mBlockSize);
            mCurrentData = loadBlock(requiredblock,expectedlength);
            mCurrentBlockIndex = requiredblock;
        }
         
    }

    public void write(byte[] Buffer, int Offset, int Count) throws Exception {
        throw new NotSupportedException("Writing is not supported");
    }

    /* Compression types in order:
    		 *  10 = BZip2
    		 *   8 = PKLib
    		 *   2 = ZLib
    		 *   1 = Huffman
    		 *  80 = IMA ADPCM Stereo
    		 *  40 = IMA ADPCM Mono
    		 */
    private static byte[] decompressMulti(byte[] Input, int OutputLength) throws Exception {
        Stream sinput = new MemoryStream(Input);
        byte comptype = (byte)sinput.ReadByte();
        // PKLib
        if ((comptype & 8) != 0)
        {
            byte[] result = pKDecompress(sinput,OutputLength);
            comptype &= 0xF7;
            if (comptype == 0)
                return result;
             
            sinput = new MemoryStream(result);
        }
         
        if ((comptype & 1) != 0)
        {
            byte[] result = MpqHuffman.decompress(sinput);
            comptype &= 0xfe;
            if (comptype == 0)
                return result;
             
            sinput = new MemoryStream(result);
        }
         
        if ((comptype & 0x80) != 0)
        {
            byte[] result = MpqWavCompression.decompress(sinput,2);
            comptype &= 0x7f;
            if (comptype == 0)
                return result;
             
            sinput = new MemoryStream(result);
        }
         
        if ((comptype & 0x40) != 0)
        {
            byte[] result = MpqWavCompression.decompress(sinput,1);
            comptype &= 0xbf;
            if (comptype == 0)
                return result;
             
            sinput = new MemoryStream(result);
        }
         
        throw new Exception(String.Format("Unhandled compression flags: 0x{0:X}", comptype));
    }

    private static byte[] pKDecompress(Stream Data, int ExpectedLength) throws Exception {
        PKLibDecompress pk = new PKLibDecompress(Data);
        return pk.explode(ExpectedLength);
    }

}


