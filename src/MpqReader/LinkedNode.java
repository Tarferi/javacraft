//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:05
//

package MpqReader;

import MpqReader.LinkedNode;

//
// MpqHuffman.cs
//
// Authors:
//		Foole (fooleau@gmail.com)
//
// (C) 2006 Foole (fooleau@gmail.com)
// Based on code from StormLib by Ladislav Zezula and ShadowFlare
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// A node which is both hierachcical (parent/child) and doubly linked (next/prev)
public class LinkedNode   
{
    public int DecompressedValue = new int();
    public int Weight = new int();
    public LinkedNode Parent;
    public LinkedNode Child0;
    public LinkedNode getChild1() throws Exception {
        return Child0.Prev;
    }

    public LinkedNode Next;
    public LinkedNode Prev;
    public LinkedNode(int DecompVal, int Weight) throws Exception {
        DecompressedValue = DecompVal;
        this.Weight = Weight;
    }

    // TODO: This would be more efficient as a member of the other class
    // ie avoid the recursion
    public LinkedNode insert(LinkedNode Other) throws Exception {
        // 'Next' should have a lower weight
        // we should return the lower weight
        if (Other.Weight <= Weight)
        {
            // insert before
            if (Next != null)
            {
                Next.Prev = Other;
                Other.Next = Next;
            }
             
            Next = Other;
            Other.Prev = this;
            return Other;
        }
        else
        {
            if (Prev == null)
            {
                // Insert after
                Other.Prev = null;
                Prev = Other;
                Other.Next = this;
            }
            else
            {
                Prev.insert(Other);
            } 
        } 
        return this;
    }

}


