//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:05
//

package MpqReader;


public class MpqHeader   
{
    public MpqHeader() {
    }

    public uint ID = new uint();
    // Signature.  Should be 0x1a51504d
    public uint DataOffset = new uint();
    // Offset of the first file
    public uint ArchiveSize = new uint();
    public ushort Offs0C = new ushort();
    // Unknown
    public ushort BlockSize = new ushort();
    // Size of file block is 0x200 << BlockSize
    public uint HashTablePos = new uint();
    public uint BlockTablePos = new uint();
    public uint HashTableSize = new uint();
    public uint BlockTableSize = new uint();
    public static final uint MpqId = 0x1a51504d;
    public static final uint Size = 32;
    public MpqHeader(BinaryReader br) throws Exception {
        ID = br.ReadUInt32();
        DataOffset = br.ReadUInt32();
        ArchiveSize = br.ReadUInt32();
        Offs0C = br.ReadUInt16();
        BlockSize = br.ReadUInt16();
        HashTablePos = br.ReadUInt32();
        BlockTablePos = br.ReadUInt32();
        HashTableSize = br.ReadUInt32();
        BlockTableSize = br.ReadUInt32();
    }

}


