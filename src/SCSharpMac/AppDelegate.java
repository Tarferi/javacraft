//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:16
//

package SCSharpMac;

import CS2JNet.System.StringSupport;
import SCSharpMac.MainWindowController;
import SCSharpMac.UI.Game;


// Should subclass MonoMac.AppKit.NSResponder
public class AppDelegate  extends NSApplicationDelegate 
{

    MainWindowController mainWindowController;
    Game game;
    public AppDelegate() throws Exception {
    }

    public void finishedLaunching(NSObject notification) throws Exception {
        mainWindowController = new MainWindowController();
        mainWindowController.getWindow().MakeKeyAndOrderFront(this);
        String sc_dir = "/Users/toshok/src/scsharp/starcraft-data/starcraft";
        String bw_cd_dir = "/Users/toshok/src/scsharp/starcraft-data/bw-cd";
        String sc_cd_dir = "/Users/toshok/src/scsharp/starcraft-data/sc-cd";
        //string sc_cd_dir = ConfigurationManager.AppSettings["StarcraftCDDirectory"];
        //string bw_cd_dir = ConfigurationManager.AppSettings["BroodwarCDDirectory"];
        /* catch this pathological condition where someone has set the cd directories to the same location. */
        if (sc_cd_dir != null && bw_cd_dir != null && StringSupport.equals(bw_cd_dir, sc_cd_dir))
        {
            Console.WriteLine("The StarcraftCDDirectory and BroodwarCDDirectory configuration settings must have unique values.");
            return ;
        }
         
        /*ConfigurationManager.AppSettings["StarcraftDirectory"]*/
        game = new Game(sc_dir,sc_cd_dir,bw_cd_dir);
        mainWindowController.getWindow().ContentView = game;
        mainWindowController.getWindow().MakeFirstResponder(game);
        game.startup();
    }

}


