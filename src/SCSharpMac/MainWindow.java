//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:16
//

package SCSharpMac;



// Should subclass MonoMac.AppKit.NSWindow
public class MainWindow  extends MonoMac.AppKit.NSWindow 
{

    // Called when created from unmanaged code
    public MainWindow(IntPtr handle) throws Exception {
        super(handle);
        initialize();
    }

    // Called when created directly from a XIB file
    public MainWindow(NSCoder coder) throws Exception {
        super(coder);
        initialize();
    }

    // Shared initialization code
    void initialize() throws Exception {
        AcceptsMouseMovedEvents = true;
    }

}


