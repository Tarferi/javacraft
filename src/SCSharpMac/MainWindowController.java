//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:16
//

package SCSharpMac;

import SCSharpMac.MainWindow;


// Should subclass MonoMac.AppKit.NSWindowController
public class MainWindowController  extends MonoMac.AppKit.NSWindowController 
{

    // Called when created from unmanaged code
    public MainWindowController(IntPtr handle) throws Exception {
        super(handle);
        initialize();
    }

    // Called when created directly from a XIB file
    public MainWindowController(NSCoder coder) throws Exception {
        super(coder);
        initialize();
    }

    // Call to load from the XIB/NIB file
    public MainWindowController() throws Exception {
        super("MainWindow");
        initialize();
    }

    // Shared initialization code
    void initialize() throws Exception {
    }

    //strongly typed window accessor
    public MainWindow getWindow() throws Exception {
        return (MainWindow)super.Window;
    }

}


