//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:12
//

package SCSharpMac.UI;

import SCSharp.BinElement;
import SCSharp.ElementFlags;
import SCSharpMac.UI.Builtins;
import SCSharpMac.UI.ButtonLayerDelegate;
import SCSharpMac.UI.GuiUtil;
import SCSharpMac.UI.UIElement;
import SCSharpMac.UI.UIScreen;

//
// SCSharpMac.UI.ButtonElement
//
// Author:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
public class ButtonElement  extends UIElement 
{
    public ButtonElement(UIScreen screen, BinElement el, byte[] palette) throws Exception {
        super(screen, el, palette);
    }

    RectangleF text_bounds = new RectangleF();
    void calculateTextBounds() throws Exception {
        int text_x = new int(), text_y = new int();
        SizeF text_size = GuiUtil.measureText(getText(),getFont());
        if ((getFlags() & ElementFlags.CenterTextHoriz) == ElementFlags.CenterTextHoriz)
            text_x = (int)((getWidth() - text_size.Width) / 2);
        else if ((getFlags() & ElementFlags.RightAlignText) == ElementFlags.RightAlignText)
            text_x = (int)(getWidth() - text_size.Width);
        else
            text_x = 0;  
        if ((getFlags() & ElementFlags.CenterTextVert) == ElementFlags.CenterTextVert)
            text_y = (int)((getHeight() - text_size.Height) / 2 - text_size.Height);
        else if ((getFlags() & ElementFlags.TopAlignText) == ElementFlags.TopAlignText)
            text_y = (int)(getHeight() - text_size.Height);
        else
            text_y = 0;  
        text_bounds = new RectangleF(new PointF(text_x, text_y), text_size);
    }

    protected CALayer createLayer() throws Exception {
        CALayer layer = CALayer.Create();
        layer.Bounds = new RectangleF(0, 0, getWidth(), getHeight());
        layer.Delegate = new ButtonLayerDelegate(this);
        calculateTextBounds();
        layer.SetNeedsDisplayInRect(text_bounds);
        return layer;
    }

    public RectangleF getTextBounds() throws Exception {
        calculateTextBounds();
        return text_bounds;
    }

    public PointF getTextPosition() throws Exception {
        calculateTextBounds();
        return text_bounds.Location;
    }

    public void mouseButtonDown(NSEvent theEvent) throws Exception {
    }

    public void mouseButtonUp(NSEvent theEvent) throws Exception {
        PointF ui_pt = getParentScreen().ScreenToLayer(theEvent.LocationInWindow);
        if (pointInside(ui_pt))
            onActivate();
         
    }

    public void mouseEnter() throws Exception {
        if (getSensitive() && (getFlags() & ElementFlags.RespondToMouse) == ElementFlags.RespondToMouse)
        {
            /* highlight the text */
            GuiUtil.playSound(getMpq(),Builtins.MouseoverWav);
        }
         
        super.mouseEnter();
    }

}


