//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:14
//

package SCSharpMac.UI;

import CS2JNet.System.StringSupport;
import java.util.ArrayList;
import java.util.List;
import SCSharp.Got;
import SCSharp.Chk;
import SCSharp.Mpq;
import SCSharp.MpqArchive;
import SCSharpMac.UI.__MultiComboBoxSelectionChanged;
import SCSharpMac.UI.__MultiListBoxSelectionChanged;
import SCSharpMac.UI.Builtins;
import SCSharpMac.UI.ComboBoxElement;
import SCSharpMac.UI.Game;
import SCSharpMac.UI.GameScreen;
import SCSharpMac.UI.GlobalResources;
import SCSharpMac.UI.ListBoxElement;
import SCSharpMac.UI.RaceSelectionScreen;
import SCSharpMac.UI.UIScreen;

//
// SCSharp.UI.PlayCustomScreen
//
// Authors:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
public class PlayCustomScreen  extends UIScreen 
{
    public PlayCustomScreen(Mpq mpq) throws Exception {
        super(mpq, "glue\\PalNl", Builtins.rez_GluCustmBin);
    }

    static final int MAPSIZE_FORMAT_INDEX = 32;
    static final int MAPDIM_FORMAT_INDEX = 31;
    // XXX we don't use this one yet..
    static final int TILESET_FORMAT_INDEX = 33;
    static final int NUMPLAYERS_FORMAT_INDEX = 30;
    static final int HUMANSLOT_FORMAT_INDEX = 37;
    static final int COMPUTERSLOT_FORMAT_INDEX = 35;
    static final int FILELISTBOX_ELEMENT_INDEX = 7;
    static final int CURRENTDIR_ELEMENT_INDEX = 8;
    static final int MAPTITLE_ELEMENT_INDEX = 9;
    static final int MAPDESCRIPTION_ELEMENT_INDEX = 10;
    static final int MAPSIZE_ELEMENT_INDEX = 11;
    static final int MAPTILESET_ELEMENT_INDEX = 12;
    static final int MAPPLAYERS1_ELEMENT_INDEX = 14;
    static final int MAPPLAYERS2_ELEMENT_INDEX = 15;
    static final int OK_ELEMENT_INDEX = 16;
    static final int CANCEL_ELEMENT_INDEX = 17;
    static final int GAMETYPECOMBO_ELEMENT_INDEX = 20;
    static final int GAMESUBTYPE_LABEL_ELEMENT_INDEX = 19;
    static final int GAMESUBTYPE_COMBO_ELEMENT_INDEX = 21;
    static final int PLAYER1_COMBOBOX_PLAYER = 22;
    static final int PLAYER1_COMBOBOX_RACE = 30;
    static final int max_players = 8;
    String mapdir = new String();
    String curdir = new String();
    Mpq selectedScenario;
    Chk selectedChk;
    Got selectedGot;
    ListBoxElement file_listbox;
    ComboBoxElement gametype_combo;
    void initializeRaceCombo(ComboBoxElement combo) throws Exception {
        combo.addItem("Zerg");
        /* XXX these should all come from some string constant table someplace */
        combo.addItem("Terran");
        combo.addItem("Protoss");
        combo.addItem("Random",true);
    }

    void initializePlayerCombo(ComboBoxElement combo) throws Exception {
        combo.AddItem(GlobalResources.getInstance().getGluAllTbl().getStrings()[130]);
        /* Closed */
        combo.AddItem(GlobalResources.getInstance().getGluAllTbl().getStrings()[128], true);
    }

    /* Computer */
    String[] files = new String[]();
    String[] directories = new String[]();
    Got[] templates = new Got[]();
    void populateFileList() throws Exception {
        file_listbox.clear();
        String[] dir = Directory.GetDirectories(curdir);
        List<String> dirs = new List<String>();
        if (!StringSupport.equals(curdir, mapdir))
        {
            dirs.Add("Up One Level");
        }
         
        for (Object __dummyForeachVar0 : dir)
        {
            String d = (String)__dummyForeachVar0;
            String dl = Path.GetFileName(d).ToLower();
            if (StringSupport.equals(curdir, mapdir))
            {
                if (!Game.getInstance().getIsBroodWar() && StringSupport.equals(dl, "broodwar"))
                    continue;
                 
                if (StringSupport.equals(dl, "replays"))
                    continue;
                 
            }
             
            dirs.Add(d);
        }
        directories = dirs.ToArray();
        files = Directory.GetFiles(curdir, "*.sc*");
        Elements[CURRENTDIR_ELEMENT_INDEX].Text = Path.GetFileName(curdir);
        for (int i = 0;i < directories.Length;i++)
        {
            file_listbox.AddItem(String.Format("[{0}]", Path.GetFileName(directories[i])));
        }
        for (int i = 0;i < files.Length;i++)
        {
            String lower = files[i].ToLower();
            if (lower.EndsWith(".scm") || lower.EndsWith(".scx"))
                file_listbox.AddItem(Path.GetFileName(files[i]));
             
        }
    }

    void populateGameTypes() throws Exception {
        /* load the templates we're interested in displaying */
        StreamReader sr = new StreamReader((Stream)mpq.getResource("templates\\templates.lst"));
        List<Got> templateList = new List<Got>();
        String l = new String();
        while ((l = sr.ReadLine()) != null)
        {
            String t = l.Replace("\"", "");
            Got got = (Got)mpq.getResource("templates\\" + t);
            if (got == null)
                continue;
             
            if (got.getComputerPlayersAllowed() && got.getNumberOfTeams() == 0)
            {
                Console.WriteLine("adding template {0}:{1}", got.getUIGameTypeName(), got.getUISubtypeLabel());
                templateList.Add(got);
            }
             
        }
        templates = new Got[templateList.Count];
        templateList.CopyTo(templates, 0);
        Array.Sort(templates);
        for (Object __dummyForeachVar1 : templates)
        {
            /* fill in the game type menu.
            			   we only show the templates that allow computer players, have 0 teams */
            Got got = (Got)__dummyForeachVar1;
            gametype_combo.addItem(got.getUIGameTypeName());
        }
        gametype_combo.setSelectedIndex(0);
        gameTypeSelectionChanged(gametype_combo.getSelectedIndex());
    }

    protected void resourceLoader() throws Exception {
        super.resourceLoader();
        for (int i = 0;i < Elements.Count;i++)
            Console.WriteLine("{0}: {1} '{2}'", i, Elements[i].Type, Elements[i].Text);
        /* these don't ever show up in the UI, that i know of... */
        Elements[GAMESUBTYPE_LABEL_ELEMENT_INDEX].Visible = false;
        Elements[GAMESUBTYPE_COMBO_ELEMENT_INDEX].Visible = false;
        for (int i = 0;i < max_players;i++)
        {
            /* initialize all the race combo boxes */
            initializePlayerCombo((ComboBoxElement)Elements[PLAYER1_COMBOBOX_PLAYER + i]);
            initializeRaceCombo((ComboBoxElement)Elements[PLAYER1_COMBOBOX_RACE + i]);
        }
        file_listbox = (ListBoxElement)Elements[FILELISTBOX_ELEMENT_INDEX];
        gametype_combo = (ComboBoxElement)Elements[GAMETYPECOMBO_ELEMENT_INDEX];
        /* initially populate the map list by scanning the maps/ directory in the starcraftdir */
        mapdir = Path.Combine(Game.getInstance().getRootDirectory(), "maps");
        curdir = mapdir;
        populateGameTypes();
        populateFileList();
        file_listbox.SelectionChanged = __MultiListBoxSelectionChanged.combine(file_listbox.SelectionChanged,new ListBoxSelectionChanged() 
          { 
            public System.Void invoke(System.Int32 selectedIndex) throws Exception {
                fileListSelectionChanged(selectedIndex);
            }

            public List<ListBoxSelectionChanged> getInvocationList() throws Exception {
                List<ListBoxSelectionChanged> ret = new ArrayList<ListBoxSelectionChanged>();
                ret.add(this);
                return ret;
            }
        
          });
        gametype_combo.SelectionChanged = __MultiComboBoxSelectionChanged.combine(gametype_combo.SelectionChanged,new ComboBoxSelectionChanged() 
          { 
            public System.Void invoke(System.Int32 selectedIndex) throws Exception {
                gameTypeSelectionChanged(selectedIndex);
            }

            public List<ComboBoxSelectionChanged> getInvocationList() throws Exception {
                List<ComboBoxSelectionChanged> ret = new ArrayList<ComboBoxSelectionChanged>();
                ret.add(this);
                return ret;
            }
        
          });
        Elements[OK_ELEMENT_INDEX].Activate += ;
        Elements[CANCEL_ELEMENT_INDEX].Activate += ;
        /* make sure the PLAYER1 player combo reads
        			 * the player's name and is desensitized */
        ((ComboBoxElement)Elements[PLAYER1_COMBOBOX_PLAYER]).addItem("toshok");
        /*XXX player name*/
        Elements[PLAYER1_COMBOBOX_PLAYER].Sensitive = false;
    }

    void updatePlayersDisplay() throws Exception {
        if (selectedGot.getUseMapSettings())
        {
            String slotString = new String();
            slotString = GlobalResources.getInstance().getGluAllTbl().getStrings()[HUMANSLOT_FORMAT_INDEX];
            slotString = slotString.Replace("%c", " ");
            /* should probably be a tab.. */
            slotString = slotString.Replace("%s", (selectedChk == null ? "" : String.Format("{0}", selectedChk.getNumHumanSlots())));
            Elements[MAPPLAYERS1_ELEMENT_INDEX].Text = slotString;
            Elements[MAPPLAYERS1_ELEMENT_INDEX].Visible = true;
            slotString = GlobalResources.getInstance().getGluAllTbl().getStrings()[COMPUTERSLOT_FORMAT_INDEX];
            slotString = slotString.Replace("%c", " ");
            /* should probably be a tab.. */
            slotString = slotString.Replace("%s", (selectedChk == null ? "" : String.Format("{0}", selectedChk.getNumComputerSlots())));
            Elements[MAPPLAYERS2_ELEMENT_INDEX].Text = slotString;
            Elements[MAPPLAYERS2_ELEMENT_INDEX].Visible = true;
        }
        else
        {
            String numPlayersString = GlobalResources.getInstance().getGluAllTbl().getStrings()[NUMPLAYERS_FORMAT_INDEX];
            numPlayersString = numPlayersString.Replace("%c", " ");
            /* should probably be a tab.. */
            numPlayersString = numPlayersString.Replace("%s", (selectedChk == null ? "" : String.Format("{0}", selectedChk.getNumPlayers())));
            Elements[MAPPLAYERS1_ELEMENT_INDEX].Text = numPlayersString;
            Elements[MAPPLAYERS1_ELEMENT_INDEX].Visible = true;
            Elements[MAPPLAYERS2_ELEMENT_INDEX].Visible = false;
        } 
        int i = 0;
        if (selectedChk != null)
        {
            for (i = 0;i < max_players;i++)
            {
                if (selectedGot.getUseMapSettings())
                {
                    if (i >= selectedChk.getNumComputerSlots() + 1)
                        break;
                     
                }
                else
                {
                    if (i >= selectedChk.getNumPlayers())
                        break;
                     
                } 
                if (i > 0)
                    ((ComboBoxElement)Elements[PLAYER1_COMBOBOX_PLAYER + i]).setSelectedIndex(1);
                 
                ((ComboBoxElement)Elements[PLAYER1_COMBOBOX_RACE + i]).setSelectedIndex(3);
                Elements[PLAYER1_COMBOBOX_PLAYER + i].Visible = true;
                Elements[PLAYER1_COMBOBOX_RACE + i].Visible = true;
            }
        }
         
        for (int j = i;j < max_players;j++)
        {
            Elements[PLAYER1_COMBOBOX_PLAYER + j].Visible = false;
            Elements[PLAYER1_COMBOBOX_RACE + j].Visible = false;
        }
    }

    void gameTypeSelectionChanged(int selectedIndex) throws Exception {
        /* the display of the number of players
        			 * changes depending upon the template */
        selectedGot = templates[selectedIndex];
        updatePlayersDisplay();
    }

    void fileListSelectionChanged(int selectedIndex) throws Exception {
        String map_path = Path.Combine(curdir, file_listbox.getSelectedItem());
        if (selectedScenario != null)
            selectedScenario.dispose();
         
        if (selectedIndex < directories.Length)
        {
            selectedScenario = null;
            selectedChk = null;
        }
        else
        {
            selectedScenario = new MpqArchive(map_path);
            selectedChk = (Chk)selectedScenario.getResource("staredit\\scenario.chk");
        } 
        Elements[MAPTITLE_ELEMENT_INDEX].Text = selectedChk == null ? "" : selectedChk.getName();
        Elements[MAPDESCRIPTION_ELEMENT_INDEX].Text = selectedChk == null ? "" : selectedChk.getDescription();
        String mapSizeString = GlobalResources.getInstance().getGluAllTbl().getStrings()[MAPSIZE_FORMAT_INDEX];
        //			string mapDimString = GlobalResources.Instance.GluAllTbl.Strings[MAPDIM_FORMAT_INDEX];
        String tileSetString = GlobalResources.getInstance().getGluAllTbl().getStrings()[TILESET_FORMAT_INDEX];
        mapSizeString = mapSizeString.Replace("%c", " ");
        /* should probably be a tab.. */
        mapSizeString = mapSizeString.Replace("%s", (selectedChk == null ? "" : String.Format("{0}x{1}", selectedChk.getWidth(), selectedChk.getHeight())));
        tileSetString = tileSetString.Replace("%c", " ");
        /* should probably be a tab.. */
        tileSetString = tileSetString.Replace("%s", (selectedChk == null ? "" : String.Format("{0}", selectedChk.getTileset())));
        Elements[MAPSIZE_ELEMENT_INDEX].Text = mapSizeString;
        Elements[MAPTILESET_ELEMENT_INDEX].Text = tileSetString;
        updatePlayersDisplay();
    }

    public void keyboardDown(NSEvent theEvent) throws Exception {
        if ((theEvent.ModifierFlags & NSEventModifierMask.NumericPadKeyMask) == NSEventModifierMask.NumericPadKeyMask && (theEvent.Characters[0] == (char)NSKey.UpArrow || theEvent.Characters[0] == (char)NSKey.DownArrow))
            file_listbox.keyboardDown(theEvent);
        else
            super.keyboardDown(theEvent); 
    }

}


