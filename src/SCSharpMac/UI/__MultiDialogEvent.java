//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:15
//

package SCSharpMac.UI;

import CS2JNet.JavaSupport.util.ListSupport;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import SCSharpMac.UI.__MultiDialogEvent;
import SCSharpMac.UI.DialogEvent;

public class __MultiDialogEvent   implements DialogEvent
{
    public void invoke() throws Exception {
        IList<DialogEvent> copy = new IList<DialogEvent>(), members = this.getInvocationList();
        synchronized (members)
        {
            copy = new LinkedList<DialogEvent>(members);
        }
        for (Object __dummyForeachVar0 : copy)
        {
            DialogEvent d = (DialogEvent)__dummyForeachVar0;
            d.invoke();
        }
    }

    private System.Collections.Generic.IList<DialogEvent> _invocationList = new ArrayList<DialogEvent>();
    public static DialogEvent combine(DialogEvent a, DialogEvent b) throws Exception {
        if (a == null)
            return b;
         
        if (b == null)
            return a;
         
        __MultiDialogEvent ret = new __MultiDialogEvent();
        ret._invocationList = a.getInvocationList();
        ret._invocationList.addAll(b.getInvocationList());
        return ret;
    }

    public static DialogEvent remove(DialogEvent a, DialogEvent b) throws Exception {
        if (a == null || b == null)
            return a;
         
        System.Collections.Generic.IList<DialogEvent> aInvList = a.getInvocationList();
        System.Collections.Generic.IList<DialogEvent> newInvList = ListSupport.removeFinalStretch(aInvList, b.getInvocationList());
        if (aInvList == newInvList)
        {
            return a;
        }
        else
        {
            __MultiDialogEvent ret = new __MultiDialogEvent();
            ret._invocationList = newInvList;
            return ret;
        } 
    }

    public System.Collections.Generic.IList<DialogEvent> getInvocationList() throws Exception {
        return _invocationList;
    }

}


