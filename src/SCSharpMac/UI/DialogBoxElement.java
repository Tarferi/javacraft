//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:12
//

package SCSharpMac.UI;

import SCSharp.BinElement;
import SCSharp.Grp;
import SCSharpMac.UI.GuiUtil;
import SCSharpMac.UI.UIElement;
import SCSharpMac.UI.UIScreen;

//
// SCSharpMac.UI.DialogBoxElement
//
// Authors:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
public class DialogBoxElement  extends UIElement 
{
    public DialogBoxElement(UIScreen screen, BinElement el, byte[] palette) throws Exception {
        super(screen, el, palette);
        tileGrp = (Grp)getMpq().getResource("dlgs\\tile.grp");
    }

    Grp tileGrp;
    static final int TILE_TR = 2;
    static final int TILE_T = 1;
    static final int TILE_TL = 0;
    static final int TILE_R = 5;
    static final int TILE_C = 4;
    static final int TILE_L = 3;
    static final int TILE_BR = 8;
    static final int TILE_B = 7;
    static final int TILE_BL = 6;
    void tileRow(CALayer surf, Grp grp, byte[] pal, int l, int c, int r, int y) throws Exception {
        CALayer l_layer = GuiUtil.CreateLayerFromBitmap(grp.getFrame(l), grp.getWidth(), grp.getHeight(), pal, 41, 0);
        CALayer c_layer = GuiUtil.CreateLayerFromBitmap(grp.getFrame(c), grp.getWidth(), grp.getHeight(), pal, 41, 0);
        CALayer r_layer = GuiUtil.CreateLayerFromBitmap(grp.getFrame(r), grp.getWidth(), grp.getHeight(), pal, 41, 0);
    }

    protected CALayer createLayer() throws Exception {
        return null;
    }

}


