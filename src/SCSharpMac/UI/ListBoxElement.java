//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:13
//

package SCSharpMac.UI;

import CS2JNet.JavaSupport.util.ListSupport;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import SCSharp.BinElement;
import SCSharpMac.UI.ListBoxElementLayerDelegate;
import SCSharpMac.UI.ListBoxSelectionChanged;
import SCSharpMac.UI.UIElement;
import SCSharpMac.UI.UIScreen;

//
// SCSharpMac.UI.ListBoxElement
//
// Authors:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
public class ListBoxElement  extends UIElement 
{
    List<String> items = new List<String>();
    int cursor = -1;
    boolean selectable = true;
    int num_visible = new int();
    int first_visible = new int();
    public ListBoxElement(UIScreen screen, BinElement el, byte[] palette) throws Exception {
        super(screen, el, palette);
        items = new List<String>();
        num_visible = getHeight() / getFont().getLineSize();
        first_visible = 0;
    }

    public void keyboardDown(NSEvent theEvent) throws Exception {
        boolean selection_changed = false;
        /* navigation keys */
        if (theEvent.CharactersIgnoringModifiers[0] == (char)NSKey.UpArrow)
        {
            if (cursor > 0)
            {
                cursor--;
                selection_changed = true;
                if (cursor < first_visible)
                    first_visible = cursor;
                 
            }
             
        }
        else if (theEvent.CharactersIgnoringModifiers[0] == (char)NSKey.DownArrow)
        {
            if (cursor < items.Count - 1)
            {
                cursor++;
                selection_changed = true;
                if (cursor >= first_visible + num_visible)
                    first_visible = cursor - num_visible + 1;
                 
            }
             
        }
          
        if (selection_changed)
        {
            invalidate();
            if (SelectionChanged != null)
                SelectionChanged.invoke(cursor);
             
        }
         
    }

    boolean selecting = new boolean();
    int selectionIndex = new int();
    public boolean getSelectable() throws Exception {
        return selectable;
    }

    public void setSelectable(boolean value) throws Exception {
        selectable = value;
    }

    public boolean getSelecting() throws Exception {
        return selecting;
    }

    public int getSelectedIndex() throws Exception {
        return cursor;
    }

    public void setSelectedIndex(int value) throws Exception {
        if (value < 0 || value > items.Count)
            throw new ArgumentException("value");
         
        if (cursor != value)
        {
            cursor = value;
            if (SelectionChanged != null)
                SelectionChanged.invoke(cursor);
             
            invalidate();
        }
         
    }

    public int getSelectionIndex() throws Exception {
        return selectionIndex;
    }

    public String getSelectedItem() throws Exception {
        return items[cursor];
    }

    public List<String> getItems() throws Exception {
        return items;
    }

    public int getFirstVisible() throws Exception {
        return first_visible;
    }

    public int getNumVisible() throws Exception {
        return num_visible;
    }

    public void addItem(String item) throws Exception {
        items.Add(item);
        if (cursor == -1)
        {
            cursor = 0;
            if (SelectionChanged != null)
                SelectionChanged.invoke(cursor);
             
        }
         
        invalidate();
    }

    public void removeAt(int index) throws Exception {
        items.RemoveAt(index);
        if (items.Count == 0)
            cursor = -1;
         
        invalidate();
    }

    public void clear() throws Exception {
        items.Clear();
        cursor = -1;
        invalidate();
    }

    public boolean contains(String item) throws Exception {
        return items.Contains(item);
    }

    protected CALayer createLayer() throws Exception {
        CALayer layer = CALayer.Create();
        layer.Bounds = new RectangleF(0, 0, getWidth(), getHeight());
        layer.AnchorPoint = new PointF(0, 0);
        layer.Delegate = new ListBoxElementLayerDelegate(this);
        layer.BorderColor = new CGColor(0, 1, 0, 1);
        layer.BorderWidth = 1;
        layer.SetNeedsDisplay();
        return layer;
    }

    public ListBoxSelectionChanged SelectionChanged;
}


