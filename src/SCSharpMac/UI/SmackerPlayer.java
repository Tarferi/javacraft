//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:15
//

package SCSharpMac.UI;

import CS2JNet.JavaSupport.util.ListSupport;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import SCSharp.Smk.SmackerDecoder;
import SCSharp.Smk.SmackerFile;
import SCSharpMac.UI.__MultiTickEventHandler;
import SCSharpMac.UI.Game;
import SCSharpMac.UI.GuiUtil;
import SCSharpMac.UI.PlayerEvent;
import SCSharpMac.UI.TickEventArgs;

//
// SCSharpMac.UI.SmackerPlayer
//
// Authors:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
public class SmackerPlayer   
{
    //Buffer this many frames
    private static final int BUFFERED_FRAMES = 100;
    Thread decoderThread = new Thread();
    boolean firstRun = true;
    int buffered_frames = new int();
    Queue<byte[]> frameQueue = new Queue<byte[]>();
    SmackerFile file;
    SmackerDecoder decoder;
    CGImage currentFrame = new CGImage();
    AutoResetEvent waitEvent = new AutoResetEvent();
    float timeElapsed = 0;
    public SmackerPlayer(Stream smk_stream) throws Exception {
        this(smk_stream, BUFFERED_FRAMES);
    }

    public SmackerPlayer(Stream smk_stream, int buffered_frames) throws Exception {
        file = SmackerFile.openFromStream(smk_stream);
        decoder = file.getDecoder();
        this.buffered_frames = buffered_frames;
        waitEvent = new AutoResetEvent(false);
    }

    public int getWidth() throws Exception {
        return (int)file.getHeader().Width;
    }

    public int getHeight() throws Exception {
        return (int)file.getHeader().Height;
    }

    public CGImage getCurrentFrame() throws Exception {
        return currentFrame;
    }

    void events_Tick(Object sender, TickEventArgs e) throws Exception {
        timeElapsed += e.getSecondsElapsed();
        while (timeElapsed > 1.0 / file.getHeader().Fps)
        {
            synchronized (((ICollection)frameQueue).SyncRoot)
            {
                {
                    if (frameQueue.Count <= 0)
                        return ;
                     
                }
            }
            timeElapsed -= (float)(1.0f / file.getHeader().Fps);
            byte[] argbData = frameQueue.Dequeue();
            currentFrame = GuiUtil.CreateImage(argbData, (ushort)getWidth(), (ushort)getHeight(), 32, getWidth() * 4);
            emitFrameReady();
            if (frameQueue.Count < (buffered_frames / 2) + 1)
                waitEvent.Set();
             
        }
    }

    void decoderThread() throws Exception {
        while (firstRun || file.getHeader().hasRingFrame())
        {
            decoder.reset();
            while (decoder.getCurrentFrame() < file.getHeader().NbFrames)
            {
                try
                {
                    decoder.readNextFrame();
                    int count = new int();
                    synchronized (((ICollection)frameQueue).SyncRoot)
                    {
                        {
                            frameQueue.Enqueue(decoder.getARGBData());
                            count = frameQueue.Count;
                        }
                    }
                    if (count >= buffered_frames)
                        waitEvent.WaitOne();
                     
                }
                catch (Exception __dummyCatchVar0)
                {
                    break;
                }
            
            }
        }
        firstRun = false;
        NSApplication.SharedApplication.BeginInvokeOnMainThread(EmitFinished);
    }

    public void play() throws Exception {
        if (decoderThread != null)
            return ;
         
        decoderThread = new Thread(DecoderThread);
        decoderThread.IsBackground = true;
        decoderThread.Start();
        Game.getInstance().Tick = __MultiTickEventHandler.combine(Game.getInstance().Tick,new TickEventHandler() 
          { 
            public System.Void invoke(System.Object sender, TickEventArgs args) throws Exception {
                events_Tick(sender, args);
            }

            public List<TickEventHandler> getInvocationList() throws Exception {
                List<TickEventHandler> ret = new ArrayList<TickEventHandler>();
                ret.add(this);
                return ret;
            }
        
          });
    }

    public void stop() throws Exception {
        if (decoderThread == null)
            return ;
         
        decoderThread.Abort();
        decoderThread = null;
        Game.getInstance().Tick = __MultiTickEventHandler.remove(Game.getInstance().Tick,new TickEventHandler() 
          { 
            public System.Void invoke(System.Object sender, TickEventArgs args) throws Exception {
                events_Tick(sender, args);
            }

            public List<TickEventHandler> getInvocationList() throws Exception {
                List<TickEventHandler> ret = new ArrayList<TickEventHandler>();
                ret.add(this);
                return ret;
            }
        
          });
    }

    public PlayerEvent FrameReady;
    public PlayerEvent Finished;
    void emitFinished() throws Exception {
        PlayerEvent h = Finished;
        if (h != null)
            h.invoke();
         
    }

    void emitFrameReady() throws Exception {
        PlayerEvent h = FrameReady;
        if (h != null)
            h.invoke();
         
    }

}


