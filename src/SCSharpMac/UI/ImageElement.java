//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:13
//

package SCSharpMac.UI;

import SCSharp.BinElement;
import SCSharp.ElementType;
import SCSharpMac.UI.GuiUtil;
import SCSharpMac.UI.Pcx;
import SCSharpMac.UI.UIElement;
import SCSharpMac.UI.UIScreen;

//
// SCSharpMac.UI.ImageElement
//
// Authors:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
public class ImageElement  extends UIElement 
{
    int translucent_index = new int();
    Pcx pcx;
    public ImageElement(UIScreen screen, BinElement el, byte[] palette, int translucent_index) throws Exception {
        super(screen, el, palette);
        this.translucent_index = translucent_index;
    }

    public ImageElement(UIScreen screen, ushort x1, ushort y1, ushort width, ushort height, int translucent_index) throws Exception {
        super(screen, x1, y1, width, height);
        this.translucent_index = translucent_index;
    }

    protected CALayer createLayer() throws Exception {
        CALayer layer = GuiUtil.layerFromPcx(getPcx());
        layer.AnchorPoint = new PointF(0, 0);
        return layer;
    }

    public Pcx getPcx() throws Exception {
        if (pcx == null)
        {
            pcx = new Pcx();
            /* [UNSUPPORTED] 'var' as type is unsupported "var" */ stream = (Stream)getMpq().getResource(getText());
            if (stream == null)
                throw new Exception(String.Format("didn't find resource at {0}", getText()));
             
            pcx.ReadFromStream(stream, translucent_index, 0);
        }
         
        return pcx;
    }

    public ElementType getType() throws Exception {
        return ElementType.Image;
    }

}


