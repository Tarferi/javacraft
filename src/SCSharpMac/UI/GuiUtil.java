//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:13
//

package SCSharpMac.UI;

import CS2JNet.JavaSupport.util.ListSupport;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import SCSharp.Fnt;
import SCSharp.Glyph;
import SCSharp.Mpq;
import SCSharpMac.UI.GuiUtil;
import SCSharpMac.UI.Pcx;

public class GuiUtil   
{
    static Fnt[] fonts = new Fnt[]();
    static String[] BroodwarFonts = { "files\\font\\font8.fnt", "files\\font\\font10.fnt", "files\\font\\font14.fnt", "files\\font\\font16.fnt", "files\\font\\font16x.fnt" };
    public static Fnt[] getFonts(Mpq mpq) throws Exception {
        if (fonts == null)
        {
            String[] font_list = new String[]();
            font_list = BroodwarFonts;
            fonts = new Fnt[font_list.Length];
            for (int i = 0;i < fonts.Length;i++)
            {
                fonts[i] = (Fnt)mpq.GetResource(font_list[i]);
                Console.WriteLine("fonts[{0}] = {1}", i, fonts[i] == null ? "null" : "not null");
            }
        }
         
        return fonts;
    }

    public static void renderGlyphToContext(CGContext context, PointF position, Glyph g, Fnt font, byte[] palette, int offset) throws Exception {
        byte[] buf = new byte[g.getWidth() * g.getHeight() * 4];
        int i = 0;
        for (int y = g.getHeight() - 1;y >= 0;y--)
        {
            for (int x = g.getWidth() - 1;x >= 0;x--)
            {
                if (g.getBitmap()[y, x] == 0)
                    buf[i + 0] = 0;
                else if (g.getBitmap()[y, x] == 1)
                    buf[i + 0] = 255;
                else
                    buf[i + 0] = 128;  
                buf[i + 1] = palette[(g.getBitmap()[y, x]) * 3 + offset + 0];
                buf[i + 2] = palette[(g.getBitmap()[y, x]) * 3 + offset + 1];
                buf[i + 3] = palette[(g.getBitmap()[y, x]) * 3 + offset + 2];
                if (buf[i + 1] == 252 && buf[i + 2] == 0 && buf[i + 3] == 252)
                    buf[i + 0] = 0;
                 
                i += 4;
            }
        }
        CGImage glyphImage = CreateImage(buf, (ushort)g.getWidth(), (ushort)g.getHeight(), 32, g.getWidth() * 4);
        context.DrawImage(new RectangleF(position, new SizeF(g.getWidth(), g.getHeight())), glyphImage);
    }

    public static void renderTextToContext(CGContext context, PointF position, String text, Fnt font, byte[] palette, int offset) throws Exception {
        int i = new int();
        /* create a run of text, for now ignoring any control codes in the string */
        StringBuilder run = new StringBuilder();
        for (i = 0;i < text.Length;i++)
        {
            /* allow newlines */
            if ((text[i] == 0x0a || !Char.IsControl(text[i])) && (text[i] >= font.getLowIndex() && text[i] <= font.getHighIndex()))
            {
                run.Append(text[i]);
            }
             
        }
        String rs = run.ToString();
        byte[] r = Encoding.ASCII.GetBytes(rs);
        for (i = 0;i < r.Length;i++)
        {
            /* the draw it */
            int glyph_width = 0;
            Glyph g = null;
            if (r[i] == 0x20)
            {
                /* space */
                glyph_width = font.getSpaceSize();
            }
            else
            {
                g = font[r[i] - 1];
                glyph_width = g.getWidth() + g.getXOffset();
                RenderGlyphToContext(context, position, g, font, palette, offset);
            } 
            position.X += glyph_width;
        }
    }

    public static CALayer renderGlyph(Fnt font, Glyph g, byte[] palette, int offset) throws Exception {
        byte[] buf = new byte[g.getWidth() * g.getHeight() * 4];
        int i = 0;
        for (int y = g.getHeight() - 1;y >= 0;y--)
        {
            for (int x = g.getWidth() - 1;x >= 0;x--)
            {
                if (g.getBitmap()[y, x] == 0)
                    buf[i + 0] = 0;
                else if (g.getBitmap()[y, x] == 1)
                    buf[i + 0] = 255;
                else
                    buf[i + 0] = 128;  
                buf[i + 1] = palette[(g.getBitmap()[y, x]) * 3 + offset + 0];
                buf[i + 2] = palette[(g.getBitmap()[y, x]) * 3 + offset + 1];
                buf[i + 3] = palette[(g.getBitmap()[y, x]) * 3 + offset + 2];
                if (buf[i + 1] == 252 && buf[i + 2] == 0 && buf[i + 3] == 252)
                    buf[i + 0] = 0;
                 
                i += 4;
            }
        }
        return CreateLayerFromRGBAData(buf, (ushort)g.getWidth(), (ushort)g.getHeight(), 32, g.getWidth() * 4);
    }

    public static CALayer composeText(String text, Fnt font, byte[] palette) throws Exception {
        return ComposeText(text, font, palette, -1, -1, 4);
    }

    public static CALayer composeText(String text, Fnt font, byte[] palette, int offset) throws Exception {
        return ComposeText(text, font, palette, -1, -1, offset);
    }

    public static SizeF measureText(String text, Fnt font) throws Exception {
        int i = new int();
        /* create a run of text, for now ignoring any control codes in the string */
        StringBuilder run = new StringBuilder();
        for (i = 0;i < text.Length;i++)
            /* allow newlines */
            if (text[i] == 0x0a || !Char.IsControl(text[i]))
                run.Append(text[i]);
             
        String rs = run.ToString();
        byte[] r = Encoding.ASCII.GetBytes(rs);
        int x = new int(), y = new int();
        int text_height = new int(), text_width = new int();
        /* measure the text first, optionally wrapping at width */
        text_width = text_height = 0;
        x = y = 0;
        for (i = 0;i < r.Length;i++)
        {
            int glyph_width = 0;
            if (r[i] != 0x0a)
            {
                /* newline */
                if (r[i] == 0x20)
                    /* space */
                    glyph_width = font.getSpaceSize();
                else
                {
                    Glyph g = font[r[i] - 1];
                    glyph_width = g.getWidth() + g.getXOffset();
                } 
            }
             
            if (r[i] == 0x0a)
            {
                if (x > text_width)
                    text_width = x;
                 
                x = 0;
                text_height += font.getLineSize();
            }
             
            x += glyph_width;
        }
        if (x > text_width)
            text_width = x;
         
        text_height += font.getLineSize();
        return new SizeF(text_width, text_height);
    }

    public static CALayer composeText(String text, Fnt font, byte[] palette, int width, int height, int offset) throws Exception {
        if (font == null)
            Console.WriteLine("aiiiieeee");
         
        int i = new int();
        /* create a run of text, for now ignoring any control codes in the string */
        StringBuilder run = new StringBuilder();
        if (text != null)
        {
            for (i = 0;i < text.Length;i++)
                /* allow newlines */
                if (text[i] == 0x0a || !Char.IsControl(text[i]))
                    run.Append(text[i]);
                 
        }
         
        String rs = run.ToString();
        byte[] r = Encoding.ASCII.GetBytes(rs);
        int x = new int(), y = new int();
        int text_height = new int(), text_width = new int();
        /* measure the text first, optionally wrapping at width */
        text_width = text_height = 0;
        x = y = 0;
        for (i = 0;i < r.Length;i++)
        {
            int glyph_width = 0;
            if (r[i] != 0x0a)
            {
                /* newline */
                if (r[i] == 0x20)
                    /* space */
                    glyph_width = font.getSpaceSize();
                else
                {
                    Glyph g = font[r[i] - 1];
                    glyph_width = g.getWidth() + g.getXOffset();
                } 
            }
             
            if (r[i] == 0x0a || (width != -1 && x + glyph_width > width))
            {
                if (x > text_width)
                    text_width = x;
                 
                x = 0;
                text_height += font.getLineSize();
            }
             
            x += glyph_width;
        }
        if (x > text_width)
            text_width = x;
         
        text_height += font.getLineSize();
        CALayer layer = CALayer.Create();
        layer.AnchorPoint = new PointF(0, 0);
        layer.Bounds = new RectangleF(0, 0, text_width, text_height);
        /* the draw it */
        x = 0;
        y = text_height;
        for (i = 0;i < r.Length;i++)
        {
            int glyph_width = 0;
            Glyph g = null;
            if (r[i] != 0x0a)
            {
                /* newline */
                if (r[i] == 0x20)
                {
                    /* space */
                    glyph_width = font.getSpaceSize();
                }
                else
                {
                    g = font[r[i] - 1];
                    glyph_width = g.getWidth() + g.getXOffset();
                    CALayer gl = RenderGlyph(font, g, palette, offset);
                    gl.AnchorPoint = new PointF(0, 0);
                    gl.Position = new PointF(x, y - g.getHeight() - g.getYOffset());
                    layer.AddSublayer(gl);
                } 
            }
             
            if (r[i] == 0x0a || x + glyph_width > text_width)
            {
                x = 0;
                y -= font.getLineSize();
            }
             
            x += glyph_width;
        }
        return layer;
    }

    public static CGImage createImage(byte[] data, ushort width, ushort height, int depth, int stride) throws Exception {
        return new CGImage(width, height, 8, depth, stride, CGColorSpace.CreateDeviceRGB(), depth == 32 ? CGImageAlphaInfo.First : CGImageAlphaInfo.None, new CGDataProvider(data, 0, data.Length), null, false, CGColorRenderingIntent.Default);
    }

    public static CALayer createLayer(byte[] data, ushort width, ushort height, int depth, int stride) throws Exception {
        CALayer layer = CALayer.Create();
        layer.Bounds = new RectangleF(0, 0, width, height);
        layer.Contents = CreateImage(data, width, height, depth, stride);
        return layer;
    }

    public static CALayer createLayerFromRGBAData(byte[] data, ushort width, ushort height, int depth, int stride) throws Exception {
        return CreateLayer(data, width, height, depth, stride);
    }

    public static CALayer createLayerFromRGBData(byte[] data, ushort width, ushort height, int depth, int stride) throws Exception {
        return CreateLayer(data, width, height, depth, stride);
    }

    public static CALayer createLayerFromPalettedData(byte[] data, byte[] palette, ushort width, ushort height) throws Exception {
        CALayer layer = CALayer.Create();
        layer.Bounds = new RectangleF(0, 0, width, height);
        layer.Contents = new CGImage(width, height, 8, 8, width, CGColorSpace.CreateIndexed(CGColorSpace.CreateDeviceRGB(), 255, palette), CGImageAlphaInfo.None, new CGDataProvider(data, 0, data.Length), null, false, CGColorRenderingIntent.Default);
        return layer;
    }

    public static byte[] getBitmapData(byte[][] grid, ushort width, ushort height, byte[] palette, boolean with_alpha) throws Exception {
        byte[] buf = new byte[width * height * (3 + (with_alpha ? 1 : 0))];
        int i = 0;
        int x = new int(), y = new int();
        for (y = height - 1;y >= 0;y--)
        {
            for (x = width - 1;x >= 0;x--)
            {
                if (with_alpha)
                    i++;
                 
                buf[i++] = palette[grid[y, x] * 3 + 2];
                buf[i++] = palette[grid[y, x] * 3 + 1];
                buf[i++] = palette[grid[y, x] * 3];
                if (with_alpha)
                {
                    if (buf[i - 3] == 0 && buf[i - 2] == 0 && buf[i - 1] == 0)
                    {
                        buf[i - 4] = 0x00;
                    }
                    else if (buf[i - 3] == 59 && buf[i - 2] == 39 && buf[i - 1] == 39)
                    {
                        buf[i - 3] = buf[i - 2] = buf[i - 1] = 0;
                        //							Console.WriteLine ("translucent shadow pixel, palette index = {0}", palette [grid[y,x]] * 3);
                        buf[i - 4] = 0xaa;
                    }
                    else
                    {
                        //							Console.WriteLine ("pixel data RGB = {0},{1},{2}, palette index = {3}",
                        //									   buf[i-3],buf[i-2],buf[i-1], palette [grid[y,x]] * 3);
                        buf[i - 4] = 0xff;
                    }  
                }
                 
            }
        }
        return buf;
    }

    public static byte[] getBitmapData(byte[][] grid, ushort width, ushort height, byte[] palette, int translucent_index, int transparent_index) throws Exception {
        byte[] buf = new byte[width * height * 4];
        int i = 0;
        int x = new int(), y = new int();
        for (y = height - 1;y >= 0;y--)
        {
            for (x = width - 1;x >= 0;x--)
            {
                if (grid[y, x] == translucent_index)
                    buf[i + 0] = 0x05;
                else /* keep this in sync with Pcx.cs */
                if (grid[y, x] == transparent_index)
                    buf[i + 0] = 0x00;
                else
                    buf[i + 0] = 0xff;  
                buf[i + 1] = palette[grid[y, x] * 3 + 2];
                buf[i + 2] = palette[grid[y, x] * 3 + 1];
                buf[i + 3] = palette[grid[y, x] * 3 + 0];
                i += 4;
            }
        }
        return buf;
    }

    public static CGImage cGImageFromBitmap(byte[][] grid, ushort width, ushort height, byte[] palette, boolean with_alpha) throws Exception {
        byte[] buf = GetBitmapData(grid, width, height, palette, with_alpha);
        return CreateImage(buf, width, height, with_alpha ? 32 : 24, width * (3 + (with_alpha ? 1 : 0)));
    }

    public static CALayer createLayerFromBitmap(byte[][] grid, ushort width, ushort height, byte[] palette, boolean with_alpha) throws Exception {
        byte[] buf = GetBitmapData(grid, width, height, palette, with_alpha);
        return CreateLayerFromRGBAData(buf, width, height, with_alpha ? 32 : 24, width * (3 + (with_alpha ? 1 : 0)));
    }

    public static CALayer createLayerFromBitmap(byte[][] grid, ushort width, ushort height, byte[] palette, int translucent_index, int transparent_index) throws Exception {
        byte[] buf = GetBitmapData(grid, width, height, palette, translucent_index, transparent_index);
        return CreateLayerFromRGBAData(buf, width, height, 32, width * 4);
    }

    public static byte[] readStream(Stream stream) throws Exception {
        if (stream instanceof MemoryStream)
        {
            return ((MemoryStream)stream).ToArray();
        }
        else
        {
            byte[] buf = new byte[stream.Length];
            stream.Read(buf, 0, buf.Length);
            return buf;
        } 
    }

    public static CALayer layerFromPcx(Pcx pcx) throws Exception {
        if (pcx.getDepth() == 24)
        {
            return createLayerFromPalettedData(pcx.getData(),pcx.getPalette(),pcx.getWidth(),pcx.getHeight());
        }
        else
        {
            return CreateLayerFromRGBAData(pcx.getData(), pcx.getWidth(), pcx.getHeight(), pcx.getDepth(), pcx.getStride());
        } 
    }

    /* paletted image */
    public static CALayer layerFromStream(Stream stream, int translucentIndex, int transparentIndex) throws Exception {
        Pcx pcx = new Pcx();
        pcx.readFromStream(stream,translucentIndex,transparentIndex);
        return layerFromPcx(pcx);
    }

    public static NSSound soundFromStream(Stream stream) throws Exception {
        byte[] buf = GuiUtil.readStream(stream);
        return new NSSound(NSData.FromArray(buf));
    }

    public static void playSound(Mpq mpq, String resourcePath) throws Exception {
        Stream stream = (Stream)mpq.getResource(resourcePath);
        if (stream == null)
            return ;
         
        NSSound s = GuiUtil.soundFromStream(stream);
        s.Play();
    }

    public static void playMusic(Mpq mpq, String resourcePath, int numLoops) throws Exception {
        Stream stream = (Stream)mpq.getResource(resourcePath);
        if (stream == null)
            return ;
         
        NSSound s = GuiUtil.soundFromStream(stream);
        s.Loops = true;
            ;
        s.Play();
    }

}


