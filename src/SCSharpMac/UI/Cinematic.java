//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:12
//

package SCSharpMac.UI;

import java.util.ArrayList;
import java.util.List;
import SCSharp.Mpq;
import SCSharpMac.UI.__MultiPlayerEvent;
import SCSharpMac.UI.MovieElement;
import SCSharpMac.UI.PlayerEvent;
import SCSharpMac.UI.SmackerPlayer;
import SCSharpMac.UI.UIScreen;

//
// SCSharpMac.UI.Cinematic
//
// Authors:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
public class Cinematic  extends UIScreen 
{
    SmackerPlayer player;
    MovieElement movieElement;
    String resourcePath = new String();
    public Cinematic(Mpq mpq, String resourcePath) throws Exception {
        super(mpq, null, null);
        this.resourcePath = resourcePath;
    }

    protected void resourceLoader() throws Exception {
        super.resourceLoader();
        movieElement = new MovieElement(this,0,0,(int)Bounds.Width,(int)Bounds.Height,true);
        this.Elements.Add(movieElement);
    }

    public void addToPainter() throws Exception {
        super.addToPainter();
        player = new SmackerPlayer((Stream)mpq.getResource(resourcePath));
        player.Finished = __MultiPlayerEvent.combine(player.Finished,new PlayerEvent() 
          { 
            public System.Void invoke() throws Exception {
                playerFinished();
            }

            public List<PlayerEvent> getInvocationList() throws Exception {
                List<PlayerEvent> ret = new ArrayList<PlayerEvent>();
                ret.add(this);
                return ret;
            }
        
          });
        movieElement.setPlayer(player);
        movieElement.play();
        /*Painter.Width*/
        if (player.getWidth() != 640 || player.getHeight() != 480)
        {
            /*Painter.Height*/
            /*Painter.Width*/
            float horiz_zoom = (float)640 / player.getWidth();
            /*Painter.Height*/
            float vert_zoom = (float)480 / player.getHeight();
            float zoom = new float();
            if (horiz_zoom < vert_zoom)
                zoom = horiz_zoom;
            else
                zoom = vert_zoom; 
            AffineTransform = CGAffineTransform.MakeScale(zoom, zoom);
        }
         
        movieElement.getLayer().AnchorPoint = new PointF(0, 0);
        AddSublayer(movieElement.getLayer());
    }

    public void removeFromPainter() throws Exception {
        player.stop();
        player = null;
        super.removeFromPainter();
    }

    public PlayerEvent Finished;
    void playerFinished() throws Exception {
        PlayerEvent h = Finished;
        if (h != null)
            h.invoke();
         
    }

}


