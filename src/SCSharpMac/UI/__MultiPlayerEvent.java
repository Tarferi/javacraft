//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:15
//

package SCSharpMac.UI;

import CS2JNet.JavaSupport.util.ListSupport;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import SCSharpMac.UI.__MultiPlayerEvent;
import SCSharpMac.UI.PlayerEvent;

public class __MultiPlayerEvent   implements PlayerEvent
{
    public void invoke() throws Exception {
        IList<PlayerEvent> copy = new IList<PlayerEvent>(), members = this.getInvocationList();
        synchronized (members)
        {
            copy = new LinkedList<PlayerEvent>(members);
        }
        for (Object __dummyForeachVar0 : copy)
        {
            PlayerEvent d = (PlayerEvent)__dummyForeachVar0;
            d.invoke();
        }
    }

    private System.Collections.Generic.IList<PlayerEvent> _invocationList = new ArrayList<PlayerEvent>();
    public static PlayerEvent combine(PlayerEvent a, PlayerEvent b) throws Exception {
        if (a == null)
            return b;
         
        if (b == null)
            return a;
         
        __MultiPlayerEvent ret = new __MultiPlayerEvent();
        ret._invocationList = a.getInvocationList();
        ret._invocationList.addAll(b.getInvocationList());
        return ret;
    }

    public static PlayerEvent remove(PlayerEvent a, PlayerEvent b) throws Exception {
        if (a == null || b == null)
            return a;
         
        System.Collections.Generic.IList<PlayerEvent> aInvList = a.getInvocationList();
        System.Collections.Generic.IList<PlayerEvent> newInvList = ListSupport.removeFinalStretch(aInvList, b.getInvocationList());
        if (aInvList == newInvList)
        {
            return a;
        }
        else
        {
            __MultiPlayerEvent ret = new __MultiPlayerEvent();
            ret._invocationList = newInvList;
            return ret;
        } 
    }

    public System.Collections.Generic.IList<PlayerEvent> getInvocationList() throws Exception {
        return _invocationList;
    }

}


