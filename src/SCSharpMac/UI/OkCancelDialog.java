//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:14
//

package SCSharpMac.UI;

import SCSharp.Mpq;
import SCSharpMac.UI.Builtins;
import SCSharpMac.UI.DialogEvent;
import SCSharpMac.UI.UIDialog;
import SCSharpMac.UI.UIScreen;

//
// SCSharp.UI.OkCancelDialog
//
// Authors:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
public class OkCancelDialog  extends UIDialog 
{
    String message = new String();
    public OkCancelDialog(UIScreen parent, Mpq mpq, String message) throws Exception {
        super(parent, mpq, "glue\\PalNl", Builtins.rez_GluPOkCancelBin);
        background_path = "glue\\PalNl\\pDPopup.pcx";
        this.message = message;
    }

    static final int OK_ELEMENT_INDEX = 1;
    static final int MESSAGE_ELEMENT_INDEX = 2;
    static final int CANCEL_ELEMENT_INDEX = 3;
    protected void resourceLoader() throws Exception {
        super.resourceLoader();
        Elements[MESSAGE_ELEMENT_INDEX].Text = message;
        Elements[OK_ELEMENT_INDEX].Activate += ;
        Elements[CANCEL_ELEMENT_INDEX].Activate += ;
    }

    public DialogEvent Ok;
    public DialogEvent Cancel;
}


