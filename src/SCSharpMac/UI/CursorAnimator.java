//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:12
//

package SCSharpMac.UI;

import java.util.ArrayList;
import java.util.List;
import SCSharp.Grp;
import SCSharpMac.UI.__MultiTickEventHandler;
import SCSharpMac.UI.Game;
import SCSharpMac.UI.GuiUtil;
import SCSharpMac.UI.TickEventArgs;
import SCSharpMac.UI.TickEventHandler;

//
// SCSharpMac.UI.CursorAnimator
//
// Authors:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
public class CursorAnimator  extends CALayer 
{
    Grp grp;
    long totalElapsed = new long();
    int millisDelay = 100;
    int current_frame = new int();
    CGImage[] frames = new CGImage[]();
    int x = new int();
    int y = new int();
    int hot_x = new int();
    int hot_y = new int();
    byte[] palette = new byte[]();
    public CursorAnimator(Grp grp, byte[] palette) throws Exception {
        this.grp = grp;
        this.x = 100;
        this.y = 100;
        this.palette = palette;
        frames = new CGImage[grp.getFrameCount()];
        Bounds = new RectangleF(100, 100, grp.getWidth(), grp.getHeight());
        AnchorPoint = new PointF(0, 0);
        SetNeedsDisplay();
    }

    public void setHotSpot(int hot_x, int hot_y) throws Exception {
        this.hot_x = hot_x;
        this.hot_y = hot_y;
        Position = new PointF(x - hot_x, y - hot_y);
    }

    public void setPosition(int x, int y) throws Exception {
        this.x = x;
        this.y = y;
        Position = new PointF(x - hot_x, y - hot_y);
    }

    public int getX() throws Exception {
        return x;
    }

    public int getY() throws Exception {
        return y;
    }

    public int getHotX() throws Exception {
        return hot_x;
    }

    public int getHotY() throws Exception {
        return hot_y;
    }

    public void addToPainter() throws Exception {
        Game.getInstance().Tick = __MultiTickEventHandler.combine(Game.getInstance().Tick,new TickEventHandler() 
          { 
            public System.Void invoke(System.Object sender, TickEventArgs args) throws Exception {
                cursorTick(sender, args);
            }

            public List<TickEventHandler> getInvocationList() throws Exception {
                List<TickEventHandler> ret = new ArrayList<TickEventHandler>();
                ret.add(this);
                return ret;
            }
        
          });
    }

    public void removeFromPainter() throws Exception {
        Game.getInstance().Tick = __MultiTickEventHandler.remove(Game.getInstance().Tick,new TickEventHandler() 
          { 
            public System.Void invoke(System.Object sender, TickEventArgs args) throws Exception {
                cursorTick(sender, args);
            }

            public List<TickEventHandler> getInvocationList() throws Exception {
                List<TickEventHandler> ret = new ArrayList<TickEventHandler>();
                ret.add(this);
                return ret;
            }
        
          });
    }

    public void cursorTick(Object sender, TickEventArgs e) throws Exception {
        totalElapsed += e.getMillisecondsElapsed();
        if (totalElapsed < millisDelay)
            return ;
         
        totalElapsed = 0;
        current_frame++;
        SetNeedsDisplay();
    }

    public void drawInContext(CGContext ctx) throws Exception {
        current_frame = current_frame % frames.Length;
        if (frames[current_frame] == null)
            frames[current_frame] = GuiUtil.cGImageFromBitmap(grp.getFrame(current_frame),grp.getWidth(),grp.getHeight(),palette,true);
         
        ctx.DrawImage(new RectangleF(x, y, grp.getWidth(), grp.getHeight()), frames[current_frame]);
    }

}


