//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:14
//

package SCSharpMac.UI;

import SCSharp.Mpq;
import SCSharpMac.UI.ReadyRoomScreen;

public class TerranReadyRoomScreen  extends ReadyRoomScreen 
{
    public TerranReadyRoomScreen(Mpq mpq, String scenario_prefix) throws Exception {
        super(mpq, scenario_prefix, START_ELEMENT_INDEX, CANCEL_ELEMENT_INDEX, SKIPTUTORIAL_ELEMENT_INDEX, REPLAY_ELEMENT_INDEX, TRANSMISSION_ELEMENT_INDEX, OBJECTIVES_ELEMENT_INDEX, FIRST_PORTRAIT_ELEMENT_INDEX);
    }

    static final int START_ELEMENT_INDEX = 1;
    static final int CANCEL_ELEMENT_INDEX = 9;
    static final int SKIPTUTORIAL_ELEMENT_INDEX = 11;
    static final int REPLAY_ELEMENT_INDEX = 12;
    static final int FIRST_PORTRAIT_ELEMENT_INDEX = 13;
    static final int TRANSMISSION_ELEMENT_INDEX = 17;
    static final int OBJECTIVES_ELEMENT_INDEX = 18;
}


