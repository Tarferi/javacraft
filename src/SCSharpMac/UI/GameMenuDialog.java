//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:12
//

package SCSharpMac.UI;

import java.util.ArrayList;
import java.util.List;
import SCSharp.Mpq;
import SCSharpMac.UI.__MultiDialogEvent;
import SCSharpMac.UI.Builtins;
import SCSharpMac.UI.DialogEvent;
import SCSharpMac.UI.EndMissionDialog;
import SCSharpMac.UI.HelpDialog;
import SCSharpMac.UI.ObjectivesDialog;
import SCSharpMac.UI.OptionsDialog;
import SCSharpMac.UI.UIDialog;
import SCSharpMac.UI.UIScreen;

//
// SCSharp.UI.GameMenuDialog
//
// Authors:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
public class GameMenuDialog  extends UIDialog 
{
    public GameMenuDialog(UIScreen parent, Mpq mpq) throws Exception {
        super(parent, mpq, "glue\\Palmm", Builtins.rez_GameMenuBin);
        background_path = null;
    }

    static final int RETURNTOGAME_ELEMENT_INDEX = 1;
    static final int SAVEGAME_ELEMENT_INDEX = 2;
    static final int LOADGAME_ELEMENT_INDEX = 3;
    static final int PAUSEGAME_ELEMENT_INDEX = 4;
    static final int RESUMEGAME_ELEMENT_INDEX = 5;
    static final int OPTIONS_ELEMENT_INDEX = 6;
    static final int HELP_ELEMENT_INDEX = 7;
    static final int MISSIONOBJECTIVES_ELEMENT_INDEX = 8;
    static final int ENDMISSION_ELEMENT_INDEX = 9;
    protected void resourceLoader() throws Exception {
        super.resourceLoader();
        for (int i = 0;i < Elements.Count;i++)
            Console.WriteLine("{0}: {1} '{2}'", i, Elements[i].Type, Elements[i].Text);
        Elements[RETURNTOGAME_ELEMENT_INDEX].Activate += ;
        Elements[MISSIONOBJECTIVES_ELEMENT_INDEX].Activate += ;
        Elements[ENDMISSION_ELEMENT_INDEX].Activate += ;
        Elements[OPTIONS_ELEMENT_INDEX].Activate += ;
        Elements[HELP_ELEMENT_INDEX].Activate += ;
    }

    public DialogEvent ReturnToGame;
}


