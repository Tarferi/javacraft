//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:15
//

package SCSharpMac.UI;

import SCSharp.BinElement;
import SCSharpMac.UI.TextElementLayerDelegate;
import SCSharpMac.UI.UIElement;
import SCSharpMac.UI.UIScreen;

//
// SCSharpMac.UI.TextBoxElement
//
// Authors:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// TODO: draw an actual cursor..
public class TextBoxElement  extends UIElement 
{
    StringBuilder value = new StringBuilder();
    int cursor = 0;
    public TextBoxElement(UIScreen screen, BinElement el, byte[] palette) throws Exception {
        super(screen, el, palette);
        value = new StringBuilder();
    }

    public void keyboardDown(NSEvent theEvent) throws Exception {
        Console.WriteLine("what up, my nig");
        boolean changed = false;
        if ((theEvent.ModifierFlags & NSEventModifierMask.NumericPadKeyMask) == NSEventModifierMask.NumericPadKeyMask)
        {
            /* navigation keys */
            if (theEvent.Characters[0] == (char)NSKey.LeftArrow)
            {
                if (cursor > 0)
                    cursor--;
                 
            }
            else if (theEvent.Characters[0] == (char)NSKey.RightArrow)
            {
                if (cursor < value.Length)
                    cursor++;
                 
            }
              
        }
        else if ((theEvent.ModifierFlags & NSEventModifierMask.FunctionKeyMask) == NSEventModifierMask.FunctionKeyMask)
        {
            if (theEvent.Characters[0] == (char)NSKey.Home)
            {
                cursor = 0;
            }
            else if (theEvent.Characters[0] == (char)NSKey.End)
            {
                cursor = value.Length;
            }
              
        }
        else /* keys that modify the text */
        if (theEvent.Characters[0] == (char)0x7f)
        {
            if (value.Length > 0)
            {
                value = value.Remove(cursor - 1, 1);
                cursor--;
                changed = true;
            }
             
        }
        else
        {
            for (Object __dummyForeachVar0 : theEvent.CharactersIgnoringModifiers)
            {
                char c = (Character)__dummyForeachVar0;
                if (!Char.IsLetterOrDigit(c) && c != ' ')
                    continue;
                 
                char cc = new char();
                if ((theEvent.ModifierFlags & NSEventModifierMask.AlphaShiftKeyMask) == NSEventModifierMask.AlphaShiftKeyMask)
                    cc = Char.ToUpper(c);
                else
                    cc = c; 
                value.Insert(cursor++, cc);
                changed = true;
            }
            changed = true;
        }   
        if (changed)
        {
            setText(getValue());
            getLayer().SetNeedsDisplay();
        }
         
    }

    public int getValueLength() throws Exception {
        return value.Length;
    }

    public String getValue() throws Exception {
        return value.ToString();
    }

    protected CALayer createLayer() throws Exception {
        CALayer layer = CALayer.Create();
        layer.AnchorPoint = new PointF(0, 0);
        layer.Bounds = new RectangleF(0, 0, getWidth(), getHeight());
        layer.Delegate = new TextElementLayerDelegate(this);
        return layer;
    }

}


