//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:12
//

package SCSharpMac.UI;

import CS2JNet.JavaSupport.util.ListSupport;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class TickEventArgs  extends EventArgs 
{
    public TickEventArgs(long millis) throws Exception {
        elapsedMillis = millis;
    }

    public long getMillisecondsElapsed() throws Exception {
        return elapsedMillis;
    }

    public float getSecondsElapsed() throws Exception {
        return (float)elapsedMillis / 1000.0f;
    }

    long elapsedMillis = new long();
}


