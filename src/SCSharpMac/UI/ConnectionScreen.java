//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:12
//

package SCSharpMac.UI;

import CS2JNet.System.StringSupport;
import java.util.ArrayList;
import java.util.List;
import SCSharp.Mpq;
import SCSharpMac.UI.__MultiListBoxSelectionChanged;
import SCSharpMac.UI.Builtins;
import SCSharpMac.UI.Game;
import SCSharpMac.UI.GlobalResources;
import SCSharpMac.UI.ListBoxElement;
import SCSharpMac.UI.ListBoxSelectionChanged;
import SCSharpMac.UI.OkDialog;
import SCSharpMac.UI.UIScreen;
import SCSharpMac.UI.UIScreenType;

//
// SCSharp.UI.ConnectionString
//
// Authors:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
public class ConnectionScreen  extends UIScreen 
{
    public ConnectionScreen(Mpq mpq) throws Exception {
        super(mpq, "glue\\PalNl", Builtins.rez_GluConnBin);
    }

    static final int GATEWAY_IMAGE_ELEMENT_INDEX = 5;
    static final int GATEWAY_LABEL_ELEMENT_INDEX = 8;
    static final int GATEWAY_LIST_ELEMENT_INDEX = 9;
    static final int LISTBOX_ELEMENT_INDEX = 7;
    static final int TITLE_ELEMENT_INDEX = 11;
    static final int DESCRIPTION_ELEMENT_INDEX = 13;
    static final int OK_ELEMENT_INDEX = 14;
    static final int CANCEL_ELEMENT_INDEX = 15;
    static final int num_choices = 4;
    static final int title_startidx = 95;
    static final int description_startidx = 99;
    String[] titles = new String[]();
    String[] descriptions = new String[]();
    ListBoxElement listbox;
    protected void resourceLoader() throws Exception {
        super.resourceLoader();
        for (int i = 0;i < Elements.Count;i++)
            Console.WriteLine("{0}: {1} '{2}'", i, Elements[i].Type, Elements[i].Text);
        titles = new String[num_choices];
        descriptions = new String[num_choices];
        for (int i = 0;i < num_choices;i++)
        {
            titles[i] = GlobalResources.getInstance().getGluAllTbl().get___idx(title_startidx + i);
            descriptions[i] = GlobalResources.getInstance().getGluAllTbl().get___idx(description_startidx + i);
        }
        listbox = (ListBoxElement)Elements[LISTBOX_ELEMENT_INDEX];
        for (Object __dummyForeachVar0 : titles)
        {
            String s = (String)__dummyForeachVar0;
            listbox.addItem(s);
        }
        listbox.setSelectedIndex(0);
        handleSelectionChanged(0);
        listbox.SelectionChanged = __MultiListBoxSelectionChanged.combine(listbox.SelectionChanged,new ListBoxSelectionChanged() 
          { 
            public System.Void invoke(System.Int32 selectedIndex) throws Exception {
                handleSelectionChanged(selectedIndex);
            }

            public List<ListBoxSelectionChanged> getInvocationList() throws Exception {
                List<ListBoxSelectionChanged> ret = new ArrayList<ListBoxSelectionChanged>();
                ret.add(this);
                return ret;
            }
        
          });
        Elements[OK_ELEMENT_INDEX].Activate += ;
        Elements[CANCEL_ELEMENT_INDEX].Activate += ;
    }

    void handleSelectionChanged(int selectedIndex) throws Exception {
        boolean visible = (StringSupport.equals(listbox.getSelectedItem(), "Battle.net"));
        Elements[GATEWAY_IMAGE_ELEMENT_INDEX].Visible = Elements[GATEWAY_LABEL_ELEMENT_INDEX].Visible = Elements[GATEWAY_LIST_ELEMENT_INDEX].Visible = visible;
        Elements[TITLE_ELEMENT_INDEX].Text = titles[selectedIndex];
        Elements[DESCRIPTION_ELEMENT_INDEX].Text = descriptions[selectedIndex];
    }

    public void keyboardDown(NSEvent theEvent) throws Exception {
        if ((theEvent.ModifierFlags & NSEventModifierMask.NumericPadKeyMask) == NSEventModifierMask.NumericPadKeyMask && (theEvent.Characters[0] == (char)NSKey.UpArrow || theEvent.Characters[0] == (char)NSKey.DownArrow))
            listbox.keyboardDown(theEvent);
        else
            super.keyboardDown(theEvent); 
    }

}


