//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:14
//

package SCSharpMac.UI;

import SCSharp.Util;

//
// SCSharpMac.UI.Pcx
//
// Authors:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
public class Pcx   
{
    public Pcx() throws Exception {
    }

    ushort xmin = new ushort();
    ushort xmax = new ushort();
    ushort ymin = new ushort();
    ushort ymax = new ushort();
    boolean with_alpha = new boolean();
    public void readFromStream(Stream stream, int translucentIndex, int transparentIndex) throws Exception {
        with_alpha = translucentIndex != -1 || transparentIndex != -1;
        byte magic = Util.readByte(stream);
        if (magic != 0x0A)
            throw new Exception("stream is not a valid .pcx file");
         
        /*version =*/
        Util.readByte(stream);
        /*encoding =*/
        Util.readByte(stream);
        ushort bpp = Util.readByte(stream);
        xmin = Util.readWord(stream);
        ymin = Util.readWord(stream);
        xmax = Util.readWord(stream);
        ymax = Util.readWord(stream);
        /*ushort h_dpi =*/
        Util.readWord(stream);
        /*ushort v_dpi =*/
        Util.readWord(stream);
        stream.Position += 48;
        /* skip the header palette */
        stream.Position++;
        /* skip the reserved byte */
        ushort numplanes = Util.readByte(stream);
        /*ushort stride =*/
        Util.readWord(stream);
        /*headerInterp =*/
        Util.readWord(stream);
        /*videoWidth =*/
        Util.readWord(stream);
        /*videoHeight =*/
        Util.readWord(stream);
        stream.Position += 54;
        if (bpp != 8 || numplanes != 1)
            throw new Exception("unsupported .pcx image type");
         
        width = (ushort)(xmax - xmin + 1);
        height = (ushort)(ymax - ymin + 1);
        long imageData = stream.Position;
        stream.Position = stream.Length - 256 * 3;
        /* read the palette */
        palette = new byte[256 * 3];
        stream.Read(palette, 0, 256 * 3);
        stream.Position = imageData;
        Console.WriteLine("imageData begins at {0}", imageData);
        /* now read the image data */
        if (with_alpha)
            data = new byte[width * height * 4];
        else
            data = new byte[width * height]; 
        int idx = 0;
        while (idx < data.Length)
        {
            byte b = Util.readByte(stream);
            byte count = new byte();
            byte value = new byte();
            if ((b & 0xC0) == 0xC0)
            {
                /* it's a count byte */
                count = (byte)(b & 0x3F);
                value = Util.readByte(stream);
            }
            else
            {
                count = 1;
                value = b;
            } 
            for (int i = 0;i < count;i++)
            {
                if (with_alpha)
                {
                    if (idx + 4 > data.Length)
                        return ;
                     
                    /* this stuff is endian
                    						 * dependent... for big endian
                    						 * we need the "idx +"'s
                    						 * reversed */
                    data[idx + 1] = palette[value * 3 + 0];
                    data[idx + 2] = palette[value * 3 + 1];
                    data[idx + 3] = palette[value * 3 + 2];
                    if (value == translucentIndex)
                        data[idx + 0] = 0xd0;
                    else if (value == transparentIndex)
                        data[idx + 0] = 0x00;
                    else
                        data[idx + 0] = 0xff;  
                    idx += 4;
                }
                else
                {
                    data[idx++] = value;
                } 
            }
        }
    }

    byte[] data = new byte[]();
    byte[] palette = new byte[]();
    ushort width = new ushort();
    ushort height = new ushort();
    public byte[] getData() throws Exception {
        return data;
    }

    public byte[] getRGBData() throws Exception {
        byte[] foo = new byte[width * height * 3];
        int i = 0;
        int j = 0;
        if (with_alpha)
        {
            while (i < data.Length)
            {
                i++;
                foo[j++] = data[i++];
                foo[j++] = data[i++];
                foo[j++] = data[i++];
            }
        }
        else
        {
            while (i < data.Length)
            {
                foo[j++] = palette[data[i] * 3 + 0];
                foo[j++] = palette[data[i] * 3 + 1];
                foo[j++] = palette[data[i] * 3 + 2];
                i += 3;
            }
        } 
        return foo;
    }

    public byte[] getPalette() throws Exception {
        return palette;
    }

    public ushort getWidth() throws Exception {
        return width;
    }

    public ushort getHeight() throws Exception {
        return height;
    }

    public ushort getDepth() throws Exception {
        return (ushort)(with_alpha ? 32 : 24);
    }

    public ushort getStride() throws Exception {
        return (ushort)(width * (3 + (with_alpha ? 1 : 0)));
    }

}


