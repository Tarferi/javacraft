//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:15
//

package SCSharpMac.UI;

import CS2JNet.JavaSupport.language.RefSupport;
import java.util.ArrayList;
import java.util.List;
import SCSharp.Mpq;
import SCSharp.UnitInfo;
import SCSharp.UnitsDat;
import SCSharpMac.UI.__MultiTickEventHandler;
import SCSharpMac.UI.AnimationType;
import SCSharpMac.UI.AStarSolver;
import SCSharpMac.UI.Game;
import SCSharpMac.UI.GlobalResources;
import SCSharpMac.UI.MapPoint;
import SCSharpMac.UI.MapRenderer;
import SCSharpMac.UI.Sprite;
import SCSharpMac.UI.SpriteManager;
import SCSharpMac.UI.TickEventArgs;

//
// SCSharp.UI.Unit
//
// Authors:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
public class Unit   
{
    int unit_id = new int();
    UnitsDat units;
    uint hitpoints = new uint();
    uint shields = new uint();
    int x = new int();
    int y = new int();
    ushort width = new ushort(), height = new ushort();
    Sprite sprite;
    public Unit(int unit_id) throws Exception {
        this.unit_id = unit_id;
        units = GlobalResources.getInstance().getUnitsDat();
        hitpoints = units.getHitpoints().get___idx(unit_id);
        shields = units.getShields().get___idx(unit_id);
        width = units.getWidths().get___idx(unit_id);
        height = units.getHeights().get___idx(unit_id);
        Console.WriteLine("compaiidle for unit {0} = {1}", unit_id, units.getCompAIIdles().get___idx(unit_id));
    }

    public Unit(UnitInfo info) throws Exception {
        this(info.unit_id);
        x = info.x;
        y = info.y;
    }

    public void move(MapRenderer mapRenderer, int goal_minitile_x, int goal_minitile_y) throws Exception {
        if (false)
        {
        }
        else
        {
            /*flying unit*/
            // easier case, take the direct route
            // Console.WriteLine ("FindPath from {0},{1} -> {2},{3}",
            // 		   x >> 2, y >> 2,
            // 		   goal_minitile_x, goal_minitile_y);
            navigateDestination = new MapPoint(goal_minitile_x,goal_minitile_y);
            AStarSolver astar = new AStarSolver(mapRenderer);
            navigatePath = astar.findPath(new MapPoint(x >> 2,y >> 2),navigateDestination);
            sprite.setDebug(true);
            if (navigatePath != null)
                navigateAlongPath();
             
        } 
    }

    int totalElapsed = new int();
    int millisDelay = 150;
    List<MapPoint> navigatePath = new List<MapPoint>();
    MapPoint navigateDestination;
    MapPoint startCurrentSegment;
    MapPoint endCurrentSegment;
    int dest_pixel_x = new int(), dest_pixel_y = new int();
    int pixel_x = new int(), pixel_y = new int();
    int delta_x = new int(), delta_y = new int();
    int classifyDirection(MapPoint startCurrentSegment, MapPoint endCurrentSegment) throws Exception {
        if (startCurrentSegment.getX() < endCurrentSegment.getX())
        {
            if (startCurrentSegment.getY() < endCurrentSegment.getY())
            {
                Console.WriteLine("1 startCurrentSegment.Y < endCurrentSegment.Y");
                return 5;
            }
            else if (startCurrentSegment.getY() == endCurrentSegment.getY())
            {
                Console.WriteLine("face = 12");
                return 12;
            }
            else
            {
                Console.WriteLine("1 startCurrentSegment.Y > endCurrentSegment.Y");
                return 2;
            }  
        }
        else if (startCurrentSegment.getX() == endCurrentSegment.getX())
        {
            if (startCurrentSegment.getY() < endCurrentSegment.getY())
            {
                Console.WriteLine("2 startCurrentSegment.Y < endCurrentSegment.Y");
                return 7;
            }
            else if (startCurrentSegment.getY() > endCurrentSegment.getY())
            {
                Console.WriteLine("2 startCurrentSegment.Y > endCurrentSegment.Y");
                return 0;
            }
            else
            {
                Console.WriteLine("@#(*@!#&( shouldn't happen");
                return 0;
            }  
        }
        else
        {
            if (startCurrentSegment.getY() < endCurrentSegment.getY())
            {
                Console.WriteLine("3 startCurrentSegment.Y < endCurrentSegment.Y");
                return 9;
            }
            else if (startCurrentSegment.getY() == endCurrentSegment.getY())
            {
                Console.WriteLine("face = 4");
                return 4;
            }
            else
            {
                Console.WriteLine("3 startCurrentSegment.Y > endCurrentSegment.Y");
                return 0;
            }  
        }  
    }

    void navigateTick(Object sender, TickEventArgs e) throws Exception {
        //			totalElapsed += e.TicksElapsed;
        if (totalElapsed < millisDelay)
            return ;
         
        sprite.invalidate();
        pixel_x += delta_x;
        pixel_y += delta_y;
        if (dest_pixel_x - pixel_x < 2)
            pixel_x = dest_pixel_x;
         
        if (dest_pixel_y - pixel_y < 2)
            pixel_y = dest_pixel_y;
         
        sprite.setPosition(pixel_x,pixel_y);
        sprite.invalidate();
        x = pixel_x << 2;
        y = pixel_y << 2;
        if (pixel_x == dest_pixel_x && pixel_y == dest_pixel_y)
        {
            startCurrentSegment = endCurrentSegment;
            navigatePath.RemoveAt(0);
            // if we're at the destination, remove the tick handler
            if (navigatePath.Count == 0)
            {
                sprite.runScript(AnimationType.WalkingToIdle);
                Game.getInstance().Tick = __MultiTickEventHandler.remove(Game.getInstance().Tick,new TickEventHandler() 
                  { 
                    public System.Void invoke(System.Object sender, TickEventArgs args) throws Exception {
                        navigateTick(sender, args);
                    }

                    public List<TickEventHandler> getInvocationList() throws Exception {
                        List<TickEventHandler> ret = new ArrayList<TickEventHandler>();
                        ret.add(this);
                        return ret;
                    }
                
                  });
            }
            else
            {
                endCurrentSegment = navigatePath[0];
                sprite.face(classifyDirection(startCurrentSegment,endCurrentSegment));
                dest_pixel_x = endCurrentSegment.getX() * 4 + 4;
                dest_pixel_y = endCurrentSegment.getY() * 4 + 4;
                delta_x = dest_pixel_x - pixel_x;
                delta_y = dest_pixel_y - pixel_y;
            } 
        }
         
    }

    void navigateAlongPath() throws Exception {
        int sprite_x = new int(), sprite_y = new int();
        RefSupport<int> refVar___0 = new RefSupport<int>();
        RefSupport<int> refVar___1 = new RefSupport<int>();
        sprite.getPosition(refVar___0,refVar___1);
        sprite_x = refVar___0.getValue();
        sprite_y = refVar___1.getValue();
        Console.WriteLine("starting pixel position = {0},{1}", sprite_x, sprite_y);
        startCurrentSegment = navigatePath[0];
        navigatePath.RemoveAt(0);
        endCurrentSegment = navigatePath[0];
        sprite.face(classifyDirection(startCurrentSegment,endCurrentSegment));
        int start_pixel_x = getX() * 4 + 4;
        int start_pixel_y = getX() * 4 + 4;
        dest_pixel_x = endCurrentSegment.getX() * 4 + 4;
        dest_pixel_y = endCurrentSegment.getY() * 4 + 4;
        delta_x = dest_pixel_x - start_pixel_x;
        delta_y = dest_pixel_y - start_pixel_y;
        pixel_x = start_pixel_x;
        pixel_y = start_pixel_y;
        sprite.runScript(AnimationType.Walking);
        Game.getInstance().Tick = __MultiTickEventHandler.combine(Game.getInstance().Tick,new TickEventHandler() 
          { 
            public System.Void invoke(System.Object sender, TickEventArgs args) throws Exception {
                navigateTick(sender, args);
            }

            public List<TickEventHandler> getInvocationList() throws Exception {
                List<TickEventHandler> ret = new ArrayList<TickEventHandler>();
                ret.add(this);
                return ret;
            }
        
          });
    }

    public Sprite createSprite(Mpq mpq, byte[] palette) throws Exception {
        if (sprite != null)
            throw new Exception();
         
        sprite = SpriteManager.CreateSprite(mpq, getSpriteId(), palette, x, y);
        sprite.runScript(AnimationType.Init);
        return sprite;
    }

    public int getX() throws Exception {
        return x;
    }

    public void setX(int value) throws Exception {
        x = value;
    }

    public int getY() throws Exception {
        return y;
    }

    public void setY(int value) throws Exception {
        y = value;
    }

    public Sprite getSprite() throws Exception {
        return sprite;
    }

    public int getUnitId() throws Exception {
        return unit_id;
    }

    public int getFlingyId() throws Exception {
        return units.getFlingyIds().get___idx(unit_id);
    }

    public int getSpriteId() throws Exception {
        return GlobalResources.getInstance().getFlingyDat().getSpriteIds().get___idx(getFlingyId());
    }

    public uint getConstructSpriteId() throws Exception {
        return units.getConstructSpriteIds().get___idx(unit_id);
    }

    public int getAnimationLevel() throws Exception {
        return units.getAnimationLevels().get___idx(unit_id);
    }

    public uint getHitPoints() throws Exception {
        return hitpoints;
    }

    public void setHitPoints(uint value) throws Exception {
        hitpoints = value;
    }

    public uint getShields() throws Exception {
        return shields;
    }

    public void setShields(uint value) throws Exception {
        shields = value;
    }

    public int getCreateScore() throws Exception {
        return units.getCreateScores().get___idx(unit_id);
    }

    public int getDestroyScore() throws Exception {
        return units.getDestroyScores().get___idx(unit_id);
    }

    public int getSelectionCircle() throws Exception {
        return GlobalResources.getInstance().getSpritesDat().getSelectionCircles().get___idx(getSpriteId());
    }

    public int getSelectionCircleOffset() throws Exception {
        return GlobalResources.getInstance().getSpritesDat().getSelectionCircleOffsets().get___idx(getSpriteId());
    }

    public String getPortrait() throws Exception {
        if (unit_id >= units.getPortraits().getCount())
            return null;
         
        int idx = (int)units.getPortraits().get___idx(unit_id);
        if (idx >= GlobalResources.getInstance().getPortDataDat().getPortraitIndexes().getCount())
            return null;
         
        int portidx = (int)GlobalResources.getInstance().getPortDataDat().getPortraitIndexes().get___idx(idx);
        if (portidx >= GlobalResources.getInstance().getPortDataTbl().getCount())
            return null;
         
        return GlobalResources.getInstance().getPortDataTbl().get___idx(portidx);
    }

    public String getYesSound() throws Exception {
        if (unit_id >= units.getYesSoundStarts().getCount() || unit_id >= units.getYesSoundEnds().getCount())
            return null;
         
        int start_idx = (int)units.getYesSoundStarts().get___idx(unit_id);
        int end_idx = (int)units.getYesSoundEnds().get___idx(unit_id);
        Console.WriteLine("start_idx = {0}, end_idx = {1}", start_idx, end_idx);
        if (end_idx >= GlobalResources.getInstance().getSfxDataDat().getFileIndexes().getCount())
            return null;
         
        int sfxidx = (int)GlobalResources.getInstance().getSfxDataDat().getFileIndexes().get___idx(end_idx - 1);
        if (sfxidx >= GlobalResources.getInstance().getSfxDataTbl().getCount())
            return null;
         
        return GlobalResources.getInstance().getSfxDataTbl().get___idx(sfxidx);
    }

    public String getWhatSound() throws Exception {
        if (unit_id >= units.getWhatSoundStarts().getCount())
            return null;
         
        int idx = (int)units.getWhatSoundStarts().get___idx(unit_id);
        if (idx >= GlobalResources.getInstance().getSfxDataDat().getFileIndexes().getCount())
            return null;
         
        int sfxidx = (int)GlobalResources.getInstance().getSfxDataDat().getFileIndexes().get___idx(idx);
        if (sfxidx >= GlobalResources.getInstance().getSfxDataTbl().getCount())
            return null;
         
        return GlobalResources.getInstance().getSfxDataTbl().get___idx(sfxidx);
    }

    public int getWidth() throws Exception {
        return units.getWidths().get___idx(unit_id);
    }

    public int getHeight() throws Exception {
        return units.getHeights().get___idx(unit_id);
    }

}


