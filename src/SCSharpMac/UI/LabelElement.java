//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:13
//

package SCSharpMac.UI;

import SCSharp.BinElement;
import SCSharp.ElementType;
import SCSharp.Fnt;
import SCSharpMac.UI.GuiUtil;
import SCSharpMac.UI.UIElement;
import SCSharpMac.UI.UIScreen;

//
// SCSharpMac.UI.LabelElement
//
// Authors:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
public class LabelElement  extends UIElement 
{
    boolean calc_width = new boolean();
    public LabelElement(UIScreen screen, BinElement el, byte[] palette) throws Exception {
        super(screen, el, palette);
    }

    public LabelElement(UIScreen screen, byte[] palette, Fnt font, ushort x, ushort y) throws Exception {
        super(screen, x, y);
        this.setPalette(palette);
        this.setFont(font);
        calc_width = true;
    }

    protected CALayer createLayer() throws Exception {
        if (calc_width)
        {
            CALayer textLayer = GuiUtil.composeText(getText(),getFont(),getPalette(),-1,-1,getSensitive() ? 4 : 24);
            textLayer.AnchorPoint = new PointF(0, 0);
            setWidth((ushort)textLayer.Bounds.Width);
            setHeight((ushort)textLayer.Bounds.Height);
            return textLayer;
        }
        else
        {
            /* this is wrong */
            CALayer layer = CALayer.Create();
            layer.Bounds = new RectangleF(0, 0, getWidth(), getHeight());
            CALayer textLayer = GuiUtil.ComposeText(getText(), getFont(), getPalette(), getWidth(), getHeight(), getSensitive() ? 4 : 24);
            float x = 0;
            if (getType() == ElementType.LabelRightAlign)
                x += getWidth() - textLayer.Bounds.Width;
            else if (getType() == ElementType.LabelCenterAlign)
                x += (getWidth() - textLayer.Bounds.Width) / 2;
              
            textLayer.AnchorPoint = new PointF(0, 0);
            textLayer.Position = new PointF(x, 0);
            layer.AddSublayer(textLayer);
            return layer;
        } 
    }

}


