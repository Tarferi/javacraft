//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:14
//

package SCSharpMac.UI;

import java.util.ArrayList;
import java.util.List;
import SCSharp.Chk;
import SCSharp.Mpq;
import SCSharp.Util;
import SCSharpMac.UI.__MultiTickEventHandler;
import SCSharpMac.UI.BriefingRunner;
import SCSharpMac.UI.Builtins;
import SCSharpMac.UI.Game;
import SCSharpMac.UI.GameScreen;
import SCSharpMac.UI.GlobalResources;
import SCSharpMac.UI.ImageElement;
import SCSharpMac.UI.MovieElement;
import SCSharpMac.UI.ProtossReadyRoomScreen;
import SCSharpMac.UI.Race;
import SCSharpMac.UI.ReadyRoomScreen;
import SCSharpMac.UI.SmackerPlayer;
import SCSharpMac.UI.TerranReadyRoomScreen;
import SCSharpMac.UI.TickEventArgs;
import SCSharpMac.UI.UIElement;
import SCSharpMac.UI.UIScreen;
import SCSharpMac.UI.UIScreenType;
import SCSharpMac.UI.ZergReadyRoomScreen;

//
// SCSharp.UI.ReadyRoomScreen
//
// Authors:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
public class ReadyRoomScreen  extends UIScreen 
{
    public ReadyRoomScreen(Mpq mpq, String scenario_prefix, int start_element_index, int cancel_element_index, int skiptutorial_element_index, int replay_element_index, int transmission_element_index, int objectives_element_index, int first_portrait_element_index) throws Exception {
        super(mpq, String.Format("glue\\Ready{0}", Util.RaceChar[((Enum)Game.getInstance().getRace()).ordinal()]), String.Format(Builtins.rez_GluRdyBin, Util.RaceCharLower[((Enum)Game.getInstance().getRace()).ordinal()]));
        background_path = String.Format("glue\\PalR{0}\\Backgnd.pcx", Util.RaceCharLower[((Enum)Game.getInstance().getRace()).ordinal()]);
        fontpal_path = String.Format("glue\\PalR{0}\\tFont.pcx", Util.RaceCharLower[((Enum)Game.getInstance().getRace()).ordinal()]);
        effectpal_path = String.Format("glue\\PalR{0}\\tEffect.pcx", Util.RaceCharLower[((Enum)Game.getInstance().getRace()).ordinal()]);
        arrowgrp_path = String.Format("glue\\PalR{0}\\arrow.grp", Util.RaceCharLower[((Enum)Game.getInstance().getRace()).ordinal()]);
        this.start_element_index = start_element_index;
        this.cancel_element_index = cancel_element_index;
        this.skiptutorial_element_index = skiptutorial_element_index;
        this.replay_element_index = replay_element_index;
        this.transmission_element_index = transmission_element_index;
        this.objectives_element_index = objectives_element_index;
        this.first_portrait_element_index = first_portrait_element_index;
        this.scenario = (Chk)mpq.getResource(scenario_prefix + "\\staredit\\scenario.chk");
        this.scenario_prefix = scenario_prefix;
    }

    BriefingRunner runner;
    Chk scenario;
    String scenario_prefix = new String();
    int start_element_index = new int();
    int cancel_element_index = new int();
    int skiptutorial_element_index = new int();
    int replay_element_index = new int();
    int transmission_element_index = new int();
    int objectives_element_index = new int();
    int first_portrait_element_index = new int();
    List<MovieElement> portraits = new List<MovieElement>();
    List<ImageElement> hframes = new List<ImageElement>();
    List<ImageElement> frames = new List<ImageElement>();
    protected void resourceLoader() throws Exception {
        setTranslucentIndex(138);
        super.resourceLoader();
        for (int i = 0;i < Elements.Count;i++)
            Console.WriteLine("{0}: {1} '{2}' {3}", i, Elements[i].Type, Elements[i].Text, Elements[i].Flags);
        if (scenario_prefix.EndsWith("tutorial"))
        {
            Elements[skiptutorial_element_index].Visible = true;
        }
         
        Elements[cancel_element_index].Activate += ;
        Elements[replay_element_index].Activate += ;
        Elements[start_element_index].Activate += ;
        runner = new BriefingRunner(this,scenario,scenario_prefix);
        portraits = new List<MovieElement>();
        hframes = new List<ImageElement>();
        frames = new List<ImageElement>();
        for (int i = 0;i < 4;i++)
        {
            MovieElement m = new MovieElement(this, Elements[first_portrait_element_index + i].BinElement, Elements[first_portrait_element_index + i].Palette, true);
            m.setX1(m.getX1() + 3);
            m.setY1(m.getY1() + 3);
            m.setWidth(m.getWidth() - 6);
            m.setHeight(m.getHeight() - 6);
            ImageElement f = new ImageElement(this, Elements[first_portrait_element_index + i].BinElement, Elements[first_portrait_element_index + i].Palette, getTranslucentIndex());
            f.setText(String.Format("glue\\Ready{0}\\{0}Frame{1}.pcx", Util.RaceChar[((Enum)Game.getInstance().getRace()).ordinal()], i + 1));
            ImageElement h = new ImageElement(this, Elements[first_portrait_element_index + i].BinElement, Elements[first_portrait_element_index + i].Palette, getTranslucentIndex());
            h.setText(String.Format("glue\\Ready{0}\\{0}FrameH{1}.pcx", Util.RaceChar[((Enum)Game.getInstance().getRace()).ordinal()], i + 1));
            f.setVisible(false);
            h.setVisible(false);
            m.setVisible(false);
            portraits.Add(m);
            hframes.Add(h);
            frames.Add(f);
            Elements.Add(m);
            Elements.Add(h);
            Elements.Add(f);
        }
    }

    void stopBriefing() throws Exception {
        Game.getInstance().Tick = __MultiTickEventHandler.remove(Game.getInstance().Tick,new TickEventHandler() 
          { 
            public System.Void invoke(System.Object sender, TickEventArgs args) throws Exception {
                runner.tick(sender, args);
            }

            public List<TickEventHandler> getInvocationList() throws Exception {
                List<TickEventHandler> ret = new ArrayList<TickEventHandler>();
                ret.add(this);
                return ret;
            }
        
          });
        runner.stop();
        Elements[transmission_element_index].Visible = false;
        Elements[transmission_element_index].Text = "";
        Elements[objectives_element_index].Visible = false;
        Elements[objectives_element_index].Text = "";
        for (int i = 0;i < 4;i++)
        {
            Elements[first_portrait_element_index + i].Visible = false;
            portraits[i].Visible = false;
        }
    }

    void playBriefing() throws Exception {
        runner.play();
    }

    public void addToPainter() throws Exception {
        super.addToPainter();
        Game.getInstance().Tick = __MultiTickEventHandler.combine(Game.getInstance().Tick,new TickEventHandler() 
          { 
            public System.Void invoke(System.Object sender, TickEventArgs args) throws Exception {
                runner.tick(sender, args);
            }

            public List<TickEventHandler> getInvocationList() throws Exception {
                List<TickEventHandler> ret = new ArrayList<TickEventHandler>();
                ret.add(this);
                return ret;
            }
        
          });
    }

    public void removeFromPainter() throws Exception {
        super.removeFromPainter();
        for (Object __dummyForeachVar0 : Elements)
        {
            UIElement el = (UIElement)__dummyForeachVar0;
            if (el instanceof MovieElement)
                ((MovieElement)el).stop();
             
        }
    }

    public void setObjectives(String str) throws Exception {
        Elements[objectives_element_index].Visible = true;
        Elements[objectives_element_index].Text = str;
    }

    public void setTransmissionText(String str) throws Exception {
        Elements[transmission_element_index].Visible = true;
        Elements[transmission_element_index].Text = str;
    }

    int highlightedPortrait = -1;
    public void highlightPortrait(int slot) throws Exception {
        if (highlightedPortrait != -1)
            unhighlightPortrait(highlightedPortrait);
         
        hframes[slot].Visible = true;
        frames[slot].Visible = false;
        highlightedPortrait = slot;
        portraits[highlightedPortrait].Dim(0);
    }

    public void unhighlightPortrait(int slot) throws Exception {
        if (portraits[slot].Visible)
        {
            hframes[slot].Visible = false;
            frames[slot].Visible = true;
            portraits[highlightedPortrait].Dim(100);
        }
         
    }

    public void showPortrait(int unit, int slot) throws Exception {
        /*portrait_resource*/
        Console.WriteLine("showing portrait {0} (unit {1}, portrait index {2}) in slot {3}", "", unit, GlobalResources.getInstance().getPortDataDat().getPortraitIndexes().get___idx(unit), slot);
        uint portraitIndex = GlobalResources.getInstance().getUnitsDat().getPortraits().get___idx(unit);
        String portrait_resource = String.Format("portrait\\{0}0.smk", GlobalResources.getInstance().getPortDataTbl().get___idx((int)GlobalResources.getInstance().getPortDataDat().getPortraitIndexes().get___idx((int)portraitIndex)));
        portraits[slot].Player = new SmackerPlayer((Stream)getMpq().getResource(portrait_resource),1);
        portraits[slot].Dim(100);
        portraits[slot].Play();
        portraits[slot].Visible = true;
        hframes[slot].Visible = false;
        frames[slot].Visible = true;
    }

    public void hidePortrait(int slot) throws Exception {
        portraits[slot].Visible = false;
        hframes[slot].Visible = false;
        frames[slot].Visible = true;
        portraits[slot].Stop();
    }

    public static ReadyRoomScreen create(Mpq mpq, String scenario_prefix) throws Exception {
        switch(Game.getInstance().getRace())
        {
            case Terran: 
                return new TerranReadyRoomScreen(mpq,scenario_prefix);
            case Protoss: 
                return new ProtossReadyRoomScreen(mpq,scenario_prefix);
            case Zerg: 
                return new ZergReadyRoomScreen(mpq,scenario_prefix);
            default: 
                return null;
        
        }
    }

}


