//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:12
//

package SCSharpMac.UI;

import SCSharp.Mpq;
import SCSharp.Tbl;
import SCSharpMac.UI.Builtins;
import SCSharpMac.UI.DialogEvent;
import SCSharpMac.UI.ListBoxElement;
import SCSharpMac.UI.UIDialog;
import SCSharpMac.UI.UIScreen;

public class KeystrokeDialog  extends UIDialog 
{
    public KeystrokeDialog(UIScreen parent, Mpq mpq) throws Exception {
        super(parent, mpq, "glue\\Palmm", Builtins.rez_HelpBin);
        background_path = null;
    }

    static final int OK_ELEMENT_INDEX = 1;
    static final int HELPLIST_ELEMENT_INDEX = 2;
    protected void resourceLoader() throws Exception {
        super.resourceLoader();
        for (int i = 0;i < Elements.Count;i++)
            Console.WriteLine("{0}: {1} '{2}'", i, Elements[i].Type, Elements[i].Text);
        Elements[OK_ELEMENT_INDEX].Activate += ;
        ListBoxElement list = (ListBoxElement)Elements[HELPLIST_ELEMENT_INDEX];
        Tbl help_txt = (Tbl)mpq.getResource(Builtins.rez_HelpTxtTbl);
        for (int i = 0;i < help_txt.getStrings().Length;i++)
        {
            list.AddItem(help_txt.getStrings()[i]);
        }
    }

    public DialogEvent Ok;
}


