//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:13
//

package SCSharpMac.UI;

import CS2JNet.JavaSupport.util.ListSupport;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import SCSharpMac.UI.GuiUtil;
import SCSharpMac.UI.ListBoxElement;

public class ListBoxElementLayerDelegate  extends CALayerDelegate 
{
    ListBoxElement el;
    public ListBoxElementLayerDelegate(ListBoxElement el) throws Exception {
        this.el = el;
    }

    public void drawLayer(CALayer layer, CGContext context) throws Exception {
        if (el.getItems() != null)
        {
            int y = el.getBounds().Height - el.getFont().getLineSize();
            for (int i = el.getFirstVisible();i < el.getFirstVisible() + el.getNumVisible();i++)
            {
                if (i >= el.getItems().Count)
                    return ;
                 
                GuiUtil.RenderTextToContext(context, new PointF(0, y), el.getItems()[i], el.getFont(), el.getPalette(), 4);
                y -= el.getFont().getLineSize();
            }
        }
         
    }

}


