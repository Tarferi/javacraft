//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:12
//

package SCSharpMac.UI;

import CS2JNet.JavaSupport.language.RefSupport;
import CS2JNet.JavaSupport.util.ListSupport;
import CS2JNet.System.StringSupport;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import SCSharp.Mpq;
import SCSharp.MpqArchive;
import SCSharp.MpqContainer;
import SCSharp.MpqDirectory;
import SCSharpMac.UI.__MultiReadyDelegate;
import SCSharpMac.UI.ConnectionScreen;
import SCSharpMac.UI.CursorAnimator;
import SCSharpMac.UI.Game;
import SCSharpMac.UI.GlobalResources;
import SCSharpMac.UI.LoginScreen;
import SCSharpMac.UI.MainMenu;
import SCSharpMac.UI.Race;
import SCSharpMac.UI.ReadyDelegate;
import SCSharpMac.UI.TickEventArgs;
import SCSharpMac.UI.TickEventHandler;
import SCSharpMac.UI.TitleScreen;
import SCSharpMac.UI.UIScreen;
import SCSharpMac.UI.UIScreenType;

public class Game  extends NSView 
{
    UIScreen[] screens = new UIScreen[]();
    boolean isBroodWar = new boolean();
    boolean playingBroodWar = new boolean();
    Race race = Race.Zerg;
    Mpq patchRtMpq;
    Mpq broodatMpq;
    Mpq stardatMpq;
    Mpq bwInstallExe;
    Mpq scInstallExe;
    MpqContainer installedMpq;
    MpqContainer playingMpq;
    int cached_cursor_x = new int();
    int cached_cursor_y = new int();
    String rootDir = new String();
    static Game instance;
    public static Game getInstance() throws Exception {
        return instance;
    }

    public Game(String starcraftDir, String scCDDir, String bwCDDir) throws Exception {
        instance = this;
        Layer = CALayer.Create();
        Layer.BackgroundColor = new CGColor(0, 0, 0, 1);
        WantsLayer = true;
        starcraftDir = starcraftDir.Replace('\\', Path.DirectorySeparatorChar).Replace('/', Path.DirectorySeparatorChar);
        scCDDir = scCDDir.Replace('\\', Path.DirectorySeparatorChar).Replace('/', Path.DirectorySeparatorChar);
        bwCDDir = bwCDDir.Replace('\\', Path.DirectorySeparatorChar).Replace('/', Path.DirectorySeparatorChar);
        screens = new UIScreen[((Enum)UIScreenType.ScreenCount).ordinal()];
        installedMpq = new MpqContainer();
        playingMpq = new MpqContainer();
        Mpq scMpq = null, bwMpq = null;
        if (starcraftDir != null)
        {
            for (Object __dummyForeachVar0 : Directory.GetFileSystemEntries(starcraftDir))
            {
                String path = (String)__dummyForeachVar0;
                if (StringSupport.equals(Path.GetFileName(path).ToLower(), "broodat.mpq") || Path.GetFileName(path).Equals("Brood War Data"))
                {
                    if (broodatMpq != null)
                        throw new Exception("You have multiple broodat.mpq files in your starcraft directory.");
                     
                    try
                    {
                        bwMpq = getMpq(path);
                        Console.WriteLine("found BrooDat.mpq");
                    }
                    catch (Exception e)
                    {
                        throw new Exception(String.Format("Could not read mpq archive {0}", path), e);
                    }
                
                }
                else if (StringSupport.equals(Path.GetFileName(path).ToLower(), "stardat.mpq") || Path.GetFileName(path).Equals("Starcraft Data"))
                {
                    if (stardatMpq != null)
                        throw new Exception("You have multiple stardat.mpq files in your starcraft directory.");
                     
                    try
                    {
                        scMpq = getMpq(path);
                        Console.WriteLine("found StarDat.mpq");
                    }
                    catch (Exception e)
                    {
                        throw new Exception(String.Format("could not read mpq archive {0}", path), e);
                    }
                
                }
                else if (StringSupport.equals(Path.GetFileName(path).ToLower(), "patch_rt.mpq") || Path.GetFileName(path).Equals("Starcraft Mac Patch"))
                {
                    if (patchRtMpq != null)
                        throw new Exception("You have multiple patch_rt.mpq files in your starcraft directory.");
                     
                    try
                    {
                        patchRtMpq = getMpq(path);
                        Console.WriteLine("found patch_rt.mpq");
                    }
                    catch (Exception e)
                    {
                        throw new Exception(String.Format("could not read mpq archive {0}", path), e);
                    }
                
                }
                else if (StringSupport.equals(Path.GetFileName(path).ToLower(), "starcraft.mpq"))
                {
                    try
                    {
                        scInstallExe = getMpq(path);
                        Console.WriteLine("found starcraft.mpq");
                    }
                    catch (Exception e)
                    {
                        throw new Exception(String.Format("could not read mpq archive {0}", path), e);
                    }
                
                }
                else if (StringSupport.equals(Path.GetFileName(path).ToLower(), "broodwar.mpq"))
                {
                    try
                    {
                        bwInstallExe = getMpq(path);
                        Console.WriteLine("found broodwar.mpq");
                    }
                    catch (Exception e)
                    {
                        throw new Exception(String.Format("could not read mpq archive {0}", path), e);
                    }
                
                }
                     
            }
        }
         
        if (scMpq == null)
        {
            throw new Exception("unable to locate stardat.mpq, please check your StarcraftDirectory configuration setting");
        }
         
        if (!String.IsNullOrEmpty(scCDDir))
        {
            for (Object __dummyForeachVar1 : Directory.GetFileSystemEntries(scCDDir))
            {
                String path = (String)__dummyForeachVar1;
                if (StringSupport.equals(Path.GetFileName(path).ToLower(), "install.exe") || Path.GetFileName(path).Equals("Starcraft Archive"))
                {
                    try
                    {
                        scInstallExe = getMpq(path);
                        Console.WriteLine("found SC install.exe");
                    }
                    catch (Exception e)
                    {
                        throw new Exception(String.Format("could not read mpq archive {0}", path), e);
                    }
                
                }
                 
            }
        }
         
        if (!String.IsNullOrEmpty(bwCDDir))
        {
            for (Object __dummyForeachVar2 : Directory.GetFileSystemEntries(bwCDDir))
            {
                String path = (String)__dummyForeachVar2;
                if (StringSupport.equals(Path.GetFileName(path).ToLower(), "install.exe") || Path.GetFileName(path).Equals("Brood War Archive"))
                {
                    try
                    {
                        bwInstallExe = getMpq(path);
                        Console.WriteLine("found BW install.exe");
                    }
                    catch (Exception e)
                    {
                        throw new Exception(String.Format("could not read mpq archive {0}", path), e);
                    }
                
                }
                 
            }
        }
         
        if (bwInstallExe == null)
            throw new Exception("unable to locate broodwar cd's install.exe, please check your BroodwarCDDirectory configuration setting");
         
        if (bwMpq != null)
        {
            if (patchRtMpq != null)
            {
                broodatMpq = new MpqContainer();
                ((MpqContainer)broodatMpq).add(patchRtMpq);
                ((MpqContainer)broodatMpq).add(bwMpq);
            }
            else
                broodatMpq = bwMpq; 
        }
         
        if (scMpq != null)
        {
            if (patchRtMpq != null)
            {
                stardatMpq = new MpqContainer();
                ((MpqContainer)stardatMpq).add(patchRtMpq);
                ((MpqContainer)stardatMpq).add(scMpq);
            }
            else
                stardatMpq = bwMpq; 
        }
         
        if (broodatMpq != null)
            installedMpq.add(broodatMpq);
         
        if (bwInstallExe != null)
            installedMpq.add(bwInstallExe);
         
        if (stardatMpq != null)
            installedMpq.add(stardatMpq);
         
        if (scInstallExe != null)
            installedMpq.add(scInstallExe);
         
        setPlayingBroodWar(isBroodWar = (broodatMpq != null));
        this.rootDir = starcraftDir;
    }

    Mpq getMpq(String path) throws Exception {
        if (Directory.Exists(path))
            return new MpqDirectory(path);
        else if (File.Exists(path))
            return new MpqArchive(path);
        else
            return null;  
    }

    public String getRootDirectory() throws Exception {
        return rootDir;
    }

    public boolean getPlayingBroodWar() throws Exception {
        return playingBroodWar;
    }

    public void setPlayingBroodWar(boolean value) throws Exception {
        playingBroodWar = value;
        playingMpq.clear();
        if (playingBroodWar)
        {
            if (bwInstallExe == null)
                throw new Exception("you need the Broodwar CD to play Broodwar games.  Please check the BroodwarCDDirectory configuration setting.");
             
            if (patchRtMpq != null)
                playingMpq.add(patchRtMpq);
             
            playingMpq.add(bwInstallExe);
            playingMpq.add(broodatMpq);
            playingMpq.add(stardatMpq);
        }
        else
        {
            if (scInstallExe == null)
                throw new Exception("you need the Starcraft CD to play original games.  Please check the StarcraftCDDirectory configuration setting.");
             
            if (patchRtMpq != null)
                playingMpq.add(patchRtMpq);
             
            playingMpq.add(scInstallExe);
            playingMpq.add(stardatMpq);
        } 
    }

    public boolean getIsBroodWar() throws Exception {
        return isBroodWar;
    }

    long lastVideoMillis = new long();
    void emitTick() throws Exception {
        if (lastVideoMillis == 0L)
        {
            // we don't actually emit on this, we just prepare for the next video update
            lastVideoMillis = tickStopWatch.ElapsedMilliseconds;
            return ;
        }
         
        TickEventHandler h = Tick;
        if (h != null)
            h.invoke(this,new TickEventArgs(tickStopWatch.ElapsedMilliseconds - lastVideoMillis));
         
        lastVideoMillis = tickStopWatch.ElapsedMilliseconds;
    }

    public TickEventHandler Tick;
    public void mouseDown(NSEvent theEvent) throws Exception {
        Console.WriteLine("MouseDown");
        cached_cursor_x = (int)theEvent.LocationInWindow.X;
        cached_cursor_y = (int)theEvent.LocationInWindow.Y;
        if (cursor != null)
            cursor.setPosition(cached_cursor_x,cached_cursor_y);
         
        if (currentScreen != null)
            currentScreen.handleMouseButtonDown(theEvent);
         
    }

    public void mouseUp(NSEvent theEvent) throws Exception {
        Console.WriteLine("MouseUp at {0} {1}", theEvent.LocationInWindow.X, theEvent.LocationInWindow.Y);
        cached_cursor_x = (int)theEvent.LocationInWindow.X;
        cached_cursor_y = (int)theEvent.LocationInWindow.Y;
        if (cursor != null)
            cursor.setPosition(cached_cursor_x,cached_cursor_y);
         
        if (currentScreen != null)
            currentScreen.handleMouseButtonUp(theEvent);
         
    }

    void mouseMotionEvent(NSEvent theEvent) throws Exception {
        cached_cursor_x = (int)theEvent.LocationInWindow.X;
        cached_cursor_y = (int)theEvent.LocationInWindow.Y;
        if (cursor != null)
            cursor.setPosition(cached_cursor_x,cached_cursor_y);
         
        if (currentScreen != null)
            currentScreen.handlePointerMotion(theEvent);
         
    }

    public void mouseDragged(NSEvent theEvent) throws Exception {
        mouseMotionEvent(theEvent);
    }

    public void mouseMoved(NSEvent theEvent) throws Exception {
        mouseMotionEvent(theEvent);
    }

    public boolean acceptsFirstResponder() throws Exception {
        return true;
    }

    public void keyDown(NSEvent theEvent) throws Exception {
        if (currentScreen != null)
            currentScreen.handleKeyboardDown(theEvent);
         
    }

    public void keyUp(NSEvent theEvent) throws Exception {
        if (currentScreen != null)
            currentScreen.handleKeyboardUp(theEvent);
         
    }

    Stopwatch tickStopWatch = new Stopwatch();
    public void startup() throws Exception {
        displayTitle();
        displayLink = new CVDisplayLink();
        displayLink.SetOutputCallback(DisplayLinkOutputCallback);
        tickStopWatch = new Stopwatch();
        tickStopWatch.Start();
        displayLink.Start();
    }

    CVDisplayLink displayLink = new CVDisplayLink();
    CVReturn displayLinkOutputCallback(CVDisplayLink displayLink, RefSupport<CVTimeStamp> inNow, RefSupport<CVTimeStamp> inOutputTime, CVOptionFlags flagsIn, RefSupport<CVOptionFlags> flagsOut) throws Exception {
        Game.getInstance().BeginInvokeOnMainThread(/* [UNSUPPORTED] to translate lambda expressions we need an explicit delegate type, try adding a cast "() => {
            Game.getInstance().emitTick();
        }" */);
        return CVReturn.Success;
    }

    public void quit() throws Exception {
        Environment.Exit(0);
    }

    void displayTitle() throws Exception {
        /* create the title screen, and make sure we
        			   don't start loading anything else until
        			   it's on the screen */
        UIScreen screen = new TitleScreen(installedMpq);
        switchToScreen(screen);
        titleScreenReady();
    }

    void createWindow(boolean fullscreen) throws Exception {
    }

    public Race getRace() throws Exception {
        return race;
    }

    public void setRace(Race value) throws Exception {
        race = value;
    }

    CursorAnimator cursor;
    public CursorAnimator getCursor() throws Exception {
        return cursor;
    }

    public void setCursor(CursorAnimator value) throws Exception {
        if (cursor == value)
            return ;
         
        if (cursor != null)
        {
            cursor.removeFromPainter();
            cursor.RemoveFromSuperLayer();
        }
         
        cursor = value;
        if (cursor != null)
        {
            cursor.addToPainter();
            cursor.setPosition(cached_cursor_x,cached_cursor_y);
            Layer.AddSublayer(cursor);
        }
         
    }

    UIScreen currentScreen;
    public void setGameScreen(UIScreen screen) throws Exception {
        if (currentScreen != null)
        {
            currentScreen.removeFromPainter();
            currentScreen.RemoveFromSuperLayer();
        }
         
        currentScreen = screen;
        if (currentScreen != null)
        {
            Layer.AddSublayer(currentScreen);
            currentScreen.addToPainter();
        }
         
    }

    UIScreen screenToSwitchTo;
    public void switchToScreen(UIScreen screen) throws Exception {
        screen.Ready = __MultiReadyDelegate.combine(screen.Ready,new ReadyDelegate() 
          { 
            public System.Void invoke() throws Exception {
                switchReady();
            }

            public List<ReadyDelegate> getInvocationList() throws Exception {
                List<ReadyDelegate> ret = new ArrayList<ReadyDelegate>();
                ret.add(this);
                return ret;
            }
        
          });
        screenToSwitchTo = screen;
        screenToSwitchTo.load();
        return ;
    }

    public void switchToScreen(UIScreenType screenType) throws Exception {
        int index = ((Enum)screenType).ordinal();
        if (screens[index] == null)
        {
            switch(screenType)
            {
                case MainMenu: 
                    screens[index] = new MainMenu(installedMpq);
                    break;
                case Login: 
                    screens[index] = new LoginScreen(playingMpq);
                    break;
                case Connection: 
                    screens[index] = new ConnectionScreen(playingMpq);
                    break;
                default: 
                    throw new Exception();
            
            }
        }
         
        SwitchToScreen(screens[((Enum)screenType).ordinal()]);
    }

    public Mpq getPlayingMpq() throws Exception {
        return playingMpq;
    }

    public Mpq getInstalledMpq() throws Exception {
        return installedMpq;
    }

    void switchReady() throws Exception {
        screenToSwitchTo.Ready = __MultiReadyDelegate.remove(screenToSwitchTo.Ready,new ReadyDelegate() 
          { 
            public System.Void invoke() throws Exception {
                switchReady();
            }

            public List<ReadyDelegate> getInvocationList() throws Exception {
                List<ReadyDelegate> ret = new ArrayList<ReadyDelegate>();
                ret.add(this);
                return ret;
            }
        
          });
        setGameScreen(screenToSwitchTo);
        screenToSwitchTo = null;
    }

    void globalResourcesLoaded() throws Exception {
        Console.WriteLine("GlobalResourcesLoaded");
        switchToScreen(UIScreenType.MainMenu);
    }

    void titleScreenReady() throws Exception {
        Console.WriteLine("Loading global resources");
        new GlobalResources(stardatMpq,broodatMpq);
        GlobalResources.getInstance().Ready = __MultiReadyDelegate.combine(GlobalResources.getInstance().Ready,new ReadyDelegate() 
          { 
            public System.Void invoke() throws Exception {
                globalResourcesLoaded();
            }

            public List<ReadyDelegate> getInvocationList() throws Exception {
                List<ReadyDelegate> ret = new ArrayList<ReadyDelegate>();
                ret.add(this);
                return ret;
            }
        
          });
        GlobalResources.getInstance().load();
    }

}


