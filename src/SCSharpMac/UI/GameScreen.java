//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:12
//

package SCSharpMac.UI;

import java.util.ArrayList;
import java.util.List;
import SCSharp.Got;
import SCSharp.Grp;
import SCSharp.Chk;
import SCSharp.InitialUnits;
import SCSharp.Mpq;
import SCSharp.Tbl;
import SCSharp.UnitInfo;
import SCSharp.Util;
import SCSharpMac.UI.__MultiDialogEvent;
import SCSharpMac.UI.__MultiTickEventHandler;
import SCSharpMac.UI.Builtins;
import SCSharpMac.UI.CursorAnimator;
import SCSharpMac.UI.Game;
import SCSharpMac.UI.GameMenuDialog;
import SCSharpMac.UI.GrpButtonElement;
import SCSharpMac.UI.GrpElement;
import SCSharpMac.UI.GuiUtil;
import SCSharpMac.UI.ImageElement;
import SCSharpMac.UI.LabelElement;
import SCSharpMac.UI.MapRenderer;
import SCSharpMac.UI.MovieElement;
import SCSharpMac.UI.Pcx;
import SCSharpMac.UI.SpriteManager;
import SCSharpMac.UI.TickEventArgs;
import SCSharpMac.UI.UIScreen;
import SCSharpMac.UI.Unit;

//
// SCSharp.UI.GameScreen
//
// Authors:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
public class GameScreen  extends UIScreen 
{
    public enum HudLabels
    {
        UnitName,
        ResourceUsed,
        ResourceProvided,
        ResourceTotal,
        ResourceMax,
        Count
    }
    //Mpq scenario_mpq;
    Chk scenario;
    Got template;
    /* the deltas associated with scrolling */
    int horiz_delta = new int();
    int vert_delta = new int();
    /* the x,y of the topleft of the screen */
    int topleft_x = new int(), topleft_y = new int();
    /* magic number alert.. */
    static final int MINIMAP_X = 7;
    static final int MINIMAP_Y = 349;
    static final int MINIMAP_WIDTH = 125;
    static final int MINIMAP_HEIGHT = 125;
    static final int SCROLL_DELTA = 15;
    static final int MOUSE_MOVE_BORDER = 10;
    static final int SCROLL_CURSOR_UL = 0;
    static final int SCROLL_CURSOR_U = 1;
    static final int SCROLL_CURSOR_UR = 2;
    static final int SCROLL_CURSOR_R = 3;
    static final int SCROLL_CURSOR_DR = 4;
    static final int SCROLL_CURSOR_D = 5;
    static final int SCROLL_CURSOR_DL = 6;
    static final int SCROLL_CURSOR_L = 7;
    CursorAnimator[] ScrollCursors = new CursorAnimator[]();
    static final int TARG_CURSOR_G = 0;
    static final int TARG_CURSOR_Y = 1;
    static final int TARG_CURSOR_R = 2;
    CursorAnimator[] TargetCursors = new CursorAnimator[]();
    static final int MAG_CURSOR_G = 0;
    static final int MAG_CURSOR_Y = 1;
    static final int MAG_CURSOR_R = 2;
    CursorAnimator[] MagCursors = new CursorAnimator[]();
    Tbl statTxt;
    Grp wireframe;
    Grp cmdicons;
    byte[] cmdicon_palette = new byte[]();
    //byte[] unit_palette;
    byte[] tileset_palette = new byte[]();
    //		Player[] players;
    List<Unit> units = new List<Unit>();
    ImageElement hudElement;
    GrpElement wireframeElement;
    MovieElement portraitElement;
    MapRenderer mapRenderer;
    GrpButtonElement[] cmdButtonElements = new GrpButtonElement[]();
    LabelElement[] labelElements = new LabelElement[]();
    public GameScreen(Mpq mpq, Mpq scenario_mpq, Chk scenario, Got template) throws Exception {
        super(mpq);
        effectpal_path = "game\\tblink.pcx";
        arrowgrp_path = "cursor\\arrow.grp";
        fontpal_path = "game\\tfontgam.pcx";
        //this.scenario_mpq = scenario_mpq;
        this.scenario = scenario;
        this.template = template;
        ScrollCursors = new CursorAnimator[8];
    }

    public GameScreen(Mpq mpq, String prefix, Chk scenario) throws Exception {
        super(mpq);
        this.effectpal_path = "game\\tblink.pcx";
        this.arrowgrp_path = "cursor\\arrow.grp";
        this.fontpal_path = "game\\tfontgam.pcx";
        //this.scenario_mpq = scenario_mpq;
        this.scenario = scenario;
        ScrollCursors = new CursorAnimator[8];
    }

    // FIXME: we need the starfield!
    ushort[] button_xs = new ushort[]{ 506, 552, 598 };
    ushort[] button_ys = new ushort[]{ 360, 400, 440 };
    public void addToPainter() throws Exception {
        super.addToPainter();
        InsertSublayerBelow(mapRenderer.getMapLayer(), hudElement.getLayer());
        Game.getInstance().Tick = __MultiTickEventHandler.combine(Game.getInstance().Tick,new TickEventHandler() 
          { 
            public System.Void invoke(System.Object sender, TickEventArgs args) throws Exception {
                scrollTick(sender, args);
            }

            public List<TickEventHandler> getInvocationList() throws Exception {
                List<TickEventHandler> ret = new ArrayList<TickEventHandler>();
                ret.add(this);
                return ret;
            }
        
          });
    }

    //			Painter.Add (Layer.Hud, PaintMinimap);
    //			if (scenario.Tileset == Tileset.Platform)
    //				Painter.Add (Layer.Background, PaintStarfield);
    //			SpriteManager.AddToPainter ();
    public void removeFromPainter() throws Exception {
        super.removeFromPainter();
    }

    //			Painter.Remove (Layer.Hud, PaintMinimap);
    //			if (scenario.Tileset == Tileset.Platform)
    //				Painter.Remove (Layer.Background, PaintStarfield);
    //			SpriteManager.RemoveFromPainter ();
    protected void resourceLoader() throws Exception {
        super.resourceLoader();
        /* create the element corresponding to the hud */
        hudElement = new ImageElement(this, 0, 0, 640, 480, getTranslucentIndex());
        hudElement.setPalette(fontpal.getRGBData());
        hudElement.setText(String.Format(Builtins.Game_ConsolePcx, Util.RaceCharLower[((Enum)Game.getInstance().getRace()).ordinal()]));
        hudElement.setVisible(true);
        Elements.Add(hudElement);
        /* create the portrait playing area */
        portraitElement = new MovieElement(this,415,415,48,48,false);
        portraitElement.setVisible(true);
        Elements.Add(portraitElement);
        Pcx pcx = new Pcx();
        pcx.readFromStream((Stream)mpq.getResource("game\\tunit.pcx"),-1,-1);
        //unit_palette = pcx.Palette;
        pcx = new Pcx();
        pcx.readFromStream((Stream)mpq.getResource("tileset\\badlands\\dark.pcx"),0,0);
        tileset_palette = pcx.getPalette();
        mapRenderer = new MapRenderer(mpq,scenario,640,480);
        /*Painter.SCREEN_RES_X*/
        /*Painter.SCREEN_RES_Y*/
        mapRenderer.getMapLayer().AnchorPoint = new PointF(0, 0);
        /* the following could be made global to speed up the entry to the game screen.. */
        statTxt = (Tbl)mpq.getResource("rez\\stat_txt.tbl");
        // load the wireframe image info
        wireframe = (Grp)mpq.getResource("unit\\wirefram\\wirefram.grp");
        // load the command icons
        cmdicons = (Grp)mpq.getResource("unit\\cmdbtns\\cmdicons.grp");
        pcx = new Pcx();
        pcx.readFromStream((Stream)mpq.getResource("unit\\cmdbtns\\ticon.pcx"),0,0);
        cmdicon_palette = pcx.getPalette();
        // create the wireframe display element
        wireframeElement = new GrpElement(this, wireframe, cmdicon_palette, 170, 390);
        wireframeElement.setVisible(false);
        Elements.Add(wireframeElement);
        labelElements = new LabelElement[((Enum)HudLabels.Count).ordinal()];
        labelElements[((Enum)HudLabels.UnitName).ordinal()] = new LabelElement(this, fontpal.getPalette(), GuiUtil.getFonts(getMpq())[1], 254, 390);
        labelElements[((Enum)HudLabels.ResourceUsed).ordinal()] = new LabelElement(this, fontpal.getPalette(), GuiUtil.getFonts(getMpq())[0], 292, 420);
        labelElements[((Enum)HudLabels.ResourceProvided).ordinal()] = new LabelElement(this, fontpal.getPalette(), GuiUtil.getFonts(getMpq())[0], 292, 434);
        labelElements[((Enum)HudLabels.ResourceTotal).ordinal()] = new LabelElement(this, fontpal.getPalette(), GuiUtil.getFonts(getMpq())[0], 292, 448);
        labelElements[((Enum)HudLabels.ResourceMax).ordinal()] = new LabelElement(this, fontpal.getPalette(), GuiUtil.getFonts(getMpq())[0], 292, 462);
        for (int i = 0;i < labelElements.Length;i++)
            Elements.Add(labelElements[i]);
        cmdButtonElements = new GrpButtonElement[9];
        int x = 0;
        int y = 0;
        for (int i = 0;i < cmdButtonElements.Length;i++)
        {
            cmdButtonElements[i] = new GrpButtonElement(this, cmdicons, cmdicon_palette, button_xs[x], button_ys[y]);
            x++;
            if (x == 3)
            {
                x = 0;
                y++;
            }
             
            cmdButtonElements[i].Visible = false;
            Elements.Add(cmdButtonElements[i]);
        }
        placeInitialUnits();
    }

    void clipTopLeft() throws Exception {
        if (topleft_x < 0)
            topleft_x = 0;
         
        if (topleft_y < 0)
            topleft_y = 0;
         
    }

    //			if (topleft_x > mapRenderer.MapWidth - Painter.SCREEN_RES_X) topleft_x = mapRenderer.MapWidth - Painter.SCREEN_RES_X;
    //			if (topleft_y > mapRenderer.MapHeight - Painter.SCREEN_RES_Y) topleft_y = mapRenderer.MapHeight - Painter.SCREEN_RES_Y;
    void updateCursor() throws Exception {
    }

    int scroll_elapsed = new int();
    public void scrollTick(Object sender, TickEventArgs e) throws Exception {
        scroll_elapsed += (int)e.getMillisecondsElapsed();
        if (scroll_elapsed < 6)
            return ;
         
        /*XXX*/
        scroll_elapsed = 0;
        if (horiz_delta == 0 && vert_delta == 0)
            return ;
         
        int old_topleft_x = topleft_x;
        int old_topleft_y = topleft_y;
        topleft_x += horiz_delta;
        topleft_y += vert_delta;
        clipTopLeft();
        if (old_topleft_x == topleft_x && old_topleft_y == topleft_y)
            return ;
         
        SpriteManager.setUpperLeft(topleft_x,topleft_y);
        mapRenderer.setUpperLeft(topleft_x,topleft_y);
        updateCursor();
    }

    boolean buttonDownInMinimap = new boolean();
    Unit unitUnderCursor;
    Unit selectedUnit;
    void recenter(int x, int y) throws Exception {
        topleft_x = (int)(x - Bounds.Width / 2);
        topleft_y = (int)(y - Bounds.Height / 2);
        clipTopLeft();
        SpriteManager.setUpperLeft(topleft_x,topleft_y);
        mapRenderer.setUpperLeft(topleft_x,topleft_y);
        updateCursor();
    }

    //			Painter.Invalidate (new Rectangle (new Point (0,0),
    //							   new Size (Painter.SCREEN_RES_X, Painter.SCREEN_RES_Y)));
    void recenterFromMinimap(int x, int y) throws Exception {
        int map_x = (int)((float)(x - MINIMAP_X) / MINIMAP_WIDTH * mapRenderer.getMapWidth());
        int map_y = (int)((float)(y - MINIMAP_Y) / MINIMAP_HEIGHT * mapRenderer.getMapHeight());
        recenter(map_x,map_y);
    }

    public void keyboardUp(NSEvent theEvent) throws Exception {
        char key = theEvent.CharactersIgnoringModifiers[0];
        if (key == (char)NSKey.RightArrow || key == (char)NSKey.LeftArrow)
        {
            horiz_delta = 0;
        }
        else if (key == (char)NSKey.UpArrow || key == (char)NSKey.DownArrow)
        {
            vert_delta = 0;
        }
          
    }

    public void keyboardDown(NSEvent theEvent) throws Exception {
        CharactersIgnoringModifiers.INDEXER __dummyScrutVar0 = theEvent.CharactersIgnoringModifiers[0];
        if (__dummyScrutVar0.equals((char)27))
        {
            /*Key.F10:*/
            GameMenuDialog d = new GameMenuDialog(this,mpq);
            d.ReturnToGame = __MultiDialogEvent.combine(d.ReturnToGame,new DialogEvent() 
              { 
                public System.Void invoke() throws Exception {
                    dismissDialog();
                }

                public List<DialogEvent> getInvocationList() throws Exception {
                    List<DialogEvent> ret = new ArrayList<DialogEvent>();
                    ret.add(this);
                    return ret;
                }
            
              });
            showDialog(d);
        }
        else if (__dummyScrutVar0.equals((char)NSKey.RightArrow))
        {
            horiz_delta = SCROLL_DELTA;
        }
        else if (__dummyScrutVar0.equals((char)NSKey.LeftArrow))
        {
            horiz_delta = -SCROLL_DELTA;
        }
        else if (__dummyScrutVar0.equals((char)NSKey.DownArrow))
        {
            vert_delta = SCROLL_DELTA;
        }
        else if (__dummyScrutVar0.equals((char)NSKey.UpArrow))
        {
            vert_delta = -SCROLL_DELTA;
        }
             
    }

    void placeInitialUnits() throws Exception {
        List<UnitInfo> unit_infos = scenario.getUnits();
        List<Unit> startLocations = new List<Unit>();
        units = new List<Unit>();
        for (Object __dummyForeachVar0 : unit_infos)
        {
            UnitInfo unitinfo = (UnitInfo)__dummyForeachVar0;
            if (unitinfo.unit_id == 0xffff)
                break;
             
            Unit unit = new Unit(unitinfo);
            /* we handle start locations in a special way, below */
            if (unit.getFlingyId() == 140)
            {
                startLocations.Add(unit);
                continue;
            }
             
            //players[unitinfo.player].AddUnit (unit);
            unit.createSprite(mpq,tileset_palette);
            units.Add(unit);
        }
        if (template != null && (template.getInitialUnits() != InitialUnits.UseMapSettings))
        {
            for (Object __dummyForeachVar1 : startLocations)
            {
                Unit sl = (Unit)__dummyForeachVar1;
                /* terran command center = 106,
                					   zerg hatchery = 131,
                					   protoss nexus = 154 */
                Unit unit = new Unit(154);
                unit.setX(sl.getX());
                unit.setY(sl.getY());
                unit.createSprite(mpq,tileset_palette);
                units.Add(unit);
            }
        }
         
        /* for now assume the player is at startLocations[0], and center the view there */
        Recenter(startLocations[0].X, startLocations[0].Y);
    }

}


