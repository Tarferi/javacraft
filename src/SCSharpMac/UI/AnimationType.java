//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:15
//

package SCSharpMac.UI;


public enum AnimationType
{
    Init,
    Death,
    GndAttkInit,
    AirAttkInit,
    SpAbility1,
    GndAttkRpt,
    AirAttkRpt,
    SpAbility2,
    GndAttkToIdle,
    AirAttkToIdle,
    SpAbility3,
    Walking,
    WalkingToIdle,
    BurrowInit,
    ConstrctHarvst,
    IsWorking,
    Landing,
    LiftOff,
    Unknown18,
    Unknown19,
    Unknown20,
    Unknown21,
    Unknown22,
    Unknown23,
    Unknown24,
    Burrow,
    UnBurrow,
    Unknown27
}

