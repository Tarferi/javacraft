//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:12
//

package SCSharpMac.UI;

import CS2JNet.JavaSupport.util.ListSupport;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import SCSharpMac.UI.__MultiComboBoxSelectionChanged;
import SCSharpMac.UI.ComboBoxSelectionChanged;

public class __MultiComboBoxSelectionChanged   implements ComboBoxSelectionChanged
{
    public void invoke(int selectedIndex) throws Exception {
        IList<ComboBoxSelectionChanged> copy = new IList<ComboBoxSelectionChanged>(), members = this.getInvocationList();
        synchronized (members)
        {
            copy = new LinkedList<ComboBoxSelectionChanged>(members);
        }
        for (Object __dummyForeachVar0 : copy)
        {
            ComboBoxSelectionChanged d = (ComboBoxSelectionChanged)__dummyForeachVar0;
            d.invoke(selectedIndex);
        }
    }

    private System.Collections.Generic.IList<ComboBoxSelectionChanged> _invocationList = new ArrayList<ComboBoxSelectionChanged>();
    public static ComboBoxSelectionChanged combine(ComboBoxSelectionChanged a, ComboBoxSelectionChanged b) throws Exception {
        if (a == null)
            return b;
         
        if (b == null)
            return a;
         
        __MultiComboBoxSelectionChanged ret = new __MultiComboBoxSelectionChanged();
        ret._invocationList = a.getInvocationList();
        ret._invocationList.addAll(b.getInvocationList());
        return ret;
    }

    public static ComboBoxSelectionChanged remove(ComboBoxSelectionChanged a, ComboBoxSelectionChanged b) throws Exception {
        if (a == null || b == null)
            return a;
         
        System.Collections.Generic.IList<ComboBoxSelectionChanged> aInvList = a.getInvocationList();
        System.Collections.Generic.IList<ComboBoxSelectionChanged> newInvList = ListSupport.removeFinalStretch(aInvList, b.getInvocationList());
        if (aInvList == newInvList)
        {
            return a;
        }
        else
        {
            __MultiComboBoxSelectionChanged ret = new __MultiComboBoxSelectionChanged();
            ret._invocationList = newInvList;
            return ret;
        } 
    }

    public System.Collections.Generic.IList<ComboBoxSelectionChanged> getInvocationList() throws Exception {
        return _invocationList;
    }

}


