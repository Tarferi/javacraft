//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:12
//

package SCSharpMac.UI;

import CS2JNet.JavaSupport.util.ListSupport;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import SCSharpMac.UI.ComboBoxElement;
import SCSharpMac.UI.GuiUtil;

public class ComboBoxElementDropdownLayerDelegate  extends CALayerDelegate 
{
    ComboBoxElement el;
    public ComboBoxElementDropdownLayerDelegate(ComboBoxElement el) throws Exception {
        this.el = el;
    }

    public void drawLayer(CALayer layer, CGContext context) throws Exception {
        int y = 0;
        for (int i = el.getItems().Count - 1;i >= 0;i--)
        {
            GuiUtil.RenderTextToContext(context, new PointF(0, y), el.getItems()[i], el.getFont(), el.getPalette(), i == el.getDropdownHoverIndex() ? 4 : 24);
            y += (int)el.getFont().getLineSize();
        }
    }

}


