//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:14
//

package SCSharpMac.UI;

import SCSharp.Mpq;
import SCSharpMac.UI.Game;
import SCSharpMac.UI.MarkupScreen;
import SCSharpMac.UI.ReadyRoomScreen;

public class EstablishingShot  extends MarkupScreen 
{
    String markup_resource = new String();
    String scenario_prefix = new String();
    public EstablishingShot(String markup_resource, String scenario_prefix, Mpq mpq) throws Exception {
        super(mpq);
        this.markup_resource = markup_resource;
        this.scenario_prefix = scenario_prefix;
    }

    protected void loadMarkup() throws Exception {
        addMarkup((Stream)mpq.getResource(markup_resource));
    }

    protected void markupFinished() throws Exception {
        Game.getInstance().switchToScreen(ReadyRoomScreen.create(mpq,scenario_prefix));
    }

}


