//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:13
//

package SCSharpMac.UI;

import java.util.ArrayList;
import java.util.List;
import SCSharp.Mpq;
import SCSharpMac.UI.__MultiDialogEvent;
import SCSharpMac.UI.__MultiListBoxSelectionChanged;
import SCSharpMac.UI.Builtins;
import SCSharpMac.UI.EntryDialog;
import SCSharpMac.UI.Game;
import SCSharpMac.UI.GlobalResources;
import SCSharpMac.UI.ListBoxElement;
import SCSharpMac.UI.OkCancelDialog;
import SCSharpMac.UI.OkDialog;
import SCSharpMac.UI.RaceSelectionScreen;
import SCSharpMac.UI.UIScreen;
import SCSharpMac.UI.UIScreenType;

//
// SCSharp.UI.LoginScreen
//
// Authors:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
public class LoginScreen  extends UIScreen 
{
    public LoginScreen(Mpq mpq) throws Exception {
        super(mpq, "glue\\PalNl", Builtins.rez_GluLoginBin);
    }

    static final int OK_ELEMENT_INDEX = 4;
    static final int CANCEL_ELEMENT_INDEX = 5;
    static final int NEW_ELEMENT_INDEX = 6;
    static final int DELETE_ELEMENT_INDEX = 7;
    static final int LISTBOX_ELEMENT_INDEX = 8;
    ListBoxElement listbox;
    String spcdir = new String();
    String[] files = new String[]();
    void populateUIFromDir() throws Exception {
        files = Directory.GetFiles(spcdir, "*.spc");
        for (int i = 0;i < files.Length;i++)
            listbox.AddItem(Path.GetFileNameWithoutExtension(files[i]));
    }

    protected void resourceLoader() throws Exception {
        super.resourceLoader();
        for (int i = 0;i < Elements.Count;i++)
            Console.WriteLine("{0}: {1} '{2}'", i, Elements[i].Type, Elements[i].Text);
        Elements[OK_ELEMENT_INDEX].Sensitive = false;
        Elements[OK_ELEMENT_INDEX].Activate += ;
        Elements[CANCEL_ELEMENT_INDEX].Activate += ;
        Elements[NEW_ELEMENT_INDEX].Activate += ;
        Elements[DELETE_ELEMENT_INDEX].Sensitive = false;
        Elements[DELETE_ELEMENT_INDEX].Activate += ;
        listbox = (ListBoxElement)Elements[LISTBOX_ELEMENT_INDEX];
        listbox.SelectionChanged = __MultiListBoxSelectionChanged.combine(listbox.SelectionChanged,new ListBoxSelectionChanged() 
          { 
            public System.Void invoke(int selectedIndex) throws Exception {
                Elements[OK_ELEMENT_INDEX].Sensitive = true;
                Elements[DELETE_ELEMENT_INDEX].Sensitive = true;
            }

            public List<ListBoxSelectionChanged> getInvocationList() throws Exception {
                List<ListBoxSelectionChanged> ret = new ArrayList<ListBoxSelectionChanged>();
                ret.add(this);
                return ret;
            }
        
          });
        spcdir = Path.Combine(Game.getInstance().getRootDirectory(), "characters");
        if (!Directory.Exists(spcdir))
            Directory.CreateDirectory(spcdir);
         
        populateUIFromDir();
    }

    public void keyboardDown(NSEvent theEvent) throws Exception {
        if ((theEvent.ModifierFlags & NSEventModifierMask.NumericPadKeyMask) == NSEventModifierMask.NumericPadKeyMask && (theEvent.Characters[0] == (char)NSKey.UpArrow || theEvent.Characters[0] == (char)NSKey.DownArrow))
            listbox.keyboardDown(theEvent);
        else
            super.keyboardDown(theEvent); 
    }

    void nameAlreadyExists(EntryDialog d) throws Exception {
        OkDialog okd = new OkDialog(d, mpq, GlobalResources.getInstance().getGluAllTbl().getStrings()[24]);
        d.showDialog(okd);
    }

}


