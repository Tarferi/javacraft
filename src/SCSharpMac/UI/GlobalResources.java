//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:12
//

package SCSharpMac.UI;

import SCSharp.FlingyDat;
import SCSharp.ImagesDat;
import SCSharp.IScriptBin;
import SCSharp.MapDataDat;
import SCSharp.Mpq;
import SCSharp.PortDataDat;
import SCSharp.SfxDataDat;
import SCSharp.SpritesDat;
import SCSharp.Tbl;
import SCSharp.UnitsDat;
import SCSharpMac.UI.Builtins;
import SCSharpMac.UI.Game;
import SCSharpMac.UI.GlobalResources;
import SCSharpMac.UI.ReadyDelegate;
import SCSharpMac.UI.Resources;

public class GlobalResources   
{
    Mpq stardatMpq;
    Mpq broodatMpq;
    Resources starcraftResources;
    Resources broodwarResources;
    static GlobalResources instance;
    public static GlobalResources getInstance() throws Exception {
        return instance;
    }

    public GlobalResources(Mpq stardatMpq, Mpq broodatMpq) throws Exception {
        if (instance != null)
            throw new Exception("There can only be one GlobalResources");
         
        this.stardatMpq = stardatMpq;
        this.broodatMpq = broodatMpq;
        starcraftResources = new Resources();
        broodwarResources = new Resources();
        instance = this;
    }

    public void load() throws Exception {
        ThreadPool.QueueUserWorkItem(ResourceLoader);
    }

    public void loadSingleThreaded() throws Exception {
        resourceLoader(null);
    }

    Resources getResources() throws Exception {
        return Game.getInstance().getPlayingBroodWar() ? broodwarResources : starcraftResources;
    }

    public Tbl getImagesTbl() throws Exception {
        return getResources().ImagesTbl;
    }

    public Tbl getSfxDataTbl() throws Exception {
        return getResources().SfxDataTbl;
    }

    public Tbl getSpritesTbl() throws Exception {
        return getResources().SpritesTbl;
    }

    public Tbl getGluAllTbl() throws Exception {
        return getResources().GluAllTbl;
    }

    public ImagesDat getImagesDat() throws Exception {
        return getResources().ImagesDat;
    }

    public SpritesDat getSpritesDat() throws Exception {
        return getResources().SpritesDat;
    }

    public SfxDataDat getSfxDataDat() throws Exception {
        return getResources().SfxDataDat;
    }

    public IScriptBin getIScriptBin() throws Exception {
        return getResources().IscriptBin;
    }

    public UnitsDat getUnitsDat() throws Exception {
        return getResources().UnitsDat;
    }

    public FlingyDat getFlingyDat() throws Exception {
        return getResources().FlingyDat;
    }

    public MapDataDat getMapDataDat() throws Exception {
        return getResources().MapDataDat;
    }

    public PortDataDat getPortDataDat() throws Exception {
        return getResources().PortDataDat;
    }

    public Tbl getMapDataTbl() throws Exception {
        return getResources().MapDataTbl;
    }

    public Tbl getPortDataTbl() throws Exception {
        return getResources().PortDataTbl;
    }

    public Resources getStarDat() throws Exception {
        return starcraftResources;
    }

    public Resources getBrooDat() throws Exception {
        return broodwarResources;
    }

    void resourceLoader(Object state) throws Exception {
        try
        {
            starcraftResources.ImagesTbl = (Tbl)stardatMpq.getResource(Builtins.ImagesTbl);
            starcraftResources.SfxDataTbl = (Tbl)stardatMpq.getResource(Builtins.SfxDataTbl);
            starcraftResources.SpritesTbl = (Tbl)stardatMpq.getResource(Builtins.SpritesTbl);
            starcraftResources.GluAllTbl = (Tbl)stardatMpq.getResource(Builtins.rez_GluAllTbl);
            starcraftResources.MapDataTbl = (Tbl)stardatMpq.getResource(Builtins.MapDataTbl);
            starcraftResources.PortDataTbl = (Tbl)stardatMpq.getResource(Builtins.PortDataTbl);
            starcraftResources.ImagesDat = (ImagesDat)stardatMpq.getResource(Builtins.ImagesDat);
            starcraftResources.SfxDataDat = (SfxDataDat)stardatMpq.getResource(Builtins.SfxDataDat);
            starcraftResources.SpritesDat = (SpritesDat)stardatMpq.getResource(Builtins.SpritesDat);
            starcraftResources.IscriptBin = (IScriptBin)stardatMpq.getResource(Builtins.IScriptBin);
            starcraftResources.UnitsDat = (UnitsDat)stardatMpq.getResource(Builtins.UnitsDat);
            starcraftResources.FlingyDat = (FlingyDat)stardatMpq.getResource(Builtins.FlingyDat);
            starcraftResources.MapDataDat = (MapDataDat)stardatMpq.getResource(Builtins.MapDataDat);
            starcraftResources.PortDataDat = (PortDataDat)stardatMpq.getResource(Builtins.PortDataDat);
            if (broodatMpq != null)
            {
                broodwarResources.ImagesTbl = (Tbl)broodatMpq.getResource(Builtins.ImagesTbl);
                broodwarResources.SfxDataTbl = (Tbl)broodatMpq.getResource(Builtins.SfxDataTbl);
                broodwarResources.SpritesTbl = (Tbl)broodatMpq.getResource(Builtins.SpritesTbl);
                broodwarResources.GluAllTbl = (Tbl)broodatMpq.getResource(Builtins.rez_GluAllTbl);
                broodwarResources.MapDataTbl = (Tbl)broodatMpq.getResource(Builtins.MapDataTbl);
                broodwarResources.PortDataTbl = (Tbl)broodatMpq.getResource(Builtins.PortDataTbl);
                broodwarResources.ImagesDat = (ImagesDat)broodatMpq.getResource(Builtins.ImagesDat);
                broodwarResources.SfxDataDat = (SfxDataDat)broodatMpq.getResource(Builtins.SfxDataDat);
                broodwarResources.SpritesDat = (SpritesDat)broodatMpq.getResource(Builtins.SpritesDat);
                broodwarResources.IscriptBin = (IScriptBin)broodatMpq.getResource(Builtins.IScriptBin);
                broodwarResources.UnitsDat = (UnitsDat)broodatMpq.getResource(Builtins.UnitsDat);
                broodwarResources.FlingyDat = (FlingyDat)broodatMpq.getResource(Builtins.FlingyDat);
                broodwarResources.MapDataDat = (MapDataDat)broodatMpq.getResource(Builtins.MapDataDat);
                broodwarResources.PortDataDat = (PortDataDat)broodatMpq.getResource(Builtins.PortDataDat);
            }
             
            // notify we're ready to roll
            NSApplication.SharedApplication.InvokeOnMainThread(FinishedLoading);
        }
        catch (Exception e)
        {
            Console.WriteLine("Global Resource loader failed: {0}", e);
        }
    
    }

    void finishedLoading() throws Exception {
        if (Ready != null)
            Ready.invoke();
         
    }

    public ReadyDelegate Ready;
}


