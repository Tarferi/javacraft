//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:14
//

package SCSharpMac.UI;

import SCSharpMac.UI.MovieElement;

public class MovieElementLayerDelegate  extends CALayerDelegate 
{
    MovieElement el;
    public MovieElementLayerDelegate(MovieElement el) throws Exception {
        this.el = el;
    }

    public void drawLayer(CALayer layer, CGContext context) throws Exception {
        if (el.getPlayer().getCurrentFrame() != null)
            context.DrawImage(new RectangleF(0, el.getHeight() - el.getPlayer().getCurrentFrame().Height * el.playerZoom, el.getPlayer().getCurrentFrame().Width, el.getPlayer().getCurrentFrame().Height), el.getPlayer().getCurrentFrame());
         
    }

}


