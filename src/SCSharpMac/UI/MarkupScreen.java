//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:14
//

package SCSharpMac.UI;

import CS2JNet.System.StringSupport;
import java.util.ArrayList;
import java.util.List;
import SCSharp.Fnt;
import SCSharp.Mpq;
import SCSharpMac.UI.__MultiTickEventHandler;
import SCSharpMac.UI.Game;
import SCSharpMac.UI.GuiUtil;
import SCSharpMac.UI.Pcx;
import SCSharpMac.UI.TickEventArgs;
import SCSharpMac.UI.UIScreen;

//
// SCSharpMac.UI.MarkupScreen
//
// Authors:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
public abstract class MarkupScreen  extends UIScreen 
{
    Fnt fnt;
    byte[] pal = new byte[]();
    public MarkupScreen(Mpq mpq) throws Exception {
        super(mpq);
        pages = new List<MarkupPage>();
    }

    public enum PageLocation
    {
        Center,
        Top,
        Bottom,
        Left,
        Right,
        LowerLeft
    }
    static class MarkupPage   
    {
        static final int X_OFFSET = 60;
        static final int Y_OFFSET = 10;
        PageLocation location = PageLocation.Center;
        List<String> lines = new List<String>();
        List<CALayer> lineLayers = new List<CALayer>();
        CALayer lineContainer = new CALayer();
        CALayer newBackground = new CALayer();
        Fnt fnt;
        byte[] pal = new byte[]();
        public MarkupPage(PageLocation loc, Fnt font, byte[] palette) throws Exception {
            location = loc;
            lines = new List<String>();
            fnt = font;
            pal = palette;
        }

        public MarkupPage(Stream background) throws Exception {
            newBackground = GuiUtil.layerFromStream(background,254,0);
            newBackground.AnchorPoint = new PointF(0, 0);
            newBackground.Position = new PointF(0, 0);
        }

        public void addLine(String line) throws Exception {
            lines.Add(line);
        }

        public void createLineLayers() throws Exception {
            lineContainer = CALayer.Create();
            lineContainer.AnchorPoint = new PointF(0, 0);
            lineContainer.Bounds = new RectangleF(0, 0, 640, 480);
            lineLayers = new List<CALayer>();
            for (Object __dummyForeachVar0 : lines)
            {
                String l = (String)__dummyForeachVar0;
                CALayer layer = new CALayer();
                if (StringSupport.equals(l.Trim(), ""))
                    layer = null;
                else
                    /*Painter.SCREEN_RES_X*/
                    layer = GuiUtil.composeText(l,fnt,pal,640 - X_OFFSET * 2,-1,4); 
                lineLayers.Add(layer);
                if (layer != null)
                    lineContainer.AddSublayer(layer);
                 
            }
        }

        public CALayer getBackground() throws Exception {
            return newBackground;
        }

        public CALayer getLines() throws Exception {
            return lineContainer;
        }

        public boolean getHasText() throws Exception {
            return lines != null && lines.Count > 0;
        }

        public void layout() throws Exception {
            float y = new float();
            createLineLayers();
            switch(location)
            {
                case Top: 
                    y = Y_OFFSET;
                    for (Object __dummyForeachVar1 : lineLayers)
                    {
                        CALayer l = (CALayer)__dummyForeachVar1;
                        if (l != null)
                        {
                            l.Position = new PointF((lineContainer.Bounds.Width - l.Bounds.Width) / 2, y);
                            y += l.Bounds.Height;
                        }
                        else
                            y += fnt.getLineSize(); 
                    }
                    break;
                case Bottom: 
                    y = lineContainer.Bounds.Height - Y_OFFSET - fnt.getLineSize() * lines.Count;
                    for (Object __dummyForeachVar2 : lineLayers)
                    {
                        CALayer l = (CALayer)__dummyForeachVar2;
                        if (l != null)
                        {
                            l.Position = new PointF((lineContainer.Bounds.Width - l.Bounds.Width) / 2, y);
                            y += l.Bounds.Height;
                        }
                        else
                            y += fnt.getLineSize(); 
                    }
                    break;
                case Left: 
                    y = (lineContainer.Bounds.Height - fnt.getLineSize() * lines.Count) / 2;
                    for (Object __dummyForeachVar3 : lineLayers)
                    {
                        CALayer l = (CALayer)__dummyForeachVar3;
                        if (l != null)
                        {
                            l.Position = new PointF(X_OFFSET, y);
                            y += l.Bounds.Height;
                        }
                        else
                            y += fnt.getLineSize(); 
                    }
                    break;
                case LowerLeft: 
                    y = lineContainer.Bounds.Height - Y_OFFSET - fnt.getLineSize() * lines.Count;
                    for (Object __dummyForeachVar4 : lineLayers)
                    {
                        CALayer l = (CALayer)__dummyForeachVar4;
                        if (l != null)
                        {
                            l.Position = new PointF(X_OFFSET, y);
                            y += l.Bounds.Height;
                        }
                        else
                            y += fnt.getLineSize(); 
                    }
                    break;
                case Right: 
                    y = (lineContainer.Bounds.Height - fnt.getLineSize() * lines.Count) / 2;
                    for (Object __dummyForeachVar5 : lineLayers)
                    {
                        CALayer l = (CALayer)__dummyForeachVar5;
                        if (l != null)
                        {
                            l.Position = new PointF(lineContainer.Bounds.Width - l.Bounds.Width - X_OFFSET, y);
                            y += l.Bounds.Height;
                        }
                        else
                            y += fnt.getLineSize(); 
                    }
                    break;
                case Center: 
                    y = (lineContainer.Bounds.Height - fnt.getLineSize() * lines.Count) / 2;
                    for (Object __dummyForeachVar6 : lineLayers)
                    {
                        CALayer l = (CALayer)__dummyForeachVar6;
                        if (l != null)
                        {
                            l.Position = new PointF((lineContainer.Bounds.Width - l.Bounds.Width) / 2, y);
                            y += l.Bounds.Height;
                        }
                        else
                            y += fnt.getLineSize(); 
                    }
                    break;
            
            }
        }
    
    }

    List<MarkupPage> pages = new List<MarkupPage>();
    protected void addMarkup(Stream s) throws Exception {
        String l = new String();
        MarkupPage currentPage = null;
        StreamReader sr = new StreamReader(s);
        while ((l = sr.ReadLine()) != null)
        {
            if (l.StartsWith("</"))
            {
                if (l.StartsWith("</PAGE>"))
                {
                    currentPage.layout();
                    pages.Add(currentPage);
                    currentPage = null;
                }
                else if (l.StartsWith("</SCREENCENTER>"))
                {
                    currentPage = new MarkupPage(PageLocation.Center,fnt,pal);
                }
                else if (l.StartsWith("</SCREENLEFT>"))
                {
                    currentPage = new MarkupPage(PageLocation.Left,fnt,pal);
                }
                else if (l.StartsWith("</SCREENLOWERLEFT>"))
                {
                    currentPage = new MarkupPage(PageLocation.LowerLeft,fnt,pal);
                }
                else if (l.StartsWith("</SCREENRIGHT>"))
                {
                    currentPage = new MarkupPage(PageLocation.Right,fnt,pal);
                }
                else if (l.StartsWith("</SCREENTOP>"))
                {
                    currentPage = new MarkupPage(PageLocation.Top,fnt,pal);
                }
                else if (l.StartsWith("</SCREENBOTTOM>"))
                {
                    currentPage = new MarkupPage(PageLocation.Bottom,fnt,pal);
                }
                else if (l.StartsWith("</BACKGROUND "))
                {
                    String bg = l.Substring("</BACKGROUND ".Length);
                    bg = bg.Substring(0, bg.Length - 1);
                    pages.Add(new MarkupPage((Stream)mpq.getResource(bg)));
                }
                        
            }
            else /* skip everything else */
            if (currentPage != null)
                currentPage.addLine(l);
              
        }
    }

    protected void resourceLoader() throws Exception {
        Console.WriteLine("loading font palette");
        Stream palStream = (Stream)mpq.getResource("glue\\Palmm\\tFont.pcx");
        Pcx pcx = new Pcx();
        pcx.readFromStream(palStream,-1,-1);
        pal = pcx.getRGBData();
        Console.WriteLine("loading font");
        fnt = GuiUtil.getFonts(mpq)[3];
        Console.WriteLine("loading markup");
        loadMarkup();
        /* set things up so we're ready to go */
        millisDelay = 4000;
        pageEnumerator = pages.GetEnumerator();
        advanceToNextPage();
    }

    CALayer currentBackground = new CALayer();
    CALayer currentLines = new CALayer();
    IEnumerator<MarkupPage> pageEnumerator = new IEnumerator<MarkupPage>();
    int millisDelay = new int();
    long totalElapsed = new long();
    public void addToPainter() throws Exception {
        super.addToPainter();
        Game.getInstance().Tick = __MultiTickEventHandler.combine(Game.getInstance().Tick,new TickEventHandler() 
          { 
            public System.Void invoke(System.Object sender, TickEventArgs args) throws Exception {
                flipPage(sender, args);
            }

            public List<TickEventHandler> getInvocationList() throws Exception {
                List<TickEventHandler> ret = new ArrayList<TickEventHandler>();
                ret.add(this);
                return ret;
            }
        
          });
    }

    public void removeFromPainter() throws Exception {
        Game.getInstance().Tick = __MultiTickEventHandler.remove(Game.getInstance().Tick,new TickEventHandler() 
          { 
            public System.Void invoke(System.Object sender, TickEventArgs args) throws Exception {
                flipPage(sender, args);
            }

            public List<TickEventHandler> getInvocationList() throws Exception {
                List<TickEventHandler> ret = new ArrayList<TickEventHandler>();
                ret.add(this);
                return ret;
            }
        
          });
        super.removeFromPainter();
    }

    void flipPage(Object sender, TickEventArgs e) throws Exception {
        totalElapsed += e.getMillisecondsElapsed();
        if (totalElapsed < millisDelay)
            return ;
         
        totalElapsed = 0;
        advanceToNextPage();
    }

    public void keyboardDown(NSEvent theEvent) throws Exception {
        char c = theEvent.Characters[0];
        switch((int)c)
        {
            case 27: 
                Game.getInstance().Tick = __MultiTickEventHandler.remove(Game.getInstance().Tick,new TickEventHandler() 
                  { 
                    /*Escape*/
                    public System.Void invoke(System.Object sender, TickEventArgs args) throws Exception {
                        flipPage(sender, args);
                    }

                    public List<TickEventHandler> getInvocationList() throws Exception {
                        List<TickEventHandler> ret = new ArrayList<TickEventHandler>();
                        ret.add(this);
                        return ret;
                    }
                
                  });
                markupFinished();
                break;
            case 32: 
            case 13: 
                /*Space*/
                /*Return*/
                totalElapsed = 0;
                advanceToNextPage();
                break;
        
        }
    }

    void advanceToNextPage() throws Exception {
        if (currentLines != null)
            currentLines.RemoveFromSuperLayer();
         
        while (pageEnumerator.MoveNext())
        {
            if (pageEnumerator.Current.Background != null)
            {
                if (currentBackground != null)
                    currentBackground.RemoveFromSuperLayer();
                 
                currentBackground = pageEnumerator.Current.Background;
                AddSublayer(currentBackground);
            }
             
            if (pageEnumerator.Current.HasText)
            {
                currentLines = pageEnumerator.Current.Lines;
                AddSublayer(currentLines);
                return ;
            }
             
        }
        Game.getInstance().Tick = __MultiTickEventHandler.remove(Game.getInstance().Tick,new TickEventHandler() 
          { 
            public System.Void invoke(System.Object sender, TickEventArgs args) throws Exception {
                flipPage(sender, args);
            }

            public List<TickEventHandler> getInvocationList() throws Exception {
                List<TickEventHandler> ret = new ArrayList<TickEventHandler>();
                ret.add(this);
                return ret;
            }
        
          });
        markupFinished();
    }

    protected abstract void loadMarkup() throws Exception ;

    protected abstract void markupFinished() throws Exception ;

}


