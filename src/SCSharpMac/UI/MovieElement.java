//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:14
//

package SCSharpMac.UI;

import java.util.ArrayList;
import java.util.List;
import SCSharp.BinElement;
import SCSharpMac.UI.__MultiPlayerEvent;
import SCSharpMac.UI.MovieElementLayerDelegate;
import SCSharpMac.UI.SmackerPlayer;
import SCSharpMac.UI.UIElement;
import SCSharpMac.UI.UIScreen;

//
// SCSharpMac.UI.MovieElement
//
// Authors:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
public class MovieElement  extends UIElement 
{
    public MovieElement(UIScreen screen, BinElement el, byte[] palette, boolean scale) throws Exception {
        super(screen, el, palette);
        this.scale = scale;
    }

    public MovieElement(UIScreen screen, BinElement el, byte[] palette, String resource, boolean scale) throws Exception {
        this(screen, el, palette, resource);
        this.scale = scale;
    }

    public MovieElement(UIScreen screen, BinElement el, byte[] palette, String resource) throws Exception {
        super(screen, el, palette);
        setPlayer(new SmackerPlayer((Stream)getMpq().getResource(resource),1));
        getPlayer().FrameReady = __MultiPlayerEvent.combine(getPlayer().FrameReady,new PlayerEvent() 
          { 
            public System.Void invoke() throws Exception {
                newFrame();
            }

            public List<PlayerEvent> getInvocationList() throws Exception {
                List<PlayerEvent> ret = new ArrayList<PlayerEvent>();
                ret.add(this);
                return ret;
            }
        
          });
    }

    public MovieElement(UIScreen screen, BinElement el, byte[] palette, SmackerPlayer player) throws Exception {
        super(screen, el, palette);
        setPlayer(player);
    }

    public MovieElement(UIScreen screen, int x, int y, int width, int height, boolean scale) throws Exception {
        super(screen, (ushort)x, (ushort)y, (ushort)width, (ushort)height);
        this.scale = scale;
    }

    public boolean getSensitive() throws Exception {
        return false;
    }

    public void setSensitive(boolean value) throws Exception {
    }

    public SmackerPlayer getPlayer() throws Exception {
        return player;
    }

    public void setPlayer(SmackerPlayer value) throws Exception {
        if (player != null)
        {
            player.FrameReady = __MultiPlayerEvent.remove(player.FrameReady,new PlayerEvent() 
              { 
                public System.Void invoke() throws Exception {
                    newFrame();
                }

                public List<PlayerEvent> getInvocationList() throws Exception {
                    List<PlayerEvent> ret = new ArrayList<PlayerEvent>();
                    ret.add(this);
                    return ret;
                }
            
              });
            player.stop();
        }
         
        player = value;
        if (player != null)
        {
            scalePlayer();
            player.FrameReady = __MultiPlayerEvent.combine(player.FrameReady,new PlayerEvent() 
              { 
                public System.Void invoke() throws Exception {
                    newFrame();
                }

                public List<PlayerEvent> getInvocationList() throws Exception {
                    List<PlayerEvent> ret = new ArrayList<PlayerEvent>();
                    ret.add(this);
                    return ret;
                }
            
              });
        }
         
    }

    public void play() throws Exception {
        if (player != null)
            player.play();
         
    }

    public void stop() throws Exception {
        if (player != null)
            player.stop();
         
    }

    public Size getMovieSize() throws Exception {
        if (player == null)
            return new Size(0, 0);
        else
            return new Size(player.getWidth(), player.getHeight()); 
    }

    void createDimLayer() throws Exception {
        dimLayer = CALayer.Create();
        dimLayer.Bounds = new RectangleF(0, 0, player.getWidth(), player.getHeight());
        dimLayer.Position = new PointF(0, 0);
        dimLayer.AnchorPoint = new PointF(0, 0);
    }

    public void dim(byte dimness) throws Exception {
        if (dim == dimness)
            return ;
         
        if (dim > 0)
        {
            if (dimness > 0)
                dimLayer.BackgroundColor = new CGColor(0, (float)dimness / 255);
            else
                dimLayer.RemoveFromSuperLayer(); 
        }
        else
        {
            if (dimness > 0)
            {
                if (dimLayer == null)
                    createDimLayer();
                 
                dimLayer.BackgroundColor = new CGColor(0, (float)dimness / 255);
                if (layer != null)
                    layer.AddSublayer(dimLayer);
                 
            }
             
        } 
        dim = dimness;
    }

    void newFrame() throws Exception {
        if (layer != null)
            layer.SetNeedsDisplay();
         
    }

    SmackerPlayer player;
    CALayer layer = new CALayer();
    CALayer dimLayer = new CALayer();
    byte dim = 0;
    boolean scale = new boolean();
    public float playerZoom = 1.0f;
    void scalePlayer() throws Exception {
        if (layer == null)
            return ;
         
        playerZoom = 1.0f;
        if (scale && (player.getWidth() != getWidth() || player.getHeight() != getHeight()))
        {
            float horiz_zoom = (float)getWidth() / player.getWidth();
            float vert_zoom = (float)getHeight() / player.getHeight();
            if (horiz_zoom < vert_zoom)
                playerZoom = horiz_zoom;
            else
                playerZoom = vert_zoom; 
        }
         
        layer.AffineTransform = CGAffineTransform.MakeScale(playerZoom, playerZoom);
    }

    protected CALayer createLayer() throws Exception {
        layer = CALayer.Create();
        layer.Bounds = new RectangleF(0, 0, getWidth(), getHeight());
        layer.AnchorPoint = new PointF(0, 0);
        layer.Delegate = new MovieElementLayerDelegate(this);
        if (player == null)
            return layer;
         
        scalePlayer();
        if (dim != 0)
        {
            createDimLayer();
            dimLayer.BackgroundColor = new CGColor(0, (float)dim / 255);
            layer.AddSublayer(dimLayer);
        }
         
        return layer;
    }

}


