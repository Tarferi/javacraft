//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:12
//

package SCSharpMac.UI;

import SCSharp.Mpq;
import SCSharpMac.UI.Builtins;
import SCSharpMac.UI.DialogEvent;
import SCSharpMac.UI.TextBoxElement;
import SCSharpMac.UI.UIDialog;
import SCSharpMac.UI.UIScreen;

//
// SCSharp.UI.EntryDialog
//
// Authors:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
public class EntryDialog  extends UIDialog 
{
    String title = new String();
    public EntryDialog(UIScreen parent, Mpq mpq, String title) throws Exception {
        super(parent, mpq, "glue\\PalNl", Builtins.rez_GluPEditBin);
        background_path = "glue\\PalNl\\pEPopup.pcx";
        this.title = title;
    }

    static final int OK_ELEMENT_INDEX = 1;
    static final int TITLE_ELEMENT_INDEX = 2;
    static final int CANCEL_ELEMENT_INDEX = 3;
    static final int ENTRY_ELEMENT_INDEX = 4;
    TextBoxElement entry;
    protected void resourceLoader() throws Exception {
        super.resourceLoader();
        Console.WriteLine("entry element is {0}", Elements[ENTRY_ELEMENT_INDEX].Type);
        Elements[TITLE_ELEMENT_INDEX].Text = title;
        Elements[OK_ELEMENT_INDEX].Activate += ;
        Elements[CANCEL_ELEMENT_INDEX].Activate += ;
        entry = (TextBoxElement)Elements[ENTRY_ELEMENT_INDEX];
        Elements[OK_ELEMENT_INDEX].Sensitive = false;
    }

    public void keyboardDown(NSEvent theEvent) throws Exception {
        entry.keyboardDown(theEvent);
        Elements[OK_ELEMENT_INDEX].Sensitive = (entry.getValue().Length > 0);
    }

    public String getValue() throws Exception {
        return entry.getValue();
    }

    public DialogEvent Cancel;
    public DialogEvent Ok;
}


