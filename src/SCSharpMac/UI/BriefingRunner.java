//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:14
//

package SCSharpMac.UI;

import SCSharp.Chk;
import SCSharp.TriggerAction;
import SCSharp.TriggerData;
import SCSharpMac.UI.GuiUtil;
import SCSharpMac.UI.ReadyRoomScreen;
import SCSharpMac.UI.TickEventArgs;

public class BriefingRunner   
{
    TriggerData triggerData;
    Chk scenario;
    ReadyRoomScreen screen;
    String prefix = new String();
    int sleepUntil = new int();
    int totalElapsed = new int();
    int current_action = new int();
    public BriefingRunner(ReadyRoomScreen screen, Chk scenario, String scenario_prefix) throws Exception {
        this.screen = screen;
        this.scenario = scenario;
        this.prefix = scenario_prefix;
        triggerData = scenario.getBriefingData();
    }

    public void play() throws Exception {
        current_action = 0;
    }

    public void stop() throws Exception {
        sleepUntil = 0;
    }

    public void tick(Object sender, TickEventArgs e) throws Exception {
        TriggerAction[] actions = triggerData.getTriggers()[0].Actions;
        if (current_action == actions.Length)
            return ;
         
        // FIXME we need to figure out how many ticks per second SC1 used for normal mode... 6?
        totalElapsed += (int)e.getMillisecondsElapsed();
        /* if we're presently waiting, make sure
        			   enough time has gone by.  otherwise
        			   return */
        if (totalElapsed < sleepUntil * 166)
            return ;
         
        /* 166 = 1/6th of a second */
        totalElapsed = 0;
        while (current_action < actions.Length)
        {
            TriggerAction action = actions[current_action];
            current_action++;
            System.Byte __dummyScrutVar0 = action.getAction();
            if (__dummyScrutVar0.equals(0))
            {
            }
            else /* no action */
            if (__dummyScrutVar0.equals(1))
            {
                sleepUntil = (int)action.getDelay();
                return ;
            }
            else if (__dummyScrutVar0.equals(2))
            {
                GuiUtil.playSound(screen.getMpq(),prefix + "\\" + scenario.getMapString((int)action.getWavIndex()));
                sleepUntil = (int)action.getDelay();
                return ;
            }
            else if (__dummyScrutVar0.equals(3))
            {
                screen.setTransmissionText(scenario.getMapString((int)action.getTextIndex()));
            }
            else if (__dummyScrutVar0.equals(4))
            {
                screen.setObjectives(scenario.getMapString((int)action.getTextIndex()));
            }
            else if (__dummyScrutVar0.equals(5))
            {
                Console.WriteLine("show portrait:");
                Console.WriteLine("location = {0}, textindex = {1}, wavindex = {2}, delay = {3}, group1 = {4}, group2 = {5}, unittype = {6}, action = {7}, switch = {8}, flags = {9}", action.getLocation(), action.getTextIndex(), action.getWavIndex(), action.getDelay(), action.getGroup1(), action.getGroup2(), action.getUnitType(), action.getAction(), action.getSwitch(), action.getFlags());
                screen.showPortrait((int)action.getUnitType(),(int)action.getGroup1());
                Console.WriteLine(scenario.getMapString((int)action.getTextIndex()));
            }
            else if (__dummyScrutVar0.equals(6))
            {
                screen.hidePortrait((int)action.getGroup1());
            }
            else if (__dummyScrutVar0.equals(7))
            {
                Console.WriteLine("Display Speaking Portrait(Slot, Time)");
                Console.WriteLine(scenario.getMapString((int)action.getTextIndex()));
            }
            else if (__dummyScrutVar0.equals(8))
            {
                Console.WriteLine("Transmission(Text, Slot, Time, Modifier, Wave, WavTime)");
                screen.setTransmissionText(scenario.getMapString((int)action.getTextIndex()));
                screen.highlightPortrait((int)action.getGroup1());
                GuiUtil.playSound(screen.getMpq(),prefix + "\\" + scenario.getMapString((int)action.getWavIndex()));
                sleepUntil = (int)action.getDelay();
                return ;
            }
            else
            {
            }         
        }
    }

}


