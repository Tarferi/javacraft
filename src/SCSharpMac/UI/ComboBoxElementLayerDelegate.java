//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:12
//

package SCSharpMac.UI;

import CS2JNet.JavaSupport.util.ListSupport;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import SCSharpMac.UI.ComboBoxElement;
import SCSharpMac.UI.GuiUtil;

public class ComboBoxElementLayerDelegate  extends CALayerDelegate 
{
    ComboBoxElement el;
    public ComboBoxElementLayerDelegate(ComboBoxElement el) throws Exception {
        this.el = el;
    }

    public void drawLayer(CALayer layer, CGContext context) throws Exception {
        /* XXX draw the arrow (and border) */
        if (el.getSelectedIndex() != -1)
        {
            GuiUtil.renderTextToContext(context,new PointF(0, 0),el.getSelectedItem(),el.getFont(),el.getPalette(),4);
        }
         
    }

}


