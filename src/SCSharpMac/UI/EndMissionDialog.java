//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:12
//

package SCSharpMac.UI;

import java.util.ArrayList;
import java.util.List;
import SCSharp.Mpq;
import SCSharpMac.UI.__MultiDialogEvent;
import SCSharpMac.UI.Builtins;
import SCSharpMac.UI.DialogEvent;
import SCSharpMac.UI.ExitConfirmationDialog;
import SCSharpMac.UI.QuitMissionDialog;
import SCSharpMac.UI.RestartConfirmationDialog;
import SCSharpMac.UI.UIDialog;
import SCSharpMac.UI.UIScreen;

public class EndMissionDialog  extends UIDialog 
{
    public EndMissionDialog(UIScreen parent, Mpq mpq) throws Exception {
        super(parent, mpq, "glue\\Palmm", Builtins.rez_AbrtMenuBin);
        background_path = null;
    }

    static final int PREVIOUS_ELEMENT_INDEX = 1;
    static final int RESTARTMISSION_ELEMENT_INDEX = 2;
    static final int QUITMISSION_ELEMENT_INDEX = 3;
    static final int EXITPROGRAM_ELEMENT_INDEX = 4;
    protected void resourceLoader() throws Exception {
        super.resourceLoader();
        for (int i = 0;i < Elements.Count;i++)
            Console.WriteLine("{0}: {1} '{2}'", i, Elements[i].Type, Elements[i].Text);
        Elements[QUITMISSION_ELEMENT_INDEX].Activate += ;
        Elements[EXITPROGRAM_ELEMENT_INDEX].Activate += ;
        Elements[RESTARTMISSION_ELEMENT_INDEX].Activate += ;
        Elements[PREVIOUS_ELEMENT_INDEX].Activate += ;
    }

    public DialogEvent Previous;
}


