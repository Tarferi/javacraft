//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:15
//

package SCSharpMac.UI;

import java.util.ArrayList;
import java.util.List;
import SCSharp.Bin;
import SCSharp.BinElement;
import SCSharp.ElementFlags;
import SCSharp.ElementType;
import SCSharp.Grp;
import SCSharp.Mpq;
import SCSharpMac.UI.__MultiReadyDelegate;
import SCSharpMac.UI.ButtonElement;
import SCSharpMac.UI.ComboBoxElement;
import SCSharpMac.UI.CursorAnimator;
import SCSharpMac.UI.DialogBoxElement;
import SCSharpMac.UI.Game;
import SCSharpMac.UI.GuiUtil;
import SCSharpMac.UI.ImageElement;
import SCSharpMac.UI.LabelElement;
import SCSharpMac.UI.ListBoxElement;
import SCSharpMac.UI.Pcx;
import SCSharpMac.UI.ReadyDelegate;
import SCSharpMac.UI.TextBoxElement;
import SCSharpMac.UI.UIDialog;
import SCSharpMac.UI.UIElement;

//
// SCSharpMac.UI.UIScreen
//
// Authors:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
public abstract class UIScreen  extends CALayer 
{
    CALayer background = new CALayer();
    protected CursorAnimator Cursor;
    protected Bin Bin;
    protected Mpq mpq;
    protected String prefix = new String();
    protected String binFile = new String();
    protected String background_path = new String();
    protected int background_transparent = new int();
    protected int background_translucent = new int();
    protected String fontpal_path = new String();
    protected String effectpal_path = new String();
    protected String arrowgrp_path = new String();
    protected Pcx fontpal;
    protected Pcx effectpal;
    protected List<UIElement> Elements = new List<UIElement>();
    protected UIDialog dialog;
    /* the currently popped up dialog */
    protected UIScreen(Mpq mpq, String prefix, String binFile) throws Exception {
        this.mpq = mpq;
        this.prefix = prefix;
        this.binFile = binFile;
        if (prefix != null)
        {
            background_path = prefix + "\\Backgnd.pcx";
            fontpal_path = prefix + "\\tFont.pcx";
            effectpal_path = prefix + "\\tEffect.pcx";
            arrowgrp_path = prefix + "\\arrow.grp";
        }
         
        background_transparent = -1;
        background_translucent = -1;
        Bounds = new RectangleF(0, 0, 640, 480);
        AnchorPoint = new PointF(0, 0);
    }

    protected UIScreen(Mpq mpq) throws Exception {
        this.mpq = mpq;
        Bounds = new RectangleF(0, 0, 640, 480);
        AnchorPoint = new PointF(0, 0);
    }

    public void addToPainter() throws Exception {
    }

    public void removeFromPainter() throws Exception {
        if (Cursor != null)
            Game.getInstance().setCursor(null);
         
    }

    public boolean getUseTiles() throws Exception {
        return false;
    }

    public Mpq getMpq() throws Exception {
        return mpq;
    }

    public CALayer getBackground() throws Exception {
        return background;
    }

    public PointF screenToLayer(PointF point) throws Exception {
        return new PointF(point.X - Position.X, point.Y - Position.Y);
    }

    public PointF layerToScreen(PointF point) throws Exception {
        return new PointF(point.X + Position.X, point.Y + Position.Y);
    }

    // FIXME we should be using CoreAnimation's HitTest code for this..
    UIElement xYToElement(PointF p, boolean onlyUI) throws Exception {
        if (Elements == null)
            return null;
         
        PointF ui_pt = screenToLayer(p);
        for (Object __dummyForeachVar0 : Elements)
        {
            UIElement e = (UIElement)__dummyForeachVar0;
            if (e.getType() == ElementType.DialogBox)
                continue;
             
            if (onlyUI && e.getType() == ElementType.Image)
                continue;
             
            if (e.getVisible() && e.pointInside(ui_pt))
                return e;
             
        }
        return null;
    }

    protected UIElement mouseDownElement;
    protected UIElement mouseOverElement;
    public void mouseEnterElement(UIElement element) throws Exception {
        element.mouseEnter();
    }

    public void mouseLeaveElement(UIElement element) throws Exception {
        element.mouseLeave();
    }

    public void activateElement(UIElement element) throws Exception {
        if (!element.getVisible() || !element.getSensitive())
            return ;
         
        Console.WriteLine("activating element {0}", Elements.IndexOf(element));
        element.onActivate();
    }

    public void mouseButtonDown(NSEvent theEvent) throws Exception {
        if (mouseDownElement != null)
            Console.WriteLine("mouseDownElement already set in MouseButtonDown");
         
        UIElement element = XYToElement(theEvent.LocationInWindow, true);
        if (element != null && element.getVisible() && element.getSensitive())
        {
            mouseDownElement = element;
            if (theEvent.Type == NSEventType.LeftMouseDown)
                mouseDownElement.mouseButtonDown(theEvent);
            else
                mouseDownElement.mouseWheel(theEvent); 
        }
         
    }

    public void handleMouseButtonDown(NSEvent theEvent) throws Exception {
        if (dialog != null)
            dialog.handleMouseButtonDown(theEvent);
        else
            mouseButtonDown(theEvent); 
    }

    public void mouseButtonUp(NSEvent theEvent) throws Exception {
        if (mouseDownElement != null)
        {
            if (theEvent.Type == NSEventType.LeftMouseUp)
                mouseDownElement.mouseButtonUp(theEvent);
             
            mouseDownElement = null;
        }
         
    }

    public void handleMouseButtonUp(NSEvent theEvent) throws Exception {
        if (dialog != null)
            dialog.handleMouseButtonUp(theEvent);
        else
            mouseButtonUp(theEvent); 
    }

    public void pointerMotion(NSEvent theEvent) throws Exception {
        if (mouseDownElement != null)
        {
            mouseDownElement.pointerMotion(theEvent);
        }
        else
        {
            UIElement newMouseOverElement = XYToElement(theEvent.LocationInWindow, true);
            if (newMouseOverElement != mouseOverElement)
            {
                if (mouseOverElement != null)
                    mouseLeaveElement(mouseOverElement);
                 
                if (newMouseOverElement != null)
                    mouseEnterElement(newMouseOverElement);
                 
            }
             
            mouseOverElement = newMouseOverElement;
        } 
    }

    public void handlePointerMotion(NSEvent theEvent) throws Exception {
        if (dialog != null)
            dialog.handlePointerMotion(theEvent);
        else
            pointerMotion(theEvent); 
    }

    public void keyboardUp(NSEvent theEvent) throws Exception {
    }

    public void handleKeyboardUp(NSEvent theEvent) throws Exception {
        if (dialog != null)
            dialog.handleKeyboardUp(theEvent);
        else
            keyboardUp(theEvent); 
    }

    public void keyboardDown(NSEvent theEvent) throws Exception {
        if (Elements != null)
        {
            String eventChars = theEvent.CharactersIgnoringModifiers;
            for (Object __dummyForeachVar1 : Elements)
            {
                UIElement e = (UIElement)__dummyForeachVar1;
                /* check for the uielement's hotkey if it has one */
                if ((e.getFlags() & ElementFlags.HasHotkey) == ElementFlags.HasHotkey && eventChars.Length == 1 && eventChars[0] == e.getHotkey())
                {
                    activateElement(e);
                    return ;
                }
                 
                /* check for the deafult button */
                if (((e.getFlags() & ElementFlags.DefaultButton) == ElementFlags.DefaultButton && eventChars[0] == 13) || ((e.getFlags() & ElementFlags.CancelButton) == ElementFlags.CancelButton && eventChars[0] == 27))
                {
                    activateElement(e);
                    return ;
                }
                 
            }
        }
         
    }

    public void handleKeyboardDown(NSEvent theEvent) throws Exception {
        if (dialog != null)
            dialog.handleKeyboardDown(theEvent);
        else
            keyboardDown(theEvent); 
    }

    protected void screenDisplayed() throws Exception {
    }

    public ReadyDelegate DoneSwooshing;
    public ReadyDelegate Ready;
    boolean loaded = new boolean();
    protected void raiseReadyEvent() throws Exception {
        if (Ready != null)
            Ready.invoke();
         
    }

    protected void raiseDoneSwooshing() throws Exception {
        if (DoneSwooshing != null)
            DoneSwooshing.invoke();
         
    }

    int translucentIndex = 254;
    protected int getTranslucentIndex() throws Exception {
        return translucentIndex;
    }

    protected void setTranslucentIndex(int value) throws Exception {
        translucentIndex = value;
    }

    protected void resourceLoader() throws Exception {
        Stream s = new Stream();
        fontpal = null;
        effectpal = null;
        if (fontpal_path != null)
        {
            Console.WriteLine("loading font palette");
            s = (Stream)mpq.getResource(fontpal_path);
            if (s != null)
            {
                fontpal = new Pcx();
                fontpal.readFromStream(s,-1,-1);
            }
             
        }
         
        if (effectpal_path != null)
        {
            Console.WriteLine("loading cursor palette");
            s = (Stream)mpq.getResource(effectpal_path);
            if (s != null)
            {
                effectpal = new Pcx();
                effectpal.readFromStream(s,-1,-1);
            }
             
            if (effectpal != null && arrowgrp_path != null)
            {
                Console.WriteLine("loading arrow cursor");
                Grp arrowgrp = (Grp)mpq.getResource(arrowgrp_path);
                if (arrowgrp != null)
                {
                    Cursor = new CursorAnimator(arrowgrp,effectpal.getPalette());
                    Cursor.setHotSpot(64,64);
                }
                 
            }
             
        }
         
        if (background_path != null)
        {
            Console.WriteLine("loading background");
            background = GuiUtil.layerFromStream((Stream)mpq.getResource(background_path),background_translucent,background_transparent);
            background.AnchorPoint = new PointF(0, 0);
            AddSublayer(background);
        }
         
        // FIXME: we should center the background (and scale it?)
        Elements = new List<UIElement>();
        if (binFile != null)
        {
            Console.WriteLine("loading ui elements");
            Bin = (Bin)mpq.getResource(binFile);
            if (Bin == null)
                throw new Exception(String.Format("specified file '{0}' does not exist", binFile));
             
            for (Object __dummyForeachVar2 : Bin.getElements())
            {
                /* convert all the BinElements to UIElements for our subclasses to use */
                BinElement el = (BinElement)__dummyForeachVar2;
                //					Console.WriteLine ("{0}: {1}", el.text, el.flags);
                UIElement ui_el = null;
                switch(el.type)
                {
                    case DialogBox: 
                        ui_el = new DialogBoxElement(this,el,fontpal.getRGBData());
                        break;
                    case Image: 
                        ui_el = new ImageElement(this,el,fontpal.getRGBData(),translucentIndex);
                        break;
                    case TextBox: 
                        ui_el = new TextBoxElement(this,el,fontpal.getRGBData());
                        break;
                    case ListBox: 
                        ui_el = new ListBoxElement(this,el,fontpal.getRGBData());
                        break;
                    case ComboBox: 
                        ui_el = new ComboBoxElement(this,el,fontpal.getRGBData());
                        break;
                    case LabelLeftAlign: 
                    case LabelCenterAlign: 
                    case LabelRightAlign: 
                        ui_el = new LabelElement(this,el,fontpal.getRGBData());
                        break;
                    case Button: 
                    case DefaultButton: 
                    case ButtonWithoutBorder: 
                        ui_el = new ButtonElement(this,el,fontpal.getRGBData());
                        break;
                    case Slider: 
                    case OptionButton: 
                    case CheckBox: 
                        ui_el = new UIElement(this,el,fontpal.getRGBData());
                        break;
                    default: 
                        Console.WriteLine("unhandled case {0}", el.type);
                        ui_el = new UIElement(this,el,fontpal.getRGBData());
                        break;
                
                }
                Elements.Add(ui_el);
            }
        }
         
    }

    void loadResources() throws Exception {
        resourceLoader();
        if (Elements != null)
        {
            for (/* [UNSUPPORTED] 'var' as type is unsupported "var" */ ui_el : Elements)
            {
                if (ui_el.Layer != null)
                {
                    ui_el.Layer.Position = new PointF(ui_el.X1, Bounds.Height - ui_el.Y1 - ui_el.Height);
                    ui_el.Layer.AnchorPoint = new PointF(0, 0);
                    AddSublayer(ui_el.Layer);
                }
                 
            }
        }
         
        NSApplication.SharedApplication.InvokeOnMainThread(FinishedLoading);
    }

    public void load() throws Exception {
        if (loaded)
            NSApplication.SharedApplication.InvokeOnMainThread(RaiseReadyEvent);
        else
            NSApplication.SharedApplication.InvokeOnMainThread(LoadResources); 
    }

    void finishedLoading() throws Exception {
        loaded = true;
        raiseReadyEvent();
    }

    public void showDialog(UIDialog dialog) throws Exception {
        Console.WriteLine("showing {0}", dialog);
        if (this.dialog != null)
            throw new Exception("only one active dialog is allowed");
         
        this.dialog = dialog;
        dialog.Ready = __MultiReadyDelegate.combine(dialog.Ready,new ReadyDelegate() 
          { 
            public System.Void invoke() throws Exception {
                dialog.addToPainter();
            }

            public List<ReadyDelegate> getInvocationList() throws Exception {
                List<ReadyDelegate> ret = new ArrayList<ReadyDelegate>();
                ret.add(this);
                return ret;
            }
        
          });
        dialog.load();
    }

    public void dismissDialog() throws Exception {
        if (dialog == null)
            return ;
         
        dialog.removeFromPainter();
        dialog = null;
    }

}


