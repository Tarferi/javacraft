//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:15
//

package SCSharpMac.UI;

import CS2JNet.JavaSupport.util.ListSupport;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import SCSharp.Mpq;
import SCSharpMac.UI.__MultiReadyDelegate;
import SCSharpMac.UI.UIDialog;
import SCSharpMac.UI.UIScreen;

//
// SCSharpMac.UI.UIDialog
//
// Authors:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
/* this should probably subclass from UIElement instead...  look into that */
public abstract class UIDialog  extends UIScreen 
{
    protected UIScreen parent;
    boolean dimScreen = new boolean();
    CALayer dimLayer = new CALayer();
    protected UIDialog(UIScreen parent, Mpq mpq, String prefix, String binFile) throws Exception {
        super(mpq, prefix, binFile);
        this.parent = parent;
        background_translucent = 254;
        background_transparent = 0;
        dimScreen = true;
        dimLayer = CALayer.Create();
        dimLayer.Bounds = parent.Bounds;
        dimLayer.AnchorPoint = new PointF(0, 0);
        dimLayer.BackgroundColor = new CGColor(0, 0, 0, .7f);
    }

    public void addToPainter() throws Exception {
        if (dimScreen)
            parent.AddSublayer(dimLayer);
         
        parent.AddSublayer(this);
    }

    public void removeFromPainter() throws Exception {
        RemoveFromSuperLayer();
        if (dimScreen)
            dimLayer.RemoveFromSuperLayer();
         
    }

    protected void resourceLoader() throws Exception {
        super.resourceLoader();
        /* figure out where we're going to be located on the screen */
        int baseX = new int(), baseY = new int();
        int si = new int();
        if (getBackground() != null)
        {
            Bounds = getBackground().Bounds;
            Position = new PointF((parent.Bounds.Width - getBackground().Bounds.Width) / 2, parent.Bounds.Height - (parent.Bounds.Height - getBackground().Bounds.Height) / 2 - getBackground().Bounds.Height);
        }
        else
        {
        } 
    }

    //				Position = new PointF (Elements[0].X1, parent.Bounds.Height - Elements[0].Y1 - Elements[0].Height);
    //				for (int i = 1; i < Elements.Count; i ++) {
    //					var ui_el = Elements[i];
    //					ui_el.X1 -= Elements[0].X1;
    //					ui_el.Y1 -= Elements[0].Y1;
    // }
    //				Elements[0].X1 = 0;
    //				Elements[0].Y1 = 0;
    public boolean getUseTiles() throws Exception {
        return true;
    }

    public UIScreen getParent() throws Exception {
        return parent;
    }

    public boolean getDimScreen() throws Exception {
        return dimScreen;
    }

    public void setDimScreen(boolean value) throws Exception {
        dimScreen = value;
    }

    public void showDialog(UIDialog dialog) throws Exception {
        Console.WriteLine("showing {0}", dialog);
        if (this.dialog != null)
            throw new Exception("only one active dialog is allowed");
         
        this.dialog = dialog;
        dialog.load();
        dialog.Ready = __MultiReadyDelegate.combine(dialog.Ready,new ReadyDelegate() 
          { 
            public System.Void invoke() throws Exception {
                dialog.addToPainter();
                removeFromPainter();
            }

            public List<ReadyDelegate> getInvocationList() throws Exception {
                List<ReadyDelegate> ret = new ArrayList<ReadyDelegate>();
                ret.add(this);
                return ret;
            }
        
          });
    }

    public void dismissDialog() throws Exception {
        if (dialog == null)
            return ;
         
        dialog.removeFromPainter();
        dialog = null;
        addToPainter();
    }

}


