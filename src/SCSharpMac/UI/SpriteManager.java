//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:15
//

package SCSharpMac.UI;

import java.util.ArrayList;
import java.util.List;
import SCSharp.Mpq;
import SCSharpMac.UI.__MultiTickEventHandler;
import SCSharpMac.UI.Game;
import SCSharpMac.UI.Sprite;
import SCSharpMac.UI.TickEventArgs;

//
// SCSharp.UI.SpriteManager
//
// Authors:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
public class SpriteManager   
{
    public static List<Sprite> sprites = new List<Sprite>();
    static Mpq our_mpq;
    public static int X = new int();
    public static int Y = new int();
    static {
        try
        {
            Game.getInstance().Tick = __MultiTickEventHandler.combine(Game.getInstance().Tick,new TickEventHandler() 
              { 
                public System.Void invoke(System.Object sender, TickEventArgs args) throws Exception {
                    spriteManagerPainterTick(sender, args);
                }

                public List<TickEventHandler> getInvocationList() throws Exception {
                    List<TickEventHandler> ret = new ArrayList<TickEventHandler>();
                    ret.add(this);
                    return ret;
                }
            
              });
        }
        catch (Exception __dummyStaticConstructorCatchVar0)
        {
            throw new ExceptionInInitializerError(__dummyStaticConstructorCatchVar0);
        }
    
    }

    public static Sprite createSprite(Mpq mpq, int sprite_number, byte[] palette, int x, int y) throws Exception {
        our_mpq = mpq;
        return CreateSprite(sprite_number, palette, x, y);
    }

    public static Sprite createSprite(Sprite parentSprite, ushort images_number, byte[] palette) throws Exception {
        Sprite sprite = new Sprite(parentSprite, images_number, palette);
        addSprite(sprite);
        return sprite;
    }

    public static void addSprite(Sprite sprite) throws Exception {
        sprites.Add(sprite);
        sprite.addToPainter();
    }

    public static Sprite createSprite(int sprite_number, byte[] palette, int x, int y) throws Exception {
        Sprite sprite = new Sprite(our_mpq, sprite_number, palette, x, y);
        addSprite(sprite);
        return sprite;
    }

    public static void removeSprite(Sprite sprite) throws Exception {
        sprite.removeFromPainter();
        sprites.Remove(sprite);
    }

    static void spriteManagerPainterTick(Object sender, TickEventArgs args) throws Exception {
    }

    public static void addToPainter() throws Exception {
        for (Object __dummyForeachVar0 : sprites)
        {
            Sprite s = (Sprite)__dummyForeachVar0;
            s.addToPainter();
        }
    }

    public static void removeFromPainter() throws Exception {
        for (Object __dummyForeachVar1 : sprites)
        {
            Sprite s = (Sprite)__dummyForeachVar1;
            s.removeFromPainter();
        }
    }

    public static void setUpperLeft(int x, int y) throws Exception {
        X = x;
        Y = y;
    }

}


