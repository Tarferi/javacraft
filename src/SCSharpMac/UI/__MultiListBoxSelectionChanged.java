//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:13
//

package SCSharpMac.UI;

import CS2JNet.JavaSupport.util.ListSupport;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import SCSharpMac.UI.__MultiListBoxSelectionChanged;
import SCSharpMac.UI.ListBoxSelectionChanged;

public class __MultiListBoxSelectionChanged   implements ListBoxSelectionChanged
{
    public void invoke(int selectedIndex) throws Exception {
        IList<ListBoxSelectionChanged> copy = new IList<ListBoxSelectionChanged>(), members = this.getInvocationList();
        synchronized (members)
        {
            copy = new LinkedList<ListBoxSelectionChanged>(members);
        }
        for (Object __dummyForeachVar0 : copy)
        {
            ListBoxSelectionChanged d = (ListBoxSelectionChanged)__dummyForeachVar0;
            d.invoke(selectedIndex);
        }
    }

    private System.Collections.Generic.IList<ListBoxSelectionChanged> _invocationList = new ArrayList<ListBoxSelectionChanged>();
    public static ListBoxSelectionChanged combine(ListBoxSelectionChanged a, ListBoxSelectionChanged b) throws Exception {
        if (a == null)
            return b;
         
        if (b == null)
            return a;
         
        __MultiListBoxSelectionChanged ret = new __MultiListBoxSelectionChanged();
        ret._invocationList = a.getInvocationList();
        ret._invocationList.addAll(b.getInvocationList());
        return ret;
    }

    public static ListBoxSelectionChanged remove(ListBoxSelectionChanged a, ListBoxSelectionChanged b) throws Exception {
        if (a == null || b == null)
            return a;
         
        System.Collections.Generic.IList<ListBoxSelectionChanged> aInvList = a.getInvocationList();
        System.Collections.Generic.IList<ListBoxSelectionChanged> newInvList = ListSupport.removeFinalStretch(aInvList, b.getInvocationList());
        if (aInvList == newInvList)
        {
            return a;
        }
        else
        {
            __MultiListBoxSelectionChanged ret = new __MultiListBoxSelectionChanged();
            ret._invocationList = newInvList;
            return ret;
        } 
    }

    public System.Collections.Generic.IList<ListBoxSelectionChanged> getInvocationList() throws Exception {
        return _invocationList;
    }

}


