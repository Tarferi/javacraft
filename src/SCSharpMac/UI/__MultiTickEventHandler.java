//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:12
//

package SCSharpMac.UI;

import CS2JNet.JavaSupport.util.ListSupport;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import SCSharpMac.UI.__MultiTickEventHandler;
import SCSharpMac.UI.TickEventArgs;
import SCSharpMac.UI.TickEventHandler;

public class __MultiTickEventHandler   implements TickEventHandler
{
    public void invoke(Object sender, TickEventArgs args) throws Exception {
        IList<TickEventHandler> copy = new IList<TickEventHandler>(), members = this.getInvocationList();
        synchronized (members)
        {
            copy = new LinkedList<TickEventHandler>(members);
        }
        for (Object __dummyForeachVar0 : copy)
        {
            TickEventHandler d = (TickEventHandler)__dummyForeachVar0;
            d.invoke(sender, args);
        }
    }

    private System.Collections.Generic.IList<TickEventHandler> _invocationList = new ArrayList<TickEventHandler>();
    public static TickEventHandler combine(TickEventHandler a, TickEventHandler b) throws Exception {
        if (a == null)
            return b;
         
        if (b == null)
            return a;
         
        __MultiTickEventHandler ret = new __MultiTickEventHandler();
        ret._invocationList = a.getInvocationList();
        ret._invocationList.addAll(b.getInvocationList());
        return ret;
    }

    public static TickEventHandler remove(TickEventHandler a, TickEventHandler b) throws Exception {
        if (a == null || b == null)
            return a;
         
        System.Collections.Generic.IList<TickEventHandler> aInvList = a.getInvocationList();
        System.Collections.Generic.IList<TickEventHandler> newInvList = ListSupport.removeFinalStretch(aInvList, b.getInvocationList());
        if (aInvList == newInvList)
        {
            return a;
        }
        else
        {
            __MultiTickEventHandler ret = new __MultiTickEventHandler();
            ret._invocationList = newInvList;
            return ret;
        } 
    }

    public System.Collections.Generic.IList<TickEventHandler> getInvocationList() throws Exception {
        return _invocationList;
    }

}


