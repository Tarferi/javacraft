//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:12
//

package SCSharpMac.UI;

import CS2JNet.JavaSupport.util.ListSupport;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import SCSharp.BinElement;
import SCSharpMac.UI.ComboBoxElementDropdownLayerDelegate;
import SCSharpMac.UI.ComboBoxElementLayerDelegate;
import SCSharpMac.UI.ComboBoxSelectionChanged;
import SCSharpMac.UI.UIElement;
import SCSharpMac.UI.UIScreen;

//
// SCSharpMac.UI.ComboBoxElement
//
// Authors:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
public class ComboBoxElement  extends UIElement 
{
    List<String> items = new List<String>();
    int cursor = -1;
    CALayer dropdownLayer = new CALayer();
    int dropdown_hover_index = new int();
    public ComboBoxElement(UIScreen screen, BinElement el, byte[] palette) throws Exception {
        super(screen, el, palette);
        items = new List<String>();
    }

    public int getSelectedIndex() throws Exception {
        return cursor;
    }

    public void setSelectedIndex(int value) throws Exception {
        cursor = value;
        invalidate();
    }

    public int getDropdownHoverIndex() throws Exception {
        return dropdown_hover_index;
    }

    public String getSelectedItem() throws Exception {
        return items[cursor];
    }

    public List<String> getItems() throws Exception {
        return items;
    }

    public void addItem(String item) throws Exception {
        addItem(item,true);
    }

    public void addItem(String item, boolean select) throws Exception {
        items.Add(item);
        if (select || cursor == -1)
            cursor = items.IndexOf(item);
         
    }

    public void removeAt(int index) throws Exception {
        items.RemoveAt(index);
        if (items.Count == 0)
            cursor = -1;
         
    }

    public void clear() throws Exception {
        items.Clear();
        cursor = -1;
    }

    public boolean contains(String item) throws Exception {
        return items.Contains(item);
    }

    public void mouseButtonDown(NSEvent theEvent) throws Exception {
        Console.WriteLine("Showing dropdown");
        showDropdown();
    }

    public void mouseButtonUp(NSEvent theEvent) throws Exception {
        Console.WriteLine("Hiding dropdown");
        hideDropdown();
    }

    public void pointerMotion(NSEvent theEvent) throws Exception {
        /* if the dropdown is visible, see if we're inside it */
        if (dropdownLayer.Hidden)
            return ;
         
        PointF p = new PointF(theEvent.LocationInWindow.Y - dropdownLayer.Position.Y, theEvent.LocationInWindow.Y - dropdownLayer.Position.Y);
        Console.WriteLine("point relative to dropdownlayer = {0}", p);
        /*
        			      starcraft doesn't include this check..  should we?
        			      p.X >= 0 && p.X < dropdownLayer.Bounds.Width &&
        			    */
        if (p.Y >= 0 && p.Y < dropdownLayer.Bounds.Height)
        {
            int new_hover_index = (int)((dropdownLayer.Bounds.Height - p.Y) / getFont().getLineSize());
            Console.WriteLine("new_hover_item = {0}", new_hover_index);
            if (dropdown_hover_index != new_hover_index)
            {
                dropdown_hover_index = new_hover_index;
                dropdownLayer.SetNeedsDisplay();
            }
             
        }
         
    }

    void showDropdown() throws Exception {
        dropdown_hover_index = cursor;
        if (dropdownLayer == null)
            createDropdownLayer();
         
        dropdownLayer.Hidden = false;
        dropdownLayer.AnchorPoint = new PointF(0, 0);
        dropdownLayer.Position = new PointF(getX1(), getLayer().Position.Y - dropdownLayer.Bounds.Height);
    }

    void hideDropdown() throws Exception {
        if (dropdownLayer == null)
            createDropdownLayer();
         
        dropdownLayer.Hidden = true;
        if (cursor != dropdown_hover_index)
        {
            cursor = dropdown_hover_index;
            invalidate();
            if (SelectionChanged != null)
                SelectionChanged.invoke(cursor);
             
        }
         
    }

    protected CALayer createLayer() throws Exception {
        CALayer layer = CALayer.Create();
        layer.Bounds = new RectangleF(0, 0, getWidth(), getHeight());
        layer.Delegate = new ComboBoxElementLayerDelegate(this);
        layer.BorderWidth = 1;
        layer.BorderColor = new CGColor(1, 0, 0, 1);
        return layer;
    }

    void createDropdownLayer() throws Exception {
        dropdownLayer = CALayer.Create();
        dropdownLayer.Bounds = new RectangleF(0, 0, getWidth(), items.Count * getFont().getLineSize());
        dropdownLayer.Delegate = new ComboBoxElementDropdownLayerDelegate(this);
        dropdownLayer.BackgroundColor = new CGColor(0, 0, 0, 1);
        dropdownLayer.BorderWidth = 1;
        dropdownLayer.BorderColor = new CGColor(1, 1, 0, 1);
        dropdownLayer.SetNeedsDisplay();
        getParentScreen().AddSublayer(dropdownLayer);
    }

    public ComboBoxSelectionChanged SelectionChanged;
}


