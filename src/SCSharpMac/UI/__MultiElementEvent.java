//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:15
//

package SCSharpMac.UI;

import CS2JNet.JavaSupport.util.ListSupport;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import SCSharpMac.UI.__MultiElementEvent;
import SCSharpMac.UI.ElementEvent;

public class __MultiElementEvent   implements ElementEvent
{
    public void invoke() throws Exception {
        IList<ElementEvent> copy = new IList<ElementEvent>(), members = this.getInvocationList();
        synchronized (members)
        {
            copy = new LinkedList<ElementEvent>(members);
        }
        for (Object __dummyForeachVar0 : copy)
        {
            ElementEvent d = (ElementEvent)__dummyForeachVar0;
            d.invoke();
        }
    }

    private System.Collections.Generic.IList<ElementEvent> _invocationList = new ArrayList<ElementEvent>();
    public static ElementEvent combine(ElementEvent a, ElementEvent b) throws Exception {
        if (a == null)
            return b;
         
        if (b == null)
            return a;
         
        __MultiElementEvent ret = new __MultiElementEvent();
        ret._invocationList = a.getInvocationList();
        ret._invocationList.addAll(b.getInvocationList());
        return ret;
    }

    public static ElementEvent remove(ElementEvent a, ElementEvent b) throws Exception {
        if (a == null || b == null)
            return a;
         
        System.Collections.Generic.IList<ElementEvent> aInvList = a.getInvocationList();
        System.Collections.Generic.IList<ElementEvent> newInvList = ListSupport.removeFinalStretch(aInvList, b.getInvocationList());
        if (aInvList == newInvList)
        {
            return a;
        }
        else
        {
            __MultiElementEvent ret = new __MultiElementEvent();
            ret._invocationList = newInvList;
            return ret;
        } 
    }

    public System.Collections.Generic.IList<ElementEvent> getInvocationList() throws Exception {
        return _invocationList;
    }

}


