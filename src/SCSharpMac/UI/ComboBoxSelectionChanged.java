//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:12
//

package SCSharpMac.UI;

import CS2JNet.JavaSupport.util.ListSupport;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import SCSharpMac.UI.ComboBoxSelectionChanged;

public interface ComboBoxSelectionChanged   
{
    void invoke(int selectedIndex) throws Exception ;

    System.Collections.Generic.IList<ComboBoxSelectionChanged> getInvocationList() throws Exception ;

}


