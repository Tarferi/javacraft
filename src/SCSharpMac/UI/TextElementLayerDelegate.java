//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:15
//

package SCSharpMac.UI;

import SCSharpMac.UI.GuiUtil;
import SCSharpMac.UI.TextBoxElement;

public class TextElementLayerDelegate  extends CALayerDelegate 
{
    TextBoxElement el;
    public TextElementLayerDelegate(TextBoxElement el) throws Exception {
        this.el = el;
    }

    public void drawLayer(CALayer layer, CGContext context) throws Exception {
        GuiUtil.renderTextToContext(context,new PointF(0, el.getHeight() / 2),el.getText(),el.getFont(),el.getPalette(),el.getSensitive() ? 4 : 24);
    }

}


