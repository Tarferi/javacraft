//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:15
//

package SCSharpMac.UI;

import CS2JNet.JavaSupport.language.RefSupport;
import CS2JNet.JavaSupport.language.ReturnPreOrPostValue;
import SCSharp.Grp;
import SCSharp.Mpq;
import SCSharp.Util;
import SCSharpMac.UI.AnimationType;
import SCSharpMac.UI.GlobalResources;
import SCSharpMac.UI.Sprite;
import SCSharpMac.UI.SpriteManager;

//
// SCSharp.UI.Sprite
//
// Authors:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
/* opcode descriptions and info here 
http://www.campaigncreations.org/starcraft/bible/chap4_ice_opcodes.shtml
http://www.stormcoast-fortress.net/cntt/tutorials/camsys/tilesetdependent/?PHPSESSID=7365d884cf33fc614c0b96d966872177
*/
public class Sprite   
{
    static Random rng = new Random(Environment.TickCount);
    Sprite dependentSprite;
    ushort images_entry = new ushort();
    String grp_path = new String();
    Grp grp;
    byte[] palette = new byte[]();
    byte[] buf = new byte[]();
    ushort script_start = new ushort();
    /* the pc of the script that started us off */
    ushort pc = new ushort();
    uint iscript_entry = new uint();
    ushort script_entry_offset = new ushort();
    int current_frame = -1;
    int facing = 0;
    boolean trace = true;
    static boolean show_sprite_borders = new boolean();
    Mpq mpq;
    int x = new int();
    int y = new int();
    Sprite parent_sprite;
    static {
        try
        {
        }
        catch (Exception __dummyStaticConstructorCatchVar0)
        {
            throw new ExceptionInInitializerError(__dummyStaticConstructorCatchVar0);
        }
    
    }

    public Sprite(Mpq mpq, int sprite_entry, byte[] palette, int x, int y) throws Exception {
        this.mpq = mpq;
        this.palette = palette;
        images_entry = GlobalResources.getInstance().getSpritesDat().getImagesDatEntries().get___idx(sprite_entry);
        //			Console.WriteLine ("image_dat_entry == {0}", images_entry);
        uint grp_index = GlobalResources.getInstance().getImagesDat().getGrpIndexes()[images_entry];
        //			Console.WriteLine ("grp_index = {0}", grp_index);
        grp_path = GlobalResources.getInstance().getImagesTbl().get___idx((int)grp_index - 1);
        //			Console.WriteLine ("grp_path = {0}", grp_path);
        grp = (Grp)mpq.getResource("unit\\" + grp_path);
        iscript_entry = GlobalResources.getInstance().getImagesDat().getIScriptIndexes()[images_entry];
        script_entry_offset = GlobalResources.getInstance().getIScriptBin().getScriptEntryOffset(iscript_entry);
        Console.WriteLine("new sprite: unit\\{0} @ {1}x{2} (image {3}, iscript id {4}, script_entry_offset {5:X})", grp_path, x, y, images_entry, iscript_entry, script_entry_offset);
        this.buf = GlobalResources.getInstance().getIScriptBin().getContents();
        /* make sure the offset points to "SCPE" */
        if (Util.ReadDWord(buf, script_entry_offset) != 0x45504353)
            Console.WriteLine("invalid script_entry_offset");
         
        setPosition(x,y);
    }

    public Sprite(Sprite parentSprite, ushort images_entry, byte[] palette) throws Exception {
        this.parent_sprite = parentSprite;
        this.mpq = parentSprite.mpq;
        this.palette = palette;
        this.images_entry = images_entry;
        uint grp_index = GlobalResources.getInstance().getImagesDat().getGrpIndexes()[images_entry];
        grp_path = GlobalResources.getInstance().getImagesTbl().get___idx((int)grp_index - 1);
        grp = (Grp)mpq.getResource("unit\\" + grp_path);
        this.buf = GlobalResources.getInstance().getIScriptBin().getContents();
        iscript_entry = GlobalResources.getInstance().getImagesDat().getIScriptIndexes()[images_entry];
        script_entry_offset = GlobalResources.getInstance().getIScriptBin().getScriptEntryOffset(iscript_entry);
        Console.WriteLine("new dependent sprite: unit\\{0} (image {1}, iscript id {2}, script_entry_offset {3:X})", grp_path, images_entry, iscript_entry, script_entry_offset);
        /* make sure the offset points to "SCEP" */
        if (Util.ReadDWord(buf, script_entry_offset) != 0x45504353)
            Console.WriteLine("invalid script_entry_offset");
         
        int x = new int(), y = new int();
        RefSupport<int> refVar___0 = new RefSupport<int>();
        RefSupport<int> refVar___1 = new RefSupport<int>();
        parentSprite.getPosition(refVar___0,refVar___1);
        x = refVar___0.getValue();
        y = refVar___1.getValue();
        setPosition(x,y);
    }

    public enum IScriptOpcode
    {
        playfram,
        // short frame */
        playframtile,
        // short frame */
        sethorpos,
        // byte byte   */
        setvertpos,
        // byte byte */
        setpos,
        // byte byte, byte byte */
        wait,
        // byte byte */
        waitrand,
        // byte byte, byte byte */
        _goto,
        // short label */
        imgol,
        // short imageid, byte byte, byte byte */
        imgul,
        // short imageid, byte byte, byte byte */
        imgolorig,
        // short imageid */
        switchul,
        // short imageid */
        __0c,
        imgoluselo,
        // short imageid, byte byte, byte byte */
        imguluselo,
        // short imageid, byte byte, byte byte
        sprol,
        //   short spriteid, byte byte, byte byte
        highsprol,
        //   short spriteid, byte byte, byte byte
        lowsprul,
        //   short spriteid, byte byte, byte byte
        uflunstable,
        //   short flingy
        spruluselo,
        //   short spriteid, byte byte, byte byte
        sprul,
        //   short spriteid, byte byte, byte byte
        sproluselo,
        //   short spriteid, byte overlayid
        end,
        setflipstate,
        //   byte flipstate
        playsnd,
        //   short soundid
        playsndrand,
        //   byte sounds(, short soundid)*sounds
        playsndbtwn,
        //   short soundid, short soundid
        domissiledmg,
        attackmelee,
        //   byte sounds(, short soundid)*sounds
        followmaingraphic,
        randcondjmp,
        //   byte byte, short label
        turnccwise,
        //   byte byte
        turncwise,
        //   byte byte
        turnlcwise,
        turnrand,
        //   byte byte
        setspawnframe,
        //   byte byte
        sigorder,
        //   byte signalid
        attackwith,
        //   byte weapon
        attack,
        castspell,
        useweapon,
        //   byte weaponid
        move,
        //   byte byte
        gotorepeatattk,
        engframe,
        //   short frame
        engset,
        //   short frame
        __2d,
        nobrkcodestart,
        nobrkcodeend,
        ignorerest,
        attkshiftproj,
        //   byte byte
        tmprmgraphicstart,
        tmprmgraphicend,
        setfldirect,
        //   byte byte
        call,
        //   short label
        _return,
        setflspeed,
        //   short speed
        creategasoverlays,
        //   byte gasoverlay
        pwrupcondjmp,
        //   short label
        trgtrangecondjmp,
        //   short short, short label
        trgtarccondjmp,
        //   short short, short short, short label
        curdirectcondjmp,
        //   short short, short short, short label
        imgulnextid,
        //   byte byte, byte byte
        __3e,
        liftoffcondjmp,
        //   short label
        warpoverlay,
        //   short frame
        orderdone,
        //   byte signalid
        grdsprol,
        //   short spriteid, byte byte, byte byte
        __43,
        dogrddamage
    }
    void trace(String fmt, Object... args) throws Exception {
        if (trace)
        {
            String msg = String.Format(fmt, args);
            Console.Write("{0}: {1}", GetHashCode(), msg);
        }
         
    }

    void traceLine(String fmt, Object... args) throws Exception {
        if (trace)
        {
            String msg = String.Format(fmt, args);
            Console.WriteLine("{0}: {1}", GetHashCode(), msg);
        }
         
    }

    public boolean getDebug() throws Exception {
        return trace;
    }

    public void setDebug(boolean value) throws Exception {
        trace = value;
    }

    public Mpq getMpq() throws Exception {
        return mpq;
    }

    public int getCurrentFrame() throws Exception {
        return current_frame;
    }

    public void setPosition(int x, int y) throws Exception {
        this.x = x;
        this.y = y;
        if (dependentSprite != null)
            dependentSprite.setPosition(x,y);
         
    }

    public void getPosition(RefSupport<int> xo, RefSupport<int> yo) throws Exception {
        xo.setValue(this.x);
        yo.setValue(this.y);
    }

    public void getTopLeftPosition(RefSupport<int> xo, RefSupport<int> yo) throws Exception {
        xo.setValue(this.x);
        yo.setValue(this.y);
        if (sprite_layer != null)
        {
            xo.setValue(xo.getValue() - (int)(sprite_layer.Bounds.Width / 2));
            yo.setValue(yo.getValue() - (int)(sprite_layer.Bounds.Height / 2));
        }
         
    }

    public void face(int facing) throws Exception {
        this.facing = facing;
    }

    public void runScript(ushort script_start) throws Exception {
        this.script_start = script_start;
        pc = script_start;
        if (dependentSprite != null)
            dependentSprite.runScript(script_start);
         
    }

    public void runScript(AnimationType animationType) throws Exception {
        traceLine("running script type = {0}",animationType);
        /* "SCEP" */
        /* the script entry "type" */
        /* the spacers */
        int offset_to_script_type = (4 + 1 + 3 + ((Enum)animationType).ordinal() * 2);
        script_start = Util.ReadWord(buf, script_entry_offset + offset_to_script_type);
        pc = script_start;
        traceLine("pc = {0}",pc);
    }

    ushort readWord(RefSupport<ushort> pc) throws Exception {
        ushort retval = Util.ReadWord(buf, pc.getValue());
        pc.setValue(pc.getValue() + 2);
        return retval;
    }

    byte readByte(RefSupport<ushort> pc) throws Exception {
        byte retval = buf[pc.getValue()];
        pc.setValue(pc.getValue() + 1, ReturnPreOrPostValue.POST);
        return retval;
    }

    CALayer sprite_layer = new CALayer();
    public void addToPainter() throws Exception {
    }

    // Painter.Add (Layer.Unit, PaintSprite);
    public void removeFromPainter() throws Exception {
    }

    // Painter.Remove (Layer.Unit, PaintSprite);
    void doPlayFrame(int frame_num) throws Exception {
        if (current_frame != frame_num)
        {
            current_frame = frame_num;
        }
         
    }

    public void invalidate() throws Exception {
    }

    int waiting = new int();
    public boolean tick(int millis_elapsed) throws Exception {
        ushort warg1 = new ushort();
        ushort warg2 = new ushort();
        ushort warg3 = new ushort();
        byte barg1 = new byte();
        byte barg2 = new byte();
        //			byte barg3;
        if (pc == 0)
            return true;
         
        if (waiting > 0)
        {
            waiting--;
            return true;
        }
         
        trace("{0}: ",pc);
        switch((IScriptOpcode)buf[pc++])
        {
            case playfram: 
                RefSupport refVar___2 = new RefSupport(pc);
                warg1 = readWord(refVar___2);
                pc = refVar___2.getValue();
                traceLine("playfram: {0}",warg1);
                DoPlayFrame(warg1 + facing % 16);
                break;
            case playframtile: 
                RefSupport refVar___3 = new RefSupport(pc);
                warg1 = readWord(refVar___3);
                pc = refVar___3.getValue();
                traceLine("playframetile: {0}",warg1);
                break;
            case sethorpos: 
                RefSupport refVar___4 = new RefSupport(pc);
                barg1 = readByte(refVar___4);
                pc = refVar___4.getValue();
                traceLine("sethorpos: {0}",barg1);
                break;
            case setpos: 
                RefSupport refVar___5 = new RefSupport(pc);
                barg1 = readByte(refVar___5);
                pc = refVar___5.getValue();
                RefSupport refVar___6 = new RefSupport(pc);
                barg2 = readByte(refVar___6);
                pc = refVar___6.getValue();
                traceLine("setpos: {0} {1}",barg1,barg2);
                break;
            case setvertpos: 
                RefSupport refVar___7 = new RefSupport(pc);
                barg1 = readByte(refVar___7);
                pc = refVar___7.getValue();
                traceLine("setvertpos: {0}",barg1);
                break;
            case wait: 
                RefSupport refVar___8 = new RefSupport(pc);
                barg1 = readByte(refVar___8);
                pc = refVar___8.getValue();
                traceLine("wait: {0}",barg1);
                waiting = barg1;
                break;
            case waitrand: 
                RefSupport refVar___9 = new RefSupport(pc);
                barg1 = readByte(refVar___9);
                pc = refVar___9.getValue();
                RefSupport refVar___10 = new RefSupport(pc);
                barg2 = readByte(refVar___10);
                pc = refVar___10.getValue();
                traceLine("waitrand: {0} {1}",barg1,barg2);
                waiting = rng.Next(255) > 127 ? barg1 : barg2;
                break;
            case _goto: 
                RefSupport refVar___11 = new RefSupport(pc);
                warg1 = readWord(refVar___11);
                pc = refVar___11.getValue();
                traceLine("goto: {0}",warg1);
                pc = warg1;
                break;
            case imgol: 
                RefSupport refVar___12 = new RefSupport(pc);
                warg1 = readWord(refVar___12);
                pc = refVar___12.getValue();
                RefSupport refVar___13 = new RefSupport(pc);
                barg1 = readByte(refVar___13);
                pc = refVar___13.getValue();
                RefSupport refVar___14 = new RefSupport(pc);
                barg2 = readByte(refVar___14);
                pc = refVar___14.getValue();
                traceLine("imgol: {0} {1} {2}",warg1,barg1,barg2);
                break;
            case imgul: 
                RefSupport refVar___15 = new RefSupport(pc);
                warg1 = readWord(refVar___15);
                pc = refVar___15.getValue();
                RefSupport refVar___16 = new RefSupport(pc);
                barg1 = readByte(refVar___16);
                pc = refVar___16.getValue();
                RefSupport refVar___17 = new RefSupport(pc);
                barg2 = readByte(refVar___17);
                pc = refVar___17.getValue();
                traceLine("imgul: {0} {1} {2}",warg1,barg1,barg2);
                Sprite dependent_sprite = SpriteManager.createSprite(this,warg1,palette);
                dependent_sprite.runScript(AnimationType.Init);
                break;
            case imgolorig: 
                RefSupport refVar___18 = new RefSupport(pc);
                warg1 = readWord(refVar___18);
                pc = refVar___18.getValue();
                traceLine("imgolorig: {0}",warg1);
                break;
            case switchul: 
                RefSupport refVar___19 = new RefSupport(pc);
                warg1 = readWord(refVar___19);
                pc = refVar___19.getValue();
                traceLine("switchul: {0}",warg1);
                break;
            case imgoluselo: 
                // __0c unknown
                RefSupport refVar___20 = new RefSupport(pc);
                warg1 = readWord(refVar___20);
                pc = refVar___20.getValue();
                RefSupport refVar___21 = new RefSupport(pc);
                barg1 = readByte(refVar___21);
                pc = refVar___21.getValue();
                RefSupport refVar___22 = new RefSupport(pc);
                barg2 = readByte(refVar___22);
                pc = refVar___22.getValue();
                traceLine("imgoluselo: {0} {1} {2}",warg1,barg1,barg2);
                break;
            case imguluselo: 
                RefSupport refVar___23 = new RefSupport(pc);
                warg1 = readWord(refVar___23);
                pc = refVar___23.getValue();
                RefSupport refVar___24 = new RefSupport(pc);
                barg1 = readByte(refVar___24);
                pc = refVar___24.getValue();
                RefSupport refVar___25 = new RefSupport(pc);
                barg2 = readByte(refVar___25);
                pc = refVar___25.getValue();
                traceLine("imguluselo: {0} {1} {2}",warg1,barg1,barg2);
                break;
            case sprol: 
                RefSupport refVar___26 = new RefSupport(pc);
                warg1 = readWord(refVar___26);
                pc = refVar___26.getValue();
                RefSupport refVar___27 = new RefSupport(pc);
                barg1 = readByte(refVar___27);
                pc = refVar___27.getValue();
                RefSupport refVar___28 = new RefSupport(pc);
                barg2 = readByte(refVar___28);
                pc = refVar___28.getValue();
                traceLine("sprol: {0} {1} {2}",warg1,barg1,barg2);
                break;
            case highsprol: 
                RefSupport refVar___29 = new RefSupport(pc);
                warg1 = readWord(refVar___29);
                pc = refVar___29.getValue();
                RefSupport refVar___30 = new RefSupport(pc);
                barg1 = readByte(refVar___30);
                pc = refVar___30.getValue();
                RefSupport refVar___31 = new RefSupport(pc);
                barg2 = readByte(refVar___31);
                pc = refVar___31.getValue();
                traceLine("highsprol: {0} {1} {2}",warg1,barg1,barg2);
                break;
            case lowsprul: 
                RefSupport refVar___32 = new RefSupport(pc);
                warg1 = readWord(refVar___32);
                pc = refVar___32.getValue();
                RefSupport refVar___33 = new RefSupport(pc);
                barg1 = readByte(refVar___33);
                pc = refVar___33.getValue();
                RefSupport refVar___34 = new RefSupport(pc);
                barg2 = readByte(refVar___34);
                pc = refVar___34.getValue();
                traceLine("lowsprul: {0} ({1},{2})",warg1,barg1,barg2);
                Sprite s = SpriteManager.CreateSprite(warg1, palette, x, y);
                s.runScript(AnimationType.Init);
                dependentSprite = s;
                break;
                RefSupport refVar___35 = new RefSupport(pc);
                warg1 = readWord(refVar___35);
                pc = refVar___35.getValue();
                RefSupport refVar___36 = new RefSupport(pc);
                barg1 = readByte(refVar___36);
                pc = refVar___36.getValue();
                RefSupport refVar___37 = new RefSupport(pc);
                barg2 = readByte(refVar___37);
                pc = refVar___37.getValue();
                traceLine("lowsprul: {0} {1} {2}",warg1,barg1,barg2);
                break;
            case uflunstable: 
                RefSupport refVar___38 = new RefSupport(pc);
                warg1 = readWord(refVar___38);
                pc = refVar___38.getValue();
                traceLine("uflunstable: {0}",warg1);
                break;
            case spruluselo: 
                RefSupport refVar___39 = new RefSupport(pc);
                warg1 = readWord(refVar___39);
                pc = refVar___39.getValue();
                RefSupport refVar___40 = new RefSupport(pc);
                barg1 = readByte(refVar___40);
                pc = refVar___40.getValue();
                RefSupport refVar___41 = new RefSupport(pc);
                barg2 = readByte(refVar___41);
                pc = refVar___41.getValue();
                traceLine("spruluselo: {0} {1} {2}",warg1,barg1,barg2);
                break;
            case sprul: 
                RefSupport refVar___42 = new RefSupport(pc);
                warg1 = readWord(refVar___42);
                pc = refVar___42.getValue();
                RefSupport refVar___43 = new RefSupport(pc);
                barg1 = readByte(refVar___43);
                pc = refVar___43.getValue();
                RefSupport refVar___44 = new RefSupport(pc);
                barg2 = readByte(refVar___44);
                pc = refVar___44.getValue();
                traceLine("sprul: {0} {1} {2}",warg1,barg1,barg2);
                break;
            case sproluselo: 
                RefSupport refVar___45 = new RefSupport(pc);
                warg1 = readWord(refVar___45);
                pc = refVar___45.getValue();
                RefSupport refVar___46 = new RefSupport(pc);
                barg1 = readByte(refVar___46);
                pc = refVar___46.getValue();
                RefSupport refVar___47 = new RefSupport(pc);
                barg2 = readByte(refVar___47);
                pc = refVar___47.getValue();
                traceLine("sproleuselo: {0} {1} {2}",warg1,barg1,barg2);
                break;
            case end: 
                traceLine("end");
                return false;
            case setflipstate: 
                RefSupport refVar___48 = new RefSupport(pc);
                barg1 = readByte(refVar___48);
                pc = refVar___48.getValue();
                traceLine("setflipstate: {0}",barg1);
                break;
            case playsnd: 
                RefSupport refVar___49 = new RefSupport(pc);
                warg1 = readWord(refVar___49);
                pc = refVar___49.getValue();
                traceLine("playsnd: {0} ({1})",warg1 - 1,GlobalResources.getInstance().getSfxDataTbl().get___idx((int)GlobalResources.getInstance().getSfxDataDat().getFileIndexes()[warg1 - 1]));
                break;
            case playsndrand: 
                {
                    RefSupport refVar___50 = new RefSupport(pc);
                    barg1 = readByte(refVar___50);
                    pc = refVar___50.getValue();
                    ushort[] wargs = new ushort[barg1];
                    for (byte b = 0;b < barg1;b++)
                    {
                        RefSupport refVar___51 = new RefSupport(pc);
                        wargs[b] = readWord(refVar___51);
                        pc = refVar___51.getValue();
                    }
                    trace("playsndrand: {0} (");
                    for (int i = 0;i < wargs.Length;i++)
                    {
                        trace("{0}",wargs[i]);
                        if (i < wargs.Length - 1)
                            trace(", ");
                         
                    }
                    traceLine(")");
                    break;
                }
            case playsndbtwn: 
                RefSupport refVar___52 = new RefSupport(pc);
                warg1 = readWord(refVar___52);
                pc = refVar___52.getValue();
                RefSupport refVar___53 = new RefSupport(pc);
                warg2 = readWord(refVar___53);
                pc = refVar___53.getValue();
                traceLine("playsndbtwn: {0} {1}",warg1,warg2);
                break;
            case domissiledmg: 
                traceLine("domissiledmg: unknown args");
                break;
            case attackmelee: 
                {
                    RefSupport refVar___54 = new RefSupport(pc);
                    barg1 = readByte(refVar___54);
                    pc = refVar___54.getValue();
                    ushort[] wargs = new ushort[barg1];
                    for (byte b = 0;b < barg1;b++)
                    {
                        RefSupport refVar___55 = new RefSupport(pc);
                        wargs[b] = readWord(refVar___55);
                        pc = refVar___55.getValue();
                    }
                    trace("attackmelee: {0} (");
                    for (int i = 0;i < wargs.Length;i++)
                    {
                        trace("{0}",wargs[i]);
                        if (i < wargs.Length - 1)
                            trace(", ");
                         
                    }
                    traceLine(")");
                    break;
                }
            case followmaingraphic: 
                traceLine("followmaingraphic:");
                if (parent_sprite != null)
                    doPlayFrame(parent_sprite.getCurrentFrame());
                 
                break;
            case randcondjmp: 
                RefSupport refVar___56 = new RefSupport(pc);
                barg1 = readByte(refVar___56);
                pc = refVar___56.getValue();
                RefSupport refVar___57 = new RefSupport(pc);
                warg1 = readWord(refVar___57);
                pc = refVar___57.getValue();
                traceLine("randcondjmp: {0} {1}",barg1,warg1);
                int rand = rng.Next(255);
                if (rand > barg1)
                {
                    traceLine("+ choosing goto branch");
                    pc = warg1;
                }
                 
                break;
            case turnccwise: 
                RefSupport refVar___58 = new RefSupport(pc);
                barg1 = readByte(refVar___58);
                pc = refVar___58.getValue();
                traceLine("turnccwise: {0}",barg1);
                if (facing - barg1 < 0)
                    facing = 15 - barg1;
                else
                    facing -= barg1; 
                break;
            case turncwise: 
                RefSupport refVar___59 = new RefSupport(pc);
                barg1 = readByte(refVar___59);
                pc = refVar___59.getValue();
                traceLine("turncwise: {0}",barg1);
                if (facing + barg1 > 15)
                    facing = facing + barg1 - 15;
                else
                    facing += barg1; 
                break;
            case turnlcwise: 
                traceLine("turnlcwise: unknown args");
                break;
            case turnrand: 
                traceLine("turnrand:");
                if (rng.Next(255) > 127)
                    goto caseIScriptOpcode.turnccwise
                else
                    goto caseIScriptOpcode.turncwise 
                break;
            case setspawnframe: 
                RefSupport refVar___60 = new RefSupport(pc);
                barg1 = readByte(refVar___60);
                pc = refVar___60.getValue();
                traceLine("setspawnframe {0}",barg1);
                break;
            case sigorder: 
                RefSupport refVar___61 = new RefSupport(pc);
                barg1 = readByte(refVar___61);
                pc = refVar___61.getValue();
                traceLine("sigorder {0}",barg1);
                break;
            case attackwith: 
                RefSupport refVar___62 = new RefSupport(pc);
                barg1 = readByte(refVar___62);
                pc = refVar___62.getValue();
                traceLine("attackwith {0}",barg1);
                break;
            case attack: 
                traceLine("attack:");
                break;
            case castspell: 
                traceLine("castspell:");
                break;
            case useweapon: 
                RefSupport refVar___63 = new RefSupport(pc);
                barg1 = readByte(refVar___63);
                pc = refVar___63.getValue();
                traceLine("useweapon: {0}",barg1);
                break;
            case move: 
                RefSupport refVar___64 = new RefSupport(pc);
                barg1 = readByte(refVar___64);
                pc = refVar___64.getValue();
                traceLine("move: {0}",barg1);
                break;
            case gotorepeatattk: 
                traceLine("gotorepeatattk");
                break;
            case engframe: 
                RefSupport refVar___65 = new RefSupport(pc);
                warg1 = readWord(refVar___65);
                pc = refVar___65.getValue();
                traceLine("engframe: {0}",warg1);
                break;
            case engset: 
                RefSupport refVar___66 = new RefSupport(pc);
                warg1 = readWord(refVar___66);
                pc = refVar___66.getValue();
                traceLine("engset: {0}",warg1);
                break;
            case nobrkcodestart: 
                // __2d unknown
                traceLine("nobrkcodestart:");
                break;
            case nobrkcodeend: 
                traceLine("nobrkcodeend:");
                break;
            case ignorerest: 
                traceLine("ignorerest");
                break;
            case attkshiftproj: 
                RefSupport refVar___67 = new RefSupport(pc);
                barg1 = readByte(refVar___67);
                pc = refVar___67.getValue();
                traceLine("attkshiftproj: {0}",barg1);
                break;
            case tmprmgraphicstart: 
                traceLine("tmprmgraphicstart:");
                break;
            case tmprmgraphicend: 
                traceLine("tmprmgraphicend:");
                break;
            case setfldirect: 
                RefSupport refVar___68 = new RefSupport(pc);
                barg1 = readByte(refVar___68);
                pc = refVar___68.getValue();
                traceLine("setfldirect: {0}",barg1);
                DoPlayFrame(barg1);
                break;
            case call: 
                RefSupport refVar___69 = new RefSupport(pc);
                warg1 = readWord(refVar___69);
                pc = refVar___69.getValue();
                traceLine("call: {0}",warg1);
                break;
            case _return: 
                traceLine("return:");
                break;
            case setflspeed: 
                RefSupport refVar___70 = new RefSupport(pc);
                barg1 = readByte(refVar___70);
                pc = refVar___70.getValue();
                traceLine("setflspeed: {0}",barg1);
                break;
            case creategasoverlays: 
                RefSupport refVar___71 = new RefSupport(pc);
                barg1 = readByte(refVar___71);
                pc = refVar___71.getValue();
                traceLine("creategasoverlays: {0}",barg1);
                break;
            case pwrupcondjmp: 
                RefSupport refVar___72 = new RefSupport(pc);
                warg1 = readWord(refVar___72);
                pc = refVar___72.getValue();
                traceLine("pwrupcondjmp: {0}",warg1);
                break;
            case trgtrangecondjmp: 
                RefSupport refVar___73 = new RefSupport(pc);
                warg1 = readWord(refVar___73);
                pc = refVar___73.getValue();
                RefSupport refVar___74 = new RefSupport(pc);
                warg2 = readWord(refVar___74);
                pc = refVar___74.getValue();
                traceLine("trgtrangecondjmp {0} {1}",warg1,warg2);
                break;
            case trgtarccondjmp: 
                RefSupport refVar___75 = new RefSupport(pc);
                warg1 = readWord(refVar___75);
                pc = refVar___75.getValue();
                RefSupport refVar___76 = new RefSupport(pc);
                warg2 = readWord(refVar___76);
                pc = refVar___76.getValue();
                RefSupport refVar___77 = new RefSupport(pc);
                warg3 = readWord(refVar___77);
                pc = refVar___77.getValue();
                traceLine("trgtarccondjmp {0} {1} {2}",warg1,warg2,warg3);
                break;
            case curdirectcondjmp: 
                RefSupport refVar___78 = new RefSupport(pc);
                warg1 = readWord(refVar___78);
                pc = refVar___78.getValue();
                RefSupport refVar___79 = new RefSupport(pc);
                warg2 = readWord(refVar___79);
                pc = refVar___79.getValue();
                RefSupport refVar___80 = new RefSupport(pc);
                warg3 = readWord(refVar___80);
                pc = refVar___80.getValue();
                traceLine("curdirectcondjmp {0} {1} {2}",warg1,warg2,warg3);
                break;
            case imgulnextid: 
                RefSupport refVar___81 = new RefSupport(pc);
                barg1 = readByte(refVar___81);
                pc = refVar___81.getValue();
                RefSupport refVar___82 = new RefSupport(pc);
                barg2 = readByte(refVar___82);
                pc = refVar___82.getValue();
                traceLine("imgulnextid {0} {1}",barg1,barg2);
                break;
            case liftoffcondjmp: 
                // __3e unknown
                RefSupport refVar___83 = new RefSupport(pc);
                warg1 = readWord(refVar___83);
                pc = refVar___83.getValue();
                traceLine("liftoffcondjmp {0}",warg1);
                break;
            case warpoverlay: 
                RefSupport refVar___84 = new RefSupport(pc);
                warg1 = readWord(refVar___84);
                pc = refVar___84.getValue();
                traceLine("warpoverlay {0}",warg1);
                break;
            case orderdone: 
                RefSupport refVar___85 = new RefSupport(pc);
                barg1 = readByte(refVar___85);
                pc = refVar___85.getValue();
                traceLine("orderdone {0}",barg1);
                break;
            case grdsprol: 
                RefSupport refVar___86 = new RefSupport(pc);
                warg1 = readWord(refVar___86);
                pc = refVar___86.getValue();
                RefSupport refVar___87 = new RefSupport(pc);
                barg1 = readByte(refVar___87);
                pc = refVar___87.getValue();
                RefSupport refVar___88 = new RefSupport(pc);
                barg2 = readByte(refVar___88);
                pc = refVar___88.getValue();
                traceLine("grdsprol {0} {1} {2}",warg1,barg1,barg2);
                break;
            case dogrddamage: 
                // __43 unknown
                traceLine("dogrddamage");
                break;
            default: 
                Console.WriteLine("Unknown iscript opcode: 0x{0:x}", buf[pc - 1]);
                break;
        
        }
        return true;
    }

}


