//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:15
//

package SCSharpMac.UI;

import java.util.ArrayList;
import java.util.List;
import SCSharp.Mpq;
import SCSharpMac.UI.__MultiTickEventHandler;
import SCSharpMac.UI.Builtins;
import SCSharpMac.UI.Game;
import SCSharpMac.UI.TickEventArgs;
import SCSharpMac.UI.UIScreen;

//
// SCSharpMac.UI.TitleScreen
//
// Authors:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
public class TitleScreen  extends UIScreen 
{
    public TitleScreen(Mpq mpq) throws Exception {
        super(mpq, "glue\\Palmm", Builtins.rez_TitleDlgBin);
        background_path = Builtins.TitlePcx;
    }

    protected void resourceLoader() throws Exception {
        super.resourceLoader();
        Cursor = null;
        /* clear out the cursor */
        Elements[COPYRIGHT1_ELEMENT_INDEX].Text = "Game code Copyright Â© 2006-2010 Chris Toshok.  All rights reserved.";
        Elements[COPYRIGHT2_ELEMENT_INDEX].Text = "Game assets Copyright Â© 1998 Blizzard Entertainment. All rights reserved.";
        Elements[COPYRIGHT3_ELEMENT_INDEX].Text = "";
    }

    public void addToPainter() throws Exception {
        super.addToPainter();
        Game.getInstance().Tick = __MultiTickEventHandler.combine(Game.getInstance().Tick,new TickEventHandler() 
          { 
            public System.Void invoke(System.Object sender, TickEventArgs args) throws Exception {
                loadingFlasher(sender, args);
            }

            public List<TickEventHandler> getInvocationList() throws Exception {
                List<TickEventHandler> ret = new ArrayList<TickEventHandler>();
                ret.add(this);
                return ret;
            }
        
          });
    }

    public void removeFromPainter() throws Exception {
        super.removeFromPainter();
        Game.getInstance().Tick = __MultiTickEventHandler.remove(Game.getInstance().Tick,new TickEventHandler() 
          { 
            public System.Void invoke(System.Object sender, TickEventArgs args) throws Exception {
                loadingFlasher(sender, args);
            }

            public List<TickEventHandler> getInvocationList() throws Exception {
                List<TickEventHandler> ret = new ArrayList<TickEventHandler>();
                ret.add(this);
                return ret;
            }
        
          });
    }

    static final int COPYRIGHT1_ELEMENT_INDEX = 1;
    static final int COPYRIGHT2_ELEMENT_INDEX = 2;
    static final int COPYRIGHT3_ELEMENT_INDEX = 3;
    static final int LOADING_ELEMENT_INDEX = 4;
    static final int FLASH_ON_DURATION = 1000;
    static final int FLASH_OFF_DURATION = 500;
    long totalElapsed = new long();
    boolean visible = true;
    void loadingFlasher(Object sender, TickEventArgs e) throws Exception {
        totalElapsed += e.getMillisecondsElapsed();
        if ((Elements[LOADING_ELEMENT_INDEX].Visible && (totalElapsed < FLASH_ON_DURATION)) || (!Elements[LOADING_ELEMENT_INDEX].Visible && (totalElapsed < FLASH_OFF_DURATION)))
            return ;
         
        Console.WriteLine("Flashing");
        Elements[LOADING_ELEMENT_INDEX].Visible = !Elements[LOADING_ELEMENT_INDEX].Visible;
        totalElapsed = 0;
    }

}


