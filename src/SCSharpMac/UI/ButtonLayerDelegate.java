//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:12
//

package SCSharpMac.UI;

import SCSharpMac.UI.ButtonElement;
import SCSharpMac.UI.GuiUtil;

public class ButtonLayerDelegate  extends CALayerDelegate 
{
    ButtonElement el;
    public ButtonLayerDelegate(ButtonElement el) throws Exception {
        this.el = el;
    }

    public void drawLayer(CALayer layer, CGContext context) throws Exception {
        GuiUtil.renderTextToContext(context,el.getTextPosition(),el.getText(),el.getFont(),el.getPalette(),el.getSensitive() ? 4 : 24);
    }

}


