//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:13
//

package SCSharpMac.UI;

import CS2JNet.JavaSupport.util.ListSupport;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import SCSharpMac.UI.ReadyDelegate;

public interface ReadyDelegate   
{
    void invoke() throws Exception ;

    System.Collections.Generic.IList<ReadyDelegate> getInvocationList() throws Exception ;

}


