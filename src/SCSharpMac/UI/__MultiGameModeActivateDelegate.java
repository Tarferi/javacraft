//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:12
//

package SCSharpMac.UI;

import CS2JNet.JavaSupport.util.ListSupport;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import SCSharpMac.UI.__MultiGameModeActivateDelegate;
import SCSharpMac.UI.GameModeActivateDelegate;

public class __MultiGameModeActivateDelegate   implements GameModeActivateDelegate
{
    public void invoke(boolean expansion) throws Exception {
        IList<GameModeActivateDelegate> copy = new IList<GameModeActivateDelegate>(), members = this.getInvocationList();
        synchronized (members)
        {
            copy = new LinkedList<GameModeActivateDelegate>(members);
        }
        for (Object __dummyForeachVar0 : copy)
        {
            GameModeActivateDelegate d = (GameModeActivateDelegate)__dummyForeachVar0;
            d.invoke(expansion);
        }
    }

    private System.Collections.Generic.IList<GameModeActivateDelegate> _invocationList = new ArrayList<GameModeActivateDelegate>();
    public static GameModeActivateDelegate combine(GameModeActivateDelegate a, GameModeActivateDelegate b) throws Exception {
        if (a == null)
            return b;
         
        if (b == null)
            return a;
         
        __MultiGameModeActivateDelegate ret = new __MultiGameModeActivateDelegate();
        ret._invocationList = a.getInvocationList();
        ret._invocationList.addAll(b.getInvocationList());
        return ret;
    }

    public static GameModeActivateDelegate remove(GameModeActivateDelegate a, GameModeActivateDelegate b) throws Exception {
        if (a == null || b == null)
            return a;
         
        System.Collections.Generic.IList<GameModeActivateDelegate> aInvList = a.getInvocationList();
        System.Collections.Generic.IList<GameModeActivateDelegate> newInvList = ListSupport.removeFinalStretch(aInvList, b.getInvocationList());
        if (aInvList == newInvList)
        {
            return a;
        }
        else
        {
            __MultiGameModeActivateDelegate ret = new __MultiGameModeActivateDelegate();
            ret._invocationList = newInvList;
            return ret;
        } 
    }

    public System.Collections.Generic.IList<GameModeActivateDelegate> getInvocationList() throws Exception {
        return _invocationList;
    }

}


