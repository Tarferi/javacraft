public class Console {

	public static void WriteLine(String string, String tileset) {
		System.out.println("[" + string + "] " + tileset);
	}

	public static void WriteLine(String string, int tile_number) {
		WriteLine(string, "" + tile_number);

	}

}
