//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:16
//


import SCSharp.Grp;
import SCSharp.MpqResource;
import SCSharp.UI.Pcx;

public class DumpGRP   
{
    public static void main(String[] args) throws Exception {
        DumpGRP.Main(args);
    }

    public static void Main(String[] args) throws Exception {
        String filename = args[0];
        String palettename = args[1];
        Console.WriteLine("grp file {0}", filename);
        Console.WriteLine("palette file {0}", palettename);
        FileStream fs = File.OpenRead(filename);
        Grp grp = new Grp();
        ((MpqResource)grp).ReadFromStream(fs);
        Pcx pal = new Pcx();
        pal.ReadFromStream(File.OpenRead(palettename), -1, -1);
        for (int i = 0;i < grp.getFrameCount();i++)
        {
            BMP.WriteBMP(String.Format("output{0:0000}.bmp", i), grp.getFrame(i), grp.getWidth(), grp.getHeight(), pal.getPalette());
        }
    }

}


