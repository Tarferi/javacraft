//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:16
//


import SCSharp.MpqResource;
import SCSharp.Tbl;

public class DumpTBL   
{
    public static void main(String[] args) throws Exception {
        DumpTBL.Main(args);
    }

    public static void Main(String[] args) throws Exception {
        String filename = args[0];
        FileStream fs = File.OpenRead(filename);
        Tbl tbl = new Tbl();
        ((MpqResource)tbl).ReadFromStream(fs);
        Console.WriteLine("dumping {0}", filename);
        for (int i = 0;i < tbl.getStrings().Length;i++)
            Console.WriteLine("strings[{0}] = {1}", i, tbl.getStrings()[i]);
    }

}


