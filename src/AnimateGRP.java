//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:16
//


import SCSharp.Grp;
import SCSharp.MpqResource;

public class AnimateGRP   
{
    static Gtk.Window window = new Gtk.Window();
    static Gtk.Widget drawing_area = new Gtk.Widget();
    static Grp grp;
    static final int WALK_CYCLE_START = 136;
    static final int WALK_CYCLE_END = 255;
    static final int DEATH_CYCLE_START = 408;
    static final int DEATH_CYCLE_END = 414;
    static final int FRAME_STEP = 17;
    static boolean walking = true;
    static int num_walks = 0;
    static int death_timer = 0;
    static Gdk.Pixbuf current_frame = new Gdk.Pixbuf();
    static int current_frame_num = WALK_CYCLE_START;
    static void createWindow() throws Exception {
        window = new Gtk.Window("grp animation");
        drawing_area = new Gtk.DrawingArea();
        window.Add(drawing_area);
        window.ShowAll();
    }

    static void onExposed(Object o, ExposeEventArgs args) throws Exception {
        if (current_frame == null)
            return ;
         
        current_frame.RenderToDrawable(drawing_area.GdkWindow, drawing_area.Style.ForegroundGC(StateType.Normal), 0, 0, 0, 0, -1, -1, RgbDither.None, 0, 0);
    }

    static byte[] createPixbufData(byte[][] grid, ushort width, ushort height, byte[] palette) throws Exception {
        byte[] rv = new byte[width * height * 3];
        int i = 0;
        int x = new int(), y = new int();
        for (y = height - 1;y >= 0;y--)
        {
            for (x = width - 1;x >= 0;x--)
            {
                rv[i++] = palette[grid[y, x] * 3];
                rv[i++] = palette[grid[y, x] * 3 + 1];
                rv[i++] = palette[grid[y, x] * 3 + 2];
            }
        }
        return rv;
    }

    static boolean animate() throws Exception {
        if (walking)
        {
            current_frame_num += FRAME_STEP;
            if (current_frame_num > WALK_CYCLE_END)
            {
                num_walks++;
                if (num_walks > 10)
                {
                    num_walks = 0;
                    walking = false;
                    current_frame_num = DEATH_CYCLE_START;
                }
                else
                {
                    current_frame_num = WALK_CYCLE_START;
                } 
            }
             
        }
        else
        {
            /* dying */
            if (current_frame_num == DEATH_CYCLE_END)
            {
                death_timer++;
                if (death_timer == 30)
                {
                    death_timer = 0;
                    walking = true;
                    current_frame_num = WALK_CYCLE_START;
                }
                 
            }
            else
                current_frame_num++; 
        } 
        byte[] pixbuf_data = createPixbufData(grp.getFrame(current_frame_num),grp.getWidth(),grp.getHeight(),Palette.default_palette);
        Gdk.Pixbuf temp = new Gdk.Pixbuf(pixbuf_data, Colorspace.Rgb, false, 8, grp.getWidth(), grp.getHeight(), grp.getWidth() * 3, null);
        current_frame = temp.ScaleSimple(grp.getWidth() * 2, grp.getHeight() * 2, InterpType.Nearest);
        temp.Dispose();
        drawing_area.QueueDraw();
        return true;
    }

    public static void main(String[] args) throws Exception {
        AnimateGRP.Main(args);
    }

    public static void Main(String[] args) throws Exception {
        Application.Init();
        String filename = args[0];
        Console.WriteLine("grp file {0}", filename);
        FileStream fs = File.OpenRead(filename);
        grp = new Grp();
        ((MpqResource)grp).ReadFromStream(fs);
        createWindow();
        drawing_area.ExposeEvent += OnExposed;
        GLib.Timeout.Add(100, Animate);
        Application.Run();
    }

}


