//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:16
//


import CS2JNet.JavaSupport.language.RefSupport;
import CS2JNet.JavaSupport.language.ReturnPreOrPostValue;
import SCSharp.IScriptBin;
import SCSharp.Mpq;
import SCSharp.MpqArchive;
import SCSharp.UI.GlobalResources;
import SCSharp.Util;

public class DumpIScript   
{
    public enum AnimationType
    {
        Init,
        Death,
        GndAttkInit,
        AirAttkInit,
        SpAbility1,
        GndAttkRpt,
        AirAttkRpt,
        SpAbility2,
        GndAttkToIdle,
        AirAttkToIdle,
        SpAbility3,
        Walking,
        Other,
        BurrowInit,
        ConstrctHarvst,
        IsWorking,
        Unknown16,
        Landing,
        LiftOff,
        Producing,
        /* used when a terran building is training troops */
        Unknown20,
        Unknown21,
        Unknown22,
        Unknown23,
        Unknown24,
        Burrow,
        UnBurrow,
        Unknown27
    }
    public enum IScriptOpcode
    {
        /* IScript opcodes */
        PlayFrame,
        PlayTilesetFrame,
        Unknown02,
        ShiftGraphicVert,
        Unknown04,
        Wait,
        Wait2Rand,
        Goto,
        PlaceActiveOverlay,
        PlaceActiveUnderlay,
        Unknown0a,
        SwitchUnderlay,
        Unknown0c,
        PlaceOverlay,
        Unknown0e,
        PlaceIndependentOverlay,
        PlaceIndependentOverlayOnTop,
        PlaceIndependentUnderlay,
        Unknown12,
        DisplayOverlayWithLO,
        Unknown14,
        DisplayIndependentOverlayWithLO,
        EndAnimation,
        Unknown17,
        PlaySound,
        PlayRandomSound,
        PlayRandomSoundRange,
        DoDamage,
        AttackWithWeaponAndPlaySound,
        FollowFrameChange,
        RandomizerValueGoto,
        TurnCCW,
        TurnCW,
        Turn1CW,
        TurnRandom,
        Unknown23,
        Unknown24,
        Attack,
        AttackWithAppropriateWeapon,
        CastSpell,
        UseWeapon,
        MoveForward,
        AttackLoopMarker,
        Unknown2b,
        Unknown2c,
        Unknown2d,
        BeginPlayerLockout,
        EndPlayerLockout,
        IgnoreOtherOpcodes,
        AttackWithDirectionalProjectile,
        Hide,
        Unhide,
        PlaySpecificFrame,
        Unknown35,
        Unknown36,
        Unknown37,
        Unknown38,
        IfPickedUp,
        IfTargetInRangeGoto,
        IfTargetInArcGoto,
        Unknown3c,
        Unknown3d,
        Unknown3e,
        Unknown3f,
        Unknown40,
        Unknown41,
        Unknown42,
        __dummyEnum__0,
        __dummyEnum__1,
        __dummyEnum__2,
        __dummyEnum__3,
        __dummyEnum__4,
        __dummyEnum__5,
        __dummyEnum__6,
        __dummyEnum__7,
        __dummyEnum__8,
        __dummyEnum__9,
        __dummyEnum__10,
        __dummyEnum__11,
        __dummyEnum__12,
        __dummyEnum__13,
        __dummyEnum__14,
        __dummyEnum__15,
        __dummyEnum__16,
        __dummyEnum__17,
        __dummyEnum__18,
        __dummyEnum__19,
        __dummyEnum__20,
        __dummyEnum__21,
        __dummyEnum__22,
        __dummyEnum__23,
        __dummyEnum__24,
        __dummyEnum__25,
        __dummyEnum__26,
        __dummyEnum__27,
        __dummyEnum__28,
        __dummyEnum__29,
        __dummyEnum__30,
        __dummyEnum__31,
        __dummyEnum__32,
        __dummyEnum__33,
        __dummyEnum__34,
        __dummyEnum__35,
        __dummyEnum__36,
        __dummyEnum__37,
        __dummyEnum__38,
        __dummyEnum__39,
        __dummyEnum__40,
        __dummyEnum__41,
        __dummyEnum__42,
        __dummyEnum__43,
        __dummyEnum__44,
        __dummyEnum__45,
        __dummyEnum__46,
        __dummyEnum__47,
        __dummyEnum__48,
        __dummyEnum__49,
        __dummyEnum__50,
        __dummyEnum__51,
        __dummyEnum__52,
        __dummyEnum__53,
        __dummyEnum__54,
        __dummyEnum__55,
        __dummyEnum__56,
        __dummyEnum__57,
        __dummyEnum__58,
        __dummyEnum__59,
        __dummyEnum__60,
        /* ICE manual says this is something dealing with sprites */
        Unknown80
    }
    static class Block   
    {
        public final ushort Pc = new ushort();
        ushort endPc = new ushort();
        public Block(ushort pc) throws Exception {
            Pc = pc;
            endPc = pc;
        }

        public ushort getEndPc() throws Exception {
            return endPc;
        }

        public void setEndPc(ushort value) throws Exception {
            endPc = value;
        }
    
    }

    List<Block> blocks = new List<Block>();
    IScriptBin bin;
    byte[] buf = new byte[]();
    Dictionary<ushort, String> labels = new Dictionary<ushort, String>();
    public DumpIScript(Mpq mpq) throws Exception {
        (new GlobalResources(mpq, null)).loadSingleThreaded();
        bin = GlobalResources.getInstance().getIScriptBin();
        buf = bin.getContents();
        blocks = new List<Block>();
        labels = new Dictionary<ushort, String>();
    }

    static int counter = 0;
    String getLabel(ushort pc) throws Exception {
        return GetLabel(pc, String.Format("local{0}", counter++));
    }

    String getLabel(ushort pc, String label) throws Exception {
        if (labels.ContainsKey(pc))
            return labels[pc];
         
        labels.Add(pc, label);
        return label;
    }

    ushort readWord(RefSupport<ushort> pc) throws Exception {
        ushort retval = Util.ReadWord(buf, pc.getValue());
        pc.setValue(pc.getValue() + 2);
        return retval;
    }

    byte readByte(RefSupport<ushort> pc) throws Exception {
        byte retval = buf[pc.getValue()];
        pc.setValue(pc.getValue() + 1, ReturnPreOrPostValue.POST);
        return retval;
    }

    String getGrpNameFromImageId(ushort arg) throws Exception {
        uint grp_index = GlobalResources.getInstance().getImagesDat().GrpIndexes[arg];
        return grp_index == 0 ? "NONE" : GlobalResources.getInstance().getImagesTbl()[(int)grp_index - 1];
    }

    String getGrpNameFromSpriteId(ushort arg) throws Exception {
        ushort image_entry = GlobalResources.getInstance().getSpritesDat().ImagesDatEntries[arg];
        return getGrpNameFromImageId(image_entry);
    }

    boolean dumpOpcode(RefSupport<ushort> pc) throws Exception {
        ushort warg1 = new ushort();
        ushort warg2 = new ushort();
        //			ushort warg3;
        byte barg1 = new byte();
        byte barg2 = new byte();
        //			byte barg3;
        Block dest;
        boolean retval = false;
        IScriptOpcode op = (IScriptOpcode)buf[pc.getValue()];
        Console.Write("{0:x}\t{1} ", pc.getValue(), op);
        pc.setValue(pc.getValue() + 1, ReturnPreOrPostValue.POST);
        try
        {
            switch(op)
            {
                case PlayFrame: 
                    RefSupport<ushort> refVar___0 = new RefSupport<ushort>(pc.getValue());
                    warg1 = readWord(refVar___0);
                    pc.setValue(refVar___0.getValue());
                    Console.Write("{0}", warg1);
                    break;
                case PlayTilesetFrame: 
                    RefSupport<ushort> refVar___1 = new RefSupport<ushort>(pc.getValue());
                    warg1 = readWord(refVar___1);
                    pc.setValue(refVar___1.getValue());
                    Console.Write("{0}", warg1);
                    break;
                case ShiftGraphicVert: 
                    RefSupport<ushort> refVar___2 = new RefSupport<ushort>(pc.getValue());
                    barg1 = readByte(refVar___2);
                    pc.setValue(refVar___2.getValue());
                    Console.Write("{0}", barg1);
                    break;
                case Wait: 
                    RefSupport<ushort> refVar___3 = new RefSupport<ushort>(pc.getValue());
                    barg1 = readByte(refVar___3);
                    pc.setValue(refVar___3.getValue());
                    Console.Write("{0}", barg1);
                    break;
                case Wait2Rand: 
                    RefSupport<ushort> refVar___4 = new RefSupport<ushort>(pc.getValue());
                    barg1 = readByte(refVar___4);
                    pc.setValue(refVar___4.getValue());
                    RefSupport<ushort> refVar___5 = new RefSupport<ushort>(pc.getValue());
                    barg2 = readByte(refVar___5);
                    pc.setValue(refVar___5.getValue());
                    Console.Write("{0} {1}", barg1, barg2);
                    break;
                case Goto: 
                    RefSupport<ushort> refVar___6 = new RefSupport<ushort>(pc.getValue());
                    warg1 = readWord(refVar___6);
                    pc.setValue(refVar___6.getValue());
                    Console.Write("{0}", getLabel(warg1));
                    dest = new Block(warg1);
                    blocks.Add(dest);
                    retval = true;
                    break;
                case PlaceActiveOverlay: 
                    RefSupport<ushort> refVar___7 = new RefSupport<ushort>(pc.getValue());
                    warg1 = readWord(refVar___7);
                    pc.setValue(refVar___7.getValue());
                    RefSupport<ushort> refVar___8 = new RefSupport<ushort>(pc.getValue());
                    warg2 = readWord(refVar___8);
                    pc.setValue(refVar___8.getValue());
                    Console.Write("{0} ({1}) {2}", warg1, getGrpNameFromImageId(warg1), warg2);
                    break;
                case PlaceActiveUnderlay: 
                    RefSupport<ushort> refVar___9 = new RefSupport<ushort>(pc.getValue());
                    warg1 = readWord(refVar___9);
                    pc.setValue(refVar___9.getValue());
                    RefSupport<ushort> refVar___10 = new RefSupport<ushort>(pc.getValue());
                    warg2 = readWord(refVar___10);
                    pc.setValue(refVar___10.getValue());
                    Console.Write("{0} ({1}) {2}", warg1, getGrpNameFromImageId(warg1), warg2);
                    break;
                case MoveForward: 
                    RefSupport<ushort> refVar___11 = new RefSupport<ushort>(pc.getValue());
                    barg1 = readByte(refVar___11);
                    pc.setValue(refVar___11.getValue());
                    Console.Write("{0}", barg1);
                    break;
                case RandomizerValueGoto: 
                    RefSupport<ushort> refVar___12 = new RefSupport<ushort>(pc.getValue());
                    barg1 = readByte(refVar___12);
                    pc.setValue(refVar___12.getValue());
                    RefSupport<ushort> refVar___13 = new RefSupport<ushort>(pc.getValue());
                    warg1 = readWord(refVar___13);
                    pc.setValue(refVar___13.getValue());
                    Console.Write("{0} {1}", barg1, getLabel(warg1));
                    dest = new Block(warg1);
                    blocks.Add(dest);
                    break;
                case TurnRandom: 
                    break;
                case TurnCCW: 
                    RefSupport<ushort> refVar___14 = new RefSupport<ushort>(pc.getValue());
                    barg1 = readByte(refVar___14);
                    pc.setValue(refVar___14.getValue());
                    Console.Write("{0}", barg1);
                    break;
                case TurnCW: 
                    RefSupport<ushort> refVar___15 = new RefSupport<ushort>(pc.getValue());
                    barg1 = readByte(refVar___15);
                    pc.setValue(refVar___15.getValue());
                    Console.Write("{0}", barg1);
                    break;
                case Turn1CW: 
                    break;
                case PlaySound: 
                    RefSupport<ushort> refVar___16 = new RefSupport<ushort>(pc.getValue());
                    warg1 = readWord(refVar___16);
                    pc.setValue(refVar___16.getValue());
                    Console.Write("{0} ({1})", warg1 - 1, GlobalResources.getInstance().getSfxDataTbl()[(int)GlobalResources.getInstance().getSfxDataDat().FileIndexes[warg1 - 1]]);
                    break;
                case PlayRandomSound: 
                    RefSupport<ushort> refVar___17 = new RefSupport<ushort>(pc.getValue());
                    barg1 = readByte(refVar___17);
                    pc.setValue(refVar___17.getValue());
                    Console.Write("{0}", barg1);
                    for (int i = 0;i < barg1;i++)
                    {
                        RefSupport<ushort> refVar___18 = new RefSupport<ushort>(pc.getValue());
                        warg1 = readWord(refVar___18);
                        pc.setValue(refVar___18.getValue());
                        Console.Write(" {0}", warg1);
                    }
                    break;
                case PlayRandomSoundRange: 
                    RefSupport<ushort> refVar___19 = new RefSupport<ushort>(pc.getValue());
                    warg1 = readWord(refVar___19);
                    pc.setValue(refVar___19.getValue());
                    RefSupport<ushort> refVar___20 = new RefSupport<ushort>(pc.getValue());
                    warg2 = readWord(refVar___20);
                    pc.setValue(refVar___20.getValue());
                    Console.Write("{0} {1}", warg1, warg2);
                    Console.WriteLine(" [");
                    for (int i = warg1;i < warg2;i++)
                        Console.Write(" {0}", GlobalResources.getInstance().getSfxDataTbl()[(int)GlobalResources.getInstance().getSfxDataDat().FileIndexes[i - 1]]);
                    Console.Write(" ]");
                    break;
                case PlaySpecificFrame: 
                    RefSupport<ushort> refVar___21 = new RefSupport<ushort>(pc.getValue());
                    barg1 = readByte(refVar___21);
                    pc.setValue(refVar___21.getValue());
                    Console.Write("{0}", barg1);
                    break;
                case PlaceIndependentUnderlay: 
                    RefSupport<ushort> refVar___22 = new RefSupport<ushort>(pc.getValue());
                    warg1 = readWord(refVar___22);
                    pc.setValue(refVar___22.getValue());
                    RefSupport<ushort> refVar___23 = new RefSupport<ushort>(pc.getValue());
                    barg1 = readByte(refVar___23);
                    pc.setValue(refVar___23.getValue());
                    RefSupport<ushort> refVar___24 = new RefSupport<ushort>(pc.getValue());
                    barg2 = readByte(refVar___24);
                    pc.setValue(refVar___24.getValue());
                    Console.Write("{0} ({1}) ({2},{3})", warg1, getGrpNameFromSpriteId(warg1), barg1, barg2);
                    break;
                case AttackWithWeaponAndPlaySound: 
                    RefSupport<ushort> refVar___25 = new RefSupport<ushort>(pc.getValue());
                    barg1 = readByte(refVar___25);
                    pc.setValue(refVar___25.getValue());
                    Console.Write("{0} ", barg1);
                    Console.Write("[");
                    for (int i = 0;i < barg1;i++)
                    {
                        RefSupport<ushort> refVar___26 = new RefSupport<ushort>(pc.getValue());
                        warg1 = readWord(refVar___26);
                        pc.setValue(refVar___26.getValue());
                        Console.Write(" {0}", GlobalResources.getInstance().getSfxDataTbl()[(int)GlobalResources.getInstance().getSfxDataDat().FileIndexes[warg1 - 1]]);
                    }
                    Console.Write(" ]");
                    break;
                case EndAnimation: 
                    retval = true;
                    break;
                case FollowFrameChange: 
                case BeginPlayerLockout: 
                case EndPlayerLockout: 
                case AttackLoopMarker: 
                    break;
                case UseWeapon: 
                    /* no arguments */
                    RefSupport<ushort> refVar___27 = new RefSupport<ushort>(pc.getValue());
                    warg1 = readWord(refVar___27);
                    pc.setValue(refVar___27.getValue());
                    Console.Write("{0}", warg1);
                    break;
                case Unknown0a: 
                    RefSupport<ushort> refVar___28 = new RefSupport<ushort>(pc.getValue());
                    barg1 = readByte(refVar___28);
                    pc.setValue(refVar___28.getValue());
                    Console.Write("{0}", barg1);
                    break;
                case Unknown24: 
                    RefSupport<ushort> refVar___29 = new RefSupport<ushort>(pc.getValue());
                    barg1 = readByte(refVar___29);
                    pc.setValue(refVar___29.getValue());
                    Console.Write("{0}", barg1);
                    break;
                case Unknown38: 
                    RefSupport<ushort> refVar___30 = new RefSupport<ushort>(pc.getValue());
                    warg1 = readWord(refVar___30);
                    pc.setValue(refVar___30.getValue());
                    Console.Write("{0}", warg1);
                    break;
                case Unknown3c: 
                    RefSupport<ushort> refVar___31 = new RefSupport<ushort>(pc.getValue());
                    warg1 = readWord(refVar___31);
                    pc.setValue(refVar___31.getValue());
                    Console.Write("{0}", warg1);
                    break;
                case Unknown3d: 
                    RefSupport<ushort> refVar___32 = new RefSupport<ushort>(pc.getValue());
                    warg1 = readWord(refVar___32);
                    pc.setValue(refVar___32.getValue());
                    Console.Write("{0}", warg1);
                    break;
                case Unknown3f: 
                    RefSupport<ushort> refVar___33 = new RefSupport<ushort>(pc.getValue());
                    warg1 = readWord(refVar___33);
                    pc.setValue(refVar___33.getValue());
                    Console.Write("{0}", warg1);
                    break;
                case Unknown40: 
                    RefSupport<ushort> refVar___34 = new RefSupport<ushort>(pc.getValue());
                    warg1 = readWord(refVar___34);
                    pc.setValue(refVar___34.getValue());
                    Console.Write("{0}", warg1);
                    break;
                case Unknown41: 
                    RefSupport<ushort> refVar___35 = new RefSupport<ushort>(pc.getValue());
                    warg1 = readWord(refVar___35);
                    pc.setValue(refVar___35.getValue());
                    Console.Write("{0}", warg1);
                    break;
                case Unknown42: 
                    RefSupport<ushort> refVar___36 = new RefSupport<ushort>(pc.getValue());
                    warg1 = readWord(refVar___36);
                    pc.setValue(refVar___36.getValue());
                    Console.Write("{0}", warg1);
                    break;
                default: 
                    Console.Write(" <Unhandled> ");
                    ushort pc2 = new ushort();
                    pc2 = pc.getValue();
                    Console.Write("[");
                    for (int i = 0;i < 3;i++)
                    {
                        RefSupport<ushort> refVar___37 = new RefSupport<ushort>(pc2);
                        Console.Write(" {0:x}", readWord(refVar___37));
                        pc2 = refVar___37.getValue();
                    }
                    Console.Write(" ]");
                    pc2 = pc.getValue();
                    Console.Write("/[");
                    for (int i = 0;i < 6;i++)
                    {
                        RefSupport<ushort> refVar___38 = new RefSupport<ushort>(pc2);
                        Console.Write(" {0:x}", readByte(refVar___38));
                        pc2 = refVar___38.getValue();
                    }
                    Console.WriteLine(" ]");
                    retval = true;
                    break;
            
            }
        }
        catch (Exception e)
        {
            Console.Write("*exception*");
        }
        finally
        {
            Console.WriteLine();
        }
        return retval;
    }

    void dumpBlock(Block block) throws Exception {
        for (int i = 0;i < blocks.Count;i++)
        {
            /* make sure we haven't already dumped this block */
            if (blocks[i] == block)
                break;
             
            if (block.Pc >= blocks[i].Pc && block.getEndPc() <= blocks[i].EndPc)
                return ;
             
        }
        ushort pc = block.Pc;
        Console.WriteLine();
        boolean done = false;
        while (!done)
        {
            block.setEndPc(pc);
            if (labels.ContainsKey(pc))
                Console.WriteLine(".label {0}", labels[pc]);
             
            RefSupport<ushort> refVar___39 = new RefSupport<ushort>(pc);
            done = dumpOpcode(refVar___39);
            pc = refVar___39.getValue();
        }
    }

    public void dump(String prefix, int entry) throws Exception {
        int entry_offset = bin.GetScriptEntryOffset((ushort)entry);
        /* make sure the offset points to "SCEP" */
        if (Util.readDWord(buf,entry_offset) != 0x45504353)
            throw new Exception("invalid script_entry_offset");
         
        Console.WriteLine(";");
        Console.WriteLine("\t.header {0}", prefix);
        for (Object __dummyForeachVar0 : Enum.GetValues(AnimationType.class))
        {
            AnimationType animationType = (AnimationType)__dummyForeachVar0;
            /* "SCEP" */
            /* the script entry "type" */
            /* the spacers */
            int offset_to_script_type = (4 + 1 + 3 + ((Enum)animationType).ordinal() * 2);
            ushort script_start = Util.readWord(buf,entry_offset + offset_to_script_type);
            String blockname = String.Format("{0}{1}", prefix, animationType.ToString());
            blockname = getLabel(script_start,blockname);
            Console.WriteLine("\t.animation{0}: {1} ({2:x})", animationType, script_start == 0 ? "NONE" : (script_start > buf.Length ? "OUT OF RANGE" : blockname), script_start);
            if (script_start < buf.Length && script_start != 0)
                blocks.Add(new Block(script_start));
             
        }
        Console.WriteLine(";");
        int index = 0;
        while (index < blocks.Count)
        {
            Block block_to_dump = blocks[index];
            dumpBlock(block_to_dump);
            index++;
        }
    }

    public static void main(String[] args) throws Exception {
        DumpIScript.Main(args);
    }

    public static void Main(String[] args) throws Exception {
        String entry_name = args[1];
        int entry = Int32.Parse(args[2]);
        Mpq mpq = new MpqArchive(args[0]);
        DumpIScript dumper = new DumpIScript(mpq);
        dumper.dump(entry_name,entry);
    }

}


