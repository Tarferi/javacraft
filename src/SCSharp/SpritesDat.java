//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:06
//

package SCSharp;

import SCSharp.Dat;
import SCSharp.DatCollection;
import SCSharp.DatVariableType;

//
// SCSharp.Mpq.SpritesDat
//
// Authors:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
public class SpritesDat  extends Dat 
{
    static final int NUM_SPRITES = 386;
    static final int NUM_DOODADS = 130;
    int imageIndexBlockId = new int();
    int barLengthBlockId = new int();
    int selectionCircleBlockId = new int();
    int selectionCircleOffsetBlockId = new int();
    public SpritesDat() throws Exception {
        Console.WriteLine("SpritesDat");
        imageIndexBlockId = addVariableBlock(386,DatVariableType.Word);
        barLengthBlockId = addVariableBlock(NUM_SPRITES - NUM_DOODADS,DatVariableType.Byte);
        selectionCircleBlockId = addVariableBlock(NUM_SPRITES - NUM_DOODADS,DatVariableType.Byte);
        selectionCircleOffsetBlockId = addVariableBlock(NUM_SPRITES - NUM_DOODADS,DatVariableType.Byte);
        Console.WriteLine("imageIndexBlockId = {0}", getVariableOffset(imageIndexBlockId));
        Console.WriteLine("barLengthBlockId = {0}", getVariableOffset(barLengthBlockId));
        Console.WriteLine("selectionCircleBlockId = {0}", getVariableOffset(selectionCircleBlockId));
        Console.WriteLine("selectionCircleOffsetBlockId = {0}", getVariableOffset(selectionCircleOffsetBlockId));
    }

    public DatCollection<ushort> getImagesDatEntries() throws Exception {
        return (DatCollection<ushort>)getCollection(imageIndexBlockId);
    }

    public DatCollection<byte> getBarLengths() throws Exception {
        return (DatCollection<byte>)getCollection(barLengthBlockId);
    }

    public DatCollection<byte> getSelectionCircles() throws Exception {
        return (DatCollection<byte>)getCollection(selectionCircleBlockId);
    }

    public DatCollection<byte> getSelectionCircleOffsets() throws Exception {
        return (DatCollection<byte>)getCollection(selectionCircleOffsetBlockId);
    }

}


