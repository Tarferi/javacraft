//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:05
//

package SCSharp;

import SCSharp.Mpq;

public class MpqContainer  extends Mpq 
{
    List<Mpq> mpqs = new List<Mpq>();
    public MpqContainer() throws Exception {
        mpqs = new List<Mpq>();
    }

    public void add(Mpq mpq) throws Exception {
        if (mpq == null)
            return ;
         
        mpqs.Add(mpq);
    }

    public void remove(Mpq mpq) throws Exception {
        if (mpq == null)
            return ;
         
        mpqs.Remove(mpq);
    }

    public void clear() throws Exception {
        mpqs.Clear();
    }

    public void dispose() throws Exception {
        for (Object __dummyForeachVar0 : mpqs)
        {
            Mpq mpq = (Mpq)__dummyForeachVar0;
            mpq.dispose();
        }
    }

    public Stream getStreamForResource(String path) throws Exception {
        for (Object __dummyForeachVar1 : mpqs)
        {
            Mpq mpq = (Mpq)__dummyForeachVar1;
            Stream s = mpq.getStreamForResource(path);
            if (s != null)
                return s;
             
        }
        Console.WriteLine("returning null stream for resource: {0}", path);
        return null;
    }

}


