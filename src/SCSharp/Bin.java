//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:02
//

package SCSharp;

import SCSharp.BinElement;
import SCSharp.MpqResource;

public class Bin   implements MpqResource
{
    Stream stream = new Stream();
    List<BinElement> elements = new List<BinElement>();
    public Bin() throws Exception {
        elements = new List<BinElement>();
    }

    public void readFromStream(Stream stream) throws Exception {
        this.stream = stream;
        readElements();
    }

    void readElements() throws Exception {
        int position = new int();
        byte[] buf = new byte[stream.Length];
        stream.Read(buf, 0, (int)stream.Length);
        position = 0;
        do
        {
            BinElement element = new BinElement(buf, position, (uint)stream.Length);
            elements.Add(element);
            position += 86;
        }
        while (position < ((BinElement)elements[0]).text_offset);
    }

    BinElement[] arr = new BinElement[]();
    public BinElement[] getElements() throws Exception {
        if (arr == null)
            arr = elements.ToArray();
         
        return arr;
    }

}


