//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:04
//

package SCSharp;

import SCSharp.DatCollection;
import SCSharp.DatVariableType;
import SCSharp.Util;

public class DatVariable   
{
    DatVariableType type = DatVariableType.Byte;
    int offset = new int();
    int num_entries = new int();
    public DatVariable(DatVariableType type, int offset, int num_entries) throws Exception {
        this.type = type;
        this.offset = offset;
        this.num_entries = num_entries;
    }

    public int getOffset() throws Exception {
        return offset;
    }

    public int getNumEntries() throws Exception {
        return num_entries;
    }

    public int blockSize() throws Exception {
        switch(type)
        {
            case Byte: 
                return num_entries;
            case Word: 
                return num_entries * 2;
            case DWord: 
                return num_entries * 4;
            default: 
                return 0;
        
        }
    }

    public Object get___idx(byte[] buf, int index) throws Exception {
        switch(type)
        {
            case Byte: 
                return buf[offset + index];
            case Word: 
                return Util.ReadWord(buf, offset + index * 2);
            case DWord: 
                return Util.ReadDWord(buf, offset + index * 4);
            default: 
                return null;
        
        }
    }

    public DatCollection createCollection(byte[] buf) throws Exception {
        switch(type)
        {
            case Byte: 
                return new DatCollection<byte>(buf, this);
            case Word: 
                return new DatCollection<ushort>(buf, this);
            case DWord: 
                return new DatCollection<uint>(buf, this);
            default: 
                return null;
        
        }
    }

}


