//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:06
//

package SCSharp;

import SCSharp.Dat;
import SCSharp.DatCollection;
import SCSharp.DatVariableType;

//
// SCSharp.Mpq.PortDataDat
//
// Authors:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
/* offsets:
portrait animation index at 0x0000. DWORD
?                        at 0x02d0. BYTE
?                        at 0x0384. BYTE
*/
public class PortDataDat  extends Dat 
{
    int portraitIndexBlock = new int();
    public PortDataDat() throws Exception {
        portraitIndexBlock = addVariableBlock(360,DatVariableType.DWord);
    }

    public DatCollection<uint> getPortraitIndexes() throws Exception {
        return (DatCollection<uint>)getCollection(portraitIndexBlock);
    }

}


