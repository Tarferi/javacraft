//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:05
//

package SCSharp;

import SCSharp.Mpq;

public class MpqDirectory  extends Mpq 
{
    Dictionary<String, String> file_hash = new Dictionary<String, String>();
    String mpq_dir_path = new String();
    public MpqDirectory(String path) throws Exception {
        mpq_dir_path = path;
        file_hash = new Dictionary<String, String>();
        recurseDirectoryTree(mpq_dir_path);
    }

    String convertBackSlashes(String path) throws Exception {
        while (path.IndexOf('\\') != -1)
        path = path.Replace('\\', Path.DirectorySeparatorChar);
        return path;
    }

    public Stream getStreamForResource(String path) throws Exception {
        String rebased_path = ConvertBackSlashes(Path.Combine(mpq_dir_path, path));
        if (file_hash.ContainsKey(rebased_path.ToLower()))
        {
            String real_path = file_hash[rebased_path.ToLower()];
            if (real_path != null)
            {
                Console.WriteLine("using {0}", real_path);
                return File.OpenRead(real_path);
            }
             
        }
         
        return null;
    }

    void recurseDirectoryTree(String path) throws Exception {
        String[] files = Directory.GetFiles(path);
        for (Object __dummyForeachVar0 : files)
        {
            String f = (String)__dummyForeachVar0;
            String platform_path = convertBackSlashes(f);
            file_hash.Add(f.ToLower(), platform_path);
        }
        String[] directories = Directory.GetDirectories(path);
        for (Object __dummyForeachVar1 : directories)
        {
            String d = (String)__dummyForeachVar1;
            recurseDirectoryTree(d);
        }
    }

}


