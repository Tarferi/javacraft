//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:02
//

package SCSharp;

import SCSharp.ElementFlags;
import SCSharp.ElementType;
import SCSharp.Util;

public class BinElement   
{
    public ushort x1 = new ushort();
    public ushort y1 = new ushort();
    public ushort x2 = new ushort();
    public ushort y2 = new ushort();
    public ushort width = new ushort();
    public ushort height = new ushort();
    public char hotkey = new char();
    public String text = new String();
    public uint text_offset = new uint();
    public ElementFlags flags = ElementFlags.Unknown00000001;
    public ElementType type = ElementType.DialogBox;
    public Object resolvedData = new Object();
    public BinElement(byte[] buf, int position, uint stream_length) throws Exception {
        x1 = Util.ReadWord(buf, position + 4);
        y1 = Util.ReadWord(buf, position + 6);
        x2 = Util.ReadWord(buf, position + 8);
        y2 = Util.ReadWord(buf, position + 10);
        width = Util.ReadWord(buf, position + 12);
        height = Util.ReadWord(buf, position + 14);
        text_offset = Util.ReadDWord(buf, position + 20);
        flags = (ElementFlags)Util.ReadDWord(buf, position + 24);
        type = (ElementType)buf[position + 34];
        if (text_offset < stream_length)
        {
            uint text_length = 0;
            while (buf[text_offset + text_length] != 0)
            text_length++;
            text = Encoding.ASCII.GetString(buf, (int)text_offset, (int)text_length);
            if ((flags & ElementFlags.HasHotkey) == ElementFlags.HasHotkey)
            {
                hotkey = text[0];
                text = text.Substring(1);
            }
             
        }
        else
            text = ""; 
    }

    public void dumpFlags() throws Exception {
        Console.Write("Flags: ");
        for (Object __dummyForeachVar0 : Enum.GetValues(ElementFlags.class))
        {
            ElementFlags f = (ElementFlags)__dummyForeachVar0;
            if ((flags & f) == f)
            {
                Console.Write(f);
                Console.Write(" ");
            }
             
        }
        Console.WriteLine();
    }

    public String toString() {
        try
        {
            return String.Format("{0} ({1})", type, text);
        }
        catch (RuntimeException __dummyCatchVar0)
        {
            throw __dummyCatchVar0;
        }
        catch (Exception __dummyCatchVar0)
        {
            throw new RuntimeException(__dummyCatchVar0);
        }
    
    }

    public String getText() throws Exception {
        return text;
    }

    public void setText(String value) throws Exception {
        text = value;
        resolvedData = null;
    }

}


