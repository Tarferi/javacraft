//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:04
//

package SCSharp;

import SCSharp.InitialUnits;
import SCSharp.MpqResource;
import SCSharp.Util;

public class Got   implements MpqResource
{
    byte[] contents = new byte[]();
    public Got() throws Exception {
    }

    public void readFromStream(Stream stream) throws Exception {
        contents = new byte[stream.Length];
        stream.Read(contents, 0, (int)stream.Length);
    }

    public String getUIGameTypeName() throws Exception {
        return Util.readUntilNull(contents,0x01);
    }

    public String getUISubtypeLabel() throws Exception {
        return Util.readUntilNull(contents,0x21);
    }

    public boolean getComputerPlayersAllowed() throws Exception {
        return (contents[0x4f] & 0x01) != 0;
    }

    public int getNumberOfTeams() throws Exception {
        return contents[0x51];
    }

    public int getListPosition() throws Exception {
        return contents[0x41];
    }

    public boolean getUseMapSettings() throws Exception {
        return (contents[0x4e] == 1);
    }

    public InitialUnits getInitialUnits() throws Exception {
        return (InitialUnits)contents[0x4d];
    }

}


