//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:04
//

package SCSharp;

import CS2JNet.JavaSupport.language.RefSupport;
import CS2JNet.JavaSupport.language.ReturnPreOrPostValue;
import SCSharp.TriggerAction;
import SCSharp.TriggerCondition;
import SCSharp.Util;

public class Trigger   
{
    TriggerCondition[] conditions = new TriggerCondition[16];
    TriggerAction[] actions = new TriggerAction[64];
    public void parse(byte[] data, RefSupport<int> offset) throws Exception {
        int i = new int();
        for (i = 0;i < conditions.Length;i++)
        {
            TriggerCondition c = new TriggerCondition();
            c.setNumber(Util.ReadDWord(data, offset.getValue()));
            offset.setValue(offset.getValue() + 4);
            c.setGroup(Util.ReadDWord(data, offset.getValue()));
            offset.setValue(offset.getValue() + 4);
            c.setAmount(Util.ReadDWord(data, offset.getValue()));
            offset.setValue(offset.getValue() + 4);
            c.setUnitType(Util.ReadWord(data, offset.getValue()));
            offset.setValue(offset.getValue() + 2);
            c.setComparisonSwitch(data[offset.setValue(offset.getValue() + 1, ReturnPreOrPostValue.POST)]);
            c.setCondition(data[offset.setValue(offset.getValue() + 1, ReturnPreOrPostValue.POST)]);
            c.setResource(data[offset.setValue(offset.getValue() + 1, ReturnPreOrPostValue.POST)]);
            c.setFlags(data[offset.setValue(offset.getValue() + 1, ReturnPreOrPostValue.POST)]);
            // padding
            offset.setValue(offset.getValue() + 2);
            conditions[i] = c;
        }
        for (i = 0;i < actions.Length;i++)
        {
            TriggerAction a = new TriggerAction();
            a.setLocation(Util.ReadDWord(data, offset.getValue()));
            offset.setValue(offset.getValue() + 4);
            a.setTextIndex(Util.ReadDWord(data, offset.getValue()));
            offset.setValue(offset.getValue() + 4);
            a.setWavIndex(Util.ReadWord(data, offset.getValue()));
            offset.setValue(offset.getValue() + 4);
            a.setDelay(Util.ReadDWord(data, offset.getValue()));
            offset.setValue(offset.getValue() + 4);
            a.setGroup1(Util.ReadDWord(data, offset.getValue()));
            offset.setValue(offset.getValue() + 4);
            a.setGroup2(Util.ReadDWord(data, offset.getValue()));
            offset.setValue(offset.getValue() + 4);
            a.setUnitType(Util.ReadWord(data, offset.getValue()));
            offset.setValue(offset.getValue() + 2);
            a.setAction(data[offset.setValue(offset.getValue() + 1, ReturnPreOrPostValue.POST)]);
            a.setSwitch(data[offset.setValue(offset.getValue() + 1, ReturnPreOrPostValue.POST)]);
            a.setFlags(data[offset.setValue(offset.getValue() + 1, ReturnPreOrPostValue.POST)]);
            // padding
            offset.setValue(offset.getValue() + 3);
            actions[i] = a;
        }
        // more padding?
        offset.setValue(offset.getValue() + 4);
        // this is 1 byte for each player in the groups list:
        //
        // 00 - trigger is not executed for player
        // 01 - trigger is executed for player
        offset.setValue(offset.getValue() + 28);
    }

    public TriggerCondition[] getConditions() throws Exception {
        return conditions;
    }

    public TriggerAction[] getActions() throws Exception {
        return actions;
    }

}


