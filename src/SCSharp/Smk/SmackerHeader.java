//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:06
//

package SCSharp.Smk;


public class SmackerHeader   
{
    /* Smacker file header */
    public UInt32 Signature = new UInt32();
    public UInt32 Width = new UInt32(), Height = new UInt32();
    public UInt32 NbFrames = new UInt32();
    public int Pts_Inc = new int();
    public double Fps = new double();
    public UInt32 Flags = new UInt32();
    public UInt32[] AudioSize = new UInt32[7];
    public UInt32 TreesSize = new UInt32();
    public UInt32 NMap_Size = new UInt32(), MClr_Size = new UInt32(), Full_Size = new UInt32(), Type_Size = new UInt32();
    public UInt32[] AudioRate = new UInt32[7];
    public UInt32 Dummy = new UInt32();
    /**
    * Returns the sample rate for the specified audio track
    * 
    *  @param i the audio track to return the sample rate for
    *  @return The smaple rate for track i
    */
    public int getSampleRate(int i) throws Exception {
        return (int)(AudioRate[i] & 0xFFFFFF);
    }

    //Mask out the upper byte
    public boolean isStereoTrack(int i) throws Exception {
        return ((AudioRate[i] >> 24) & 16) > 0;
    }

    public boolean is16BitTrack(int i) throws Exception {
        return ((AudioRate[i] >> 24) & 32) > 0;
    }

    public boolean isCompressedTrack(int i) throws Exception {
        return ((AudioRate[i] >> 24) & 128) > 0;
    }

    public boolean hasRingFrame() throws Exception {
        return (Flags & 1) > 0;
    }

    public boolean isYInterlaced() throws Exception {
        return (Flags & 2) > 0;
    }

    public boolean isYDoubled() throws Exception {
        return (Flags & 4) > 0;
    }

}


