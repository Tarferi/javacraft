//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:06
//

package SCSharp.Smk;

import SCSharp.Smk.BigHuffmanTree;
import SCSharp.Smk.BitStream;
import SCSharp.Smk.SmackerDecoder;
import SCSharp.Smk.SmackerFile;
import SCSharp.Smk.SmackerHeader;
import SCSharp.Util;

public class SmackerFile   
{
    private SmackerHeader header;
    public SmackerHeader getHeader() throws Exception {
        return header;
    }

    public void setHeader(SmackerHeader value) throws Exception {
        header = value;
    }

    private UInt32[] frameSizes = new UInt32[]();
    public UInt32[] getFrameSizes() throws Exception {
        return frameSizes;
    }

    public void setFrameSizes(UInt32[] value) throws Exception {
        frameSizes = value;
    }

    private byte[] frameTypes = new byte[]();
    public byte[] getFrameTypes() throws Exception {
        return frameTypes;
    }

    public void setFrameTypes(byte[] value) throws Exception {
        frameTypes = value;
    }

    private boolean isV4 = new boolean();
    public boolean getIsV4() throws Exception {
        return isV4;
    }

    public void setIsV4(boolean value) throws Exception {
        isV4 = value;
    }

    BigHuffmanTree mMap, mClr, full, type;
    public BigHuffmanTree getType() throws Exception {
        return type;
    }

    public void setType(BigHuffmanTree value) throws Exception {
        type = value;
    }

    public BigHuffmanTree getFull() throws Exception {
        return full;
    }

    public void setFull(BigHuffmanTree value) throws Exception {
        full = value;
    }

    public BigHuffmanTree getMClr() throws Exception {
        return mClr;
    }

    public void setMClr(BigHuffmanTree value) throws Exception {
        mClr = value;
    }

    public BigHuffmanTree getMMap() throws Exception {
        return mMap;
    }

    public void setMMap(BigHuffmanTree value) throws Exception {
        mMap = value;
    }

    private Stream stream = new Stream();
    public Stream getStream() throws Exception {
        return stream;
    }

    public void setStream(Stream value) throws Exception {
        stream = value;
    }

    SmackerFile() throws Exception {
    }

    private static SmackerHeader readHeader(Stream s) throws Exception {
        SmackerHeader smk = new SmackerHeader();
        int i = new int();
        /* read and check header */
        smk.Signature = Util.readDWord(s);
        if (smk.Signature != Util.makeTag('S','M','K','2') && smk.Signature != Util.makeTag('S','M','K','4'))
            throw new InvalidDataException("Not an SMK stream");
         
        smk.Width = Util.readDWord(s);
        smk.Height = Util.readDWord(s);
        smk.NbFrames = Util.readDWord(s);
        smk.Pts_Inc = (int)Util.readDWord(s);
        smk.Fps = calcFps(smk);
        smk.Flags = Util.readDWord(s);
        for (i = 0;i < 7;i++)
            smk.AudioSize[i] = Util.readDWord(s);
        smk.TreesSize = Util.readDWord(s);
        smk.NMap_Size = Util.readDWord(s);
        smk.MClr_Size = Util.readDWord(s);
        smk.Full_Size = Util.readDWord(s);
        smk.Type_Size = Util.readDWord(s);
        for (i = 0;i < 7;i++)
            smk.AudioRate[i] = Util.readDWord(s);
            ;
        smk.Dummy = Util.readDWord(s);
        /* setup data */
        if (smk.NbFrames > 0xFFFFFF)
            throw new InvalidDataException("Too many frames: " + smk.NbFrames);
         
        return smk;
    }

    private static double calcFps(SmackerHeader smk) throws Exception {
        if ((int)smk.Pts_Inc > 0)
            return 1000.0 / (int)smk.Pts_Inc;
        else if ((int)smk.Pts_Inc < 0)
            return 100000.0 / (-((int)smk.Pts_Inc));
        else
            return 10.0;  
    }

    public static SmackerFile openFromStream(Stream s) throws Exception {
        int i = new int();
        SmackerFile file = new SmackerFile();
        file.setHeader(readHeader(s));
        uint nbFrames = file.getHeader().NbFrames;
        //The ring frame is not counted!
        if (file.getHeader().hasRingFrame())
            nbFrames++;
         
        file.setFrameSizes(new UInt32[nbFrames]);
        file.setFrameTypes(new byte[nbFrames]);
        file.setIsV4((file.getHeader().Signature != Util.makeTag('S','M','K','2')));
        for (i = 0;i < nbFrames;i++)
        {
            /* read frame info */
            file.getFrameSizes()[i] = Util.readDWord(s);
        }
        for (i = 0;i < nbFrames;i++)
        {
            file.getFrameTypes()[i] = Util.readByte(s);
        }
        //The rest of the header is a bitstream
        BitStream m = new BitStream(s);
        //Read huffman trees
        //MMap
        // System.Console.WriteLine("Mono map tree");
        file.setMMap(new BigHuffmanTree());
        file.getMMap().buildTree(m);
        //MClr (color map)
        //  System.Console.WriteLine("Mono Color tree");
        file.setMClr(new BigHuffmanTree());
        file.getMClr().buildTree(m);
        //Full (full block stuff)
        // System.Console.WriteLine("Full tree");
        file.setFull(new BigHuffmanTree());
        file.getFull().buildTree(m);
        //Type (full block stuff)
        // System.Console.WriteLine("Type descriptor tree");
        file.setType(new BigHuffmanTree());
        file.getType().buildTree(m);
        //We are ready to decode frames
        file.setStream(s);
        return file;
    }

    /**
    * Returns a decoder for this Smackerfile
    */
    public SmackerDecoder getDecoder() throws Exception {
        return new SmackerDecoder(this);
    }

}


