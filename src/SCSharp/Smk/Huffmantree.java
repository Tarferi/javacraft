//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:06
//

package SCSharp.Smk;

import SCSharp.Smk.BitStream;

//using NUnit.Framework;
public class Huffmantree   
{
    protected public static class Node   
    {
        //"0"- branch
        private Node left;
        public Node getLeft() throws Exception {
            return left;
        }

        public void setLeft(Node value) throws Exception {
            left = value;
        }

        //"1"-branch
        private Node right;
        public Node getRight() throws Exception {
            return right;
        }

        public void setRight(Node value) throws Exception {
            right = value;
        }

        //Made public for more speed
        public boolean isLeaf = new boolean();
        public boolean getIsLeaf() throws Exception {
            return isLeaf;
        }

        public void setIsLeaf(boolean value) throws Exception {
            isLeaf = value;
        }

        private int value = new int();
        public int getValue() throws Exception {
            return this.value;
        }

        public void setValue(int value) throws Exception {
            this.value = value;
        }

        public void print(String prefix) throws Exception {
            System.Console.WriteLine(prefix + ((getIsLeaf()) ? "Leaf: " + getValue().ToString() : "No Leaf"));
            if (left != null)
                left.print(prefix + " L:");
             
            if (right != null)
                right.print(prefix + " R:");
             
        }

        public void print() throws Exception {
            print("");
        }
    
    }

    /**
    * Builds a new 8-bit huffmantree. The used algorithm is based on
    * http://wiki.multimedia.cx/index.php?title=Smacker#Packed_Huffman_Trees
    * 
    *  @param m The stream to build the tree from
    */
    public void buildTree(BitStream m) throws Exception {
        //Read tag
        int tag = m.readBits(1);
        //If tag is zero, finish
        if (tag == 0)
            return ;
         
        //Init tree
        rootNode = new Node();
        buildTree(m,rootNode);
        //For some reason we have to skip a bit
        m.readBits(1);
    }

    /**
    * Decodes a value using this tree based on the next bits in the specified stream
    * 
    *  @param m The stream to read bits from
    */
    public int decode(BitStream m) throws Exception {
        Node currentNode = getRootNode();
        if (currentNode == null)
            return 0;
         
        while (!currentNode.isLeaf)
        {
            int bit = m.readBits(1);
            if (bit == 0)
            {
                currentNode = currentNode.getLeft();
            }
            else
            {
                currentNode = currentNode.getRight();
            } 
        }
        return currentNode.getValue();
    }

    protected void buildTree(BitStream m, Node current) throws Exception {
        //Read flag
        int flag = m.readBits(1);
        //If flag is nonzero
        if (flag != 0)
        {
            //Advance to "0"-branch
            Node left = new Node();
            //Recursive call
            buildTree(m,left);
            //The first left-node is actually the root
            if (current == null)
            {
                rootNode = left;
                return ;
            }
            else
                current.setLeft(left); 
        }
        else
        {
            //If flag is zero
            if (current == null)
            {
                current = new Node();
                rootNode = current;
            }
             
            //Read 8 bit leaf
            int leaf = m.readBits(8);
            //Console.WriteLine("Decoded :" + leaf);
            current.setIsLeaf(true);
            current.setValue(leaf);
            return ;
        } 
        //Continue on the "1"-branch
        current.setRight(new Node());
        buildTree(m,current.getRight());
    }

    Node rootNode;
    public Node getRootNode() throws Exception {
        return rootNode;
    }

    public void setRootNode(Node value) throws Exception {
        rootNode = value;
    }

    public void printTree() throws Exception {
        rootNode.print();
    }

}


