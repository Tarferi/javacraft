//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:06
//

package SCSharp.Smk;

import SCSharp.Smk.BitStream;
import SCSharp.Smk.Huffmantree;

public class BigHuffmanTree  extends Huffmantree 
{
    /**
    * Decodes a value using this tree based on the next bits in the specified stream
    * 
    *  @param m The stream to read bits from
    */
    public int decode(BitStream m) throws Exception {
        //int v = base.Decode(m);
        Node currentNode = getRootNode();
        if (currentNode == null)
            return 0;
         
        while (!currentNode.isLeaf)
        {
            int bit = m.readBits(1);
            if (bit == 0)
            {
                currentNode = currentNode.Left;
            }
            else
            {
                currentNode = currentNode.Right;
            } 
        }
        int v = currentNode.Value;
        if (v != iMarker1)
        {
            iMarker3 = iMarker2;
            iMarker2 = iMarker1;
            iMarker1 = v;
            marker3.Value = marker2.Value;
            marker2.Value = marker1.Value;
            marker1.Value = v;
        }
         
        return v;
    }

    /**
    * Resets the dynamic decoder markers to zero
    */
    public void resetDecoder() throws Exception {
        marker1.Value = 0;
        marker2.Value = 0;
        marker3.Value = 0;
        iMarker1 = 0;
        iMarker2 = 0;
        iMarker3 = 0;
    }

    Huffmantree highByteTree;
    Huffmantree lowByteTree;
    Node marker1 = new Node();
    Node marker2 = new Node();
    Node marker3 = new Node();
    int iMarker1 = new int(), iMarker2 = new int(), iMarker3 = new int();
    public void buildTree(BitStream m) throws Exception {
        //Read tag
        int tag = m.readBits(1);
        //If tag is zero, finish
        if (tag == 0)
            return ;
         
        lowByteTree = new Huffmantree();
        lowByteTree.buildTree(m);
        highByteTree = new Huffmantree();
        highByteTree.buildTree(m);
        iMarker1 = m.readBits(16);
        //System.Console.WriteLine("M1:" + iMarker1);
        iMarker2 = m.readBits(16);
        //System.Console.WriteLine("M2:" + iMarker2);
        iMarker3 = m.readBits(16);
        //System.Console.WriteLine("M3:" + iMarker3);
        setRootNode(new Node());
        buildTree(m,getRootNode());
        //For some reason we have to skip a bit
        m.readBits(1);
        if (marker1 == null)
        {
            // System.Console.WriteLine("Not using marker 1");
            marker1 = new Node();
        }
         
        if (marker2 == null)
        {
            //  System.Console.WriteLine("Not using marker 2");
            marker2 = new Node();
        }
         
        if (marker3 == null)
        {
            //   System.Console.WriteLine("Not using marker 3");
            marker3 = new Node();
        }
         
    }

    protected void buildTree(BitStream m, Node current) throws Exception {
        //Read flag
        int flag = m.readBits(1);
        //If flag is nonzero
        if (flag != 0)
        {
            //Advance to "0"-branch
            Node left = new Node();
            //Recursive call
            buildTree(m,left);
            //The first left-node is actually the root
            if (current == null)
            {
                setRootNode(left);
                return ;
            }
            else
                current.Left = left; 
        }
        else
        {
            //If flag is zero
            if (current == null)
            {
                current = new Node();
                setRootNode(current);
            }
             
            //Read 16 bit leaf by decoding the low byte, then the high byte
            int lower = lowByteTree.decode(m);
            int higher = highByteTree.decode(m);
            int leaf = lower | (higher << 8);
            //System.Console.WriteLine("Decoded: " + leaf);
            //If we found one of the markers, store pointers to those nodes.
            if (leaf == iMarker1)
            {
                leaf = 0;
                marker1 = current;
            }
             
            if (leaf == iMarker2)
            {
                leaf = 0;
                marker2 = current;
            }
             
            if (leaf == iMarker3)
            {
                leaf = 0;
                marker3 = current;
            }
             
            current.IsLeaf = true;
            current.Value = leaf;
            return ;
        } 
        //Continue on the "1"-branch
        current.Right = new Node();
        BuildTree(m, current.Right);
    }

}


//This should move to another file
// [TestFixture]
// public class HuffmantreeTest
// {
// [Test]
// public void TestBuild()
// {
//Set up a memory stream first
// MemoryStream stream = new MemoryStream();
// stream.WriteByte(0x6F); //These bytes correspond to the example
// stream.WriteByte(0x20); //bit-sequence on
// stream.WriteByte(0x02); //http://wiki.multimedia.cx/index.php?title=Smacker#Packed_Huffman_Trees
// stream.WriteByte(0x05);
// stream.WriteByte(0x0C);
// stream.WriteByte(0x3A);
// stream.WriteByte(0x80);
// stream.WriteByte(0x00);
// BitStream bs = new BitStream(stream);
// bs.Reset();
//Build a new tree
// Huffmantree tree = new Huffmantree();
// tree.TestDecodeTree(bs);
//Our tree should look like this:
// Assert.AreEqual(tree.RootNode.Left.Left.Left.Value, 3);
// Assert.AreEqual(tree.RootNode.Left.Left.Right.Left.Value, 4);
// Assert.AreEqual(tree.RootNode.Left.Left.Right.Right.Value, 5);
// Assert.AreEqual(tree.RootNode.Left.Right.Value, 6);
// Assert.AreEqual(tree.RootNode.Right.Left.Value, 7);
// Assert.AreEqual(tree.RootNode.Right.Right.Value, 8);
// }
// [Test]
// public void TestDecode()
// {
//Set up a memory stream first
// MemoryStream stream = new MemoryStream();
// stream.WriteByte(0x6F); //These bytes correspond to the example
// stream.WriteByte(0x20); //bit-sequence on
// stream.WriteByte(0x02); //http://wiki.multimedia.cx/index.php?title=Smacker#Packed_Huffman_Trees
// stream.WriteByte(0x05);
// stream.WriteByte(0x0C);
// stream.WriteByte(0x3A);
// stream.WriteByte(0x80);
// stream.WriteByte(0x00);
// BitStream bs = new BitStream(stream);
// bs.Reset();
//Build a new tree
// Huffmantree tree = new Huffmantree();
// tree.TestDecodeTree(bs);
// stream = new MemoryStream();
// stream.WriteByte(0x20); //Bit stream that visits every leaf
// stream.WriteByte(0xB6); //000 ->3 0010 ->4 0011->5 01 -> 6 10 -> 7 11 -> 8
// stream.WriteByte(0x01);
// bs = new BitStream(stream);
// bs.Reset();
// Assert.AreEqual(tree.Decode(bs), 3);
// Assert.AreEqual(tree.Decode(bs), 4);
// Assert.AreEqual(tree.Decode(bs), 5);
// Assert.AreEqual(tree.Decode(bs), 6);
// Assert.AreEqual(tree.Decode(bs), 7);
// Assert.AreEqual(tree.Decode(bs), 8);
// }
// }