//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:04
//

package SCSharp;


public class TriggerAction   
{
    uint location = new uint();
    public uint getLocation() throws Exception {
        return location;
    }

    public void setLocation(uint value) throws Exception {
        location = value;
    }

    uint textIndex = new uint();
    public uint getTextIndex() throws Exception {
        return textIndex;
    }

    public void setTextIndex(uint value) throws Exception {
        textIndex = value;
    }

    uint wavIndex = new uint();
    public uint getWavIndex() throws Exception {
        return wavIndex;
    }

    public void setWavIndex(uint value) throws Exception {
        wavIndex = value;
    }

    uint delay = new uint();
    public uint getDelay() throws Exception {
        return delay;
    }

    public void setDelay(uint value) throws Exception {
        delay = value;
    }

    uint group1 = new uint();
    public uint getGroup1() throws Exception {
        return group1;
    }

    public void setGroup1(uint value) throws Exception {
        group1 = value;
    }

    uint group2 = new uint();
    public uint getGroup2() throws Exception {
        return group2;
    }

    public void setGroup2(uint value) throws Exception {
        group2 = value;
    }

    ushort unitType = new ushort();
    public ushort getUnitType() throws Exception {
        return unitType;
    }

    public void setUnitType(ushort value) throws Exception {
        unitType = value;
    }

    byte action = new byte();
    public byte getAction() throws Exception {
        return action;
    }

    public void setAction(byte value) throws Exception {
        action = value;
    }

    byte _switch = new byte();
    public byte getSwitch() throws Exception {
        return _switch;
    }

    public void setSwitch(byte value) throws Exception {
        _switch = value;
    }

    byte flags = new byte();
    public byte getFlags() throws Exception {
        return flags;
    }

    public void setFlags(byte value) throws Exception {
        flags = value;
    }

}


