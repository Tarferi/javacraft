//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:06
//

package SCSharp;

import SCSharp.ParallaxObject;
import SCSharp.Util;

public class ParallaxLayer   
{
    ParallaxObject[] objects = new ParallaxObject[]();
    public ParallaxLayer(int num_objects) throws Exception {
        objects = new ParallaxObject[num_objects];
    }

    public void readFromStream(Stream stream) throws Exception {
        for (int i = 0;i < objects.Length;i++)
        {
            ushort X = Util.readWord(stream);
            ushort Y = Util.readWord(stream);
            ushort offset = Util.readWord(stream);
            Util.readWord(stream);
            /* read that last, unused word */
            objects[i] = new ParallaxObject(X, Y, offset);
        }
    }

    public ParallaxObject[] getObjects() throws Exception {
        return objects;
    }

}


