//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:08
//

package SCSharp.UI;

import SCSharp.Grp;
import SCSharp.UI.GrpElement;
import SCSharp.UI.UIScreen;

//
// SCSharp.UI.GrpButtonElement
//
// Authors:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
public class GrpButtonElement  extends GrpElement 
{
    public GrpButtonElement(UIScreen screen, Grp grp, byte[] palette, ushort x, ushort y) throws Exception {
        super(screen, grp, palette, x, y);
    }

    public void mouseButtonDown(MouseButtonEventArgs args) throws Exception {
    }

    public void mouseButtonUp(MouseButtonEventArgs args) throws Exception {
        if (PointInside(args.X, args.Y))
        {
            Console.WriteLine("Activating!");
            onActivate();
        }
         
    }

}


