//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:07
//

package SCSharp.UI;

import java.util.ArrayList;
import java.util.List;
import SCSharp.Mpq;
import SCSharp.UI.__MultiDialogEvent;
import SCSharp.UI.Builtins;
import SCSharp.UI.DialogEvent;
import SCSharp.UI.KeystrokeDialog;
import SCSharp.UI.UIDialog;
import SCSharp.UI.UIScreen;

public class HelpDialog  extends UIDialog 
{
    public HelpDialog(UIScreen parent, Mpq mpq) throws Exception {
        super(parent, mpq, "glue\\Palmm", Builtins.rez_HelpMenuBin);
        background_path = null;
    }

    static final int PREVIOUS_ELEMENT_INDEX = 1;
    static final int KEYSTROKE_ELEMENT_INDEX = 2;
    static final int TIPS_INDEX = 3;
    protected void resourceLoader() throws Exception {
        super.resourceLoader();
        for (int i = 0;i < Elements.Count;i++)
            Console.WriteLine("{0}: {1} '{2}'", i, Elements[i].Type, Elements[i].Text);
        Elements[PREVIOUS_ELEMENT_INDEX].Activate += ;
        Elements[KEYSTROKE_ELEMENT_INDEX].Activate += ;
    }

    public DialogEvent Previous;
}


