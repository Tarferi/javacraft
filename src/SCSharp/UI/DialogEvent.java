//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:11
//

package SCSharp.UI;

import CS2JNet.JavaSupport.util.ListSupport;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import SCSharp.UI.DialogEvent;

public interface DialogEvent   
{
    void invoke() throws Exception ;

    System.Collections.Generic.IList<DialogEvent> getInvocationList() throws Exception ;

}


