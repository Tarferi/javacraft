//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:10
//

package SCSharp.UI;

import SCSharp.Chk;
import SCSharp.TriggerAction;
import SCSharp.TriggerData;
import SCSharp.UI.GuiUtil;
import SCSharp.UI.ReadyRoomScreen;

public class BriefingRunner   
{
    TriggerData triggerData;
    Chk scenario;
    ReadyRoomScreen screen;
    String prefix = new String();
    int sleepUntil = new int();
    int totalElapsed = new int();
    int current_action = new int();
    public BriefingRunner(ReadyRoomScreen screen, Chk scenario, String scenario_prefix) throws Exception {
        this.screen = screen;
        this.scenario = scenario;
        this.prefix = scenario_prefix;
        triggerData = scenario.getBriefingData();
    }

    public void play() throws Exception {
        current_action = 0;
    }

    public void stop() throws Exception {
        sleepUntil = 0;
    }

    public void tick(Object sender, TickEventArgs e) throws Exception {
        TriggerAction[] actions = triggerData.Triggers[0].Actions;
        if (current_action == actions.Length)
            return ;
         
        totalElapsed += e.TicksElapsed;
        /* if we're presently waiting, make sure
        			   enough time has gone by.  otherwise
        			   return */
        if (totalElapsed < sleepUntil)
            return ;
         
        totalElapsed = 0;
        while (current_action < actions.Length)
        {
            TriggerAction action = actions[current_action];
            current_action++;
            System.Byte __dummyScrutVar0 = action.getAction();
            if (__dummyScrutVar0.equals(0))
            {
            }
            else /* no action */
            if (__dummyScrutVar0.equals(1))
            {
                sleepUntil = (int)action.getDelay();
                return ;
            }
            else if (__dummyScrutVar0.equals(2))
            {
                GuiUtil.playSound(screen.getMpq(),prefix + "\\" + scenario.GetMapString((int)action.getWavIndex()));
                sleepUntil = (int)action.getDelay();
                return ;
            }
            else if (__dummyScrutVar0.equals(3))
            {
                screen.SetTransmissionText(scenario.GetMapString((int)action.getTextIndex()));
            }
            else if (__dummyScrutVar0.equals(4))
            {
                screen.SetObjectives(scenario.GetMapString((int)action.getTextIndex()));
            }
            else if (__dummyScrutVar0.equals(5))
            {
                Console.WriteLine("show portrait:");
                Console.WriteLine("location = {0}, textindex = {1}, wavindex = {2}, delay = {3}, group1 = {4}, group2 = {5}, unittype = {6}, action = {7}, switch = {8}, flags = {9}", action.getLocation(), action.getTextIndex(), action.getWavIndex(), action.getDelay(), action.getGroup1(), action.getGroup2(), action.getUnitType(), action.getAction(), action.getSwitch(), action.getFlags());
                screen.showPortrait((int)action.getUnitType(),(int)action.getGroup1());
                Console.WriteLine(scenario.GetMapString((int)action.getTextIndex()));
            }
            else if (__dummyScrutVar0.equals(6))
            {
                screen.hidePortrait((int)action.getGroup1());
            }
            else if (__dummyScrutVar0.equals(7))
            {
                Console.WriteLine("Display Speaking Portrait(Slot, Time)");
                Console.WriteLine(scenario.GetMapString((int)action.getTextIndex()));
            }
            else if (__dummyScrutVar0.equals(8))
            {
                Console.WriteLine("Transmission(Text, Slot, Time, Modifier, Wave, WavTime)");
                screen.SetTransmissionText(scenario.GetMapString((int)action.getTextIndex()));
                screen.highlightPortrait((int)action.getGroup1());
                GuiUtil.playSound(screen.getMpq(),prefix + "\\" + scenario.GetMapString((int)action.getWavIndex()));
                sleepUntil = (int)action.getDelay();
                return ;
            }
            else
            {
            }         
        }
    }

}


