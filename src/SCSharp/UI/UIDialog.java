//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:11
//

package SCSharp.UI;

import CS2JNet.JavaSupport.util.ListSupport;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import SCSharp.Mpq;
import SCSharp.UI.__MultiReadyDelegate;
import SCSharp.UI.Layer;
import SCSharp.UI.Painter;
import SCSharp.UI.UIDialog;
import SCSharp.UI.UIScreen;

//
// SCSharp.UI.UIDialog
//
// Authors:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
/* this should probably subclass from UIElement instead...  look into that */
public abstract class UIDialog  extends UIScreen 
{
    protected UIScreen parent;
    boolean dimScreen = new boolean();
    Surface dimScreenSurface = new Surface();
    protected UIDialog(UIScreen parent, Mpq mpq, String prefix, String binFile) throws Exception {
        super(mpq, prefix, binFile);
        this.parent = parent;
        background_translucent = 254;
        background_transparent = 0;
        dimScreen = true;
        dimScreenSurface = new Surface(Painter.SCREEN_RES_X, Painter.SCREEN_RES_Y);
        dimScreenSurface.Alpha = 100;
        dimScreenSurface.AlphaBlending = true;
    }

    public void addToPainter() throws Exception {
        if (getBackground() != null)
            Painter.Add(Layer.DialogBackground, BackgroundPainter);
         
        if (UIPainter != null)
            Painter.Add(Layer.DialogUI, UIPainter.Paint);
         
        if (dimScreen)
            Painter.Add(Layer.DialogDimScreenHack, DimScreenPainter);
         
        Painter.invalidate();
    }

    public void removeFromPainter() throws Exception {
        if (getBackground() != null)
            Painter.Remove(Layer.DialogBackground, BackgroundPainter);
         
        if (UIPainter != null)
            Painter.Remove(Layer.DialogUI, UIPainter.Paint);
         
        if (dimScreen)
            Painter.Remove(Layer.DialogDimScreenHack, DimScreenPainter);
         
        Painter.invalidate();
    }

    void dimScreenPainter(DateTime dt) throws Exception {
        Painter.blit(dimScreenSurface,Painter.getDirty(),Painter.getDirty());
    }

    protected void resourceLoader() throws Exception {
        super.resourceLoader();
        /* figure out where we're going to be located on the screen */
        int baseX = new int(), baseY = new int();
        int si = new int();
        if (getBackground() != null)
        {
            baseX = (Painter.SCREEN_RES_X - getBackground().Width) / 2;
            baseY = (Painter.SCREEN_RES_Y - getBackground().Height) / 2;
            si = 0;
        }
        else
        {
            baseX = Elements[0].X1;
            baseY = Elements[0].Y1;
            si = 1;
        } 
        for (int i = si;i < Elements.Count;i++)
        {
            /* and add that offset to all our elements */
            Elements[i].X1 += (ushort)baseX;
            Elements[i].Y1 += (ushort)baseY;
        }
    }

    public boolean getUseTiles() throws Exception {
        return true;
    }

    public UIScreen getParent() throws Exception {
        return parent;
    }

    public boolean getDimScreen() throws Exception {
        return dimScreen;
    }

    public void setDimScreen(boolean value) throws Exception {
        dimScreen = value;
    }

    public void showDialog(UIDialog dialog) throws Exception {
        Console.WriteLine("showing {0}", dialog);
        if (this.dialog != null)
            throw new Exception("only one active dialog is allowed");
         
        this.dialog = dialog;
        dialog.load();
        dialog.Ready = __MultiReadyDelegate.combine(dialog.Ready,new ReadyDelegate() 
          { 
            public System.Void invoke() throws Exception {
                dialog.addToPainter();
                removeFromPainter();
            }

            public List<ReadyDelegate> getInvocationList() throws Exception {
                List<ReadyDelegate> ret = new ArrayList<ReadyDelegate>();
                ret.add(this);
                return ret;
            }
        
          });
    }

    public void dismissDialog() throws Exception {
        if (dialog == null)
            return ;
         
        dialog.removeFromPainter();
        dialog = null;
        addToPainter();
    }

}


