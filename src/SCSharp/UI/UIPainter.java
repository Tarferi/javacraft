//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:11
//

package SCSharp.UI;

import CS2JNet.System.StringSupport;
import SCSharp.UI.Painter;
import SCSharp.UI.UIElement;

//
// SCSharp.UI.UIPainter
//
// Authors:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
public class UIPainter   
{
    static boolean showBorders = new boolean();
    static {
        try
        {
        }
        catch (Exception __dummyStaticConstructorCatchVar0)
        {
            throw new ExceptionInInitializerError(__dummyStaticConstructorCatchVar0);
        }
    
    }

    List<UIElement> elements = new List<UIElement>();
    public UIPainter(List<UIElement> elements) throws Exception {
        this.elements = elements;
    }

    public void paint(DateTime now) throws Exception {
        for (int i = 0;i < elements.Count;i++)
        {
            UIElement e = elements[i];
            if (e == null)
                continue;
             
            e.paint(now);
            if (showBorders)
            {
                Painter.DrawBox(new Rectangle(new Point(e.getX1(), e.getY1()), new Size(e.getWidth() - 1, e.getHeight() - 1)), e.getVisible() ? Color.Green : Color.Yellow);
                if (StringSupport.equals(e.getText(), ""))
                    e.setText(i.ToString());
                 
            }
             
        }
    }

}


