//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:11
//

package SCSharp.UI;

import CS2JNet.JavaSupport.util.ListSupport;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import SCSharp.UI.__MultiDoneSwooshingDelegate;
import SCSharp.UI.DoneSwooshingDelegate;

//
// SCSharp.UI.SwooshPainter
//
// Authors:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
public class __MultiDoneSwooshingDelegate   implements DoneSwooshingDelegate
{
    public void invoke() throws Exception {
        IList<DoneSwooshingDelegate> copy = new IList<DoneSwooshingDelegate>(), members = this.getInvocationList();
        synchronized (members)
        {
            copy = new LinkedList<DoneSwooshingDelegate>(members);
        }
        for (Object __dummyForeachVar0 : copy)
        {
            DoneSwooshingDelegate d = (DoneSwooshingDelegate)__dummyForeachVar0;
            d.invoke();
        }
    }

    private System.Collections.Generic.IList<DoneSwooshingDelegate> _invocationList = new ArrayList<DoneSwooshingDelegate>();
    public static DoneSwooshingDelegate combine(DoneSwooshingDelegate a, DoneSwooshingDelegate b) throws Exception {
        if (a == null)
            return b;
         
        if (b == null)
            return a;
         
        __MultiDoneSwooshingDelegate ret = new __MultiDoneSwooshingDelegate();
        ret._invocationList = a.getInvocationList();
        ret._invocationList.addAll(b.getInvocationList());
        return ret;
    }

    public static DoneSwooshingDelegate remove(DoneSwooshingDelegate a, DoneSwooshingDelegate b) throws Exception {
        if (a == null || b == null)
            return a;
         
        System.Collections.Generic.IList<DoneSwooshingDelegate> aInvList = a.getInvocationList();
        System.Collections.Generic.IList<DoneSwooshingDelegate> newInvList = ListSupport.removeFinalStretch(aInvList, b.getInvocationList());
        if (aInvList == newInvList)
        {
            return a;
        }
        else
        {
            __MultiDoneSwooshingDelegate ret = new __MultiDoneSwooshingDelegate();
            ret._invocationList = newInvList;
            return ret;
        } 
    }

    public System.Collections.Generic.IList<DoneSwooshingDelegate> getInvocationList() throws Exception {
        return _invocationList;
    }

}


