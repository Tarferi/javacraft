//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:11
//

package SCSharp.UI;

import java.util.ArrayList;
import java.util.List;
import SCSharp.Bin;
import SCSharp.BinElement;
import SCSharp.ElementFlags;
import SCSharp.ElementType;
import SCSharp.Grp;
import SCSharp.Mpq;
import SCSharp.UI.__MultiReadyDelegate;
import SCSharp.UI.ButtonElement;
import SCSharp.UI.ComboBoxElement;
import SCSharp.UI.CursorAnimator;
import SCSharp.UI.DialogBoxElement;
import SCSharp.UI.Game;
import SCSharp.UI.GuiUtil;
import SCSharp.UI.ImageElement;
import SCSharp.UI.LabelElement;
import SCSharp.UI.Layer;
import SCSharp.UI.ListBoxElement;
import SCSharp.UI.Painter;
import SCSharp.UI.Pcx;
import SCSharp.UI.ReadyDelegate;
import SCSharp.UI.TextBoxElement;
import SCSharp.UI.UIDialog;
import SCSharp.UI.UIElement;
import SCSharp.UI.UIPainter;

//
// SCSharp.UI.UIScreen
//
// Authors:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
public abstract class UIScreen   
{
    Surface background = new Surface();
    protected CursorAnimator Cursor;
    protected UIPainter UIPainter;
    protected Bin Bin;
    protected Mpq mpq;
    protected String prefix = new String();
    protected String binFile = new String();
    protected String background_path = new String();
    protected int background_transparent = new int();
    protected int background_translucent = new int();
    protected String fontpal_path = new String();
    protected String effectpal_path = new String();
    protected String arrowgrp_path = new String();
    protected Pcx fontpal;
    protected Pcx effectpal;
    protected List<UIElement> Elements = new List<UIElement>();
    protected UIDialog dialog;
    /* the currently popped up dialog */
    protected UIScreen(Mpq mpq, String prefix, String binFile) throws Exception {
        this.mpq = mpq;
        this.prefix = prefix;
        this.binFile = binFile;
        if (prefix != null)
        {
            background_path = prefix + "\\Backgnd.pcx";
            fontpal_path = prefix + "\\tFont.pcx";
            effectpal_path = prefix + "\\tEffect.pcx";
            arrowgrp_path = prefix + "\\arrow.grp";
        }
         
        background_transparent = 0;
        background_translucent = 254;
    }

    protected UIScreen(Mpq mpq) throws Exception {
        this.mpq = mpq;
    }

    public void swooshIn() throws Exception {
        try
        {
            Console.WriteLine("swooshing in");
            Events.PushUserEvent(new UserEventArgs(new ReadyDelegate() 
              { 
                public System.Void invoke() throws Exception {
                    raiseDoneSwooshing();
                }

                public List<ReadyDelegate> getInvocationList() throws Exception {
                    List<ReadyDelegate> ret = new ArrayList<ReadyDelegate>();
                    ret.add(this);
                    return ret;
                }
            
              }));
        }
        catch (Exception e)
        {
            Console.WriteLine("failed pushing UIScreen.RiseDoneSwooshing: {0}", e);
            Events.PushUserEvent(new UserEventArgs(new ReadyDelegate() 
              { 
                public System.Void invoke() throws Exception {
                    Events.QuitApplication();
                }

                public List<ReadyDelegate> getInvocationList() throws Exception {
                    List<ReadyDelegate> ret = new ArrayList<ReadyDelegate>();
                    ret.add(this);
                    return ret;
                }
            
              }));
        }
    
    }

    public void swooshOut() throws Exception {
        try
        {
            Console.WriteLine("swooshing out");
            Events.PushUserEvent(new UserEventArgs(new ReadyDelegate() 
              { 
                public System.Void invoke() throws Exception {
                    raiseDoneSwooshing();
                }

                public List<ReadyDelegate> getInvocationList() throws Exception {
                    List<ReadyDelegate> ret = new ArrayList<ReadyDelegate>();
                    ret.add(this);
                    return ret;
                }
            
              }));
        }
        catch (Exception e)
        {
            Console.WriteLine("failed pushing UIScreen.RiseDoneSwooshing: {0}", e);
            Events.PushUserEvent(new UserEventArgs(new ReadyDelegate() 
              { 
                public System.Void invoke() throws Exception {
                    Events.QuitApplication();
                }

                public List<ReadyDelegate> getInvocationList() throws Exception {
                    List<ReadyDelegate> ret = new ArrayList<ReadyDelegate>();
                    ret.add(this);
                    return ret;
                }
            
              }));
        }
    
    }

    public void addToPainter() throws Exception {
        Painter.Painting += FirstPaint;
        if (background != null)
            Painter.Add(Layer.Background, BackgroundPainter);
         
        if (UIPainter != null)
            Painter.Add(Layer.UI, UIPainter.Paint);
         
        if (Cursor != null)
            Game.getInstance().setCursor(Cursor);
         
        Painter.invalidate();
    }

    public void removeFromPainter() throws Exception {
        Painter.Painting -= FirstPaint;
        if (background != null)
            Painter.Remove(Layer.Background, BackgroundPainter);
         
        if (UIPainter != null)
            Painter.Remove(Layer.UI, UIPainter.Paint);
         
        if (Cursor != null)
            Game.getInstance().setCursor(null);
         
    }

    public boolean getUseTiles() throws Exception {
        return false;
    }

    public Mpq getMpq() throws Exception {
        return mpq;
    }

    public Surface getBackground() throws Exception {
        return background;
    }

    UIElement xYToElement(int x, int y, boolean onlyUI) throws Exception {
        if (Elements == null)
            return null;
         
        for (Object __dummyForeachVar0 : Elements)
        {
            UIElement e = (UIElement)__dummyForeachVar0;
            if (e.getType() == ElementType.DialogBox)
                continue;
             
            if (onlyUI && e.getType() == ElementType.Image)
                continue;
             
            if (e.getVisible() && e.pointInside(x,y))
                return e;
             
        }
        return null;
    }

    protected UIElement mouseDownElement;
    protected UIElement mouseOverElement;
    public void mouseEnterElement(UIElement element) throws Exception {
        element.mouseEnter();
    }

    public void mouseLeaveElement(UIElement element) throws Exception {
        element.mouseLeave();
    }

    public void activateElement(UIElement element) throws Exception {
        if (!element.getVisible() || !element.getSensitive())
            return ;
         
        Console.WriteLine("activating element {0}", Elements.IndexOf(element));
        element.onActivate();
    }

    // SDL Event handling
    public void mouseButtonDown(MouseButtonEventArgs args) throws Exception {
        if (args.Button != MouseButton.PrimaryButton && args.Button != MouseButton.WheelUp && args.Button != MouseButton.WheelDown)
            return ;
         
        if (mouseDownElement != null)
            Console.WriteLine("mouseDownElement already set in MouseButtonDown");
         
        UIElement element = XYToElement(args.X, args.Y, true);
        if (element != null && element.getVisible() && element.getSensitive())
        {
            mouseDownElement = element;
            if (args.Button == MouseButton.PrimaryButton)
                mouseDownElement.mouseButtonDown(args);
            else
                mouseDownElement.mouseWheel(args); 
        }
         
    }

    public void handleMouseButtonDown(MouseButtonEventArgs args) throws Exception {
        if (dialog != null)
            dialog.handleMouseButtonDown(args);
        else
            mouseButtonDown(args); 
    }

    public void mouseButtonUp(MouseButtonEventArgs args) throws Exception {
        if (args.Button != MouseButton.PrimaryButton && args.Button != MouseButton.WheelUp && args.Button != MouseButton.WheelDown)
            return ;
         
        if (mouseDownElement != null)
        {
            if (args.Button == MouseButton.PrimaryButton)
                mouseDownElement.mouseButtonUp(args);
             
            mouseDownElement = null;
        }
         
    }

    public void handleMouseButtonUp(MouseButtonEventArgs args) throws Exception {
        if (dialog != null)
            dialog.handleMouseButtonUp(args);
        else
            mouseButtonUp(args); 
    }

    public void pointerMotion(MouseMotionEventArgs args) throws Exception {
        if (mouseDownElement != null)
        {
            mouseDownElement.pointerMotion(args);
        }
        else
        {
            UIElement newMouseOverElement = XYToElement(args.X, args.Y, true);
            if (newMouseOverElement != mouseOverElement)
            {
                if (mouseOverElement != null)
                    mouseLeaveElement(mouseOverElement);
                 
                if (newMouseOverElement != null)
                    mouseEnterElement(newMouseOverElement);
                 
            }
             
            mouseOverElement = newMouseOverElement;
        } 
    }

    public void handlePointerMotion(MouseMotionEventArgs args) throws Exception {
        if (dialog != null)
            dialog.handlePointerMotion(args);
        else
            pointerMotion(args); 
    }

    public void keyboardUp(KeyboardEventArgs args) throws Exception {
    }

    public void handleKeyboardUp(KeyboardEventArgs args) throws Exception {
        /* just return if the modifier keys are released */
        if (args.Key >= Key.NumLock && args.Key <= Key.Compose)
            return ;
         
        if (dialog != null)
            dialog.handleKeyboardUp(args);
        else
            keyboardUp(args); 
    }

    public void keyboardDown(KeyboardEventArgs args) throws Exception {
        if (Elements != null)
        {
            for (Object __dummyForeachVar1 : Elements)
            {
                UIElement e = (UIElement)__dummyForeachVar1;
                if ((args.Key == e.getHotkey()) || (args.Key == Key.Return && (e.getFlags() & ElementFlags.DefaultButton) == ElementFlags.DefaultButton) || (args.Key == Key.Escape && (e.getFlags() & ElementFlags.CancelButton) == ElementFlags.CancelButton))
                {
                    activateElement(e);
                    return ;
                }
                 
            }
        }
         
    }

    public void handleKeyboardDown(KeyboardEventArgs args) throws Exception {
        /* just return if the modifier keys are pressed */
        if (args.Key >= Key.NumLock && args.Key <= Key.Compose)
            return ;
         
        if (dialog != null)
            dialog.handleKeyboardDown(args);
        else
            keyboardDown(args); 
    }

    protected void screenDisplayed() throws Exception {
    }

    public ReadyDelegate FirstPainted;
    public ReadyDelegate DoneSwooshing;
    public ReadyDelegate Ready;
    boolean loaded = new boolean();
    protected void firstPaint(Object sender, EventArgs args) throws Exception {
        if (FirstPainted != null)
            FirstPainted.invoke();
         
        Painter.Painting -= FirstPaint;
    }

    protected void raiseReadyEvent() throws Exception {
        if (Ready != null)
            Ready.invoke();
         
    }

    protected void raiseDoneSwooshing() throws Exception {
        if (DoneSwooshing != null)
            DoneSwooshing.invoke();
         
    }

    protected void backgroundPainter(DateTime dt) throws Exception {
        int background_x = (Painter.getWidth() - background.Width) / 2;
        int background_y = (Painter.getHeight() - background.Height) / 2;
        Painter.blit(background,new Point(background_x, background_y));
    }

    int translucentIndex = 254;
    protected int getTranslucentIndex() throws Exception {
        return translucentIndex;
    }

    protected void setTranslucentIndex(int value) throws Exception {
        translucentIndex = value;
    }

    protected void resourceLoader() throws Exception {
        Stream s = new Stream();
        fontpal = null;
        effectpal = null;
        if (fontpal_path != null)
        {
            Console.WriteLine("loading font palette");
            s = (Stream)mpq.GetResource(fontpal_path);
            if (s != null)
            {
                fontpal = new Pcx();
                fontpal.readFromStream(s,-1,-1);
            }
             
        }
         
        if (effectpal_path != null)
        {
            Console.WriteLine("loading cursor palette");
            s = (Stream)mpq.GetResource(effectpal_path);
            if (s != null)
            {
                effectpal = new Pcx();
                effectpal.readFromStream(s,-1,-1);
            }
             
            if (effectpal != null && arrowgrp_path != null)
            {
                Console.WriteLine("loading arrow cursor");
                Grp arrowgrp = (Grp)mpq.GetResource(arrowgrp_path);
                if (arrowgrp != null)
                {
                    Cursor = new CursorAnimator(arrowgrp, effectpal.getPalette());
                    Cursor.setHotSpot(64,64);
                }
                 
            }
             
        }
         
        if (background_path != null)
        {
            Console.WriteLine("loading background");
            background = GuiUtil.surfaceFromStream((Stream)mpq.GetResource(background_path),background_translucent,background_transparent);
        }
         
        Elements = new List<UIElement>();
        if (binFile != null)
        {
            Console.WriteLine("loading ui elements");
            Bin = (Bin)mpq.GetResource(binFile);
            if (Bin == null)
                throw new Exception(String.Format("specified file '{0}' does not exist", binFile));
             
            for (Object __dummyForeachVar2 : Bin.Elements)
            {
                /* convert all the BinElements to UIElements for our subclasses to use */
                BinElement el = (BinElement)__dummyForeachVar2;
                //					Console.WriteLine ("{0}: {1}", el.text, el.flags);
                UIElement ui_el = null;
                switch(el.type)
                {
                    case DialogBox: 
                        ui_el = new DialogBoxElement(this, el, fontpal.getRgbData());
                        break;
                    case Image: 
                        ui_el = new ImageElement(this, el, fontpal.getRgbData(), translucentIndex);
                        break;
                    case TextBox: 
                        ui_el = new TextBoxElement(this, el, fontpal.getRgbData());
                        break;
                    case ListBox: 
                        ui_el = new ListBoxElement(this, el, fontpal.getRgbData());
                        break;
                    case ComboBox: 
                        ui_el = new ComboBoxElement(this, el, fontpal.getRgbData());
                        break;
                    case LabelLeftAlign: 
                    case LabelCenterAlign: 
                    case LabelRightAlign: 
                        ui_el = new LabelElement(this, el, fontpal.getRgbData());
                        break;
                    case Button: 
                    case DefaultButton: 
                    case ButtonWithoutBorder: 
                        ui_el = new ButtonElement(this, el, fontpal.getRgbData());
                        break;
                    case Slider: 
                    case OptionButton: 
                    case CheckBox: 
                        ui_el = new UIElement(this, el, fontpal.getRgbData());
                        break;
                    default: 
                        Console.WriteLine("unhandled case {0}", el.type);
                        ui_el = new UIElement(this, el, fontpal.getRgbData());
                        break;
                
                }
                Elements.Add(ui_el);
            }
        }
         
        UIPainter = new UIPainter(Elements);
    }

    void loadResources() throws Exception {
        resourceLoader();
        Events.PushUserEvent(new UserEventArgs(new ReadyDelegate() 
          { 
            public System.Void invoke() throws Exception {
                finishedLoading();
            }

            public List<ReadyDelegate> getInvocationList() throws Exception {
                List<ReadyDelegate> ret = new ArrayList<ReadyDelegate>();
                ret.add(this);
                return ret;
            }
        
          }));
    }

    public void load() throws Exception {
        if (loaded)
            Events.PushUserEvent(new UserEventArgs(new ReadyDelegate() 
              { 
                public System.Void invoke() throws Exception {
                    raiseReadyEvent();
                }

                public List<ReadyDelegate> getInvocationList() throws Exception {
                    List<ReadyDelegate> ret = new ArrayList<ReadyDelegate>();
                    ret.add(this);
                    return ret;
                }
            
              }));
        else
            Events.PushUserEvent(new UserEventArgs(new ReadyDelegate() 
              { 
                public System.Void invoke() throws Exception {
                    loadResources();
                }

                public List<ReadyDelegate> getInvocationList() throws Exception {
                    List<ReadyDelegate> ret = new ArrayList<ReadyDelegate>();
                    ret.add(this);
                    return ret;
                }
            
              })); 
    }

    void finishedLoading() throws Exception {
        loaded = true;
        raiseReadyEvent();
    }

    public void showDialog(UIDialog dialog) throws Exception {
        Console.WriteLine("showing {0}", dialog);
        if (this.dialog != null)
            throw new Exception("only one active dialog is allowed");
         
        this.dialog = dialog;
        dialog.load();
        dialog.Ready = __MultiReadyDelegate.combine(dialog.Ready,new ReadyDelegate() 
          { 
            public System.Void invoke() throws Exception {
                dialog.addToPainter();
            }

            public List<ReadyDelegate> getInvocationList() throws Exception {
                List<ReadyDelegate> ret = new ArrayList<ReadyDelegate>();
                ret.add(this);
                return ret;
            }
        
          });
    }

    public void dismissDialog() throws Exception {
        if (dialog == null)
            return ;
         
        dialog.removeFromPainter();
        dialog = null;
    }

}


