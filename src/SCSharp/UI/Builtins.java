//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:06
//

package SCSharp.UI;


//
// SCSharp.UI.Builtins
//
// Author:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
public class Builtins   
{
    /* title screen */
    public static String rez_TitleDlgBin = "rez\\titledlg.bin";
    public static String TitlePcx = "glue\\title\\title-beta.pcx";
    /* UI strings */
    public static String rez_GluAllTbl = "rez\\gluAll.tbl";
    /* Main menu */
    public static String rez_GluMainBin = "rez\\gluMain.bin";
    public static String rez_GluGameModeBin = "rez\\gluGameMode.bin";
    /* Campaign screen */
    public static String rez_GluCmpgnBin = "rez\\glucmpgn.bin";
    // original
    public static String rez_GluExpcmpgnBin = "rez\\gluexpcmpgn.bin";
    // broodwar
    /* Play custom screen */
    public static String rez_GluCustmBin = "rez\\gluCustm.bin";
    public static String rez_GluCreatBin = "rez\\gluCreat.bin";
    /* load saved screen */
    public static String rez_GluLoadBin = "rez\\gluLoad.bin";
    /* Login screen */
    public static String rez_GluLoginBin = "rez\\gluLogin.bin";
    public static String rez_GluPEditBin = "rez\\gluPEdit.bin";
    public static String rez_GluPOkBin = "rez\\gluPOk.bin";
    public static String rez_GluPOkCancelBin = "rez\\gluPOkCancel.bin";
    /* Ready room */
    public static String rez_GluRdyBin = "rez\\glurdy{0}.bin";
    /* Connection screen */
    public static String rez_GluConnBin = "rez\\gluConn.bin";
    /* Score screen */
    public static String rez_GluScoreBin = "rez\\gluScore.bin";
    public static String Scorev_pMainPcx = "glue\\score{0}v\\pMain.pcx";
    public static String Scored_pMainPcx = "glue\\score{0}d\\pMain.pcx";
    public static String Game_ConsolePcx = "game\\{0}console.pcx";
    /* scripts */
    public static String IScriptBin = "scripts\\iscript.bin";
    public static String AIScriptBin = "scripts\\aiscript.bin";
    /* arr files */
    public static String FlingyDat = "arr\\flingy.dat";
    public static String FlingyTbl = "arr\\flingy.tbl";
    public static String ImagesDat = "arr\\images.dat";
    public static String ImagesTbl = "arr\\images.tbl";
    public static String MapdataDat = "arr\\mapdata.dat";
    public static String MapdataTbl = "arr\\mapdata.tbl";
    public static String OrdersDat = "arr\\orders.dat";
    public static String OrdersTbl = "arr\\orders.tbl";
    public static String MapDataDat = "arr\\mapdata.dat";
    public static String MapDataTbl = "arr\\mapdata.tbl";
    public static String PortDataDat = "arr\\portdata.dat";
    public static String PortDataTbl = "arr\\portdata.tbl";
    public static String SfxDataDat = "arr\\sfxdata.dat";
    public static String SfxDataTbl = "arr\\sfxdata.tbl";
    public static String SpritesDat = "arr\\sprites.dat";
    public static String SpritesTbl = "arr\\sprites.tbl";
    public static String TechdataDat = "arr\\techdata.dat";
    public static String TechdataTbl = "arr\\techdata.tbl";
    public static String UnitsDat = "arr\\units.dat";
    public static String UnitsTbl = "arr\\units.tbl";
    public static String UpgradesDat = "arr\\upgrades.dat";
    public static String UpgradesTbl = "arr\\upgrades.tbl";
    public static String WeaponsDat = "arr\\weapons.dat";
    public static String WeaponsTbl = "arr\\weapons.tbl";
    /* sounds */
    public static String MouseoverWav = "sound\\glue\\mouseover.wav";
    public static String Mousedown2Wav = "sound\\glue\\mousedown2.wav";
    public static String SwishinWav = "sound\\glue\\swishin.wav";
    public static String SwishoutWav = "sound\\glue\\swishout.wav";
    /* credits */
    public static String RezCrdtexpTxt = "rez\\crdt_exp.txt";
    public static String RezCrdtlistTxt = "rez\\crdt_lst.txt";
    /* music */
    public static String music_titleWav = "music\\title.wav";
    /* game menus */
    public static String rez_GameMenuBin = "rez\\gamemenu.bin";
    public static String rez_OptionsBin = "rez\\options.bin";
    public static String rez_SndDlgBin = "rez\\snd_dlg.bin";
    public static String rez_SpdDlgBin = "rez\\spd_dlg.bin";
    public static String rez_VideoBin = "rez\\video.bin";
    public static String rez_NetDlgBin = "rez\\netdlg.bin";
    public static String rez_ObjctDlgBin = "rez\\objctdlg.bin";
    public static String rez_AbrtMenuBin = "rez\\abrtmenu.bin";
    public static String rez_RestartBin = "rez\\restart.bin";
    public static String rez_QuitBin = "rez\\quit.bin";
    public static String rez_Quit2MnuBin = "rez\\quit2mnu.bin";
    public static String rez_HelpMenuBin = "rez\\helpmenu.bin";
    public static String rez_HelpBin = "rez\\help.bin";
    public static String rez_HelpTxtTbl = "rez\\help_txt.tbl";
    public static String rez_MinimapBin = "rez\\minimap.bin";
}


