//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:08
//

package SCSharp.UI;

import java.util.ArrayList;
import java.util.List;
import SCSharp.FlingyDat;
import SCSharp.ImagesDat;
import SCSharp.IScriptBin;
import SCSharp.MapDataDat;
import SCSharp.Mpq;
import SCSharp.PortDataDat;
import SCSharp.SfxDataDat;
import SCSharp.SpritesDat;
import SCSharp.Tbl;
import SCSharp.UI.Builtins;
import SCSharp.UI.Game;
import SCSharp.UI.GlobalResources;
import SCSharp.UI.ReadyDelegate;
import SCSharp.UI.Resources;
import SCSharp.UnitsDat;

public class GlobalResources   
{
    Mpq stardatMpq;
    Mpq broodatMpq;
    Resources starcraftResources;
    Resources broodwarResources;
    static GlobalResources instance;
    public static GlobalResources getInstance() throws Exception {
        return instance;
    }

    public GlobalResources(Mpq stardatMpq, Mpq broodatMpq) throws Exception {
        if (instance != null)
            throw new Exception("There can only be one GlobalResources");
         
        this.stardatMpq = stardatMpq;
        this.broodatMpq = broodatMpq;
        starcraftResources = new Resources();
        broodwarResources = new Resources();
        instance = this;
    }

    public void load() throws Exception {
        ThreadPool.QueueUserWorkItem(ResourceLoader);
    }

    public void loadSingleThreaded() throws Exception {
        resourceLoader(null);
    }

    Resources getResources() throws Exception {
        return Game.getInstance().getPlayingBroodWar() ? broodwarResources : starcraftResources;
    }

    public Tbl getImagesTbl() throws Exception {
        return getResources().ImagesTbl;
    }

    public Tbl getSfxDataTbl() throws Exception {
        return getResources().SfxDataTbl;
    }

    public Tbl getSpritesTbl() throws Exception {
        return getResources().SpritesTbl;
    }

    public Tbl getGluAllTbl() throws Exception {
        return getResources().GluAllTbl;
    }

    public ImagesDat getImagesDat() throws Exception {
        return getResources().ImagesDat;
    }

    public SpritesDat getSpritesDat() throws Exception {
        return getResources().SpritesDat;
    }

    public SfxDataDat getSfxDataDat() throws Exception {
        return getResources().SfxDataDat;
    }

    public IScriptBin getIScriptBin() throws Exception {
        return getResources().IscriptBin;
    }

    public UnitsDat getUnitsDat() throws Exception {
        return getResources().UnitsDat;
    }

    public FlingyDat getFlingyDat() throws Exception {
        return getResources().FlingyDat;
    }

    public MapDataDat getMapDataDat() throws Exception {
        return getResources().MapDataDat;
    }

    public PortDataDat getPortDataDat() throws Exception {
        return getResources().PortDataDat;
    }

    public Tbl getMapDataTbl() throws Exception {
        return getResources().MapDataTbl;
    }

    public Tbl getPortDataTbl() throws Exception {
        return getResources().PortDataTbl;
    }

    public Resources getStarDat() throws Exception {
        return starcraftResources;
    }

    public Resources getBrooDat() throws Exception {
        return broodwarResources;
    }

    void resourceLoader(Object state) throws Exception {
        try
        {
            starcraftResources.ImagesTbl = (Tbl)stardatMpq.GetResource(Builtins.ImagesTbl);
            starcraftResources.SfxDataTbl = (Tbl)stardatMpq.GetResource(Builtins.SfxDataTbl);
            starcraftResources.SpritesTbl = (Tbl)stardatMpq.GetResource(Builtins.SpritesTbl);
            starcraftResources.GluAllTbl = (Tbl)stardatMpq.GetResource(Builtins.rez_GluAllTbl);
            starcraftResources.MapDataTbl = (Tbl)stardatMpq.GetResource(Builtins.MapDataTbl);
            starcraftResources.PortDataTbl = (Tbl)stardatMpq.GetResource(Builtins.PortDataTbl);
            starcraftResources.ImagesDat = (ImagesDat)stardatMpq.GetResource(Builtins.ImagesDat);
            starcraftResources.SfxDataDat = (SfxDataDat)stardatMpq.GetResource(Builtins.SfxDataDat);
            starcraftResources.SpritesDat = (SpritesDat)stardatMpq.GetResource(Builtins.SpritesDat);
            starcraftResources.IscriptBin = (IScriptBin)stardatMpq.GetResource(Builtins.IScriptBin);
            starcraftResources.UnitsDat = (UnitsDat)stardatMpq.GetResource(Builtins.UnitsDat);
            starcraftResources.FlingyDat = (FlingyDat)stardatMpq.GetResource(Builtins.FlingyDat);
            starcraftResources.MapDataDat = (MapDataDat)stardatMpq.GetResource(Builtins.MapDataDat);
            starcraftResources.PortDataDat = (PortDataDat)stardatMpq.GetResource(Builtins.PortDataDat);
            if (broodatMpq != null)
            {
                broodwarResources.ImagesTbl = (Tbl)broodatMpq.GetResource(Builtins.ImagesTbl);
                broodwarResources.SfxDataTbl = (Tbl)broodatMpq.GetResource(Builtins.SfxDataTbl);
                broodwarResources.SpritesTbl = (Tbl)broodatMpq.GetResource(Builtins.SpritesTbl);
                broodwarResources.GluAllTbl = (Tbl)broodatMpq.GetResource(Builtins.rez_GluAllTbl);
                broodwarResources.MapDataTbl = (Tbl)broodatMpq.GetResource(Builtins.MapDataTbl);
                broodwarResources.PortDataTbl = (Tbl)broodatMpq.GetResource(Builtins.PortDataTbl);
                broodwarResources.ImagesDat = (ImagesDat)broodatMpq.GetResource(Builtins.ImagesDat);
                broodwarResources.SfxDataDat = (SfxDataDat)broodatMpq.GetResource(Builtins.SfxDataDat);
                broodwarResources.SpritesDat = (SpritesDat)broodatMpq.GetResource(Builtins.SpritesDat);
                broodwarResources.IscriptBin = (IScriptBin)broodatMpq.GetResource(Builtins.IScriptBin);
                broodwarResources.UnitsDat = (UnitsDat)broodatMpq.GetResource(Builtins.UnitsDat);
                broodwarResources.FlingyDat = (FlingyDat)broodatMpq.GetResource(Builtins.FlingyDat);
                broodwarResources.MapDataDat = (MapDataDat)broodatMpq.GetResource(Builtins.MapDataDat);
                broodwarResources.PortDataDat = (PortDataDat)broodatMpq.GetResource(Builtins.PortDataDat);
            }
             
            Events.PushUserEvent(new UserEventArgs(new ReadyDelegate() 
              { 
                // notify we're ready to roll
                public System.Void invoke() throws Exception {
                    finishedLoading();
                }

                public List<ReadyDelegate> getInvocationList() throws Exception {
                    List<ReadyDelegate> ret = new ArrayList<ReadyDelegate>();
                    ret.add(this);
                    return ret;
                }
            
              }));
        }
        catch (Exception e)
        {
            Console.WriteLine("Global Resource loader failed: {0}", e);
            Events.PushUserEvent(new UserEventArgs(new ReadyDelegate() 
              { 
                public System.Void invoke() throws Exception {
                    Events.QuitApplication();
                }

                public List<ReadyDelegate> getInvocationList() throws Exception {
                    List<ReadyDelegate> ret = new ArrayList<ReadyDelegate>();
                    ret.add(this);
                    return ret;
                }
            
              }));
        }
    
    }

    void finishedLoading() throws Exception {
        if (Ready != null)
            Ready.invoke();
         
    }

    public ReadyDelegate Ready;
}


