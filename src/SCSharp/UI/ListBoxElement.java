//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:08
//

package SCSharp.UI;

import CS2JNet.JavaSupport.util.ListSupport;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import SCSharp.BinElement;
import SCSharp.UI.GuiUtil;
import SCSharp.UI.ListBoxSelectionChanged;
import SCSharp.UI.UIElement;
import SCSharp.UI.UIScreen;

//
// SCSharp.UI.ListBoxElement
//
// Authors:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
public class ListBoxElement  extends UIElement 
{
    List<String> items = new List<String>();
    int cursor = -1;
    boolean selectable = true;
    int num_visible = new int();
    int first_visible = new int();
    public ListBoxElement(UIScreen screen, BinElement el, byte[] palette) throws Exception {
        super(screen, el, palette);
        items = new List<String>();
        num_visible = getHeight() / getFont().LineSize;
        first_visible = 0;
    }

    public void keyboardDown(KeyboardEventArgs args) throws Exception {
        boolean selection_changed = false;
        /* navigation keys */
        if (args.Key == Key.UpArrow)
        {
            if (cursor > 0)
            {
                cursor--;
                selection_changed = true;
                if (cursor < first_visible)
                    first_visible = cursor;
                 
            }
             
        }
        else if (args.Key == Key.DownArrow)
        {
            if (cursor < items.Count - 1)
            {
                cursor++;
                selection_changed = true;
                if (cursor >= first_visible + num_visible)
                    first_visible = cursor - num_visible + 1;
                 
            }
             
        }
          
        if (selection_changed)
        {
            invalidate();
            if (SelectionChanged != null)
                SelectionChanged.invoke(cursor);
             
        }
         
    }

    boolean selecting = new boolean();
    int selectionIndex = new int();
    public void mouseWheel(MouseButtonEventArgs args) throws Exception {
        boolean need_redraw = false;
        if (args.Button == MouseButton.WheelUp)
        {
            if (first_visible > 0)
            {
                first_visible--;
                need_redraw = true;
            }
             
        }
        else
        {
            if (first_visible + num_visible < items.Count - 1)
            {
                first_visible++;
                need_redraw = true;
            }
             
        } 
        if (need_redraw)
            invalidate();
         
    }

    public void mouseButtonDown(MouseButtonEventArgs args) throws Exception {
        boolean need_redraw = false;
        /* if we're over the scrollbar handle that here */
        /* otherwise start our selection */
        selecting = true;
        /* otherwise, select the clicked-on item (if
        			 * there is one) */
        int index = (args.Y - getY1()) / getFont().LineSize + first_visible;
        if (index < items.Count)
        {
            selectionIndex = index;
            need_redraw = true;
        }
         
        if (need_redraw)
            invalidate();
         
    }

    public void pointerMotion(MouseMotionEventArgs args) throws Exception {
        if (!selecting)
            return ;
         
        int index = (args.Y - getY1()) / getFont().LineSize + first_visible;
        if (index < items.Count)
        {
            selectionIndex = index;
            invalidate();
        }
         
    }

    public void mouseButtonUp(MouseButtonEventArgs args) throws Exception {
        if (!selecting)
            return ;
         
        selecting = false;
        if (selectionIndex != cursor)
        {
            cursor = selectionIndex;
            if (SelectionChanged != null)
                SelectionChanged.invoke(cursor);
             
        }
         
        invalidate();
    }

    public boolean getSelectable() throws Exception {
        return selectable;
    }

    public void setSelectable(boolean value) throws Exception {
        selectable = value;
    }

    public int getSelectedIndex() throws Exception {
        return cursor;
    }

    public void setSelectedIndex(int value) throws Exception {
        if (value < 0 || value > items.Count)
            throw new ArgumentException("value");
         
        if (cursor != value)
        {
            cursor = value;
            if (SelectionChanged != null)
                SelectionChanged.invoke(cursor);
             
            invalidate();
        }
         
    }

    public String getSelectedItem() throws Exception {
        return items[cursor];
    }

    public void addItem(String item) throws Exception {
        items.Add(item);
        if (cursor == -1)
        {
            cursor = 0;
            if (SelectionChanged != null)
                SelectionChanged.invoke(cursor);
             
        }
         
        invalidate();
    }

    public void removeAt(int index) throws Exception {
        items.RemoveAt(index);
        if (items.Count == 0)
            cursor = -1;
         
        invalidate();
    }

    public void clear() throws Exception {
        items.Clear();
        cursor = -1;
        invalidate();
    }

    public boolean contains(String item) throws Exception {
        return items.Contains(item);
    }

    protected Surface createSurface() throws Exception {
        Surface surf = new Surface(getWidth(), getHeight());
        int y = 0;
        for (int i = first_visible;i < first_visible + num_visible;i++)
        {
            if (i >= items.Count)
                break;
             
            Surface item_surface = GuiUtil.ComposeText(items[i], getFont(), getPalette(), (!selectable || (!selecting && cursor == i) || (selecting && selectionIndex == i)) ? 4 : 24);
            surf.Blit(item_surface, new Point(0, y));
            y += item_surface.Height;
        }
        surf.TransparentColor = Color.Black;
        return surf;
    }

    /* XXX */
    public ListBoxSelectionChanged SelectionChanged;
}


