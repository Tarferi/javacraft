//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:07
//

package SCSharp.UI;

import SCSharp.Grp;
import SCSharp.UI.GuiUtil;
import SCSharp.UI.Layer;
import SCSharp.UI.Painter;

//
// SCSharp.UI.CursorAnimator
//
// Authors:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
public class CursorAnimator   
{
    Grp grp;
    int totalElapsed = new int();
    int millisDelay = 100;
    int current_frame = new int();
    Surface[] surfaces = new Surface[]();
    int x = new int();
    int y = new int();
    int hot_x = new int();
    int hot_y = new int();
    byte[] palette = new byte[]();
    public CursorAnimator(Grp grp, byte[] palette) throws Exception {
        this.grp = grp;
        this.x = 100;
        this.y = 100;
        this.palette = palette;
        surfaces = new Surface[grp.getFrameCount()];
    }

    public void setHotSpot(int hot_x, int hot_y) throws Exception {
        Painter.invalidate(new Rectangle(x - hot_x, y - hot_y, grp.Width, grp.Height));
        this.hot_x = hot_x;
        this.hot_y = hot_y;
        Painter.invalidate(new Rectangle(x - hot_x, y - hot_y, grp.Width, grp.Height));
    }

    public void setPosition(int x, int y) throws Exception {
        Painter.invalidate(new Rectangle(x - hot_x, y - hot_y, grp.Width, grp.Height));
        this.x = x;
        this.y = y;
        Painter.invalidate(new Rectangle(x - hot_x, y - hot_y, grp.Width, grp.Height));
    }

    public int getX() throws Exception {
        return x;
    }

    public int getY() throws Exception {
        return y;
    }

    public int getHotX() throws Exception {
        return hot_x;
    }

    public int getHotY() throws Exception {
        return hot_y;
    }

    public void addToPainter() throws Exception {
        Painter.Add(Layer.Cursor, Paint);
        Events.Tick += CursorTick;
    }

    public void removeFromPainter() throws Exception {
        Painter.Remove(Layer.Cursor, Paint);
        Events.Tick -= CursorTick;
    }

    public void cursorTick(Object sender, TickEventArgs e) throws Exception {
        totalElapsed += e.TicksElapsed;
        if (totalElapsed < millisDelay)
            return ;
         
        totalElapsed = 0;
        current_frame++;
        Painter.invalidate(new Rectangle(x - hot_x, y - hot_y, grp.Width, grp.Height));
    }

    void paint(DateTime now) throws Exception {
        int draw_x = (int)(x - hot_x);
        int draw_y = (int)(y - hot_y);
        if (current_frame == grp.FrameCount)
            current_frame = 0;
         
        if (surfaces[current_frame] == null)
            surfaces[current_frame] = GuiUtil.CreateSurfaceFromBitmap(grp.GetFrame(current_frame), grp.Width, grp.Height, palette, true);
         
        Painter.Blit(surfaces[current_frame], new Point(draw_x, draw_y));
    }

}


