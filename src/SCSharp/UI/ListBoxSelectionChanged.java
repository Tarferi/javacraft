//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:08
//

package SCSharp.UI;

import CS2JNet.JavaSupport.util.ListSupport;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import SCSharp.UI.ListBoxSelectionChanged;

public interface ListBoxSelectionChanged   
{
    void invoke(int selectedIndex) throws Exception ;

    System.Collections.Generic.IList<ListBoxSelectionChanged> getInvocationList() throws Exception ;

}


