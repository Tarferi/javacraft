//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:09
//

package SCSharp.UI;

import CS2JNet.JavaSupport.util.ListSupport;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import SCSharp.UI.__MultiPainterDelegate;
import SCSharp.UI.PainterDelegate;

public class __MultiPainterDelegate   implements PainterDelegate
{
    public void invoke(DateTime now) throws Exception {
        IList<PainterDelegate> copy = new IList<PainterDelegate>(), members = this.getInvocationList();
        synchronized (members)
        {
            copy = new LinkedList<PainterDelegate>(members);
        }
        for (Object __dummyForeachVar0 : copy)
        {
            PainterDelegate d = (PainterDelegate)__dummyForeachVar0;
            d.invoke(now);
        }
    }

    private System.Collections.Generic.IList<PainterDelegate> _invocationList = new ArrayList<PainterDelegate>();
    public static PainterDelegate combine(PainterDelegate a, PainterDelegate b) throws Exception {
        if (a == null)
            return b;
         
        if (b == null)
            return a;
         
        __MultiPainterDelegate ret = new __MultiPainterDelegate();
        ret._invocationList = a.getInvocationList();
        ret._invocationList.addAll(b.getInvocationList());
        return ret;
    }

    public static PainterDelegate remove(PainterDelegate a, PainterDelegate b) throws Exception {
        if (a == null || b == null)
            return a;
         
        System.Collections.Generic.IList<PainterDelegate> aInvList = a.getInvocationList();
        /* the time of the last animation tick */
        System.Collections.Generic.IList<PainterDelegate> newInvList = ListSupport.removeFinalStretch(aInvList, b.getInvocationList());
        if (aInvList == newInvList)
        {
            return a;
        }
        else
        {
            __MultiPainterDelegate ret = new __MultiPainterDelegate();
            ret._invocationList = newInvList;
            return ret;
        } 
    }

    public System.Collections.Generic.IList<PainterDelegate> getInvocationList() throws Exception {
        return _invocationList;
    }

}


