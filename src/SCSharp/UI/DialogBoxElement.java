//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:07
//

package SCSharp.UI;

import SCSharp.BinElement;
import SCSharp.Grp;
import SCSharp.UI.GuiUtil;
import SCSharp.UI.Pcx;
import SCSharp.UI.UIElement;
import SCSharp.UI.UIScreen;

//
// SCSharp.UI.DialogBoxElement
//
// Authors:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
public class DialogBoxElement  extends UIElement 
{
    public DialogBoxElement(UIScreen screen, BinElement el, byte[] palette) throws Exception {
        super(screen, el, palette);
        tileGrp = (Grp)getMpq().GetResource("dlgs\\tile.grp");
    }

    Grp tileGrp;
    static final int TILE_TR = 2;
    static final int TILE_T = 1;
    static final int TILE_TL = 0;
    static final int TILE_R = 5;
    static final int TILE_C = 4;
    static final int TILE_L = 3;
    static final int TILE_BR = 8;
    static final int TILE_B = 7;
    static final int TILE_BL = 6;
    void tileRow(Surface surf, Grp grp, byte[] pal, int l, int c, int r, int y) throws Exception {
        Surface lsurf = GuiUtil.CreateSurfaceFromBitmap(grp.getFrame(l), grp.getWidth(), grp.getHeight(), pal, 41, 0);
        Surface csurf = GuiUtil.CreateSurfaceFromBitmap(grp.getFrame(c), grp.getWidth(), grp.getHeight(), pal, 41, 0);
        Surface rsurf = GuiUtil.CreateSurfaceFromBitmap(grp.getFrame(r), grp.getWidth(), grp.getHeight(), pal, 41, 0);
        surf.Blit(lsurf, new Point(0, y));
        for (int x = grp.getWidth();x < surf.Width - grp.getWidth();x += grp.getWidth())
            surf.Blit(csurf, new Point(x, y));
        surf.Blit(rsurf, new Point(surf.Width - grp.getWidth(), y));
    }

    protected Surface createSurface() throws Exception {
        if (getParentScreen().getBackground() == null && getParentScreen().getUseTiles())
        {
            Surface surf = new Surface(getWidth(), getHeight());
            surf.Fill(new Rectangle(new Point(0, 0), new Size(getWidth(), getHeight())), Color.FromArgb(0, 0, 0, 0));
            surf.TransparentColor = Color.Black;
            /* XXX */
            Pcx pal = new Pcx();
            pal.readFromStream((Stream)getMpq().GetResource("unit\\cmdbtns\\ticon.pcx"),-1,-1);
            /* tile the top border */
            tileRow(surf,tileGrp,pal.getPalette(),TILE_TL,TILE_T,TILE_TR,0);
            for (int y = tileGrp.Height - 2;y < surf.Height - tileGrp.Height;y += tileGrp.Height - 2)
                /* tile everything down to the bottom border */
                tileRow(surf,tileGrp,pal.getPalette(),TILE_L,TILE_C,TILE_R,y);
            /* tile the bottom row */
            TileRow(surf, tileGrp, pal.getPalette(), TILE_BL, TILE_B, TILE_BR, surf.Height - tileGrp.Height);
            return surf;
        }
        else
            return null; 
    }

}


