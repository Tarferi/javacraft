//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:07
//

package SCSharp.UI;

import java.util.ArrayList;
import java.util.List;
import SCSharp.Mpq;
import SCSharp.UI.__MultiPlayerEvent;
import SCSharp.UI.Layer;
import SCSharp.UI.Painter;
import SCSharp.UI.PlayerEvent;
import SCSharp.UI.SmackerPlayer;
import SCSharp.UI.UIScreen;

//
// SCSharp.UI.Cinematic
//
// Authors:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
public class Cinematic  extends UIScreen 
{
    SmackerPlayer player;
    String resourcePath = new String();
    public Cinematic(Mpq mpq, String resourcePath) throws Exception {
        super(mpq, null, null);
        this.resourcePath = resourcePath;
    }

    protected void firstPaint(Object sender, EventArgs args) throws Exception {
        super.firstPaint(sender,args);
        player = new SmackerPlayer((Stream)mpq.GetResource(resourcePath));
        player.Finished = __MultiPlayerEvent.combine(player.Finished,new PlayerEvent() 
          { 
            public System.Void invoke() throws Exception {
                playerFinished();
            }

            public List<PlayerEvent> getInvocationList() throws Exception {
                List<PlayerEvent> ret = new ArrayList<PlayerEvent>();
                ret.add(this);
                return ret;
            }
        
          });
        player.FrameReady = __MultiPlayerEvent.combine(player.FrameReady,new PlayerEvent() 
          { 
            public System.Void invoke() throws Exception {
                playerFrameReady();
            }

            public List<PlayerEvent> getInvocationList() throws Exception {
                List<PlayerEvent> ret = new ArrayList<PlayerEvent>();
                ret.add(this);
                return ret;
            }
        
          });
        player.play();
    }

    public void addToPainter() throws Exception {
        super.addToPainter();
        Painter.Add(Layer.Background, VideoPainter);
    }

    public void removeFromPainter() throws Exception {
        super.removeFromPainter();
        player.stop();
        player = null;
        Painter.Remove(Layer.Background, VideoPainter);
    }

    Surface surf = new Surface();
    void playerFrameReady() throws Exception {
        if (surf != null)
            surf.Dispose();
         
        surf = new Surface(player.getSurface());
        if (player.getWidth() != Painter.getWidth() || player.getHeight() != Painter.getHeight())
        {
            double horiz_zoom = (double)Painter.getWidth() / player.getWidth();
            double vert_zoom = (double)Painter.getHeight() / player.getHeight();
            double zoom = new double();
            if (horiz_zoom < vert_zoom)
                zoom = horiz_zoom;
            else
                zoom = vert_zoom; 
        }
         
        // FIXME: newSDL
        //surf.Scale (zoom);
        /* signal to the painter to redraw the screen */
        Painter.invalidate();
    }

    void videoPainter(DateTime dt) throws Exception {
        if (surf != null)
            Painter.blit(surf,new Point((Painter.getWidth() - surf.Width) / 2, (Painter.getHeight() - surf.Height) / 2));
         
    }

    public PlayerEvent Finished;
    void playerFinished() throws Exception {
        if (Finished != null)
            Finished.invoke();
         
    }

    public void keyboardDown(KeyboardEventArgs args) throws Exception {
        if (args.Key == Key.Escape || args.Key == Key.Return || args.Key == Key.Space)
        {
            player.stop();
            playerFinished();
        }
         
    }

}


