//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:08
//

package SCSharp.UI;

import CS2JNet.JavaSupport.language.RefSupport;
import java.util.ArrayList;
import java.util.List;
import SCSharp.Got;
import SCSharp.Grp;
import SCSharp.Chk;
import SCSharp.InitialUnits;
import SCSharp.Mpq;
import SCSharp.ParallaxObject;
import SCSharp.Spk;
import SCSharp.Tbl;
import SCSharp.Tileset;
import SCSharp.UI.__MultiDialogEvent;
import SCSharp.UI.Builtins;
import SCSharp.UI.CursorAnimator;
import SCSharp.UI.Game;
import SCSharp.UI.GameMenuDialog;
import SCSharp.UI.GrpButtonElement;
import SCSharp.UI.GrpElement;
import SCSharp.UI.GuiUtil;
import SCSharp.UI.ImageElement;
import SCSharp.UI.LabelElement;
import SCSharp.UI.Layer;
import SCSharp.UI.MapRenderer;
import SCSharp.UI.MovieElement;
import SCSharp.UI.Painter;
import SCSharp.UI.Pcx;
import SCSharp.UI.SmackerPlayer;
import SCSharp.UI.Sprite;
import SCSharp.UI.SpriteManager;
import SCSharp.UI.UIScreen;
import SCSharp.UI.Unit;
import SCSharp.UnitInfo;
import SCSharp.Util;

//
// SCSharp.UI.GameScreen
//
// Authors:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
public class GameScreen  extends UIScreen 
{
    public enum HudLabels
    {
        UnitName,
        ResourceUsed,
        ResourceProvided,
        ResourceTotal,
        ResourceMax,
        Count
    }
    //Mpq scenario_mpq;
    Chk scenario;
    Got template;
    /* the deltas associated with scrolling */
    int horiz_delta = new int();
    int vert_delta = new int();
    /* the x,y of the topleft of the screen */
    int topleft_x = new int(), topleft_y = new int();
    /* magic number alert.. */
    static final int MINIMAP_X = 7;
    static final int MINIMAP_Y = 349;
    static final int MINIMAP_WIDTH = 125;
    static final int MINIMAP_HEIGHT = 125;
    static final int SCROLL_DELTA = 15;
    static final int MOUSE_MOVE_BORDER = 10;
    static final int SCROLL_CURSOR_UL = 0;
    static final int SCROLL_CURSOR_U = 1;
    static final int SCROLL_CURSOR_UR = 2;
    static final int SCROLL_CURSOR_R = 3;
    static final int SCROLL_CURSOR_DR = 4;
    static final int SCROLL_CURSOR_D = 5;
    static final int SCROLL_CURSOR_DL = 6;
    static final int SCROLL_CURSOR_L = 7;
    CursorAnimator[] ScrollCursors = new CursorAnimator[]();
    static final int TARG_CURSOR_G = 0;
    static final int TARG_CURSOR_Y = 1;
    static final int TARG_CURSOR_R = 2;
    CursorAnimator[] TargetCursors = new CursorAnimator[]();
    static final int MAG_CURSOR_G = 0;
    static final int MAG_CURSOR_Y = 1;
    static final int MAG_CURSOR_R = 2;
    CursorAnimator[] MagCursors = new CursorAnimator[]();
    Tbl statTxt;
    Grp wireframe;
    Grp cmdicons;
    byte[] cmdicon_palette = new byte[]();
    //byte[] unit_palette;
    byte[] tileset_palette = new byte[]();
    //		Player[] players;
    List<Unit> units = new List<Unit>();
    ImageElement hudElement;
    GrpElement wireframeElement;
    MovieElement portraitElement;
    MapRenderer mapRenderer;
    GrpButtonElement[] cmdButtonElements = new GrpButtonElement[]();
    LabelElement[] labelElements = new LabelElement[]();
    public GameScreen(Mpq mpq, Mpq scenario_mpq, Chk scenario, Got template) throws Exception {
        super(mpq);
        effectpal_path = "game\\tblink.pcx";
        arrowgrp_path = "cursor\\arrow.grp";
        fontpal_path = "game\\tfontgam.pcx";
        //this.scenario_mpq = scenario_mpq;
        this.scenario = scenario;
        this.template = template;
        ScrollCursors = new CursorAnimator[8];
    }

    public GameScreen(Mpq mpq, String prefix, Chk scenario) throws Exception {
        super(mpq);
        this.effectpal_path = "game\\tblink.pcx";
        this.arrowgrp_path = "cursor\\arrow.grp";
        this.fontpal_path = "game\\tfontgam.pcx";
        //this.scenario_mpq = scenario_mpq;
        this.scenario = scenario;
        ScrollCursors = new CursorAnimator[8];
    }

    Surface[] starfield_layers = new Surface[]();
    void paintStarfield(DateTime dt) throws Exception {
        float scroll_factor = 1.0f;
        float[] factors = new float[starfield_layers.Length];
        for (int i = 0;i < starfield_layers.Length;i++)
        {
            factors[i] = scroll_factor;
            scroll_factor *= 0.75f;
        }
        Rectangle dest = new Rectangle();
        Rectangle source = new Rectangle();
        for (int i = starfield_layers.Length - 1;i >= 0;i--)
        {
            int scroll_x = (int)(topleft_x * factors[i]);
            int scroll_y = (int)(topleft_y * factors[i]);
            if (scroll_x > Painter.SCREEN_RES_X)
                scroll_x %= Painter.SCREEN_RES_X;
             
            if (scroll_y > 375)
                scroll_y %= 375;
             
            dest = new Rectangle(new Point(0, 0), new Size(Painter.SCREEN_RES_X - scroll_x, 375 - scroll_y));
            source = dest;
            source.X += scroll_x;
            source.Y += scroll_y;
            Painter.Blit(starfield_layers[i], dest, source);
            if (scroll_x != 0)
            {
                dest = new Rectangle(new Point(Painter.SCREEN_RES_X - scroll_x, 0), new Size(scroll_x, 375 - scroll_y));
                source = dest;
                source.X -= Painter.SCREEN_RES_X - scroll_x;
                source.Y += scroll_y;
                Painter.Blit(starfield_layers[i], dest, source);
            }
             
            if (scroll_y != 0)
            {
                dest = new Rectangle(new Point(0, 375 - scroll_y), new Size(Painter.SCREEN_RES_X - scroll_x, scroll_y));
                source = dest;
                source.X += scroll_x;
                source.Y -= 375 - scroll_y;
                Painter.Blit(starfield_layers[i], dest, source);
            }
             
            if (scroll_x != 0 || scroll_y != 0)
            {
                dest = new Rectangle(new Point(Painter.SCREEN_RES_X - scroll_x, 375 - scroll_y), new Size(scroll_x, scroll_y));
                source = dest;
                source.X -= Painter.SCREEN_RES_X - scroll_x;
                source.Y -= 375 - scroll_y;
                Painter.Blit(starfield_layers[i], dest, source);
            }
             
        }
    }

    ushort[] button_xs = new ushort[]{ 506, 552, 598 };
    ushort[] button_ys = new ushort[]{ 360, 400, 440 };
    void paintMinimap(DateTime dt) throws Exception {
        Rectangle rect = new Rectangle(new Point((int)((float)topleft_x / (float)mapRenderer.getMapWidth() * MINIMAP_WIDTH + MINIMAP_X), (int)((float)topleft_y / (float)mapRenderer.getMapHeight() * MINIMAP_HEIGHT + MINIMAP_Y)), new Size((int)((float)Painter.SCREEN_RES_X / (float)mapRenderer.getMapWidth() * MINIMAP_WIDTH), (int)((float)Painter.SCREEN_RES_Y / (float)mapRenderer.getMapHeight() * MINIMAP_HEIGHT)));
        Painter.DrawBox(rect, Color.Green);
    }

    public void addToPainter() throws Exception {
        super.addToPainter();
        Painter.Add(Layer.Hud, PaintMinimap);
        if (scenario.Tileset == Tileset.Platform)
            Painter.Add(Layer.Background, PaintStarfield);
         
        Painter.Add(Layer.Map, mapRenderer.Paint);
        SpriteManager.addToPainter();
    }

    public void removeFromPainter() throws Exception {
        super.removeFromPainter();
        Painter.Remove(Layer.Hud, PaintMinimap);
        if (scenario.Tileset == Tileset.Platform)
            Painter.Remove(Layer.Background, PaintStarfield);
         
        Painter.Remove(Layer.Map, mapRenderer.Paint);
        SpriteManager.removeFromPainter();
    }

    protected void resourceLoader() throws Exception {
        super.resourceLoader();
        /* create the element corresponding to the hud */
        hudElement = new ImageElement(this, 0, 0, 640, 480, getTranslucentIndex());
        hudElement.setText(String.Format(Builtins.Game_ConsolePcx, Util.RaceCharLower[((Enum)Game.getInstance().getRace()).ordinal()]));
        hudElement.setVisible(true);
        Elements.Add(hudElement);
        /* create the portrait playing area */
        portraitElement = new MovieElement(this,415,415,48,48,false);
        portraitElement.setVisible(true);
        Elements.Add(portraitElement);
        Pcx pcx = new Pcx();
        pcx.readFromStream((Stream)mpq.GetResource("game\\tunit.pcx"),-1,-1);
        //unit_palette = pcx.Palette;
        pcx = new Pcx();
        pcx.readFromStream((Stream)mpq.GetResource("tileset\\badlands\\dark.pcx"),0,0);
        tileset_palette = pcx.getPalette();
        if (scenario.Tileset == Tileset.Platform)
        {
            Spk starfield = (Spk)mpq.GetResource("parallax\\star.spk");
            starfield_layers = new Surface[starfield.getLayers().Length];
            for (int i = 0;i < starfield_layers.Length;i++)
            {
                starfield_layers[i] = new Surface(Painter.SCREEN_RES_X, Painter.SCREEN_RES_Y);
                starfield_layers[i].TransparentColor = Color.Black;
                for (int o = 0;o < starfield.getLayers()[i].Objects.Length;o++)
                {
                    ParallaxObject obj = starfield.getLayers()[i].Objects[o];
                    starfield_layers[i].Fill(new Rectangle(new Point(obj.X, obj.Y), new Size(2, 2)), Color.White);
                }
            }
        }
         
        mapRenderer = new MapRenderer(mpq,scenario,Painter.SCREEN_RES_X,Painter.SCREEN_RES_Y);
        // load the cursors we'll show when scrolling with the mouse
        String[] cursornames = new String[]{ "cursor\\ScrollUL.grp", "cursor\\ScrollU.grp", "cursor\\ScrollUR.grp", "cursor\\ScrollR.grp", "cursor\\ScrollDR.grp", "cursor\\ScrollD.grp", "cursor\\ScrollDL.grp", "cursor\\ScrollL.grp" };
        ScrollCursors = new CursorAnimator[cursornames.Length];
        for (int i = 0;i < cursornames.Length;i++)
        {
            ScrollCursors[i] = new CursorAnimator((Grp)mpq.GetResource(cursornames[i]), effectpal.getPalette());
            ScrollCursors[i].SetHotSpot(60, 60);
        }
        // load the mag cursors
        String[] magcursornames = new String[]{ "cursor\\MagG.grp", "cursor\\MagY.grp", "cursor\\MagR.grp" };
        MagCursors = new CursorAnimator[magcursornames.Length];
        for (int i = 0;i < magcursornames.Length;i++)
        {
            MagCursors[i] = new CursorAnimator((Grp)mpq.GetResource(magcursornames[i]), effectpal.getPalette());
            MagCursors[i].SetHotSpot(60, 60);
        }
        // load the targeting cursors
        String[] targetcursornames = new String[]{ "cursor\\TargG.grp", "cursor\\TargY.grp", "cursor\\TargR.grp" };
        TargetCursors = new CursorAnimator[targetcursornames.Length];
        for (int i = 0;i < targetcursornames.Length;i++)
        {
            TargetCursors[i] = new CursorAnimator((Grp)mpq.GetResource(targetcursornames[i]), effectpal.getPalette());
            TargetCursors[i].SetHotSpot(60, 60);
        }
        /* the following could be made global to speed up the entry to the game screen.. */
        statTxt = (Tbl)mpq.GetResource("rez\\stat_txt.tbl");
        // load the wireframe image info
        wireframe = (Grp)mpq.GetResource("unit\\wirefram\\wirefram.grp");
        // load the command icons
        cmdicons = (Grp)mpq.GetResource("unit\\cmdbtns\\cmdicons.grp");
        pcx = new Pcx();
        pcx.readFromStream((Stream)mpq.GetResource("unit\\cmdbtns\\ticon.pcx"),0,0);
        cmdicon_palette = pcx.getPalette();
        // create the wireframe display element
        wireframeElement = new GrpElement(this, wireframe, cmdicon_palette, 170, 390);
        wireframeElement.setVisible(false);
        Elements.Add(wireframeElement);
        labelElements = new LabelElement[((Enum)HudLabels.Count).ordinal()];
        labelElements[((Enum)HudLabels.UnitName).ordinal()] = new LabelElement(this, fontpal.getPalette(), GuiUtil.getFonts(getMpq())[1], 254, 390);
        labelElements[((Enum)HudLabels.ResourceUsed).ordinal()] = new LabelElement(this, fontpal.getPalette(), GuiUtil.getFonts(getMpq())[0], 292, 420);
        labelElements[((Enum)HudLabels.ResourceProvided).ordinal()] = new LabelElement(this, fontpal.getPalette(), GuiUtil.getFonts(getMpq())[0], 292, 434);
        labelElements[((Enum)HudLabels.ResourceTotal).ordinal()] = new LabelElement(this, fontpal.getPalette(), GuiUtil.getFonts(getMpq())[0], 292, 448);
        labelElements[((Enum)HudLabels.ResourceMax).ordinal()] = new LabelElement(this, fontpal.getPalette(), GuiUtil.getFonts(getMpq())[0], 292, 462);
        for (int i = 0;i < labelElements.Length;i++)
            Elements.Add(labelElements[i]);
        cmdButtonElements = new GrpButtonElement[9];
        int x = 0;
        int y = 0;
        for (int i = 0;i < cmdButtonElements.Length;i++)
        {
            cmdButtonElements[i] = new GrpButtonElement(this, cmdicons, cmdicon_palette, button_xs[x], button_ys[y]);
            x++;
            if (x == 3)
            {
                x = 0;
                y++;
            }
             
            cmdButtonElements[i].Visible = false;
            Elements.Add(cmdButtonElements[i]);
        }
        placeInitialUnits();
        Events.Tick += ScrollTick;
    }

    void clipTopLeft() throws Exception {
        if (topleft_x < 0)
            topleft_x = 0;
         
        if (topleft_y < 0)
            topleft_y = 0;
         
        if (topleft_x > mapRenderer.getMapWidth() - Painter.SCREEN_RES_X)
            topleft_x = mapRenderer.getMapWidth() - Painter.SCREEN_RES_X;
         
        if (topleft_y > mapRenderer.getMapHeight() - Painter.SCREEN_RES_Y)
            topleft_y = mapRenderer.getMapHeight() - Painter.SCREEN_RES_Y;
         
    }

    void updateCursor() throws Exception {
        if (mouseOverElement == null)
        {
            /* are we over a unit?  if so, display the mag cursor */
            unitUnderCursor = null;
            for (int i = 0;i < units.Count;i++)
            {
                Unit u = units[i];
                Sprite s = u.getSprite();
                int sx = new int(), sy = new int();
                RefSupport<int> refVar___0 = new RefSupport<int>();
                RefSupport<int> refVar___1 = new RefSupport<int>();
                s.getPosition(refVar___0,refVar___1);
                sx = refVar___0.getValue();
                sy = refVar___1.getValue();
                int cursor_x = Game.getInstance().getCursor().getX() + topleft_x;
                int cursor_y = Game.getInstance().getCursor().getY() + topleft_y;
                int half_width = u.getWidth() / 2;
                int half_height = u.getHeight() / 2;
                if (cursor_x < sx + half_width && cursor_x > sx - half_width && cursor_y < sy + half_height && cursor_y > sy - half_height)
                {
                    Game.getInstance().setCursor(MagCursors[MAG_CURSOR_G]);
                    unitUnderCursor = u;
                    break;
                }
                 
            }
        }
         
    }

    int scroll_elapsed = new int();
    public void scrollTick(Object sender, TickEventArgs e) throws Exception {
        scroll_elapsed += e.TicksElapsed;
        if (scroll_elapsed < 20)
            return ;
         
        scroll_elapsed = 0;
        if (horiz_delta == 0 && vert_delta == 0)
            return ;
         
        int old_topleft_x = topleft_x;
        int old_topleft_y = topleft_y;
        topleft_x += horiz_delta;
        topleft_y += vert_delta;
        clipTopLeft();
        if (old_topleft_x == topleft_x && old_topleft_y == topleft_y)
            return ;
         
        SpriteManager.setUpperLeft(topleft_x,topleft_y);
        mapRenderer.setUpperLeft(topleft_x,topleft_y);
        updateCursor();
        Painter.invalidate(new Rectangle(new Point(0, 0), new Size(Painter.SCREEN_RES_X, 375)));
    }

    boolean buttonDownInMinimap = new boolean();
    Unit unitUnderCursor;
    Unit selectedUnit;
    void recenter(int x, int y) throws Exception {
        topleft_x = x - Painter.SCREEN_RES_X / 2;
        topleft_y = y - Painter.SCREEN_RES_Y / 2;
        clipTopLeft();
        SpriteManager.setUpperLeft(topleft_x,topleft_y);
        mapRenderer.setUpperLeft(topleft_x,topleft_y);
        updateCursor();
        Painter.invalidate(new Rectangle(new Point(0, 0), new Size(Painter.SCREEN_RES_X, Painter.SCREEN_RES_Y)));
    }

    void recenterFromMinimap(int x, int y) throws Exception {
        int map_x = (int)((float)(x - MINIMAP_X) / MINIMAP_WIDTH * mapRenderer.getMapWidth());
        int map_y = (int)((float)(y - MINIMAP_Y) / MINIMAP_HEIGHT * mapRenderer.getMapHeight());
        recenter(map_x,map_y);
    }

    public void mouseButtonDown(MouseButtonEventArgs args) throws Exception {
        if (mouseOverElement != null)
        {
            super.mouseButtonDown(args);
        }
        else if (args.X > MINIMAP_X && args.X < MINIMAP_X + MINIMAP_WIDTH && args.Y > MINIMAP_Y && args.Y < MINIMAP_Y + MINIMAP_HEIGHT)
        {
            RecenterFromMinimap(args.X, args.Y);
            buttonDownInMinimap = true;
        }
        else
        {
            if (selectedUnit != unitUnderCursor)
            {
                Console.WriteLine("hey there, keyboard.modifierkeystate = {0}", Keyboard.ModifierKeyState);
                // if we have a selected unit and we
                // right click (or ctrl-click?), try
                // to move there
                if (selectedUnit != null && (Keyboard.ModifierKeyState & ModifierKeys.ShiftKeys) != 0)
                {
                    Console.WriteLine("And... we're walking");
                    int pixel_x = args.X + topleft_x;
                    int pixel_y = args.Y + topleft_y;
                    // calculate the megatile
                    int megatile_x = pixel_x >> 5;
                    int megatile_y = pixel_y >> 5;
                    Console.WriteLine("megatile {0},{1}", megatile_x, megatile_y);
                    // the mini tile
                    int minitile_x = pixel_x >> 2;
                    int minitile_y = pixel_y >> 2;
                    Console.WriteLine("minitile {0},{1} ({2},{3} in the megatile)", minitile_x, minitile_y, minitile_x - megatile_x * 8, minitile_y - megatile_y * 8);
                    if (selectedUnit.getYesSound() != null)
                    {
                        String sound_resource = String.Format("sound\\{0}", selectedUnit.getYesSound());
                        Console.WriteLine("sound_resource = {0}", sound_resource);
                        GuiUtil.playSound(mpq,sound_resource);
                    }
                     
                    selectedUnit.move(mapRenderer,minitile_x,minitile_y);
                    return ;
                }
                 
                portraitElement.stop();
                selectedUnit = unitUnderCursor;
                if (selectedUnit == null)
                {
                    portraitElement.setVisible(false);
                    wireframeElement.setVisible(false);
                    for (int i = 0;i < ((Enum)HudLabels.Count).ordinal();i++)
                        labelElements[i].Visible = false;
                }
                else
                {
                    Console.WriteLine("selected unit: {0}", selectedUnit);
                    Console.WriteLine("selectioncircle = {0}", selectedUnit.getSelectionCircleOffset());
                    if (selectedUnit.getPortrait() == null)
                    {
                        portraitElement.setVisible(false);
                    }
                    else
                    {
                        String portrait_resource = String.Format("portrait\\{0}0.smk", selectedUnit.getPortrait());
                        portraitElement.setPlayer(new SmackerPlayer((Stream)mpq.GetResource(portrait_resource),1));
                        portraitElement.play();
                        portraitElement.setVisible(true);
                    } 
                    if (selectedUnit.getWhatSound() != null)
                    {
                        String sound_resource = String.Format("sound\\{0}", selectedUnit.getWhatSound());
                        Console.WriteLine("sound_resource = {0}", sound_resource);
                        GuiUtil.playSound(mpq,sound_resource);
                    }
                     
                    /* set up the wireframe */
                    wireframeElement.setFrame(selectedUnit.getUnitId());
                    wireframeElement.setVisible(true);
                    /* then display info about the selected unit */
                    labelElements[((Enum)HudLabels.UnitName).ordinal()].Text = statTxt[selectedUnit.getUnitId()];
                    if (true)
                    {
                        /* XXX unit is a building */
                        labelElements[((Enum)HudLabels.ResourceUsed).ordinal()].Text = statTxt[820 + ((Enum)Game.getInstance().getRace()).ordinal()];
                        labelElements[((Enum)HudLabels.ResourceProvided).ordinal()].Text = statTxt[814 + ((Enum)Game.getInstance().getRace()).ordinal()];
                        labelElements[((Enum)HudLabels.ResourceTotal).ordinal()].Text = statTxt[817 + ((Enum)Game.getInstance().getRace()).ordinal()];
                        labelElements[((Enum)HudLabels.ResourceMax).ordinal()].Text = statTxt[823 + ((Enum)Game.getInstance().getRace()).ordinal()];
                        for (int i = 0;i < ((Enum)HudLabels.Count).ordinal();i++)
                            labelElements[i].Visible = true;
                    }
                     
                    /* then fill in the command buttons */
                    int[] cmd_indices = new int[]();
                    switch(selectedUnit.getUnitId())
                    {
                        case 106: 
                            cmd_indices = new int[]{ 7, -1, -1, -1, -1, 286, -1, -1, 282 };
                            break;
                        default: 
                            Console.WriteLine("cmd_indices == -1");
                            cmd_indices = new int[]{ -1, -1, -1, -1, -1, -1, -1, -1, -1 };
                            break;
                    
                    }
                    for (int i = 0;i < cmd_indices.Length;i++)
                    {
                        if (cmd_indices[i] == -1)
                        {
                            cmdButtonElements[i].Visible = false;
                        }
                        else
                        {
                            cmdButtonElements[i].Visible = true;
                            cmdButtonElements[i].Frame = cmd_indices[i];
                        } 
                    }
                } 
            }
             
        }  
    }

    public void mouseButtonUp(MouseButtonEventArgs args) throws Exception {
        if (mouseDownElement != null)
            super.mouseButtonUp(args);
        else if (buttonDownInMinimap)
            buttonDownInMinimap = false;
          
    }

    public void pointerMotion(MouseMotionEventArgs args) throws Exception {
        if (buttonDownInMinimap)
        {
            RecenterFromMinimap(args.X, args.Y);
        }
        else
        {
            super.pointerMotion(args);
            // if the mouse is over one of the
            // normal UIElements in the HUD, deal
            // with it
            if (mouseOverElement != null)
            {
                return ;
            }
             
            // XXX for now, just return.
            // if the mouse is over the hud area, return
            int hudIndex = (args.X + args.Y * hudElement.getPcx().getWidth()) * 4;
            if (hudElement.getPcx().getRgbaData()[hudIndex] == 0xff)
            {
                Game.getInstance().setCursor(Cursor);
                return ;
            }
             
            if (args.X < MOUSE_MOVE_BORDER)
            {
                horiz_delta = -SCROLL_DELTA;
            }
            else if (args.X + MOUSE_MOVE_BORDER > Painter.SCREEN_RES_X)
            {
                horiz_delta = SCROLL_DELTA;
            }
            else
            {
                horiz_delta = 0;
            }  
            if (args.Y < MOUSE_MOVE_BORDER)
            {
                vert_delta = -SCROLL_DELTA;
            }
            else if (args.Y + MOUSE_MOVE_BORDER > Painter.SCREEN_RES_Y)
            {
                vert_delta = SCROLL_DELTA;
            }
            else
            {
                vert_delta = 0;
            }  
            // we update the cursor to show the
            // scrolling animations here, since it
            // only happens on pointer motion.
            if (horiz_delta < 0)
                if (vert_delta < 0)
                    Game.getInstance().setCursor(ScrollCursors[SCROLL_CURSOR_UL]);
                else if (vert_delta > 0)
                    Game.getInstance().setCursor(ScrollCursors[SCROLL_CURSOR_DL]);
                else
                    Game.getInstance().setCursor(ScrollCursors[SCROLL_CURSOR_L]);  
            else if (horiz_delta > 0)
                if (vert_delta < 0)
                    Game.getInstance().setCursor(ScrollCursors[SCROLL_CURSOR_UR]);
                else if (vert_delta > 0)
                    Game.getInstance().setCursor(ScrollCursors[SCROLL_CURSOR_DR]);
                else
                    Game.getInstance().setCursor(ScrollCursors[SCROLL_CURSOR_R]);  
            else if (vert_delta < 0)
                Game.getInstance().setCursor(ScrollCursors[SCROLL_CURSOR_U]);
            else if (vert_delta > 0)
                Game.getInstance().setCursor(ScrollCursors[SCROLL_CURSOR_D]);
            else
                Game.getInstance().setCursor(Cursor);    
            updateCursor();
        } 
    }

    public void keyboardUp(KeyboardEventArgs args) throws Exception {
        if (args.Key == Key.RightArrow || args.Key == Key.LeftArrow)
        {
            horiz_delta = 0;
        }
        else if (args.Key == Key.UpArrow || args.Key == Key.DownArrow)
        {
            vert_delta = 0;
        }
          
    }

    public void keyboardDown(KeyboardEventArgs args) throws Exception {
        Key __dummyScrutVar1 = args.Key;
        if (__dummyScrutVar1.equals(Key.F10))
        {
            GameMenuDialog d = new GameMenuDialog(this,mpq);
            d.ReturnToGame = __MultiDialogEvent.combine(d.ReturnToGame,new DialogEvent() 
              { 
                public System.Void invoke() throws Exception {
                    dismissDialog();
                }

                public List<DialogEvent> getInvocationList() throws Exception {
                    List<DialogEvent> ret = new ArrayList<DialogEvent>();
                    ret.add(this);
                    return ret;
                }
            
              });
            showDialog(d);
        }
        else if (__dummyScrutVar1.equals(Key.RightArrow))
        {
            horiz_delta = SCROLL_DELTA;
        }
        else if (__dummyScrutVar1.equals(Key.LeftArrow))
        {
            horiz_delta = -SCROLL_DELTA;
        }
        else if (__dummyScrutVar1.equals(Key.DownArrow))
        {
            vert_delta = SCROLL_DELTA;
        }
        else if (__dummyScrutVar1.equals(Key.UpArrow))
        {
            vert_delta = -SCROLL_DELTA;
        }
             
    }

    void placeInitialUnits() throws Exception {
        List<UnitInfo> unit_infos = scenario.Units;
        List<Unit> startLocations = new List<Unit>();
        units = new List<Unit>();
        for (Object __dummyForeachVar0 : unit_infos)
        {
            UnitInfo unitinfo = (UnitInfo)__dummyForeachVar0;
            if (unitinfo.unit_id == 0xffff)
                break;
             
            Unit unit = new Unit(unitinfo);
            /* we handle start locations in a special way, below */
            if (unit.getFlingyId() == 140)
            {
                startLocations.Add(unit);
                continue;
            }
             
            //players[unitinfo.player].AddUnit (unit);
            unit.createSprite(mpq,tileset_palette);
            units.Add(unit);
        }
        if (template != null && (template.InitialUnits != InitialUnits.UseMapSettings))
        {
            for (Object __dummyForeachVar1 : startLocations)
            {
                Unit sl = (Unit)__dummyForeachVar1;
                /* terran command center = 106,
                					   zerg hatchery = 131,
                					   protoss nexus = 154 */
                Unit unit = new Unit(154);
                unit.setX(sl.getX());
                unit.setY(sl.getY());
                unit.createSprite(mpq,tileset_palette);
                units.Add(unit);
            }
        }
         
        /* for now assume the player is at startLocations[0], and center the view there */
        Recenter(startLocations[0].X, startLocations[0].Y);
    }

}


