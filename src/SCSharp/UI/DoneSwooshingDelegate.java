//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:11
//

package SCSharp.UI;

import CS2JNet.JavaSupport.util.ListSupport;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import SCSharp.UI.DoneSwooshingDelegate;

public interface DoneSwooshingDelegate   
{
    void invoke() throws Exception ;

    List<DoneSwooshingDelegate> getInvocationList() throws Exception ;

}


