//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:09
//

package SCSharp.UI;

import SCSharp.Chk;
import SCSharp.Mpq;
import SCSharp.UI.GuiUtil;
import SCSharp.UI.MapPoint;
import SCSharp.UI.Painter;
import SCSharp.Util;

//
// SCSharp.UI.MapRenderer
//
// Authors:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
public class MapRenderer   
{
    Mpq mpq;
    Chk chk;
    byte[] cv5 = new byte[]();
    byte[] vx4 = new byte[]();
    byte[] vr4 = new byte[]();
    byte[] vf4 = new byte[]();
    byte[] wpe = new byte[]();
    Dictionary<int, Surface> tileCache = new Dictionary<int, Surface>();
    int pixel_width = new int();
    int pixel_height = new int();
    public MapRenderer(Mpq mpq, Chk chk, int width, int height) throws Exception {
        this(mpq, chk);
        this.width = width;
        this.height = height;
    }

    public MapRenderer(Mpq mpq, Chk chk) throws Exception {
        this.mpq = mpq;
        this.chk = chk;
        pixel_width = (ushort)(chk.getWidth() * 32);
        pixel_height = (ushort)(chk.getHeight() * 32);
        Stream cv5_fs = (Stream)mpq.GetResource(String.Format("tileset\\{0}.cv5", Util.TilesetNames[((Enum)chk.getTileset()).ordinal()]));
        cv5 = new byte[cv5_fs.Length];
        cv5_fs.Read(cv5, 0, (int)cv5_fs.Length);
        cv5_fs.Close();
        Stream vx4_fs = (Stream)mpq.GetResource(String.Format("tileset\\{0}.vx4", Util.TilesetNames[((Enum)chk.getTileset()).ordinal()]));
        vx4 = new byte[vx4_fs.Length];
        vx4_fs.Read(vx4, 0, (int)vx4_fs.Length);
        vx4_fs.Close();
        Stream vr4_fs = (Stream)mpq.GetResource(String.Format("tileset\\{0}.vr4", Util.TilesetNames[((Enum)chk.getTileset()).ordinal()]));
        vr4 = new byte[vr4_fs.Length];
        vr4_fs.Read(vr4, 0, (int)vr4_fs.Length);
        vr4_fs.Close();
        Stream vf4_fs = (Stream)mpq.GetResource(String.Format("tileset\\{0}.vf4", Util.TilesetNames[((Enum)chk.getTileset()).ordinal()]));
        vf4 = new byte[vf4_fs.Length];
        vf4_fs.Read(vf4, 0, (int)vf4_fs.Length);
        vf4_fs.Close();
        Stream wpe_fs = (Stream)mpq.GetResource(String.Format("tileset\\{0}.wpe", Util.TilesetNames[((Enum)chk.getTileset()).ordinal()]));
        wpe = new byte[wpe_fs.Length];
        wpe_fs.Read(wpe, 0, (int)wpe_fs.Length);
        wpe_fs.Close();
    }

    public int getMapWidth() throws Exception {
        return pixel_width;
    }

    public int getMapHeight() throws Exception {
        return pixel_height;
    }

    int width = new int();
    int height = new int();
    Surface mapSurface = new Surface();
    public void setUpperLeft(int x, int y) throws Exception {
        /* XXX be naive for now, and redraw the entire section we need */
        if (mapSurface == null)
            mapSurface = new Surface(width, height);
         
        int tl_x = x / 32;
        int tl_y = y / 32;
        for (int map_y = tl_y;map_y <= tl_y + width / 32 && map_y < chk.Height;map_y++)
        {
            for (int map_x = tl_x;map_x <= tl_x + width / 32 && map_x < chk.Width;map_x++)
            {
                Surface tile = GetTile(chk.MapTiles[map_x, map_y]);
                mapSurface.Blit(tile, new Point(map_x * 32 - x, map_y * 32 - y));
            }
        }
        Painter.invalidate(new Rectangle(new Point(0, 0), new Size(width, height)));
    }

    public void paint(DateTime dt) throws Exception {
        Painter.blit(mapSurface,Painter.getDirty(),Painter.getDirty());
    }

    byte[] image = new byte[]();
    Surface getTile(int mapTile) throws Exception {
        if (tileCache == null)
            tileCache = new Dictionary<int, Surface>();
         
        //					bool odd = (mapTile & 0x10) == 0x10;
        int tile_group = mapTile >> 4;
        /* the tile's group in the cv5 file */
        int tile_number = mapTile & 0x0F;
        /* the megatile within the tile group */
        int megatile_id = Util.readWord(cv5,(tile_group * 26 + 10 + tile_number) * 2);
        if (tileCache.ContainsKey(megatile_id))
            return tileCache[megatile_id];
         
        if (image == null)
            image = new byte[32 * 32 * 4];
         
        int minitile_x = new int(), minitile_y = new int();
        boolean show_flag = true;
        ushort flag = 0x0001;
        for (minitile_y = 0;minitile_y < 4;minitile_y++)
        {
            for (minitile_x = 0;minitile_x < 4;minitile_x++)
            {
                /* walkable flag */
                ushort minitile_id = Util.readWord(vx4,megatile_id * 32 + minitile_y * 8 + minitile_x * 2);
                ushort minitile_flags = Util.readWord(vf4,megatile_id * 32 + minitile_y * 8 + minitile_x * 2);
                boolean flipped = (minitile_id & 0x01) == 0x01;
                minitile_id >>= 1;
                int pixel_x = new int(), pixel_y = new int();
                if (flipped)
                {
                    for (pixel_y = 0;pixel_y < 8;pixel_y++)
                        for (pixel_x = 0;pixel_x < 8;pixel_x++)
                        {
                            int x = (minitile_x + 1) * 8 - pixel_x - 1;
                            int y = (minitile_y * 8) * 32 + pixel_y * 32;
                            if (show_flag && (minitile_flags & flag) == flag && (pixel_x == pixel_y))
                            {
                                image[0 + 4 * (x + y)] = 255;
                                image[1 + 4 * (x + y)] = 255;
                                image[2 + 4 * (x + y)] = 0;
                                image[3 + 4 * (x + y)] = 0;
                            }
                            else
                            {
                                byte palette_entry = vr4[minitile_id * 64 + pixel_y * 8 + pixel_x];
                                image[0 + 4 * (x + y)] = (byte)(255 - wpe[palette_entry * 4 + 3]);
                                image[1 + 4 * (x + y)] = wpe[palette_entry * 4 + 2];
                                image[2 + 4 * (x + y)] = wpe[palette_entry * 4 + 1];
                                image[3 + 4 * (x + y)] = wpe[palette_entry * 4 + 0];
                            } 
                        }
                }
                else
                {
                    for (pixel_y = 0;pixel_y < 8;pixel_y++)
                    {
                        for (pixel_x = 0;pixel_x < 8;pixel_x++)
                        {
                            int x = minitile_x * 8 + pixel_x;
                            int y = (minitile_y * 8) * 32 + pixel_y * 32;
                            if (show_flag && (minitile_flags & flag) == flag && (pixel_x == pixel_y))
                            {
                                image[0 + 4 * (x + y)] = 255;
                                image[1 + 4 * (x + y)] = 255;
                                image[2 + 4 * (x + y)] = 0;
                                image[3 + 4 * (x + y)] = 0;
                            }
                            else
                            {
                                byte palette_entry = vr4[minitile_id * 64 + pixel_y * 8 + pixel_x];
                                image[0 + 4 * (x + y)] = (byte)(255 - wpe[palette_entry * 4 + 3]);
                                image[1 + 4 * (x + y)] = wpe[palette_entry * 4 + 2];
                                image[2 + 4 * (x + y)] = wpe[palette_entry * 4 + 1];
                                image[3 + 4 * (x + y)] = wpe[palette_entry * 4 + 0];
                            } 
                        }
                    }
                } 
            }
        }
        Surface surf = GuiUtil.CreateSurfaceFromRGBAData(image, 32, 32, 32, 32 * 4);
        tileCache[megatile_id] = surf;
        return surf;
    }

    public boolean navigable(MapPoint point) throws Exception {
        // this assumes that point corresponds to a mini tile location
        // first calculate the megatile
        int megatile_x = new int(), megatile_y = new int();
        megatile_x = point.getX() >> 3;
        megatile_y = point.getY() >> 3;
        if ((megatile_x >= chk.Width || megatile_x < 0) || (megatile_y >= chk.Height || megatile_y < 0))
            return false;
         
        int mapTile = chk.MapTiles[megatile_x, megatile_y];
        int tile_group = mapTile >> 4;
        /* the tile's group in the cv5 file */
        int tile_number = mapTile & 0x0F;
        /* the megatile within the tile group */
        int megatile_id = Util.readWord(cv5,(tile_group * 26 + 10 + tile_number) * 2);
        ushort minitile_flags = Util.readWord(vf4,megatile_id * 32 + point.getY() * 8 + point.getX() * 2);
        return (minitile_flags & 0x0001) == 0x0001;
    }

    // Console.WriteLine ("minitile {0},{1} is navigable?  {2}", point.X, point.Y, (minitile_flags & 0x0001) == 0x0001);
    public Surface renderToSurface() throws Exception {
        byte[] bitmap = renderToBitmap(mpq,chk);
        return GuiUtil.CreateSurfaceFromRGBAData(bitmap, (ushort)pixel_width, (ushort)pixel_height, 32, (ushort)(pixel_width * 4));
    }

    public byte[] renderToBitmap(Mpq mpq, Chk chk) throws Exception {
        ushort[][] mapTiles = chk.getMapTiles();
        byte[] image = new byte[pixel_width * pixel_height * 4];
        for (int map_y = 0;map_y < chk.getHeight();map_y++)
        {
            for (int map_x = 0;map_x < chk.getWidth();map_x++)
            {
                int mapTile = mapTiles[map_x, map_y];
                //					bool odd = (mapTile & 0x10) == 0x10;
                int tile_group = mapTile >> 4;
                /* the tile's group in the cv5 file */
                int tile_number = mapTile & 0x0F;
                /* the megatile within the tile group */
                int megatile_id = Util.readWord(cv5,(tile_group * 26 + 10 + tile_number) * 2);
                int minitile_x = new int(), minitile_y = new int();
                for (minitile_y = 0;minitile_y < 4;minitile_y++)
                {
                    for (minitile_x = 0;minitile_x < 4;minitile_x++)
                    {
                        ushort minitile_id = Util.readWord(vx4,megatile_id * 32 + minitile_y * 8 + minitile_x * 2);
                        boolean flipped = (minitile_id & 0x01) == 0x01;
                        minitile_id >>= 1;
                        int pixel_x = new int(), pixel_y = new int();
                        if (flipped)
                        {
                            for (pixel_y = 0;pixel_y < 8;pixel_y++)
                                for (pixel_x = 0;pixel_x < 8;pixel_x++)
                                {
                                    int x = map_x * 32 + (minitile_x + 1) * 8 - pixel_x - 1;
                                    int y = (map_y * 32 + minitile_y * 8) * pixel_width + pixel_y * pixel_width;
                                    byte palette_entry = vr4[minitile_id * 64 + pixel_y * 8 + pixel_x];
                                    image[0 + 4 * (x + y)] = (byte)(255 - wpe[palette_entry * 4 + 3]);
                                    image[1 + 4 * (x + y)] = wpe[palette_entry * 4 + 2];
                                    image[2 + 4 * (x + y)] = wpe[palette_entry * 4 + 1];
                                    image[3 + 4 * (x + y)] = wpe[palette_entry * 4 + 0];
                                }
                        }
                        else
                        {
                            for (pixel_y = 0;pixel_y < 8;pixel_y++)
                            {
                                for (pixel_x = 0;pixel_x < 8;pixel_x++)
                                {
                                    int x = map_x * 32 + minitile_x * 8 + pixel_x;
                                    int y = (map_y * 32 + minitile_y * 8) * pixel_width + pixel_y * pixel_width;
                                    byte palette_entry = vr4[minitile_id * 64 + pixel_y * 8 + pixel_x];
                                    image[0 + 4 * (x + y)] = (byte)(255 - wpe[palette_entry * 4 + 3]);
                                    image[1 + 4 * (x + y)] = wpe[palette_entry * 4 + 2];
                                    image[2 + 4 * (x + y)] = wpe[palette_entry * 4 + 1];
                                    image[3 + 4 * (x + y)] = wpe[palette_entry * 4 + 0];
                                }
                            }
                        } 
                    }
                }
            }
        }
        return image;
    }

    public Chk getChk() throws Exception {
        return chk;
    }

}


