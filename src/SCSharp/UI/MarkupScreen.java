//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:09
//

package SCSharp.UI;

import CS2JNet.System.StringSupport;
import SCSharp.Fnt;
import SCSharp.Mpq;
import SCSharp.UI.GuiUtil;
import SCSharp.UI.Layer;
import SCSharp.UI.Painter;
import SCSharp.UI.Pcx;
import SCSharp.UI.UIScreen;

//
// SCSharp.UI.MarkupScreen
//
// Authors:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
public abstract class MarkupScreen  extends UIScreen 
{
    Fnt fnt;
    byte[] pal = new byte[]();
    public MarkupScreen(Mpq mpq) throws Exception {
        super(mpq);
        pages = new List<MarkupPage>();
    }

    public enum PageLocation
    {
        Center,
        Top,
        Bottom,
        Left,
        Right,
        LowerLeft
    }
    static class MarkupPage   
    {
        static final int X_OFFSET = 60;
        static final int Y_OFFSET = 10;
        PageLocation location = PageLocation.Center;
        List<String> lines = new List<String>();
        List<Surface> lineSurfaces = new List<Surface>();
        Surface newBackground = new Surface();
        Fnt fnt;
        byte[] pal = new byte[]();
        public MarkupPage(PageLocation loc, Fnt font, byte[] palette) throws Exception {
            location = loc;
            lines = new List<String>();
            fnt = font;
            pal = palette;
        }

        public MarkupPage(Stream background) throws Exception {
            newBackground = GuiUtil.surfaceFromStream(background,254,0);
        }

        public void addLine(String line) throws Exception {
            lines.Add(line);
        }

        public void layout() throws Exception {
            lineSurfaces = new List<Surface>();
            for (Object __dummyForeachVar0 : lines)
            {
                String l = (String)__dummyForeachVar0;
                if (StringSupport.equals(l.Trim(), ""))
                    lineSurfaces.Add(null);
                else
                    lineSurfaces.Add(GuiUtil.composeText(l,fnt,pal,Painter.SCREEN_RES_X - X_OFFSET * 2,-1,4)); 
            }
        }

        public Surface getBackground() throws Exception {
            return newBackground;
        }

        public boolean getHasText() throws Exception {
            return lines != null && lines.Count > 0;
        }

        public void paint() throws Exception {
            int y = new int();
            switch(location)
            {
                case Top: 
                    y = Y_OFFSET;
                    for (Object __dummyForeachVar1 : lineSurfaces)
                    {
                        Surface s = (Surface)__dummyForeachVar1;
                        if (s != null)
                        {
                            Painter.blit(s,new Point((Painter.getWidth() - s.Width) / 2, y));
                            y += s.Height;
                        }
                        else
                            y += fnt.LineSize; 
                    }
                    break;
                case Bottom: 
                    y = Painter.getHeight() - Y_OFFSET - fnt.LineSize * lines.Count;
                    for (Object __dummyForeachVar2 : lineSurfaces)
                    {
                        Surface s = (Surface)__dummyForeachVar2;
                        if (s != null)
                        {
                            Painter.blit(s,new Point((Painter.getWidth() - s.Width) / 2, y));
                            y += s.Height;
                        }
                        else
                            y += fnt.LineSize; 
                    }
                    break;
                case Left: 
                    y = (Painter.getHeight() - fnt.LineSize * lines.Count) / 2;
                    for (Object __dummyForeachVar3 : lineSurfaces)
                    {
                        Surface s = (Surface)__dummyForeachVar3;
                        if (s != null)
                        {
                            Painter.blit(s,new Point(X_OFFSET, y));
                            y += s.Height;
                        }
                        else
                            y += fnt.LineSize; 
                    }
                    break;
                case LowerLeft: 
                    y = Painter.getHeight() - Y_OFFSET - fnt.LineSize * lines.Count;
                    for (Object __dummyForeachVar4 : lineSurfaces)
                    {
                        Surface s = (Surface)__dummyForeachVar4;
                        if (s != null)
                        {
                            Painter.blit(s,new Point(X_OFFSET, y));
                            y += s.Height;
                        }
                        else
                            y += fnt.LineSize; 
                    }
                    break;
                case Right: 
                    y = (Painter.getHeight() - fnt.LineSize * lines.Count) / 2;
                    for (Object __dummyForeachVar5 : lineSurfaces)
                    {
                        Surface s = (Surface)__dummyForeachVar5;
                        if (s != null)
                        {
                            Painter.blit(s,new Point(Painter.getWidth() - s.Width - X_OFFSET, y));
                            y += s.Height;
                        }
                        else
                            y += fnt.LineSize; 
                    }
                    break;
                case Center: 
                    y = (Painter.getHeight() - fnt.LineSize * lines.Count) / 2;
                    for (Object __dummyForeachVar6 : lineSurfaces)
                    {
                        Surface s = (Surface)__dummyForeachVar6;
                        if (s != null)
                        {
                            Painter.blit(s,new Point((Painter.getWidth() - s.Width) / 2, y));
                            y += s.Height;
                        }
                        else
                            y += fnt.LineSize; 
                    }
                    break;
            
            }
        }
    
    }

    List<MarkupPage> pages = new List<MarkupPage>();
    protected void addMarkup(Stream s) throws Exception {
        String l = new String();
        MarkupPage currentPage = null;
        StreamReader sr = new StreamReader(s);
        while ((l = sr.ReadLine()) != null)
        {
            if (l.StartsWith("</"))
            {
                if (l.StartsWith("</PAGE>"))
                {
                    currentPage.layout();
                    pages.Add(currentPage);
                    currentPage = null;
                }
                else if (l.StartsWith("</SCREENCENTER>"))
                {
                    currentPage = new MarkupPage(PageLocation.Center,fnt,pal);
                }
                else if (l.StartsWith("</SCREENLEFT>"))
                {
                    currentPage = new MarkupPage(PageLocation.Left,fnt,pal);
                }
                else if (l.StartsWith("</SCREENLOWERLEFT>"))
                {
                    currentPage = new MarkupPage(PageLocation.LowerLeft,fnt,pal);
                }
                else if (l.StartsWith("</SCREENRIGHT>"))
                {
                    currentPage = new MarkupPage(PageLocation.Right,fnt,pal);
                }
                else if (l.StartsWith("</SCREENTOP>"))
                {
                    currentPage = new MarkupPage(PageLocation.Top,fnt,pal);
                }
                else if (l.StartsWith("</SCREENBOTTOM>"))
                {
                    currentPage = new MarkupPage(PageLocation.Bottom,fnt,pal);
                }
                else if (l.StartsWith("</BACKGROUND "))
                {
                    String bg = l.Substring("</BACKGROUND ".Length);
                    bg = bg.Substring(0, bg.Length - 1);
                    pages.Add(new MarkupPage((Stream)mpq.GetResource(bg)));
                }
                        
            }
            else /* skip everything else */
            if (currentPage != null)
                currentPage.addLine(l);
              
        }
    }

    protected void resourceLoader() throws Exception {
        Console.WriteLine("loading font palette");
        Stream palStream = (Stream)mpq.GetResource("glue\\Palmm\\tFont.pcx");
        Pcx pcx = new Pcx();
        pcx.readFromStream(palStream,-1,-1);
        pal = pcx.getRgbData();
        Console.WriteLine("loading font");
        fnt = GuiUtil.getFonts(mpq)[3];
        Console.WriteLine("loading markup");
        loadMarkup();
        /* set things up so we're ready to go */
        millisDelay = 4000;
        pageEnumerator = pages.GetEnumerator();
        advanceToNextPage();
    }

    // painting
    Surface currentBackground = new Surface();
    IEnumerator<MarkupPage> pageEnumerator = new IEnumerator<MarkupPage>();
    int millisDelay = new int();
    int totalElapsed = new int();
    public void addToPainter() throws Exception {
        super.addToPainter();
        Painter.Add(Layer.Background, PaintBackground);
        Painter.Add(Layer.UI, PaintMarkup);
    }

    public void removeFromPainter() throws Exception {
        super.removeFromPainter();
        Painter.Remove(Layer.Background, PaintBackground);
        Painter.Remove(Layer.UI, PaintMarkup);
    }

    protected void firstPaint(Object sender, EventArgs args) throws Exception {
        super.firstPaint(sender,args);
        /* set ourselves up to invalidate at a regular interval*/
        Events.Tick += FlipPage;
    }

    void paintBackground(DateTime now) throws Exception {
        if (currentBackground != null)
            Painter.blit(currentBackground);
         
    }

    void paintMarkup(DateTime now) throws Exception {
        pageEnumerator.Current.Paint();
    }

    void flipPage(Object sender, TickEventArgs e) throws Exception {
        totalElapsed += e.TicksElapsed;
        if (totalElapsed < millisDelay)
            return ;
         
        totalElapsed = 0;
        advanceToNextPage();
    }

    public void keyboardDown(KeyboardEventArgs args) throws Exception {
        Key __dummyScrutVar1 = args.Key;
        if (__dummyScrutVar1.equals(Key.Escape))
        {
            Events.Tick -= FlipPage;
            markupFinished();
        }
        else if (__dummyScrutVar1.equals(Key.Space) || __dummyScrutVar1.equals(Key.Return))
        {
            totalElapsed = 0;
            advanceToNextPage();
        }
          
    }

    public void keyboardUp(KeyboardEventArgs args) throws Exception {
    }

    void advanceToNextPage() throws Exception {
        Painter.invalidate();
        while (pageEnumerator.MoveNext())
        {
            if (pageEnumerator.Current.Background != null)
                currentBackground = pageEnumerator.Current.Background;
             
            if (pageEnumerator.Current.HasText)
                return ;
             
        }
        Console.WriteLine("finished!");
        Events.Tick -= FlipPage;
        markupFinished();
    }

    protected abstract void loadMarkup() throws Exception ;

    protected abstract void markupFinished() throws Exception ;

}


