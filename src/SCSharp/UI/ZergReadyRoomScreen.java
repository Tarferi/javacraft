//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:10
//

package SCSharp.UI;

import SCSharp.Mpq;
import SCSharp.UI.ReadyRoomScreen;

public class ZergReadyRoomScreen  extends ReadyRoomScreen 
{
    public ZergReadyRoomScreen(Mpq mpq, String scenario_prefix) throws Exception {
        super(mpq, scenario_prefix, START_ELEMENT_INDEX, CANCEL_ELEMENT_INDEX, SKIPTUTORIAL_ELEMENT_INDEX, REPLAY_ELEMENT_INDEX, TRANSMISSION_ELEMENT_INDEX, OBJECTIVES_ELEMENT_INDEX, FIRST_PORTRAIT_ELEMENT_INDEX);
    }

    static final int START_ELEMENT_INDEX = 1;
    static final int CANCEL_ELEMENT_INDEX = 10;
    static final int SKIPTUTORIAL_ELEMENT_INDEX = 12;
    static final int REPLAY_ELEMENT_INDEX = 13;
    static final int FIRST_PORTRAIT_ELEMENT_INDEX = 14;
    static final int TRANSMISSION_ELEMENT_INDEX = 18;
    static final int OBJECTIVES_ELEMENT_INDEX = 19;
}


