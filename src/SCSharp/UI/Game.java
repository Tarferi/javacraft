//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:07
//

package SCSharp.UI;

import CS2JNet.System.StringSupport;
import java.util.ArrayList;
import java.util.List;
import SCSharp.Mpq;
import SCSharp.MpqArchive;
import SCSharp.MpqContainer;
import SCSharp.MpqDirectory;
import SCSharp.UI.__MultiReadyDelegate;
import SCSharp.UI.ConnectionScreen;
import SCSharp.UI.CursorAnimator;
import SCSharp.UI.Game;
import SCSharp.UI.GlobalResources;
import SCSharp.UI.LoginScreen;
import SCSharp.UI.MainMenu;
import SCSharp.UI.Painter;
import SCSharp.UI.Race;
import SCSharp.UI.ReadyDelegate;
import SCSharp.UI.TitleScreen;
import SCSharp.UI.UIScreen;
import SCSharp.UI.UIScreenType;

public class Game   
{
    UIScreen[] screens = new UIScreen[]();
    static final int GAME_ANIMATION_TICK = 20;
    // number of milliseconds between animation updates
    boolean isBroodWar = new boolean();
    boolean playingBroodWar = new boolean();
    Race race = Race.Zerg;
    Mpq patchRtMpq;
    Mpq broodatMpq;
    Mpq stardatMpq;
    Mpq bwInstallExe;
    Mpq scInstallExe;
    MpqContainer installedMpq;
    MpqContainer playingMpq;
    int cached_cursor_x = new int();
    int cached_cursor_y = new int();
    String rootDir = new String();
    static Game instance;
    public static Game getInstance() throws Exception {
        return instance;
    }

    public Game(String starcraftDir, String scCDDir, String bwCDDir) throws Exception {
        instance = this;
        starcraftDir = starcraftDir.Replace('\\', Path.DirectorySeparatorChar).Replace('/', Path.DirectorySeparatorChar);
        scCDDir = scCDDir.Replace('\\', Path.DirectorySeparatorChar).Replace('/', Path.DirectorySeparatorChar);
        bwCDDir = bwCDDir.Replace('\\', Path.DirectorySeparatorChar).Replace('/', Path.DirectorySeparatorChar);
        screens = new UIScreen[((Enum)UIScreenType.ScreenCount).ordinal()];
        installedMpq = new MpqContainer();
        playingMpq = new MpqContainer();
        Mpq scMpq = null, bwMpq = null;
        if (starcraftDir != null)
        {
            for (Object __dummyForeachVar0 : Directory.GetFileSystemEntries(starcraftDir))
            {
                String path = (String)__dummyForeachVar0;
                if (StringSupport.equals(Path.GetFileName(path).ToLower(), "broodat.mpq") || Path.GetFileName(path).Equals("Brood War Data"))
                {
                    if (broodatMpq != null)
                        throw new Exception("You have multiple broodat.mpq files in your starcraft directory.");
                     
                    try
                    {
                        bwMpq = getMpq(path);
                        Console.WriteLine("found BrooDat.mpq");
                    }
                    catch (Exception e)
                    {
                        throw new Exception(String.Format("Could not read mpq archive {0}", path), e);
                    }
                
                }
                else if (StringSupport.equals(Path.GetFileName(path).ToLower(), "stardat.mpq") || Path.GetFileName(path).Equals("Starcraft Data"))
                {
                    if (stardatMpq != null)
                        throw new Exception("You have multiple stardat.mpq files in your starcraft directory.");
                     
                    try
                    {
                        scMpq = getMpq(path);
                        Console.WriteLine("found StarDat.mpq");
                    }
                    catch (Exception e)
                    {
                        throw new Exception(String.Format("could not read mpq archive {0}", path), e);
                    }
                
                }
                else if (StringSupport.equals(Path.GetFileName(path).ToLower(), "patch_rt.mpq") || Path.GetFileName(path).Equals("Starcraft Mac Patch"))
                {
                    if (patchRtMpq != null)
                        throw new Exception("You have multiple patch_rt.mpq files in your starcraft directory.");
                     
                    try
                    {
                        patchRtMpq = getMpq(path);
                        Console.WriteLine("found patch_rt.mpq");
                    }
                    catch (Exception e)
                    {
                        throw new Exception(String.Format("could not read mpq archive {0}", path), e);
                    }
                
                }
                else if (StringSupport.equals(Path.GetFileName(path).ToLower(), "starcraft.mpq"))
                {
                    try
                    {
                        scInstallExe = getMpq(path);
                        Console.WriteLine("found starcraft.mpq");
                    }
                    catch (Exception e)
                    {
                        throw new Exception(String.Format("could not read mpq archive {0}", path), e);
                    }
                
                }
                else if (StringSupport.equals(Path.GetFileName(path).ToLower(), "broodwar.mpq"))
                {
                    try
                    {
                        bwInstallExe = getMpq(path);
                        Console.WriteLine("found broodwar.mpq");
                    }
                    catch (Exception e)
                    {
                        throw new Exception(String.Format("could not read mpq archive {0}", path), e);
                    }
                
                }
                     
            }
        }
         
        if (scMpq == null)
        {
            throw new Exception("unable to locate stardat.mpq, please check your StarcraftDirectory configuration setting");
        }
         
        if (!String.IsNullOrEmpty(scCDDir))
        {
            for (Object __dummyForeachVar1 : Directory.GetFileSystemEntries(scCDDir))
            {
                String path = (String)__dummyForeachVar1;
                if (StringSupport.equals(Path.GetFileName(path).ToLower(), "install.exe") || Path.GetFileName(path).Equals("Starcraft Archive"))
                {
                    try
                    {
                        scInstallExe = getMpq(path);
                        Console.WriteLine("found SC install.exe");
                    }
                    catch (Exception e)
                    {
                        throw new Exception(String.Format("could not read mpq archive {0}", path), e);
                    }
                
                }
                 
            }
        }
         
        if (!String.IsNullOrEmpty(bwCDDir))
        {
            for (Object __dummyForeachVar2 : Directory.GetFileSystemEntries(bwCDDir))
            {
                String path = (String)__dummyForeachVar2;
                if (StringSupport.equals(Path.GetFileName(path).ToLower(), "install.exe") || Path.GetFileName(path).Equals("Brood War Archive"))
                {
                    try
                    {
                        bwInstallExe = getMpq(path);
                        Console.WriteLine("found BW install.exe");
                    }
                    catch (Exception e)
                    {
                        throw new Exception(String.Format("could not read mpq archive {0}", path), e);
                    }
                
                }
                 
            }
        }
         
        if (bwInstallExe == null)
            throw new Exception("unable to locate broodwar cd's install.exe, please check your BroodwarCDDirectory configuration setting");
         
        if (bwMpq != null)
        {
            if (patchRtMpq != null)
            {
                broodatMpq = new MpqContainer();
                ((MpqContainer)broodatMpq).Add(patchRtMpq);
                ((MpqContainer)broodatMpq).add(bwMpq);
            }
            else
                broodatMpq = bwMpq; 
        }
         
        if (scMpq != null)
        {
            if (patchRtMpq != null)
            {
                stardatMpq = new MpqContainer();
                ((MpqContainer)stardatMpq).Add(patchRtMpq);
                ((MpqContainer)stardatMpq).add(scMpq);
            }
            else
                stardatMpq = bwMpq; 
        }
         
        if (broodatMpq != null)
            installedMpq.Add(broodatMpq);
         
        if (bwInstallExe != null)
            installedMpq.Add(bwInstallExe);
         
        if (stardatMpq != null)
            installedMpq.Add(stardatMpq);
         
        if (scInstallExe != null)
            installedMpq.Add(scInstallExe);
         
        setPlayingBroodWar(isBroodWar = (broodatMpq != null));
        this.rootDir = starcraftDir;
    }

    Mpq getMpq(String path) throws Exception {
        if (Directory.Exists(path))
            return new MpqDirectory(path);
        else if (File.Exists(path))
            return new MpqArchive(path);
        else
            return null;  
    }

    public String getRootDirectory() throws Exception {
        return rootDir;
    }

    public boolean getPlayingBroodWar() throws Exception {
        return playingBroodWar;
    }

    public void setPlayingBroodWar(boolean value) throws Exception {
        playingBroodWar = value;
        playingMpq.Clear();
        if (playingBroodWar)
        {
            if (bwInstallExe == null)
                throw new Exception("you need the Broodwar CD to play Broodwar games.  Please check the BroodwarCDDirectory configuration setting.");
             
            if (patchRtMpq != null)
                playingMpq.Add(patchRtMpq);
             
            playingMpq.Add(bwInstallExe);
            playingMpq.Add(broodatMpq);
            playingMpq.Add(stardatMpq);
        }
        else
        {
            if (scInstallExe == null)
                throw new Exception("you need the Starcraft CD to play original games.  Please check the StarcraftCDDirectory configuration setting.");
             
            if (patchRtMpq != null)
                playingMpq.Add(patchRtMpq);
             
            playingMpq.Add(scInstallExe);
            playingMpq.Add(stardatMpq);
        } 
    }

    public boolean getIsBroodWar() throws Exception {
        return isBroodWar;
    }

    public void startup(boolean fullscreen) throws Exception {
        /* create our window and hook up to the events we care about */
        createWindow(fullscreen);
        Mouse.ShowCursor = false;
        Events.UserEvent += UserEvent;
        Events.MouseMotion += PointerMotion;
        Events.MouseButtonDown += MouseButtonDown;
        Events.MouseButtonUp += MouseButtonUp;
        Events.KeyboardUp += KeyboardUp;
        Events.KeyboardDown += KeyboardDown;
        displayTitle();
        /* start everything up */
        Events.Run();
    }

    public void quit() throws Exception {
        Events.QuitApplication();
    }

    void displayTitle() throws Exception {
        /* create the title screen, and make sure we
        			   don't start loading anything else until
        			   it's on the screen */
        UIScreen screen = new TitleScreen(installedMpq);
        screen.FirstPainted = __MultiReadyDelegate.combine(screen.FirstPainted,new ReadyDelegate() 
          { 
            public System.Void invoke() throws Exception {
                titleScreenReady();
            }

            public List<ReadyDelegate> getInvocationList() throws Exception {
                List<ReadyDelegate> ret = new ArrayList<ReadyDelegate>();
                ret.add(this);
                return ret;
            }
        
          });
        switchToScreen(screen);
    }

    void createWindow(boolean fullscreen) throws Exception {
        Video.WindowIcon();
        Video.WindowCaption = "SCSharp";
        Painter.initializePainter(fullscreen,GAME_ANIMATION_TICK);
    }

    void userEvent(Object sender, UserEventArgs args) throws Exception {
        ReadyDelegate d = (ReadyDelegate)new ReadyDelegate() 
          { 
            public System.Void invoke() throws Exception {
                args.userEvent();
            }

            public List<ReadyDelegate> getInvocationList() throws Exception {
                List<ReadyDelegate> ret = new ArrayList<ReadyDelegate>();
                ret.add(this);
                return ret;
            }
        
          };
        d.invoke();
    }

    void pointerMotion(Object o, MouseMotionEventArgs args) throws Exception {
        cached_cursor_x = args.X;
        cached_cursor_y = args.Y;
        if (cursor != null)
            cursor.setPosition(cached_cursor_x,cached_cursor_y);
         
        if (currentScreen != null)
            currentScreen.handlePointerMotion(args);
         
    }

    void mouseButtonDown(Object o, MouseButtonEventArgs args) throws Exception {
        cached_cursor_x = args.X;
        cached_cursor_y = args.Y;
        if (cursor != null)
            cursor.setPosition(cached_cursor_x,cached_cursor_y);
         
        if (currentScreen != null)
            currentScreen.handleMouseButtonDown(args);
         
    }

    void mouseButtonUp(Object o, MouseButtonEventArgs args) throws Exception {
        cached_cursor_x = args.X;
        cached_cursor_y = args.Y;
        if (cursor != null)
            cursor.setPosition(cached_cursor_x,cached_cursor_y);
         
        if (currentScreen != null)
            currentScreen.handleMouseButtonUp(args);
         
    }

    void keyboardUp(Object o, KeyboardEventArgs args) throws Exception {
        if (currentScreen != null)
            currentScreen.handleKeyboardUp(args);
         
    }

    void keyboardDown(Object o, KeyboardEventArgs args) throws Exception {
        if ((args.Mod & ModifierKeys.LeftControl) != 0)
        {
            if (args.Key == Key.Q)
            {
                quit();
                return ;
            }
            else if (args.Key == Key.F)
            {
                Painter.setFullscreen(!Painter.getFullscreen());
                return ;
            }
              
        }
         
        if (currentScreen != null)
            currentScreen.handleKeyboardDown(args);
         
    }

    public Race getRace() throws Exception {
        return race;
    }

    public void setRace(Race value) throws Exception {
        race = value;
    }

    CursorAnimator cursor;
    public CursorAnimator getCursor() throws Exception {
        return cursor;
    }

    public void setCursor(CursorAnimator value) throws Exception {
        if (cursor == value)
            return ;
         
        if (cursor != null)
            cursor.removeFromPainter();
         
        cursor = value;
        if (cursor != null)
        {
            cursor.addToPainter();
            cursor.setPosition(cached_cursor_x,cached_cursor_y);
        }
         
    }

    UIScreen currentScreen;
    public void setGameScreen(UIScreen screen) throws Exception {
        Painter.pause();
        if (currentScreen != null)
            currentScreen.removeFromPainter();
         
        currentScreen = screen;
        if (currentScreen != null)
            currentScreen.addToPainter();
         
        Painter.resume();
    }

    UIScreen screenToSwitchTo;
    public void switchToScreen(UIScreen screen) throws Exception {
        screen.Ready = __MultiReadyDelegate.combine(screen.Ready,new ReadyDelegate() 
          { 
            public System.Void invoke() throws Exception {
                switchReady();
            }

            public List<ReadyDelegate> getInvocationList() throws Exception {
                List<ReadyDelegate> ret = new ArrayList<ReadyDelegate>();
                ret.add(this);
                return ret;
            }
        
          });
        screenToSwitchTo = screen;
        screenToSwitchTo.load();
        return ;
    }

    public void switchToScreen(UIScreenType screenType) throws Exception {
        int index = ((Enum)screenType).ordinal();
        if (screens[index] == null)
        {
            switch(screenType)
            {
                case MainMenu: 
                    screens[index] = new MainMenu(installedMpq);
                    break;
                case Login: 
                    screens[index] = new LoginScreen(playingMpq);
                    break;
                case Connection: 
                    screens[index] = new ConnectionScreen(playingMpq);
                    break;
                default: 
                    throw new Exception();
            
            }
        }
         
        SwitchToScreen(screens[((Enum)screenType).ordinal()]);
    }

    public Mpq getPlayingMpq() throws Exception {
        return playingMpq;
    }

    public Mpq getInstalledMpq() throws Exception {
        return installedMpq;
    }

    void switchReady() throws Exception {
        screenToSwitchTo.Ready = __MultiReadyDelegate.remove(screenToSwitchTo.Ready,new ReadyDelegate() 
          { 
            public System.Void invoke() throws Exception {
                switchReady();
            }

            public List<ReadyDelegate> getInvocationList() throws Exception {
                List<ReadyDelegate> ret = new ArrayList<ReadyDelegate>();
                ret.add(this);
                return ret;
            }
        
          });
        setGameScreen(screenToSwitchTo);
        screenToSwitchTo = null;
    }

    void globalResourcesLoaded() throws Exception {
        switchToScreen(UIScreenType.MainMenu);
    }

    void titleScreenReady() throws Exception {
        Console.WriteLine("Loading global resources");
        new GlobalResources(stardatMpq,broodatMpq);
        GlobalResources.getInstance().Ready = __MultiReadyDelegate.combine(GlobalResources.getInstance().Ready,new ReadyDelegate() 
          { 
            public System.Void invoke() throws Exception {
                globalResourcesLoaded();
            }

            public List<ReadyDelegate> getInvocationList() throws Exception {
                List<ReadyDelegate> ret = new ArrayList<ReadyDelegate>();
                ret.add(this);
                return ret;
            }
        
          });
        GlobalResources.getInstance().load();
    }

}


