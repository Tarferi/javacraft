//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:07
//

package SCSharp.UI;

import SCSharp.BinElement;
import SCSharp.ElementFlags;
import SCSharp.UI.Builtins;
import SCSharp.UI.GuiUtil;
import SCSharp.UI.UIElement;
import SCSharp.UI.UIScreen;

//
// SCSharp.UI.ButtonElement
//
// Author:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
public class ButtonElement  extends UIElement 
{
    public ButtonElement(UIScreen screen, BinElement el, byte[] palette) throws Exception {
        super(screen, el, palette);
    }

    Surface text_surf = new Surface();
    int text_x = new int(), text_y = new int();
    void calculateTextPosition() throws Exception {
        if (text_surf == null)
            text_surf = GuiUtil.composeText(getText(),getFont(),getPalette(),-1,-1,getSensitive() ? 4 : 24);
         
        if ((getFlags() & ElementFlags.CenterTextHoriz) == ElementFlags.CenterTextHoriz)
            text_x = (getWidth() - text_surf.Width) / 2;
        else if ((getFlags() & ElementFlags.RightAlignText) == ElementFlags.RightAlignText)
            text_x = (getWidth() - text_surf.Width);
        else
            text_x = 0;  
        if ((getFlags() & ElementFlags.CenterTextVert) == ElementFlags.CenterTextVert)
            text_y = (getHeight() - text_surf.Height) / 2;
        else if ((getFlags() & ElementFlags.BottomAlignText) == ElementFlags.BottomAlignText)
            text_y = (getHeight() - text_surf.Height);
        else
            text_y = 0;  
    }

    protected Surface createSurface() throws Exception {
        Surface surf = new Surface(getWidth(), getHeight());
        surf.TransparentColor = Color.Black;
        /* XXX */
        text_surf = null;
        calculateTextPosition();
        surf.Blit(text_surf, new Point(text_x, text_y));
        return surf;
    }

    public Point getTextPosition() throws Exception {
        calculateTextPosition();
        return new Point(text_x, text_y);
    }

    public void mouseButtonDown(MouseButtonEventArgs args) throws Exception {
    }

    public void mouseButtonUp(MouseButtonEventArgs args) throws Exception {
        if (PointInside(args.X, args.Y))
            onActivate();
         
    }

    public void mouseEnter() throws Exception {
        if (getSensitive() && (getFlags() & ElementFlags.RespondToMouse) == ElementFlags.RespondToMouse)
        {
            /* highlight the text */
            GuiUtil.playSound(getMpq(),Builtins.MouseoverWav);
        }
         
        super.mouseEnter();
    }

}


