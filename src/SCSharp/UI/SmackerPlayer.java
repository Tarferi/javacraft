//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:10
//

package SCSharp.UI;

import CS2JNet.JavaSupport.util.ListSupport;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import SCSharp.Smk.SmackerDecoder;
import SCSharp.Smk.SmackerFile;
import SCSharp.UI.GuiUtil;
import SCSharp.UI.PlayerEvent;
import SCSharp.UI.ReadyDelegate;

//
// SCSharp.UI.SmackerPlayer
//
// Authors:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
public class SmackerPlayer   
{
    //Buffer this many frames
    private static final int BUFFERED_FRAMES = 100;
    Thread decoderThread = new Thread();
    boolean firstRun = true;
    int buffered_frames = new int();
    Queue<byte[]> frameQueue = new Queue<byte[]>();
    SmackerFile file;
    SmackerDecoder decoder;
    AutoResetEvent waitEvent = new AutoResetEvent();
    public SmackerPlayer(Stream smk_stream) throws Exception {
        this(smk_stream, BUFFERED_FRAMES);
    }

    public SmackerPlayer(Stream smk_stream, int buffered_frames) throws Exception {
        file = SmackerFile.openFromStream(smk_stream);
        decoder = file.getDecoder();
        this.buffered_frames = buffered_frames;
        waitEvent = new AutoResetEvent(false);
    }

    public int getWidth() throws Exception {
        return (int)file.getHeader().Width;
    }

    public int getHeight() throws Exception {
        return (int)file.getHeader().Height;
    }

    float timeElapsed = 0;
    Surface surf = new Surface();
    void events_Tick(Object sender, TickEventArgs e) throws Exception {
        //There should be an easier way to get the video data to SDL
        timeElapsed += (e.SecondsElapsed);
        while (timeElapsed > 1.0 / file.getHeader().Fps && frameQueue.Count > 0)
        {
            timeElapsed -= (float)(1.0f / file.getHeader().Fps);
            byte[] rgbData = frameQueue.Dequeue();
            if (surf == null)
            {
                surf = GuiUtil.CreateSurface(rgbData, (ushort)file.getHeader().Width, (ushort)file.getHeader().Height, 32, (int)file.getHeader().Width * 4, (int)0x00ff0000, (int)0x0000ff00, (int)0x000000ff, /* [UNSUPPORTED] unchecked expressions are not supported "unchecked (int)0xff000000" */);
            }
            else
            {
                surf.Lock();
                Marshal.Copy(rgbData, 0, surf.Pixels, rgbData.Length);
                surf.Unlock();
                surf.Update();
            } 
            emitFrameReady();
            if (frameQueue.Count < (buffered_frames / 2) + 1)
                waitEvent.Set();
             
        }
    }

    void decoderThread() throws Exception {
        while (firstRun || file.getHeader().hasRingFrame())
        {
            decoder.reset();
            while (decoder.getCurrentFrame() < file.getHeader().NbFrames)
            {
                try
                {
                    decoder.readNextFrame();
                    frameQueue.Enqueue(decoder.getARGBData());
                    if (frameQueue.Count >= buffered_frames)
                        waitEvent.WaitOne();
                     
                }
                catch (Exception __dummyCatchVar0)
                {
                    break;
                }
            
            }
        }
        firstRun = false;
        Events.PushUserEvent(new UserEventArgs(new ReadyDelegate() 
          { 
            public System.Void invoke() throws Exception {
                emitFinished();
            }

            public List<ReadyDelegate> getInvocationList() throws Exception {
                List<ReadyDelegate> ret = new ArrayList<ReadyDelegate>();
                ret.add(this);
                return ret;
            }
        
          }));
    }

    public void play() throws Exception {
        if (decoderThread != null)
            return ;
         
        decoderThread = new Thread(DecoderThread);
        decoderThread.IsBackground = true;
        decoderThread.Start();
        Events.Tick += Events_Tick;
    }

    public void stop() throws Exception {
        if (decoderThread == null)
            return ;
         
        decoderThread.Abort();
        decoderThread = null;
        Events.Tick -= Events_Tick;
    }

    public Surface getSurface() throws Exception {
        return surf;
    }

    public PlayerEvent Finished;
    public PlayerEvent FrameReady;
    void emitFinished() throws Exception {
        if (Finished != null)
            Finished.invoke();
         
    }

    void emitFrameReady() throws Exception {
        if (FrameReady != null)
            FrameReady.invoke();
         
    }

}


