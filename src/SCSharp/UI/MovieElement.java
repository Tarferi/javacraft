//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:09
//

package SCSharp.UI;

import java.util.ArrayList;
import java.util.List;
import SCSharp.BinElement;
import SCSharp.UI.__MultiPlayerEvent;
import SCSharp.UI.Painter;
import SCSharp.UI.SmackerPlayer;
import SCSharp.UI.UIElement;
import SCSharp.UI.UIScreen;

//
// SCSharp.UI.MovieElement
//
// Authors:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
public class MovieElement  extends UIElement 
{
    public MovieElement(UIScreen screen, BinElement el, byte[] palette, boolean scale) throws Exception {
        super(screen, el, palette);
        this.scale = scale;
    }

    public MovieElement(UIScreen screen, BinElement el, byte[] palette, String resource, boolean scale) throws Exception {
        this(screen, el, palette, resource);
        this.scale = scale;
    }

    public MovieElement(UIScreen screen, BinElement el, byte[] palette, String resource) throws Exception {
        super(screen, el, palette);
        setSensitive(false);
        setPlayer(new SmackerPlayer((Stream)getMpq().GetResource(resource),1));
    }

    public MovieElement(UIScreen screen, BinElement el, byte[] palette, SmackerPlayer player) throws Exception {
        super(screen, el, palette);
        setSensitive(false);
        setPlayer(player);
    }

    public MovieElement(UIScreen screen, int x, int y, int width, int height, boolean scale) throws Exception {
        super(screen, (ushort)x, (ushort)y, (ushort)width, (ushort)height);
        setSensitive(false);
        this.scale = scale;
    }

    public SmackerPlayer getPlayer() throws Exception {
        return player;
    }

    public void setPlayer(SmackerPlayer value) throws Exception {
        if (player != null)
        {
            player.FrameReady = __MultiPlayerEvent.remove(player.FrameReady,new PlayerEvent() 
              { 
                public System.Void invoke() throws Exception {
                    playerFrameReady();
                }

                public List<PlayerEvent> getInvocationList() throws Exception {
                    List<PlayerEvent> ret = new ArrayList<PlayerEvent>();
                    ret.add(this);
                    return ret;
                }
            
              });
            player.stop();
        }
         
        player = value;
        if (player != null)
            player.FrameReady = __MultiPlayerEvent.combine(player.FrameReady,new PlayerEvent() 
              { 
                public System.Void invoke() throws Exception {
                    playerFrameReady();
                }

                public List<PlayerEvent> getInvocationList() throws Exception {
                    List<PlayerEvent> ret = new ArrayList<PlayerEvent>();
                    ret.add(this);
                    return ret;
                }
            
              });
         
    }

    public void play() throws Exception {
        if (player != null)
            player.play();
         
    }

    public void stop() throws Exception {
        if (player != null)
            player.stop();
         
    }

    public Size getMovieSize() throws Exception {
        if (player == null)
            return new Size(0, 0);
        else
            return new Size(player.getWidth(), player.getHeight()); 
    }

    public void dim(byte dimness) throws Exception {
        dim = dimness;
        invalidate();
    }

    SmackerPlayer player;
    byte dim = 0;
    boolean scale = new boolean();
    protected void invalidate() throws Exception {
        if (getVisible())
            Painter.invalidate(getBounds());
         
        if (getSurface() != null)
            getSurface().Fill(Color.Transparent);
         
    }

    protected Surface createSurface() throws Exception {
        if (player == null || player.getSurface() == null)
            return null;
         
        Console.WriteLine("MovieElement: Creating new surface");
        Surface surf = new Surface();
        surf = new Surface(player.getSurface());
        if (scale && (player.getWidth() != getWidth() || player.getHeight() != getHeight()))
        {
            double horiz_zoom = (double)getWidth() / player.getWidth();
            double vert_zoom = (double)getHeight() / player.getHeight();
            double zoom = new double();
            if (horiz_zoom < vert_zoom)
                zoom = horiz_zoom;
            else
                zoom = vert_zoom; 
        }
         
        // FIXME: NewSDL
        // surf.Scale (zoom);
        if (dim != 0)
        {
            Surface dim_surf = new Surface(surf.Size);
            dim_surf.Alpha = dim;
            dim_surf.AlphaBlending = true;
            dim_surf.Blit(surf);
            surf.Dispose();
            surf = dim_surf;
        }
         
        return surf;
    }

    void playerFrameReady() throws Exception {
        invalidate();
    }

}


