//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:07
//

package SCSharp.UI;

import CS2JNet.JavaSupport.util.ListSupport;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import SCSharp.Mpq;
import SCSharp.UI.Builtins;
import SCSharp.UI.DialogEvent;
import SCSharp.UI.GameModeActivateDelegate;
import SCSharp.UI.GlobalResources;
import SCSharp.UI.UIDialog;
import SCSharp.UI.UIScreen;

//
// SCSharp.UI.GameModeDialog
//
// Authors:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
public class GameModeDialog  extends UIDialog 
{
    public GameModeDialog(UIScreen parent, Mpq mpq) throws Exception {
        super(parent, mpq, "glue\\Palmm", Builtins.rez_GluGameModeBin);
        background_path = "glue\\Palmm\\retail_ex.pcx";
        background_translucent = 42;
        background_transparent = 0;
    }

    static final int ORIGINAL_ELEMENT_INDEX = 1;
    static final int TITLE_ELEMENT_INDEX = 2;
    static final int EXPANSION_ELEMENT_INDEX = 3;
    static final int CANCEL_ELEMENT_INDEX = 4;
    protected void resourceLoader() throws Exception {
        super.resourceLoader();
        for (int i = 0;i < Elements.Count;i++)
        {
            Console.WriteLine("{0}: {1}", i, Elements[i].Text);
        }
        Elements[TITLE_ELEMENT_INDEX].Text = GlobalResources.getInstance().getBrooDat().GluAllTbl.Strings[172];
        Elements[ORIGINAL_ELEMENT_INDEX].Activate += ;
        Elements[EXPANSION_ELEMENT_INDEX].Activate += ;
        Elements[CANCEL_ELEMENT_INDEX].Activate += ;
    }

    public DialogEvent Cancel;
    public GameModeActivateDelegate Activate;
}


