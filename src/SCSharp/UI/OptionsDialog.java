//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:09
//

package SCSharp.UI;

import java.util.ArrayList;
import java.util.List;
import SCSharp.Mpq;
import SCSharp.UI.__MultiDialogEvent;
import SCSharp.UI.Builtins;
import SCSharp.UI.DialogEvent;
import SCSharp.UI.NetworkDialog;
import SCSharp.UI.SoundDialog;
import SCSharp.UI.SpeedDialog;
import SCSharp.UI.UIDialog;
import SCSharp.UI.UIScreen;
import SCSharp.UI.VideoDialog;

public class OptionsDialog  extends UIDialog 
{
    public OptionsDialog(UIScreen parent, Mpq mpq) throws Exception {
        super(parent, mpq, "glue\\Palmm", Builtins.rez_OptionsBin);
        background_path = null;
    }

    static final int PREVIOUS_ELEMENT_INDEX = 1;
    static final int SPEED_ELEMENT_INDEX = 2;
    static final int SOUND_ELEMENT_INDEX = 3;
    static final int VIDEO_ELEMENT_INDEX = 4;
    static final int NETWORK_ELEMENT_INDEX = 5;
    protected void resourceLoader() throws Exception {
        super.resourceLoader();
        Elements[SOUND_ELEMENT_INDEX].Activate += ;
        Elements[SPEED_ELEMENT_INDEX].Activate += ;
        Elements[VIDEO_ELEMENT_INDEX].Activate += ;
        Elements[NETWORK_ELEMENT_INDEX].Activate += ;
        Elements[PREVIOUS_ELEMENT_INDEX].Activate += ;
        for (int i = 0;i < Elements.Count;i++)
            Console.WriteLine("{0}: {1} '{2}'", i, Elements[i].Type, Elements[i].Text);
    }

    public DialogEvent Previous;
}


