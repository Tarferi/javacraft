//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:06
//

package SCSharp.UI;

import SCSharp.UI.MapPoint;
import SCSharp.UI.MapRenderer;

public class AStarSolver   
{
    Dictionary<MapPoint, MapPoint> came_from = new Dictionary<MapPoint, MapPoint>();
    MapRenderer map;
    public AStarSolver(MapRenderer map) throws Exception {
        came_from = new Dictionary<MapPoint, MapPoint>();
        this.map = map;
    }

    double dist_between(MapPoint point2, MapPoint point1) throws Exception {
        return Math.Sqrt((point2.getX() - point1.getX()) * (point2.getX() - point1.getX()) + (point2.getY() - point1.getY()) * (point2.getY() - point1.getY()));
    }

    // euclidian distance
    double heuristic_estimate_of_distance(MapPoint point, MapPoint goal) throws Exception {
        return Math.Abs(point.getX() - goal.getX()) + Math.Abs(point.getY() - goal.getY());
    }

    MapPoint findLowestScoringNode(Dictionary<MapPoint, boolean> openset, Dictionary<MapPoint, double> f_score) throws Exception {
        MapPoint current_lowest = null;
        double current_lowest_score = Double.MaxValue;
        for (/* [UNSUPPORTED] 'var' as type is unsupported "var" */ mp : openset.Keys)
        {
            if (f_score[mp] < current_lowest_score)
            {
                current_lowest_score = f_score[mp];
                current_lowest = mp;
            }
             
        }
        return current_lowest;
    }

    List<MapPoint> neighborNodes(MapPoint point) throws Exception {
        List<MapPoint> neighbors = new List<MapPoint>();
        for (int x = -1;x <= 1;x++)
        {
            for (int y = -1;y <= 1;y++)
            {
                // Console.WriteLine ("checking neighbors of {0}", point);
                // Console.WriteLine ("map minitile size = {0}x{1}", map.Chk.Width, map.Chk.Height);
                MapPoint p = new MapPoint(x + point.getX(),y + point.getY());
                // Console.WriteLine ("checking neighbor {0}", p);
                if (x == 0 && y == 0)
                {
                    continue;
                }
                 
                // disallow non-moves
                // Console.WriteLine ("no (1)");
                if (p.getX() < 0 || p.getX() >= (map.getChk().Width << 3))
                {
                    continue;
                }
                 
                // Console.WriteLine ("no (2)");
                if (p.getY() < 0 || p.getY() >= (map.getChk().Height << 3))
                {
                    continue;
                }
                 
                // Console.WriteLine ("no (3)");
                // don't move into walls
                if (!map.navigable(p))
                {
                    continue;
                }
                 
                // Console.WriteLine ("no (4)");
                // Console.WriteLine ("adding neighbor of {0}", p);
                neighbors.Add(p);
            }
        }
        return neighbors;
    }

    Dictionary<MapPoint, boolean> closedset = new Dictionary<MapPoint, boolean>();
    // The set of nodes already evaluated.
    public List<MapPoint> findPath(MapPoint start, MapPoint goal) throws Exception {
        if (!map.navigable(start))
            throw new Exception("is it valid to start in a non-navigable spot?");
         
        closedset = new Dictionary<MapPoint, boolean>();
        /* [UNSUPPORTED] 'var' as type is unsupported "var" */ openset = new Dictionary<MapPoint, boolean>();
        // The set of tentative nodes to be evaluated.
        openset.Add(start, true);
        /* [UNSUPPORTED] 'var' as type is unsupported "var" */ g_score = new Dictionary<MapPoint, double>();
        /* [UNSUPPORTED] 'var' as type is unsupported "var" */ h_score = new Dictionary<MapPoint, double>();
        /* [UNSUPPORTED] 'var' as type is unsupported "var" */ f_score = new Dictionary<MapPoint, double>();
        g_score[start] = 0.0;
        // Distance from start along optimal path.
        h_score[start] = heuristic_estimate_of_distance(start,goal);
        f_score[start] = h_score[start];
        while (openset.Keys.Count > 0)
        {
            // Estimated total distance from start to goal through y.
            /* [UNSUPPORTED] 'var' as type is unsupported "var" */ x = FindLowestScoringNode(openset, f_score);
            if (x == goal)
            {
                Console.WriteLine("done!");
                return reconstructPath(goal);
            }
             
            openset.Remove(x);
            closedset.Add(x, true);
            /* [UNSUPPORTED] 'var' as type is unsupported "var" */ neighbors = NeighborNodes(x);
            for (/* [UNSUPPORTED] 'var' as type is unsupported "var" */ y : neighbors)
            {
                boolean tentative_is_better = new boolean();
                if (closedset.ContainsKey(y))
                    continue;
                 
                /* [UNSUPPORTED] 'var' as type is unsupported "var" */ tentative_g_score = g_score[x] + dist_between(x, y);
                if (!openset.ContainsKey(y))
                {
                    openset.Add(y, true);
                    tentative_is_better = true;
                }
                else if (tentative_g_score < g_score[y])
                {
                    tentative_is_better = true;
                }
                else
                {
                    tentative_is_better = false;
                }  
                if (tentative_is_better)
                {
                    came_from[y] = x;
                    g_score[y] = tentative_g_score;
                    h_score[y] = heuristic_estimate_of_distance(y, goal);
                    f_score[y] = g_score[y] + h_score[y];
                }
                 
            }
        }
        Console.WriteLine("failed to find path");
        return null;
    }

    List<MapPoint> reconstructPath(MapPoint current_node) throws Exception {
        List<MapPoint> path = new List<MapPoint>();
        while (current_node != null)
        {
            path.Insert(0, current_node);
            if (came_from.ContainsKey(current_node))
                current_node = came_from[current_node];
            else
                current_node = null; 
        }
        return path;
    }

}


