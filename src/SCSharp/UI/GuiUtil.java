//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:08
//

package SCSharp.UI;

import CS2JNet.JavaSupport.util.ListSupport;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import SCSharp.Fnt;
import SCSharp.Glyph;
import SCSharp.Mpq;
import SCSharp.UI.GuiUtil;
import SCSharp.UI.Pcx;

public class GuiUtil   
{
    static Fnt[] fonts = new Fnt[]();
    static String[] BroodwarFonts = { "files\\font\\font8.fnt", "files\\font\\font10.fnt", "files\\font\\font14.fnt", "files\\font\\font16.fnt", "files\\font\\font16x.fnt" };
    public static Fnt[] getFonts(Mpq mpq) throws Exception {
        if (fonts == null)
        {
            String[] font_list = new String[]();
            font_list = BroodwarFonts;
            fonts = new Fnt[font_list.Length];
            for (int i = 0;i < fonts.Length;i++)
            {
                fonts[i] = (Fnt)mpq.GetResource(font_list[i]);
                Console.WriteLine("fonts[{0}] = {1}", i, fonts[i] == null ? "null" : "not null");
            }
        }
         
        return fonts;
    }

    public static Surface renderGlyph(Fnt font, Glyph g, byte[] palette, int offset) throws Exception {
        byte[] buf = new byte[g.getWidth() * g.getHeight() * 4];
        int i = 0;
        for (int y = g.getHeight() - 1;y >= 0;y--)
        {
            for (int x = g.getWidth() - 1;x >= 0;x--)
            {
                if (g.getBitmap()[y, x] == 0)
                    buf[i + 0] = 0;
                else if (g.getBitmap()[y, x] == 1)
                    buf[i + 0] = 255;
                else
                    buf[i + 0] = 128;  
                buf[i + 1] = palette[(g.getBitmap()[y, x] + offset) * 3 + 2];
                buf[i + 2] = palette[(g.getBitmap()[y, x] + offset) * 3 + 1];
                buf[i + 3] = palette[(g.getBitmap()[y, x] + offset) * 3];
                if (buf[i + 1] == 252 && buf[i + 2] == 0 && buf[i + 3] == 252)
                    buf[i + 0] = 0;
                 
                i += 4;
            }
        }
        return CreateSurfaceFromRGBAData(buf, (ushort)g.getWidth(), (ushort)g.getHeight(), 32, g.getWidth() * 4);
    }

    public static Surface composeText(String text, Fnt font, byte[] palette) throws Exception {
        return ComposeText(text, font, palette, -1, -1, 4);
    }

    public static Surface composeText(String text, Fnt font, byte[] palette, int offset) throws Exception {
        return ComposeText(text, font, palette, -1, -1, offset);
    }

    public static Surface composeText(String text, Fnt font, byte[] palette, int width, int height, int offset) throws Exception {
        if (font == null)
            Console.WriteLine("aiiiieeee");
         
        int i = new int();
        /* create a run of text, for now ignoring any control codes in the string */
        StringBuilder run = new StringBuilder();
        for (i = 0;i < text.Length;i++)
            /* allow newlines */
            if (text[i] == 0x0a || !Char.IsControl(text[i]))
                run.Append(text[i]);
             
        String rs = run.ToString();
        byte[] r = Encoding.ASCII.GetBytes(rs);
        int x = new int(), y = new int();
        int text_height = new int(), text_width = new int();
        /* measure the text first, optionally wrapping at width */
        text_width = text_height = 0;
        x = y = 0;
        for (i = 0;i < r.Length;i++)
        {
            int glyph_width = 0;
            if (r[i] != 0x0a)
            {
                /* newline */
                if (r[i] == 0x20)
                    /* space */
                    glyph_width = font.getSpaceSize();
                else
                {
                    Glyph g = font[r[i] - 1];
                    glyph_width = g.getWidth() + g.getXOffset();
                } 
            }
             
            if (r[i] == 0x0a || (width != -1 && x + glyph_width > width))
            {
                if (x > text_width)
                    text_width = x;
                 
                x = 0;
                text_height += font.getLineSize();
            }
             
            x += glyph_width;
        }
        if (x > text_width)
            text_width = x;
         
        text_height += font.getLineSize();
        Surface surf = new Surface(text_width, text_height);
        surf.TransparentColor = Color.Black;
        /* the draw it */
        x = y = 0;
        for (i = 0;i < r.Length;i++)
        {
            int glyph_width = 0;
            Glyph g = null;
            if (r[i] != 0x0a)
            {
                /* newline */
                if (r[i] == 0x20)
                {
                    /* space */
                    glyph_width = font.getSpaceSize();
                }
                else
                {
                    g = font[r[i] - 1];
                    glyph_width = g.getWidth() + g.getXOffset();
                    Surface gs = RenderGlyph(font, g, palette, offset);
                    surf.Blit(gs, new Point(x, y + g.getYOffset()));
                } 
            }
             
            if (r[i] == 0x0a || x + glyph_width > text_width)
            {
                x = 0;
                y += font.getLineSize();
            }
             
            x += glyph_width;
        }
        return surf;
    }

    public static Surface createSurface(byte[] data, ushort width, ushort height, int depth, int stride, int rmask, int gmask, int bmask, int amask) throws Exception {
        /* beware, kind of a gross hack below */
        Surface surf = new Surface();
        IntPtr blob = Marshal.AllocCoTaskMem(data.Length);
        Marshal.Copy(data, 0, blob, data.Length);
        IntPtr handle = Sdl.SDL_CreateRGBSurfaceFrom(blob, width, height, depth, stride, rmask, gmask, bmask, amask);
        surf = (Surface)Activator.CreateInstance(Surface.class, BindingFlags.NonPublic | BindingFlags.Instance, null, new Object[]{ handle }, null);
        return surf;
    }

    public static Surface createSurfaceFromRGBAData(byte[] data, ushort width, ushort height, int depth, int stride) throws Exception {
        return CreateSurface(data, width, height, depth, stride, /* [UNSUPPORTED] unchecked expressions are not supported "unchecked (int)0xff000000" */, (int)0x00ff0000, (int)0x0000ff00, (int)0x000000ff);
    }

    /* XXX this needs addressing in Tao.Sdl - these arguments should be uints */
    public static Surface createSurfaceFromRGBData(byte[] data, ushort width, ushort height, int depth, int stride) throws Exception {
        return CreateSurface(data, width, height, depth, stride, (int)0x00ff0000, (int)0x0000ff00, (int)0x000000ff, (int)0x00000000);
    }

    public static byte[] getBitmapData(byte[][] grid, ushort width, ushort height, byte[] palette, boolean with_alpha) throws Exception {
        byte[] buf = new byte[width * height * (3 + (with_alpha ? 1 : 0))];
        int i = 0;
        int x = new int(), y = new int();
        for (y = height - 1;y >= 0;y--)
        {
            for (x = width - 1;x >= 0;x--)
            {
                if (with_alpha)
                    i++;
                 
                buf[i++] = palette[grid[y, x] * 3 + 2];
                buf[i++] = palette[grid[y, x] * 3 + 1];
                buf[i++] = palette[grid[y, x] * 3];
                if (with_alpha)
                {
                    if (buf[i - 3] == 0 && buf[i - 2] == 0 && buf[i - 1] == 0)
                    {
                        buf[i - 4] = 0x00;
                    }
                    else if (buf[i - 3] == 59 && buf[i - 2] == 39 && buf[i - 1] == 39)
                    {
                        buf[i - 3] = buf[i - 2] = buf[i - 1] = 0;
                        //							Console.WriteLine ("translucent shadow pixel, palette index = {0}", palette [grid[y,x]] * 3);
                        buf[i - 4] = 0xaa;
                    }
                    else
                    {
                        //							Console.WriteLine ("pixel data RGB = {0},{1},{2}, palette index = {3}",
                        //									   buf[i-3],buf[i-2],buf[i-1], palette [grid[y,x]] * 3);
                        buf[i - 4] = 0xff;
                    }  
                }
                 
            }
        }
        return buf;
    }

    public static byte[] getBitmapData(byte[][] grid, ushort width, ushort height, byte[] palette, int translucent_index, int transparent_index) throws Exception {
        byte[] buf = new byte[width * height * 4];
        int i = 0;
        int x = new int(), y = new int();
        for (y = height - 1;y >= 0;y--)
        {
            for (x = width - 1;x >= 0;x--)
            {
                if (grid[y, x] == translucent_index)
                    buf[i + 0] = 0x05;
                else /* keep this in sync with Pcx.cs */
                if (grid[y, x] == transparent_index)
                    buf[i + 0] = 0x00;
                else
                    buf[i + 0] = 0xff;  
                buf[i + 1] = palette[grid[y, x] * 3 + 2];
                buf[i + 2] = palette[grid[y, x] * 3 + 1];
                buf[i + 3] = palette[grid[y, x] * 3 + 0];
                i += 4;
            }
        }
        return buf;
    }

    public static Surface createSurfaceFromBitmap(byte[][] grid, ushort width, ushort height, byte[] palette, boolean with_alpha) throws Exception {
        byte[] buf = GetBitmapData(grid, width, height, palette, with_alpha);
        return CreateSurfaceFromRGBAData(buf, width, height, with_alpha ? 32 : 24, width * (3 + (with_alpha ? 1 : 0)));
    }

    public static Surface createSurfaceFromBitmap(byte[][] grid, ushort width, ushort height, byte[] palette, int translucent_index, int transparent_index) throws Exception {
        byte[] buf = GetBitmapData(grid, width, height, palette, translucent_index, transparent_index);
        return CreateSurfaceFromRGBAData(buf, width, height, 32, width * 4);
    }

    public static byte[] readStream(Stream stream) throws Exception {
        if (stream instanceof MemoryStream)
        {
            return ((MemoryStream)stream).ToArray();
        }
        else
        {
            byte[] buf = new byte[stream.Length];
            stream.Read(buf, 0, buf.Length);
            return buf;
        } 
    }

    public static Surface surfaceFromPcx(Pcx pcx) throws Exception {
        return CreateSurfaceFromRGBAData(pcx.getRgbaData(), pcx.getWidth(), pcx.getHeight(), pcx.getDepth(), pcx.getStride());
    }

    public static Surface surfaceFromStream(Stream stream, int translucentIndex, int transparentIndex) throws Exception {
        Pcx pcx = new Pcx();
        pcx.readFromStream(stream,translucentIndex,transparentIndex);
        return surfaceFromPcx(pcx);
    }

    public static Surface surfaceFromStream(Stream stream) throws Exception {
        return GuiUtil.surfaceFromStream(stream,-1,-1);
    }

    public static Sound soundFromStream(Stream stream) throws Exception {
        byte[] buf = GuiUtil.readStream(stream);
        return Mixer.Sound(buf);
    }

    public static void playSound(Mpq mpq, String resourcePath) throws Exception {
        Stream stream = (Stream)mpq.getResource(resourcePath);
        if (stream == null)
            return ;
         
        Sound s = GuiUtil.soundFromStream(stream);
        s.Play();
    }

    public static void playMusic(Mpq mpq, String resourcePath, int numLoops) throws Exception {
        Stream stream = (Stream)mpq.getResource(resourcePath);
        if (stream == null)
            return ;
         
        Sound s = GuiUtil.soundFromStream(stream);
        s.Play(true);
    }

}


