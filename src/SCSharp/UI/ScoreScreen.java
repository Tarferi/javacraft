//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:10
//

package SCSharp.UI;

import SCSharp.Mpq;
import SCSharp.UI.Builtins;
import SCSharp.UI.Game;
import SCSharp.UI.UIScreen;
import SCSharp.Util;

//
// SCSharp.UI.ScoreScreen
//
// Authors:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
public class ScoreScreen  extends UIScreen 
{
    boolean victorious = new boolean();
    public ScoreScreen(Mpq mpq, boolean victorious) throws Exception {
        super(mpq, String.Format("glue\\Pal{0}{1}", Util.RaceChar[((Enum)Game.getInstance().getRace()).ordinal()], victorious ? 'v' : 'd'), Builtins.rez_GluScoreBin);
        this.mpq = mpq;
        this.victorious = victorious;
    }

    static final int MAIN_IMAGE_INDEX = 1;
    protected void resourceLoader() throws Exception {
        super.resourceLoader();
        Elements[MAIN_IMAGE_INDEX].Text = String.Format(victorious ? Builtins.Scorev_pMainPcx : Builtins.Scored_pMainPcx, Util.RaceChar[((Enum)Game.getInstance().getRace()).ordinal()]);
        Console.WriteLine("screen path = {0}", Elements[MAIN_IMAGE_INDEX].Text);
    }

}


