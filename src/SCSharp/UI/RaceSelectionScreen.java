//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:10
//

package SCSharp.UI;

import SCSharp.ElementFlags;
import SCSharp.Mpq;
import SCSharp.UI.Builtins;
import SCSharp.UI.ButtonElement;
import SCSharp.UI.EstablishingShot;
import SCSharp.UI.Game;
import SCSharp.UI.GlobalResources;
import SCSharp.UI.Layer;
import SCSharp.UI.LoadSavedScreen;
import SCSharp.UI.MovieElement;
import SCSharp.UI.Painter;
import SCSharp.UI.PlayCustomScreen;
import SCSharp.UI.Race;
import SCSharp.UI.SmackerPlayer;
import SCSharp.UI.UIElement;
import SCSharp.UI.UIPainter;
import SCSharp.UI.UIScreen;
import SCSharp.UI.UIScreenType;
import SCSharp.Util;

//
// SCSharp.UI.RaceSelectionScreen
//
// Authors:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
public class RaceSelectionScreen  extends UIScreen 
{
    public RaceSelectionScreen(Mpq mpq) throws Exception {
        super(mpq, "glue\\PalCs", Game.getInstance().getPlayingBroodWar() ? Builtins.rez_GluExpcmpgnBin : Builtins.rez_GluCmpgnBin);
    }

    static class RaceData   
    {
        public RaceData() {
        }

        public Race race = Race.Zerg;
        public String normalMovie = new String();
        public String onMovie = new String();
        public int mapDataStart = new int();
        public RaceData(Race race, String normalMovie, String onMovie, int mapDataStart) throws Exception {
            this.race = race;
            this.normalMovie = normalMovie;
            this.onMovie = onMovie;
            this.mapDataStart = mapDataStart;
        }
    
    }

    RaceData[] StarcraftCampaigns = new RaceData[]{ new RaceData(Race.Terran,"glue\\campaign\\terr.smk","glue\\campaign\\terron.smk",0), new RaceData(Race.Zerg,"glue\\campaign\\zerg.smk","glue\\campaign\\zergon.smk",11), new RaceData(Race.Protoss,"glue\\campaign\\prot.smk","glue\\campaign\\proton.smk",21) };
    RaceData[] BroodwarCampaigns = new RaceData[]{ new RaceData(Race.Protoss,"glue\\Expcampaign\\XProt.smk","glue\\Expcampaign\\XProtOn.smk",31), new RaceData(Race.Terran,"glue\\Expcampaign\\XTerr.smk","glue\\Expcampaign\\XTerrOn.smk",40), new RaceData(Race.Zerg,"glue\\Expcampaign\\XZerg.smk","glue\\Expcampaign\\XZergOn.smk",49) };
    static final int LOADSAVED_ELEMENT_INDEX = 3;
    static final int LOADREPLAY_ELEMENT_INDEX = 4;
    static final int THIRD_CAMPAIGN_ELEMENT_INDEX = 5;
    static final int FIRST_CAMPAIGN_ELEMENT_INDEX = 6;
    static final int SECOND_CAMPAIGN_ELEMENT_INDEX = 7;
    static final int CANCEL_ELEMENT_INDEX = 8;
    static final int PLAYCUSTOM_ELEMENT_INDEX = 9;
    static final int SECOND_BUT_FIRST_INCOMPLETE_INDEX = 10;
    static final int THIRD_BUT_FIRST_INCOMPLETE_INDEX = 11;
    static final int THIRD_BUT_SECOND_INCOMPLETE_INDEX = 12;
    List<UIElement> smkElements = new List<UIElement>();
    UIPainter smkPainter;
    public void addToPainter() throws Exception {
        super.addToPainter();
        for (Object __dummyForeachVar0 : smkElements)
        {
            MovieElement el = (MovieElement)__dummyForeachVar0;
            el.play();
        }
        Painter.Add(Layer.Background, smkPainter.Paint);
    }

    public void removeFromPainter() throws Exception {
        super.removeFromPainter();
        for (Object __dummyForeachVar1 : smkElements)
        {
            MovieElement el = (MovieElement)__dummyForeachVar1;
            el.stop();
        }
        Painter.Remove(Layer.Background, smkPainter.Paint);
        diskPlayer = null;
    }

    protected void resourceLoader() throws Exception {
        super.resourceLoader();
        for (int i = 0;i < Elements.Count;i++)
            Console.WriteLine("{0}: {1} '{2}' : {3}", i, Elements[i].Type, Elements[i].Text, Elements[i].Flags);
        Elements[FIRST_CAMPAIGN_ELEMENT_INDEX].Flags |= ElementFlags.BottomAlignText;
        Elements[SECOND_CAMPAIGN_ELEMENT_INDEX].Flags |= ElementFlags.BottomAlignText;
        Elements[THIRD_CAMPAIGN_ELEMENT_INDEX].Flags |= ElementFlags.BottomAlignText;
        Elements[THIRD_CAMPAIGN_ELEMENT_INDEX].MouseEnterEvent += ;
        Elements[THIRD_CAMPAIGN_ELEMENT_INDEX].MouseLeaveEvent += ;
        Elements[SECOND_CAMPAIGN_ELEMENT_INDEX].MouseEnterEvent += ;
        Elements[SECOND_CAMPAIGN_ELEMENT_INDEX].MouseLeaveEvent += ;
        Elements[FIRST_CAMPAIGN_ELEMENT_INDEX].Activate += ;
        Elements[SECOND_CAMPAIGN_ELEMENT_INDEX].Activate += ;
        Elements[THIRD_CAMPAIGN_ELEMENT_INDEX].Activate += ;
        Elements[CANCEL_ELEMENT_INDEX].Activate += ;
        Elements[LOADSAVED_ELEMENT_INDEX].Activate += ;
        Elements[PLAYCUSTOM_ELEMENT_INDEX].Activate += ;
        smkElements = new List<UIElement>();
        addMovieElements(FIRST_CAMPAIGN_ELEMENT_INDEX,0,-40,0);
        addMovieElements(SECOND_CAMPAIGN_ELEMENT_INDEX,1,0,0);
        addMovieElements(THIRD_CAMPAIGN_ELEMENT_INDEX,2,0,0);
        smkPainter = new UIPainter(smkElements);
    }

    void selectCampaign(int campaign) throws Exception {
        uint mapdata_index = new uint();
        String prefix = new String();
        String markup = new String();
        Game.getInstance().setRace((Game.getInstance().getPlayingBroodWar() ? BroodwarCampaigns : StarcraftCampaigns)[campaign].race);
        mapdata_index = GlobalResources.getInstance().getMapDataDat().FileIndexes[(Game.getInstance().getPlayingBroodWar() ? BroodwarCampaigns : StarcraftCampaigns)[campaign].mapDataStart];
        prefix = GlobalResources.getInstance().getMapDataTbl()[(int)mapdata_index];
        markup = String.Format("rez\\Est{0}{1}{2}.txt", Util.RaceChar[((Enum)Game.getInstance().getRace()).ordinal()], prefix.EndsWith("tutorial") ? "0t" : prefix.Substring(prefix.Length - 2), Game.getInstance().getPlayingBroodWar() ? "x" : "");
        Game.getInstance().switchToScreen(new EstablishingShot(markup,prefix,mpq));
    }

    SmackerPlayer diskPlayer;
    void addMovieElements(int elementIndex, int campaign, int off_x, int off_y) throws Exception {
        MovieElement normalElement, onElement, diskElement;
        if (diskPlayer == null)
            diskPlayer = new SmackerPlayer((Stream)getMpq().GetResource(Game.getInstance().getPlayingBroodWar() ? "glue\\Expcampaign\\disk.smk" : "glue\\campaign\\disk.smk"),1);
         
        diskElement = new MovieElement(this, Elements[elementIndex].BinElement, Elements[elementIndex].Palette, diskPlayer);
        diskElement.setX1((ushort)(Elements[elementIndex].X1 + ((Elements[elementIndex].Width - diskElement.getMovieSize().Width) / 2)));
        diskElement.setY1((ushort)(((ButtonElement)Elements[elementIndex]).getTextPosition().Y - diskElement.getMovieSize().Height));
        normalElement = new MovieElement(this, Elements[elementIndex].BinElement, Elements[elementIndex].Palette, (Game.getInstance().getPlayingBroodWar() ? BroodwarCampaigns : StarcraftCampaigns)[campaign].normalMovie);
        normalElement.setX1((ushort)(Elements[elementIndex].X1 + ((Elements[elementIndex].Width - normalElement.getMovieSize().Width) / 2) + off_x));
        normalElement.setY1((ushort)(((ButtonElement)Elements[elementIndex]).getTextPosition().Y - normalElement.getMovieSize().Height + off_y));
        onElement = new MovieElement(this, Elements[elementIndex].BinElement, Elements[elementIndex].Palette, (Game.getInstance().getPlayingBroodWar() ? BroodwarCampaigns : StarcraftCampaigns)[campaign].onMovie);
        onElement.setX1((ushort)(Elements[elementIndex].X1 + ((Elements[elementIndex].Width - onElement.getMovieSize().Width) / 2)));
        onElement.setY1((ushort)(((ButtonElement)Elements[elementIndex]).getTextPosition().Y - onElement.getMovieSize().Height));
        smkElements.Add(diskElement);
        smkElements.Add(normalElement);
        smkElements.Add(onElement);
        onElement.setVisible(false);
        normalElement.Dim(100);
        Elements[elementIndex].MouseEnterEvent += ;
        Elements[elementIndex].MouseLeaveEvent += ;
    }

}


