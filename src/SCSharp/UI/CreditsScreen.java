//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:07
//

package SCSharp.UI;

import SCSharp.Mpq;
import SCSharp.UI.Builtins;
import SCSharp.UI.Game;
import SCSharp.UI.MarkupScreen;
import SCSharp.UI.UIScreenType;

//
// SCSharp.UI.CreditsScreen
//
// Authors:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
public class CreditsScreen  extends MarkupScreen 
{
    public CreditsScreen(Mpq mpq) throws Exception {
        super(mpq);
    }

    protected void loadMarkup() throws Exception {
        AddMarkup(Assembly.GetExecutingAssembly().GetManifestResourceStream("credits.txt"));
        /* broodwar credits */
        if (Game.getInstance().getIsBroodWar())
            addMarkup((Stream)mpq.GetResource(Builtins.RezCrdtexpTxt));
         
        /* starcraft credits */
        addMarkup((Stream)mpq.GetResource(Builtins.RezCrdtlistTxt));
    }

    protected void markupFinished() throws Exception {
        Game.getInstance().switchToScreen(UIScreenType.MainMenu);
    }

}


