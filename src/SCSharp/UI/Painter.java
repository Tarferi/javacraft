//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:09
//

package SCSharp.UI;

import CS2JNet.JavaSupport.util.ListSupport;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import SCSharp.UI.Game;
import SCSharp.UI.GuiUtil;
import SCSharp.UI.Layer;
import SCSharp.UI.Painter;
import SCSharp.UI.PainterDelegate;
import SCSharp.UI.Pcx;

public class Painter   
{
    static List<PainterDelegate>[] layers = new List<PainterDelegate>[]();
    static int millis = new int();
    static int total_elapsed = new int();
    static DateTime now = new DateTime();
    static Surface paintingSurface = new Surface();
    static boolean fullscreen = new boolean();
    public static final int SCREEN_RES_X = 640;
    public static final int SCREEN_RES_Y = 480;
    static DateTime last_time = new DateTime();
    static byte[] fontpal = new byte[]();
    static double fps = new double();
    static int frame_count = new int();
    static Surface fps_surface = new Surface();
    static boolean debug_dirty = new boolean();
    static boolean show_fps = new boolean();
    static {
        try
        {
            String v = new String();
            v = ConfigurationManager.AppSettings["ShowFps"];
            if (v != null)
                show_fps = Boolean.Parse(v);
             
            v = ConfigurationManager.AppSettings["DebugPainterDirty"];
            if (v != null)
                debug_dirty = Boolean.Parse(v);
             
        }
        catch (Exception __dummyStaticConstructorCatchVar0)
        {
            throw new ExceptionInInitializerError(__dummyStaticConstructorCatchVar0);
        }
    
    }

    static boolean init_done = new boolean();
    public static void initializePainter(boolean fullscreen, int milli) throws Exception {
        if (init_done)
            throw new Exception("painter can only be initialized once");
         
        init_done = true;
        if (show_fps)
        {
            Pcx pcx = new Pcx();
            pcx.readFromStream((Stream)Game.getInstance().getPlayingMpq().GetResource("game\\tfontgam.pcx"),0,0);
            fontpal = pcx.getPalette();
        }
         
        millis = milli;
        setFullscreen(fullscreen);
        /* init our list of painter delegates */
        layers = new List<PainterDelegate>[((Enum)Layer.Count).ordinal()];
        for (Layer i = Layer.Background;i < Layer.Count;i++)
            layers[((Enum)i).ordinal()] = new List<PainterDelegate>();
        /* and set ourselves up to invalidate at a regular interval*/
        Events.Tick += Tick;
    }

    public static boolean getFullscreen() throws Exception {
        return fullscreen;
    }

    public static void setFullscreen(boolean value) throws Exception {
        if (fullscreen != value || paintingSurface == null)
        {
            fullscreen = value;
            if (fullscreen)
                paintingSurface = Video.SetVideoMode(SCREEN_RES_X, SCREEN_RES_Y);
            else
            {
                paintingSurface = Video.SetVideoMode(SCREEN_RES_X, SCREEN_RES_Y);
            } 
            invalidate();
        }
         
    }

    public static void prepend(Layer layer, PainterDelegate painter) throws Exception {
        layers[((Enum)layer).ordinal()].Insert(0, painter);
    }

    public static void add(Layer layer, PainterDelegate painter) throws Exception {
        layers[((Enum)layer).ordinal()].Add(painter);
    }

    public static void remove(Layer layer, PainterDelegate painter) throws Exception {
        layers[((Enum)layer).ordinal()].Remove(painter);
    }

    public static void clear(Layer layer) throws Exception {
        layers[((Enum)layer).ordinal()].Clear();
    }

    static int paused = new int();
    public static void pause() throws Exception {
        paused++;
    }

    public static void resume() throws Exception {
        paused--;
    }

    static void tick(Object sender, TickEventArgs e) throws Exception {
        total_elapsed += e.TicksElapsed;
        if (total_elapsed < millis)
            return ;
         
        if (paused == 0)
            redraw();
         
    }

    public static EventHandler Painting = new EventHandler();
    public static void redraw() throws Exception {
        Rectangle fps_rect = Rectangle.Empty;
        if (show_fps)
        {
            fps_rect = new Rectangle(new Point(10, 10), new Size(80, 30));
            frame_count++;
            if (frame_count == 50)
            {
                DateTime after = DateTime.Now;
                fps = 1.0 / (after - last_time).TotalSeconds * 50;
                last_time = after;
                frame_count = 0;
                /* make sure we invalidate the region where we're going to draw the fps/related info */
                invalidate(fps_rect);
            }
             
        }
         
        //Console.WriteLine ("Redraw");
        if (Painting != null)
            Painting(null, EventArgs.Empty);
         
        if (dirty.IsEmpty)
            return ;
         
        //Console.WriteLine (" + dirty = {0}", dirty);
        total_elapsed = 0;
        now = DateTime.Now;
        paintingSurface.ClipRectangle = dirty;
        if (debug_dirty)
        {
            paintingSurface.Fill(dirty, Color.Red);
            paintingSurface.Update();
        }
         
        paintingSurface.Fill(dirty, Color.Black);
        for (Layer i = Layer.Background;i < Layer.Count;i++)
        {
            DrawLayer(layers[((Enum)i).ordinal()]);
        }
        if (show_fps)
        {
            if (fps_surface != null)
                fps_surface.Dispose();
             
            fps_surface = GuiUtil.ComposeText(String.Format("fps: {0,0:F}", fps), GuiUtil.getFonts(Game.getInstance().getPlayingMpq())[1], fontpal);
            paintingSurface.Blit(fps_surface, new Point(10, 10));
        }
         
        paintingSurface.Update();
        paintingSurface.ClipRectangle = paintingSurface.Rectangle;
        dirty = Rectangle.Empty;
        total_elapsed = (DateTime.Now - now).Milliseconds;
    }

    public static void drawLayer(List<PainterDelegate> painters) throws Exception {
        for (int i = 0;i < painters.Count;i++)
            painters[i](now);
    }

    public static void blit(Surface surf, Point p) throws Exception {
        paintingSurface.Blit(surf, p);
    }

    public static void blit(Surface surf) throws Exception {
        paintingSurface.Blit(surf);
    }

    public static void blit(Surface surf, Rectangle r1, Rectangle r2) throws Exception {
        paintingSurface.Blit(surf, r1, r2);
    }

    public static void drawBox(Rectangle rect, Color color) throws Exception {
    }

    // newsdl			paintingSurface.DrawBox (rect, color);
    static Rectangle dirty = Rectangle.Empty;
    public static Rectangle getDirty() throws Exception {
        return dirty;
    }

    public static void invalidate(Rectangle r) throws Exception {
        if (r.X >= Painter.SCREEN_RES_X || r.Y >= Painter.SCREEN_RES_Y || r.X + r.Width <= 0 || r.Y + r.Height <= 0)
            return ;
         
        if (r.X < 0)
        {
            r.Width += r.X;
            r.X = 0;
        }
         
        if (r.Y < 0)
        {
            r.Height += r.Y;
            r.Y = 0;
        }
         
        if (r.X + r.Width > Painter.SCREEN_RES_X)
            r.Width = Painter.SCREEN_RES_X - r.X;
         
        if (r.Y + r.Height > Painter.SCREEN_RES_Y)
            r.Height = Painter.SCREEN_RES_Y - r.Y;
         
        if (dirty.IsEmpty)
            dirty = r;
        else if (dirty.Contains(r))
            return ;
        else
            dirty = Rectangle.Union(dirty, r);  
    }

    public static void invalidate() throws Exception {
        invalidate(new Rectangle(0, 0, Painter.SCREEN_RES_X, Painter.SCREEN_RES_Y));
    }

    public static int getWidth() throws Exception {
        return paintingSurface.Width;
    }

    public static int getHeight() throws Exception {
        return paintingSurface.Height;
    }

}


