//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:07
//

package SCSharp.UI;

import CS2JNet.JavaSupport.util.ListSupport;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import SCSharp.BinElement;
import SCSharp.UI.ComboBoxSelectionChanged;
import SCSharp.UI.GuiUtil;
import SCSharp.UI.Layer;
import SCSharp.UI.Painter;
import SCSharp.UI.UIElement;
import SCSharp.UI.UIScreen;

//
// SCSharp.UI.ComboBoxElement
//
// Authors:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
public class ComboBoxElement  extends UIElement 
{
    List<String> items = new List<String>();
    int cursor = -1;
    Surface dropdownSurface = new Surface();
    boolean dropdown_visible = new boolean();
    int selected_item = new int();
    public ComboBoxElement(UIScreen screen, BinElement el, byte[] palette) throws Exception {
        super(screen, el, palette);
        items = new List<String>();
    }

    public int getSelectedIndex() throws Exception {
        return cursor;
    }

    public void setSelectedIndex(int value) throws Exception {
        cursor = value;
        invalidate();
    }

    public String getSelectedItem() throws Exception {
        return items[cursor];
    }

    public void addItem(String item) throws Exception {
        addItem(item,true);
    }

    public void addItem(String item, boolean select) throws Exception {
        items.Add(item);
        if (select || cursor == -1)
            cursor = items.IndexOf(item);
         
        invalidate();
    }

    public void removeAt(int index) throws Exception {
        items.RemoveAt(index);
        if (items.Count == 0)
            cursor = -1;
         
        invalidate();
    }

    public void clear() throws Exception {
        items.Clear();
        cursor = -1;
        invalidate();
    }

    public boolean contains(String item) throws Exception {
        return items.Contains(item);
    }

    public void mouseButtonDown(MouseButtonEventArgs args) throws Exception {
        showDropdown();
    }

    public void mouseButtonUp(MouseButtonEventArgs args) throws Exception {
        hideDropdown();
    }

    public void pointerMotion(MouseMotionEventArgs args) throws Exception {
        /* if the dropdown is visible, see if we're inside it */
        if (!dropdown_visible)
            return ;
         
        /*
        			      starcraft doesn't include this check..  should we?
        			      args.X >= X1 && args.X < X1 + dropdownSurface.Width &&
        			    */
        if (args.Y >= getY1() + getHeight() && args.Y < getY1() + getHeight() + dropdownSurface.Height)
        {
            int new_selected_item = (args.Y - (getY1() + getHeight())) / getFont().LineSize;
            if (selected_item != new_selected_item)
            {
                selected_item = new_selected_item;
                createDropdownSurface();
                Painter.invalidate(new Rectangle(new Point(getX1(), getY1() + getHeight()), dropdownSurface.Size));
            }
             
        }
         
    }

    void paintDropdown(DateTime dt) throws Exception {
        Painter.blit(dropdownSurface,new Point(getX1(), getY1() + getHeight()));
    }

    void showDropdown() throws Exception {
        dropdown_visible = true;
        selected_item = cursor;
        createDropdownSurface();
        Painter.Add(Layer.Popup, PaintDropdown);
        Painter.invalidate(new Rectangle(new Point(getX1(), getY1() + getHeight()), dropdownSurface.Size));
    }

    void hideDropdown() throws Exception {
        dropdown_visible = false;
        if (cursor != selected_item)
        {
            cursor = selected_item;
            if (SelectionChanged != null)
                SelectionChanged.invoke(cursor);
             
            invalidate();
        }
         
        Painter.Remove(Layer.Popup, PaintDropdown);
        Painter.invalidate(new Rectangle(new Point(getX1(), getY1() + getHeight()), dropdownSurface.Size));
    }

    protected Surface createSurface() throws Exception {
        Surface surf = new Surface(getWidth(), getHeight());
        /* XXX draw the arrow (and border) */
        if (cursor != -1)
        {
            Surface item_surface = GuiUtil.ComposeText(items[cursor], getFont(), getPalette(), 4);
            item_surface.TransparentColor = Color.Black;
            surf.Blit(item_surface, new Point(0, 0));
        }
         
        return surf;
    }

    void createDropdownSurface() throws Exception {
        dropdownSurface = new Surface(getWidth(), items.Count * getFont().LineSize);
        int y = 0;
        for (int i = 0;i < items.Count;i++)
        {
            Surface item_surface = GuiUtil.ComposeText(items[i], getFont(), getPalette(), i == selected_item ? 4 : 24);
            item_surface.TransparentColor = Color.Black;
            dropdownSurface.Blit(item_surface, new Point(0, y));
            y += item_surface.Height;
        }
    }

    public ComboBoxSelectionChanged SelectionChanged;
}


