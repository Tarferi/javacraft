//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:08
//

package SCSharp.UI;

import CS2JNet.JavaSupport.util.ListSupport;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import SCSharp.UI.__MultiReadyDelegate;
import SCSharp.UI.ReadyDelegate;

//
// SCSharp.UI.GuiUtils
//
// Authors:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
/* for the surface creation hack below */
public class __MultiReadyDelegate   implements ReadyDelegate
{
    public void invoke() throws Exception {
        IList<ReadyDelegate> copy = new IList<ReadyDelegate>(), members = this.getInvocationList();
        synchronized (members)
        {
            copy = new LinkedList<ReadyDelegate>(members);
        }
        for (Object __dummyForeachVar0 : copy)
        {
            ReadyDelegate d = (ReadyDelegate)__dummyForeachVar0;
            d.invoke();
        }
    }

    private System.Collections.Generic.IList<ReadyDelegate> _invocationList = new ArrayList<ReadyDelegate>();
    public static ReadyDelegate combine(ReadyDelegate a, ReadyDelegate b) throws Exception {
        if (a == null)
            return b;
         
        if (b == null)
            return a;
         
        __MultiReadyDelegate ret = new __MultiReadyDelegate();
        ret._invocationList = a.getInvocationList();
        ret._invocationList.addAll(b.getInvocationList());
        return ret;
    }

    public static ReadyDelegate remove(ReadyDelegate a, ReadyDelegate b) throws Exception {
        if (a == null || b == null)
            return a;
         
        System.Collections.Generic.IList<ReadyDelegate> aInvList = a.getInvocationList();
        System.Collections.Generic.IList<ReadyDelegate> newInvList = ListSupport.removeFinalStretch(aInvList, b.getInvocationList());
        if (aInvList == newInvList)
        {
            return a;
        }
        else
        {
            __MultiReadyDelegate ret = new __MultiReadyDelegate();
            ret._invocationList = newInvList;
            return ret;
        } 
    }

    public System.Collections.Generic.IList<ReadyDelegate> getInvocationList() throws Exception {
        return _invocationList;
    }

}


