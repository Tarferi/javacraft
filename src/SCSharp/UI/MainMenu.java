//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:09
//

package SCSharp.UI;

import java.util.ArrayList;
import java.util.List;
import SCSharp.ElementFlags;
import SCSharp.Mpq;
import SCSharp.UI.__MultiDialogEvent;
import SCSharp.UI.__MultiGameModeActivateDelegate;
import SCSharp.UI.__MultiPlayerEvent;
import SCSharp.UI.Builtins;
import SCSharp.UI.Cinematic;
import SCSharp.UI.CreditsScreen;
import SCSharp.UI.Game;
import SCSharp.UI.GameModeDialog;
import SCSharp.UI.GuiUtil;
import SCSharp.UI.Layer;
import SCSharp.UI.MovieElement;
import SCSharp.UI.OkDialog;
import SCSharp.UI.Painter;
import SCSharp.UI.UIElement;
import SCSharp.UI.UIPainter;
import SCSharp.UI.UIScreen;
import SCSharp.UI.UIScreenType;

//
// SCSharp.UI.MainMenu
//
// Authors:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
public class MainMenu  extends UIScreen 
{
    public MainMenu(Mpq mpq) throws Exception {
        super(mpq, "glue\\Palmm", Builtins.rez_GluMainBin);
    }

    static final int EXIT_ELEMENT_INDEX = 2;
    static final int SINGLEPLAYER_ELEMENT_INDEX = 3;
    static final int MULTIPLAYER_ELEMENT_INDEX = 4;
    static final int CAMPAIGNEDITOR_ELEMENT_INDEX = 5;
    static final int INTRO_ELEMENT_INDEX = 8;
    static final int CREDITS_ELEMENT_INDEX = 9;
    static final int VERSION_ELEMENT_INDEX = 10;
    void showGameModeDialog(UIScreenType nextScreen) throws Exception {
        GameModeDialog d = new GameModeDialog(this,mpq);
        d.Cancel = __MultiDialogEvent.combine(d.Cancel,new DialogEvent() 
          { 
            public System.Void invoke() throws Exception {
                dismissDialog();
            }

            public List<DialogEvent> getInvocationList() throws Exception {
                List<DialogEvent> ret = new ArrayList<DialogEvent>();
                ret.add(this);
                return ret;
            }
        
          });
        d.Activate = __MultiGameModeActivateDelegate.combine(d.Activate,new GameModeActivateDelegate() 
          { 
            public System.Void invoke(boolean expansion) throws Exception {
                dismissDialog();
                try
                {
                    Game.getInstance().setPlayingBroodWar(expansion);
                    GuiUtil.playSound(mpq,Builtins.Mousedown2Wav);
                    Game.getInstance().switchToScreen(nextScreen);
                }
                catch (Exception e)
                {
                    showDialog(new OkDialog(this, mpq, e.Message));
                }
            
            }

            public List<GameModeActivateDelegate> getInvocationList() throws Exception {
                List<GameModeActivateDelegate> ret = new ArrayList<GameModeActivateDelegate>();
                ret.add(this);
                return ret;
            }
        
          });
        showDialog(d);
    }

    List<UIElement> smkElements = new List<UIElement>();
    UIPainter smkPainter;
    public void addToPainter() throws Exception {
        super.addToPainter();
        for (Object __dummyForeachVar0 : smkElements)
        {
            MovieElement el = (MovieElement)__dummyForeachVar0;
            el.play();
        }
        Painter.Add(Layer.Background, smkPainter.Paint);
    }

    public void removeFromPainter() throws Exception {
        super.removeFromPainter();
        for (Object __dummyForeachVar1 : smkElements)
        {
            MovieElement el = (MovieElement)__dummyForeachVar1;
            el.stop();
        }
        Painter.Remove(Layer.Background, smkPainter.Paint);
    }

    protected void resourceLoader() throws Exception {
        super.resourceLoader();
        for (int i = 0;i < Elements.Count;i++)
            Console.WriteLine("{0}: {1} '{2}' : {3}", i, Elements[i].Type, Elements[i].Text, Elements[i].Flags);
        Elements[VERSION_ELEMENT_INDEX].Text = "v" + Consts.Version;
        Elements[SINGLEPLAYER_ELEMENT_INDEX].Flags |= ElementFlags.RightAlignText | ElementFlags.CenterTextVert;
        Elements[SINGLEPLAYER_ELEMENT_INDEX].Activate += ;
        Elements[MULTIPLAYER_ELEMENT_INDEX].Activate += ;
        Elements[CAMPAIGNEDITOR_ELEMENT_INDEX].Activate += ;
        Elements[INTRO_ELEMENT_INDEX].Activate += ;
        Elements[CREDITS_ELEMENT_INDEX].Activate += ;
        Elements[EXIT_ELEMENT_INDEX].Activate += ;
        smkElements = new List<UIElement>();
        AddMovieElements(SINGLEPLAYER_ELEMENT_INDEX, "glue\\mainmenu\\Single.smk", "glue\\mainmenu\\SingleOn.smk", 50, 70, false);
        AddMovieElements(MULTIPLAYER_ELEMENT_INDEX, "glue\\mainmenu\\Multi.smk", "glue\\mainmenu\\MultiOn.smk", 20, 12, true);
        AddMovieElements(CAMPAIGNEDITOR_ELEMENT_INDEX, "glue\\mainmenu\\Editor.smk", "glue\\mainmenu\\EditorOn.smk", 20, 18, true);
        AddMovieElements(EXIT_ELEMENT_INDEX, "glue\\mainmenu\\Exit.smk", "glue\\mainmenu\\ExitOn.smk", 15, 0, true);
        smkPainter = new UIPainter(smkElements);
    }

    void addMovieElements(int elementIndex, String normalMovie, String onMovie, ushort off_x, ushort off_y, boolean on_movie_on_top) throws Exception {
        UIElement normalElement, onElement;
        normalElement = new MovieElement(this, Elements[elementIndex].BinElement, Elements[elementIndex].Palette, normalMovie);
        onElement = new MovieElement(this, Elements[elementIndex].BinElement, Elements[elementIndex].Palette, onMovie);
        onElement.setX1(onElement.getX1() + off_x);
        onElement.setY1(onElement.getY1() + off_y);
        if (!on_movie_on_top)
            smkElements.Add(onElement);
         
        smkElements.Add(normalElement);
        if (on_movie_on_top)
            smkElements.Add(onElement);
         
        onElement.setVisible(false);
        Elements[elementIndex].MouseEnterEvent += ;
        Elements[elementIndex].MouseLeaveEvent += ;
    }

}


