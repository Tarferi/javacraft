//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:11
//

package SCSharp.UI;

import CS2JNet.JavaSupport.util.ListSupport;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import SCSharp.BinElement;
import SCSharp.ElementFlags;
import SCSharp.ElementType;
import SCSharp.Fnt;
import SCSharp.Mpq;
import SCSharp.UI.ElementEvent;
import SCSharp.UI.GuiUtil;
import SCSharp.UI.Painter;
import SCSharp.UI.UIScreen;

//
// SCSharp.UI.UIElement
//
// Authors:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
public class UIElement   
{
    BinElement el;
    Surface surface = new Surface();
    UIScreen screen;
    byte[] palette = new byte[]();
    boolean sensitive = new boolean();
    boolean visible = new boolean();
    Fnt fnt;
    String text = new String();
    public UIElement(UIScreen screen, ushort x1, ushort y1, ushort width, ushort height) throws Exception {
        this(screen, x1, y1);
        this.width = width;
        this.height = height;
    }

    public UIElement(UIScreen screen, ushort x1, ushort y1) throws Exception {
        this.screen = screen;
        this.x1 = x1;
        this.y1 = y1;
        this.sensitive = true;
        this.visible = false;
    }

    public UIElement(UIScreen screen, BinElement el, byte[] palette) throws Exception {
        this.screen = screen;
        this.el = el;
        this.x1 = el.x1;
        this.y1 = el.y1;
        this.width = el.width;
        this.height = el.height;
        this.palette = palette;
        this.sensitive = true;
        this.text = el.text;
        this.visible = (el.flags & ElementFlags.Visible) != 0;
    }

    public BinElement getBinElement() throws Exception {
        return el;
    }

    public UIScreen getParentScreen() throws Exception {
        return screen;
    }

    public Mpq getMpq() throws Exception {
        return screen.getMpq();
    }

    public String getText() throws Exception {
        return text;
    }

    public void setText(String value) throws Exception {
        text = value;
        invalidate();
    }

    public boolean getSensitive() throws Exception {
        return sensitive;
    }

    public void setSensitive(boolean value) throws Exception {
        if (sensitive == value)
            return ;
         
        sensitive = value;
        invalidate();
    }

    public boolean getVisible() throws Exception {
        return visible;
    }

    public void setVisible(boolean value) throws Exception {
        if (visible == value)
            return ;
         
        visible = value;
        invalidate();
    }

    public byte[] getPalette() throws Exception {
        return palette;
    }

    public void setPalette(byte[] value) throws Exception {
        palette = value;
        invalidate();
    }

    public Surface getSurface() throws Exception {
        if (surface == null)
            surface = createSurface();
         
        return surface;
    }

    public Fnt getFont() throws Exception {
        if (fnt == null)
        {
            int idx = 2;
            if ((getFlags() & ElementFlags.FontSmallest) != 0)
                idx = 0;
            else if ((getFlags() & ElementFlags.FontSmaller) != 0)
                idx = 3;
            else if ((getFlags() & ElementFlags.FontLarger) != 0)
                idx = 3;
            else if ((getFlags() & ElementFlags.FontLargest) != 0)
                idx = 4;
                
            fnt = GuiUtil.getFonts(getMpq())[idx];
            if (fnt == null)
                throw new Exception(String.Format("null font at index {0}..  bad things are afoot", idx));
             
        }
         
        return fnt;
    }

    public void setFont(Fnt value) throws Exception {
        fnt = value;
        invalidate();
    }

    public ElementFlags getFlags() throws Exception {
        return el.flags;
    }

    public void setFlags(ElementFlags value) throws Exception {
        el.flags = value;
    }

    public ElementType getType() throws Exception {
        return el == null ? ElementType.UserElement : el.type;
    }

    ushort x1 = new ushort();
    public ushort getX1() throws Exception {
        return x1;
    }

    public void setX1(ushort value) throws Exception {
        x1 = value;
    }

    ushort y1 = new ushort();
    public ushort getY1() throws Exception {
        return y1;
    }

    public void setY1(ushort value) throws Exception {
        y1 = value;
    }

    ushort width = new ushort();
    public ushort getWidth() throws Exception {
        return width;
    }

    public void setWidth(ushort value) throws Exception {
        width = value;
    }

    ushort height = new ushort();
    public ushort getHeight() throws Exception {
        return height;
    }

    public void setHeight(ushort value) throws Exception {
        height = value;
    }

    public Key getHotkey() throws Exception {
        return (Key)el.hotkey;
    }

    public ElementEvent Activate;
    public void onActivate() throws Exception {
        if (Activate != null)
            Activate.invoke();
         
    }

    public ElementEvent MouseEnterEvent;
    public void onMouseEnter() throws Exception {
        if (MouseEnterEvent != null)
            MouseEnterEvent.invoke();
         
    }

    public ElementEvent MouseLeaveEvent;
    public void onMouseLeave() throws Exception {
        if (MouseLeaveEvent != null)
            MouseLeaveEvent.invoke();
         
    }

    protected void invalidate() throws Exception {
        if (visible)
            Painter.invalidate(getBounds());
         
        surface = null;
    }

    public Rectangle getBounds() throws Exception {
        return new Rectangle(getX1(), getY1(), getWidth(), getHeight());
    }

    protected Surface createSurface() throws Exception {
        ElementType __dummyScrutVar0 = getType();
        if (__dummyScrutVar0.equals(ElementType.DefaultButton) || __dummyScrutVar0.equals(ElementType.Button) || __dummyScrutVar0.equals(ElementType.ButtonWithoutBorder))
        {
            return GuiUtil.ComposeText(getText(), getFont(), palette, getWidth(), getHeight(), sensitive ? 4 : 24);
        }
        else
        {
            return null;
        } 
    }

    public void paint(DateTime now) throws Exception {
        if (!visible)
            return ;
         
        Surface s = getSurface();
        if (s == null)
            return ;
         
        Painter.blit(s,new Point(getX1(), getY1()));
    }

    public boolean pointInside(int x, int y) throws Exception {
        if (x >= getX1() && x < getX1() + getWidth() && y >= getY1() && y < getY1() + getHeight())
            return true;
         
        return false;
    }

    public void mouseWheel(MouseButtonEventArgs args) throws Exception {
    }

    public void mouseButtonDown(MouseButtonEventArgs args) throws Exception {
    }

    public void mouseButtonUp(MouseButtonEventArgs args) throws Exception {
    }

    public void pointerMotion(MouseMotionEventArgs args) throws Exception {
    }

    public void mouseEnter() throws Exception {
        onMouseEnter();
    }

    public void mouseLeave() throws Exception {
        onMouseLeave();
    }

}


