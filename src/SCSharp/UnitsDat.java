//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:06
//

package SCSharp;

import SCSharp.Dat;
import SCSharp.DatCollection;
import SCSharp.DatVariableType;

//
// SCSharp.Mpq.UnitsDat
//
// Authors:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
/*
227 units
general flingy   at 0x0000.  BYTE   - index into flingy.dat
overlay          at 0x00e4.  WORD
      ?          at 0x02ab.
construct sprite at 0x0534.  DWORD  - index into images.dat
shields          at 0x0a8c.  WORD 
hitpoints        at 0x0c54.  DWORD
animation level  at 0x0fe4.  BYTE
movement flags   at 0x10c8.  BYTE
ground weapon?   at 0x11ac.  BYTE?
subunit range    at 0x1d40.  BYTE
sight range      at 0x1e24.  BYTE
      ?          at 0x1f08.  BYTE
portraits        at 0x367c?  WORD
mineral cost at 0x3844. WORD
gas cost at 0x3a0c. WORD
build time at 0x3bd4.  WORD
Restricts at 0x3d9c. WORD
? at 0x3f64.  BYTE
unit width at 0x2f5c.  WORD
unit height at 0x30ac.  WORD
space required at 0x4120.  BYTE
score for producing at 0x43d8. WORD
score for destroying at 0x45a0. WORD
*/
public class UnitsDat  extends Dat 
{
    static final int NUM_UNITS = 228;
    static final int flingy_offset = 0x0000;
    static final int overlay_offset = 0x00e4;
    static final int construct_sprite_offset = 0x0534;
    static final int animation_level_offset = 0x0fe4;
    static final int create_score_offset = 0x43d8;
    static final int destroy_score_offset = 0x45a0;
    static final int shield_offset = 0x0a8c;
    static final int hitpoint_offset = 0x0c54;
    // these are from the patch_rt.mpq for 1.13f
    static final int width_offset = 0x3124;
    static final int height_offset = 0x32ec;
    static final int portrait_offset = 0x3844;
    static final int compaiidle_offset = 0x1290;
    static final int yessoundstart_offset = 0x2a6c;
    static final int yessoundend_offset = 0x2b40;
    static final int whatsoundstart_offset = 0x2534;
    static final int whatsoundend_offset = 0x26fc;
    int flingyBlockId = new int();
    //int overlayBlockId;
    int constructSpriteBlockId = new int();
    int animationLevelBlockId = new int();
    int createScoreBlockId = new int();
    int destroyScoreBlockId = new int();
    int shieldBlockId = new int();
    int hitpointsBlockId = new int();
    int portraitBlockId = new int();
    int widthBlockId = new int();
    int heightBlockId = new int();
    int yesSoundStartBlockId = new int();
    int yesSoundEndBlockId = new int();
    int whatSoundStartBlockId = new int();
    int whatSoundEndBlockId = new int();
    int compAIIdleBlockId = new int();
    public UnitsDat() throws Exception {
        flingyBlockId = addPlacedVariableBlock(flingy_offset,NUM_UNITS,DatVariableType.Byte);
        //overlayBlockId = AddPlacedVariableBlock (overlay_offset, NUM_UNITS, DatVariableType.Word);
        constructSpriteBlockId = addPlacedVariableBlock(construct_sprite_offset,NUM_UNITS,DatVariableType.DWord);
        animationLevelBlockId = addPlacedVariableBlock(animation_level_offset,NUM_UNITS,DatVariableType.Byte);
        createScoreBlockId = addPlacedVariableBlock(create_score_offset,NUM_UNITS,DatVariableType.Word);
        destroyScoreBlockId = addPlacedVariableBlock(destroy_score_offset,NUM_UNITS,DatVariableType.Word);
        shieldBlockId = addPlacedVariableBlock(shield_offset,NUM_UNITS,DatVariableType.Word);
        hitpointsBlockId = addPlacedVariableBlock(hitpoint_offset,NUM_UNITS,DatVariableType.DWord);
        portraitBlockId = addPlacedVariableBlock(portrait_offset,NUM_UNITS,DatVariableType.Word);
        widthBlockId = addPlacedVariableBlock(width_offset,NUM_UNITS,DatVariableType.Word);
        heightBlockId = addPlacedVariableBlock(height_offset,NUM_UNITS,DatVariableType.Word);
        compAIIdleBlockId = addPlacedVariableBlock(compaiidle_offset,NUM_UNITS,DatVariableType.Byte);
        /*Is this right?*/
        yesSoundStartBlockId = addPlacedVariableBlock(yessoundstart_offset,105,DatVariableType.Word);
        /*Is this right?*/
        yesSoundEndBlockId = addPlacedVariableBlock(yessoundend_offset,105,DatVariableType.Word);
        whatSoundStartBlockId = addPlacedVariableBlock(whatsoundstart_offset,NUM_UNITS,DatVariableType.Word);
        whatSoundEndBlockId = addPlacedVariableBlock(whatsoundend_offset,NUM_UNITS,DatVariableType.Word);
    }

    /* offsets from the stardat.mpq version */
    public DatCollection<byte> getFlingyIds() throws Exception {
        return (DatCollection<byte>)getCollection(flingyBlockId);
    }

    public DatCollection<uint> getConstructSpriteIds() throws Exception {
        return (DatCollection<uint>)getCollection(constructSpriteBlockId);
    }

    public DatCollection<byte> getAnimationLevels() throws Exception {
        return (DatCollection<byte>)getCollection(animationLevelBlockId);
    }

    public DatCollection<ushort> getCreateScores() throws Exception {
        return (DatCollection<ushort>)getCollection(createScoreBlockId);
    }

    public DatCollection<ushort> getDestroyScores() throws Exception {
        return (DatCollection<ushort>)getCollection(destroyScoreBlockId);
    }

    public DatCollection<ushort> getShields() throws Exception {
        return (DatCollection<ushort>)getCollection(shieldBlockId);
    }

    public DatCollection<uint> getHitpoints() throws Exception {
        return (DatCollection<uint>)getCollection(hitpointsBlockId);
    }

    public DatCollection<ushort> getPortraits() throws Exception {
        return (DatCollection<ushort>)getCollection(portraitBlockId);
    }

    public DatCollection<ushort> getWidths() throws Exception {
        return (DatCollection<ushort>)getCollection(widthBlockId);
    }

    public DatCollection<ushort> getHeights() throws Exception {
        return (DatCollection<ushort>)getCollection(heightBlockId);
    }

    public DatCollection<byte> getCompAIIdles() throws Exception {
        return (DatCollection<byte>)getCollection(compAIIdleBlockId);
    }

    public DatCollection<ushort> getYesSoundStarts() throws Exception {
        return (DatCollection<ushort>)getCollection(yesSoundStartBlockId);
    }

    public DatCollection<ushort> getYesSoundEnds() throws Exception {
        return (DatCollection<ushort>)getCollection(yesSoundEndBlockId);
    }

    public DatCollection<ushort> getWhatSoundStarts() throws Exception {
        return (DatCollection<ushort>)getCollection(whatSoundStartBlockId);
    }

    public DatCollection<ushort> getWhatSoundEnds() throws Exception {
        return (DatCollection<ushort>)getCollection(whatSoundEndBlockId);
    }

}


