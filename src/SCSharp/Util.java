//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:11
//

package SCSharp;


public class Util   
{
    // read in a LE word
    public static ushort readWord(Stream fs) throws Exception {
        return ((ushort)(fs.ReadByte() | (fs.ReadByte() << 8)));
    }

    public static ushort readWord(byte[] buf, int position) throws Exception {
        return ((ushort)((int)buf[position] | (int)buf[position + 1] << 8));
    }

    // read in a LE doubleword
    public static uint readDWord(Stream fs) throws Exception {
        return (uint)(fs.ReadByte() | (fs.ReadByte() << 8) | (fs.ReadByte() << 16) | (fs.ReadByte() << 24));
    }

    public static uint readDWord(byte[] buf, int position) throws Exception {
        return ((uint)((uint)buf[position] | (uint)buf[position + 1] << 8 | (uint)buf[position + 2] << 16 | (uint)buf[position + 3] << 24));
    }

    // read in a byte
    public static byte readByte(Stream fs) throws Exception {
        return (byte)fs.ReadByte();
    }

    // write a LE word
    public static void writeWord(Stream fs, ushort word) throws Exception {
        fs.WriteByte((byte)(word & 0xff));
        fs.WriteByte((byte)((word >> 8) & 0xff));
    }

    // write a LE doubleword
    public static void writeDWord(Stream fs, uint dword) throws Exception {
        fs.WriteByte((byte)(dword & 0xff));
        fs.WriteByte((byte)((dword >> 8) & 0xff));
        fs.WriteByte((byte)((dword >> 16) & 0xff));
        fs.WriteByte((byte)((dword >> 24) & 0xff));
    }

    public static String readUntilNull(StreamReader r) throws Exception {
        StringBuilder sb = new StringBuilder();
        char c = new char();
        do
        {
            c = (char)r.Read();
            if (c != 0)
                sb.Append(c);
             
        }
        while (c != 0);
        return sb.ToString();
    }

    public static String readUntilNull(byte[] buf, int position) throws Exception {
        int i = position;
        while (buf[i] != 0)
        i++;
        byte[] bs = new byte[i - position];
        Array.Copy(buf, position, bs, 0, i - position);
        return Encoding.UTF8.GetString(bs);
    }

    public static char[] RaceChar = { 'Z', 'T', 'P' };
    public static char[] RaceCharLower = { 'z', 't', 'p' };
    public static String[] TilesetNames = { "badlands", "platform", "install", "ashworld", "jungle", "desert", "ice", "twilight" };
    /**
    * Make a magic number from the specified chars
    */
    public static UInt32 makeTag(char... chars) throws Exception {
        if (chars.Length != 4)
            throw new ArgumentException("We need 4 chars");
         
        return (uint)(chars[0] | (chars[1] << 8) | (chars[2] << 16) | (chars[3] << 24));
    }

}


