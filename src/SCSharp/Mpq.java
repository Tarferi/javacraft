//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:05
//

package SCSharp;

import CS2JNet.System.StringSupport;
import SCSharp.Bin;
import SCSharp.FlingyDat;
import SCSharp.Fnt;
import SCSharp.Got;
import SCSharp.Grp;
import SCSharp.Chk;
import SCSharp.ImagesDat;
import SCSharp.IScriptBin;
import SCSharp.MapDataDat;
import SCSharp.MpqResource;
import SCSharp.PortDataDat;
import SCSharp.SfxDataDat;
import SCSharp.Spk;
import SCSharp.SpritesDat;
import SCSharp.Tbl;
import SCSharp.UnitsDat;

public abstract class Mpq  extends IDisposable 
{
    Dictionary<String, Object> cached_resources = new Dictionary<String, Object>();
    protected Mpq() throws Exception {
        cached_resources = new Dictionary<String, Object>();
    }

    public abstract Stream getStreamForResource(String path) throws Exception ;

    protected Type getTypeFromResourcePath(String path) throws Exception {
        String ext = Path.GetExtension(path);
        if (StringSupport.equals(ext.ToLower(), ".tbl"))
        {
            return Tbl.class;
        }
        else if (StringSupport.equals(ext.ToLower(), ".fnt"))
        {
            return Fnt.class;
        }
        else if (StringSupport.equals(ext.ToLower(), ".got"))
        {
            return Got.class;
        }
        else if (StringSupport.equals(ext.ToLower(), ".grp"))
        {
            return Grp.class;
        }
        else if (StringSupport.equals(ext.ToLower(), ".bin"))
        {
            if (path.ToLower().EndsWith("aiscript.bin"))
                return null;
            else /* must come before iscript.bin */
            if (path.ToLower().EndsWith("iscript.bin"))
                return IScriptBin.class;
            else
                return Bin.class;  
        }
        else if (StringSupport.equals(ext.ToLower(), ".chk"))
        {
            return Chk.class;
        }
        else if (StringSupport.equals(ext.ToLower(), ".dat"))
        {
            if (path.ToLower().EndsWith("flingy.dat"))
                return FlingyDat.class;
            else if (path.ToLower().EndsWith("images.dat"))
                return ImagesDat.class;
            else if (path.ToLower().EndsWith("sfxdata.dat"))
                return SfxDataDat.class;
            else if (path.ToLower().EndsWith("sprites.dat"))
                return SpritesDat.class;
            else if (path.ToLower().EndsWith("units.dat"))
                return UnitsDat.class;
            else if (path.ToLower().EndsWith("mapdata.dat"))
                return MapDataDat.class;
            else if (path.ToLower().EndsWith("portdata.dat"))
                return PortDataDat.class;
                   
        }
        else if (StringSupport.equals(ext.ToLower(), ".spk"))
        {
            return Spk.class;
        }
                
        return null;
    }

    public Object getResource(String path) throws Exception {
        if (cached_resources.ContainsKey(path))
            return cached_resources[path];
         
        Stream stream = getStreamForResource(path);
        if (stream == null)
            return null;
         
        Type t = getTypeFromResourcePath(path);
        if (t == null)
            return stream;
         
        MpqResource res = Activator.CreateInstance(t) instanceof MpqResource ? (MpqResource)Activator.CreateInstance(t) : (MpqResource)null;
        if (res == null)
            return null;
         
        res.readFromStream(stream);
        /* don't cache .smk files */
        if (!path.ToLower().EndsWith(".smk"))
            cached_resources[path] = res;
         
        return res;
    }

    public void dispose() throws Exception {
    }

}


