//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:04
//

package SCSharp;


public class TriggerCondition   
{
    uint number = new uint();
    public uint getNumber() throws Exception {
        return number;
    }

    public void setNumber(uint value) throws Exception {
        number = value;
    }

    uint group = new uint();
    public uint getGroup() throws Exception {
        return group;
    }

    public void setGroup(uint value) throws Exception {
        group = value;
    }

    uint amount = new uint();
    public uint getAmount() throws Exception {
        return amount;
    }

    public void setAmount(uint value) throws Exception {
        amount = value;
    }

    ushort unitType = new ushort();
    public ushort getUnitType() throws Exception {
        return unitType;
    }

    public void setUnitType(ushort value) throws Exception {
        unitType = value;
    }

    byte comparison = new byte();
    public byte getComparisonSwitch() throws Exception {
        return comparison;
    }

    public void setComparisonSwitch(byte value) throws Exception {
        comparison = value;
    }

    byte condition = new byte();
    public byte getCondition() throws Exception {
        return condition;
    }

    public void setCondition(byte value) throws Exception {
        condition = value;
    }

    byte resource = new byte();
    public byte getResource() throws Exception {
        return resource;
    }

    public void setResource(byte value) throws Exception {
        resource = value;
    }

    byte flags = new byte();
    public byte getFlags() throws Exception {
        return flags;
    }

    public void setFlags(byte value) throws Exception {
        flags = value;
    }

    public String toString() {
        try
        {
            return String.Format("Trigger{{ Condition={0} }}", getCondition());
        }
        catch (RuntimeException __dummyCatchVar0)
        {
            throw __dummyCatchVar0;
        }
        catch (Exception __dummyCatchVar0)
        {
            throw new RuntimeException(__dummyCatchVar0);
        }
    
    }

}


