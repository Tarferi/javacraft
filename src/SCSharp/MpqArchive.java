//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:05
//

package SCSharp;

import SCSharp.Mpq;

public class MpqArchive  extends Mpq 
{
    MpqReader.MpqArchive mpq;
    public MpqArchive(String path) throws Exception {
        mpq = new MpqReader.MpqArchive(path);
    }

    public Stream getStreamForResource(String path) throws Exception {
        try
        {
            return mpq.openFile(path);
        }
        catch (FileNotFoundException __dummyCatchVar0)
        {
            return null;
        }
    
    }

    public void dispose() throws Exception {
        mpq.dispose();
    }

}


