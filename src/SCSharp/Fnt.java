//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:04
//

package SCSharp;

import SCSharp.Glyph;
import SCSharp.MpqResource;
import SCSharp.Util;

public class Fnt   implements MpqResource
{
    Stream stream = new Stream();
    public Fnt() throws Exception {
    }

    public void readFromStream(Stream stream) throws Exception {
        this.stream = stream;
        readFontHeader();
        readGlyphOffsets();
        glyphs = new Dictionary<int, Glyph>();
    }

    void readFontHeader() throws Exception {
        /*uint name =*/
        Util.readDWord(stream);
        lowIndex = Util.readByte(stream);
        highIndex = Util.readByte(stream);
        if (lowIndex > highIndex)
        {
            byte tmp = lowIndex;
            lowIndex = highIndex;
            highIndex = tmp;
        }
         
        maxWidth = Util.readByte(stream);
        maxHeight = Util.readByte(stream);
        /*uint unknown =*/
        Util.readDWord(stream);
    }

    Dictionary<uint, uint> offsets = new Dictionary<uint, uint>();
    void readGlyphOffsets() throws Exception {
        offsets = new Dictionary<uint, uint>();
        for (uint c = lowIndex;c < highIndex;c++)
            offsets.Add(c, Util.readDWord(stream));
    }

    Glyph getGlyph(int c) throws Exception {
        if (glyphs.ContainsKey(c))
            return glyphs[c];
         
        stream.Position = offsets[(uint)c];
        byte letterWidth = Util.readByte(stream);
        byte letterHeight = Util.readByte(stream);
        byte letterXOffset = Util.readByte(stream);
        byte letterYOffset = Util.readByte(stream);
        byte[][] bitmap = new byte[letterHeight, letterWidth];
        int x = new int(), y = new int();
        x = letterWidth - 1;
        y = letterHeight - 1;
        while (true)
        {
            byte b = Util.readByte(stream);
            int count = (b & 0xF8) >> 3;
            byte cmap_entry = (byte)(b & 0x07);
            for (int i = 0;i < count;i++)
            {
                bitmap[y, x] = 0;
                x--;
                if (x < 0)
                {
                    x = letterWidth - 1;
                    y--;
                    if (y < 0)
                        goto done
                     
                }
                 
            }
            bitmap[y, x] = (byte)cmap_entry;
            x--;
            if (x < 0)
            {
                x = letterWidth - 1;
                y--;
                if (y < 0)
                    goto done
                 
            }
             
        }
        done:glyphs.Add(c, new Glyph(letterWidth, letterHeight, letterXOffset, letterYOffset, bitmap));
        return glyphs[c];
    }

    public Glyph get___idx(int index) throws Exception {
        if (index < lowIndex || index > highIndex)
            throw new ArgumentOutOfRangeException("index", String.Format("value of {0} out of range of {1}-{2}", index, lowIndex, highIndex));
         
        return getGlyph(index);
    }

    public int getSpaceSize() throws Exception {
        return this.get___idx(109 - 1).getWidth();
    }

    /* 109 = ascii for 'm' */
    public int getLineSize() throws Exception {
        return maxHeight;
    }

    public int getMaxWidth() throws Exception {
        return maxWidth;
    }

    public int getMaxHeight() throws Exception {
        return maxHeight;
    }

    public int getLowIndex() throws Exception {
        return lowIndex;
    }

    public int getHighIndex() throws Exception {
        return highIndex;
    }

    Dictionary<int, Glyph> glyphs = new Dictionary<int, Glyph>();
    byte highIndex = new byte();
    byte lowIndex = new byte();
    byte maxWidth = new byte();
    byte maxHeight = new byte();
    public void dumpGlyphs() throws Exception {
        for (int c = lowIndex;c < highIndex;c++)
        {
            Console.WriteLine("Letter: {0}", c);
            dumpGlyph(c);
        }
    }

    public void dumpGlyph(int c) throws Exception {
        Glyph g = getGlyph(c);
        byte[][] bitmap = g.getBitmap();
        for (int y = g.getHeight() - 1;y >= 0;y--)
        {
            for (int x = g.getWidth() - 1;x >= 0;x--)
            {
                if (bitmap[y, x] == 0)
                    Console.Write(" ");
                else
                    Console.Write("#"); 
            }
            Console.WriteLine();
        }
        Console.WriteLine();
    }

}


