//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:04
//

package SCSharp;

import CS2JNet.JavaSupport.language.RefSupport;
import SCSharp.Trigger;

public class TriggerData   
{
    public TriggerData() throws Exception {
        triggers = new List<Trigger>();
    }

    public void parse(byte[] data, boolean briefing) throws Exception {
        int offset = 0;
        while (offset < data.Length)
        {
            Trigger t = new Trigger();
            RefSupport<int> refVar___0 = new RefSupport<int>(offset);
            t.Parse(data, refVar___0);
            offset = refVar___0.getValue();
            triggers.Add(t);
        }
    }

    List<Trigger> triggers = new List<Trigger>();
    public List<Trigger> getTriggers() throws Exception {
        return triggers;
    }

}


