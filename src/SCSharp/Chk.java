//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:03
//

package SCSharp;

import CS2JNet.System.StringSupport;
import SCSharp.MpqResource;
import SCSharp.Tileset;
import SCSharp.TriggerData;
import SCSharp.UnitInfo;
import SCSharp.Util;

public class Chk   implements MpqResource
{
    public Chk() throws Exception {
    }

    static final String DIM = "DIM ";
    static final String MTXM = "MTXM";
    static class SectionData   
    {
        public long data_position = new long();
        public uint data_length = new uint();
        public byte[] buffer = new byte[]();
    }

    Stream stream = new Stream();
    Dictionary<String, SectionData> sections = new Dictionary<String, SectionData>();
    public void readFromStream(Stream stream) throws Exception {
        this.stream = stream;
        byte[] section_name_buf = new byte[4];
        String section_name = new String();
        sections = new Dictionary<String, SectionData>();
        while (true)
        {
            stream.Read(section_name_buf, 0, 4);
            SectionData sec_data = new SectionData();
            sec_data.data_length = Util.readDWord(stream);
            sec_data.data_position = stream.Position;
            section_name = Encoding.ASCII.GetString(section_name_buf, 0, 4);
            sections.Add(section_name, sec_data);
            if (stream.Position + sec_data.data_length >= stream.Length)
                break;
             
            stream.Position += sec_data.data_length;
        }
        /* parse only the sections we really care
        			 * about up front.  the rest are done as
        			 * needed.  this speeds up the Play Custom
        			 * screen immensely. */
        /* find out what version we're dealing with */
        parseSection("VER ");
        if (sections.ContainsKey("IVER"))
            parseSection("IVER");
         
        if (sections.ContainsKey("IVE2"))
            parseSection("IVE2");
         
        /* strings */
        parseSection("STR ");
        /* load in the map info */
        /* map name/description */
        parseSection("SPRP");
        /* dimension */
        parseSection("DIM ");
        /* tileset info */
        parseSection("ERA ");
        /* player information */
        parseSection("OWNR");
        parseSection("SIDE");
    }

    void parseSection(String section_name) throws Exception {
        SectionData sec = sections[section_name];
        if (sec == null)
            throw new Exception(String.Format("map file is missing section {0}, cannot load", section_name));
         
        if (sec.buffer == null)
        {
            stream.Position = sec.data_position;
            sec.buffer = new byte[sec.data_length];
            stream.Read(sec.buffer, 0, (int)sec.data_length);
        }
         
        byte[] section_data = sec.buffer;
        if (StringSupport.equals(section_name, "TYPE"))
        {
            scenarioType = Util.ReadWord(section_data, 0);
        }
        else if (StringSupport.equals(section_name, "ERA "))
            tileSet = (Tileset)Util.ReadWord(section_data, 0);
        else if (StringSupport.equals(section_name, "DIM "))
        {
            width = Util.ReadWord(section_data, 0);
            height = Util.ReadWord(section_data, 2);
        }
        else if (StringSupport.equals(section_name, "MTXM"))
        {
            mapTiles = new ushort[width, height];
            int y = new int(), x = new int();
            for (y = 0;y < height;y++)
                for (x = 0;x < width;x++)
                    mapTiles[x, y] = Util.ReadWord(section_data, (y * width + x) * 2);
        }
        else if (StringSupport.equals(section_name, "MASK"))
        {
            mapMask = new byte[width, height];
            int y = new int(), x = new int();
            int i = 0;
            for (y = 0;y < height;y++)
                for (x = 0;x < width;x++)
                    mapMask[x, y] = section_data[i++];
        }
        else if (StringSupport.equals(section_name, "SPRP"))
        {
            int nameStringIndex = Util.ReadWord(section_data, 0);
            int descriptionStringIndex = Util.ReadWord(section_data, 2);
            Console.WriteLine("mapName = {0}", nameStringIndex);
            Console.WriteLine("mapDescription = {0}", descriptionStringIndex);
            mapName = getMapString(nameStringIndex);
            mapDescription = getMapString(descriptionStringIndex);
        }
        else if (StringSupport.equals(section_name, "STR "))
        {
            ReadStrings(section_data);
        }
        else if (StringSupport.equals(section_name, "OWNR"))
        {
            numPlayers = 0;
            for (int i = 0;i < 12;i++)
            {
                /* 
                					   00 - Unused
                					   03 - Rescuable
                					   05 - Computer
                					   06 - Human
                					   07 - Neutral
                					*/
                if (section_data[i] == 0x05)
                    numComputerSlots++;
                else if (section_data[i] == 0x06)
                    numHumanSlots++;
                  
            }
        }
        else if (StringSupport.equals(section_name, "SIDE"))
        {
            /*
            				  00 - Zerg
            				  01 - Terran
            				  02 - Protoss
            				  03 - Independent
            				  04 - Neutral
            				  05 - User Select
            				  07 - Inactive
            				  10 - Human
            				*/
            numPlayers = 0;
            for (int i = 0;i < 12;i++)
            {
                if (section_data[i] == 0x05)
                    /* user select */
                    numPlayers++;
                 
            }
        }
        else if (StringSupport.equals(section_name, "UNIT"))
        {
            ReadUnits(section_data);
        }
        else if (StringSupport.equals(section_name, "MBRF"))
        {
            briefingData = new TriggerData();
            briefingData.Parse(section_data, true);
        }
        else
            Console.WriteLine("Unhandled Chk section type {0}, length {1}", section_name, section_data.Length);           
    }

    List<UnitInfo> units = new List<UnitInfo>();
    void readUnits(byte[] data) throws Exception {
        units = new List<UnitInfo>();
        MemoryStream stream = new MemoryStream(data);
        Console.WriteLine("unit section data = {0} bytes long", data.Length);
        int i = 0;
        while (i <= data.Length / 36)
        {
            /*uint serial =*/
            Util.ReadDWord(stream);
            ushort x = Util.ReadWord(stream);
            ushort y = Util.ReadWord(stream);
            ushort type = Util.ReadWord(stream);
            Util.ReadWord(stream);
            Util.ReadWord(stream);
            Util.ReadWord(stream);
            byte player = Util.ReadByte(stream);
            Util.ReadByte(stream);
            Util.ReadByte(stream);
            Util.ReadByte(stream);
            Util.ReadDWord(stream);
            Util.ReadWord(stream);
            Util.ReadWord(stream);
            Util.ReadByte(stream);
            Util.ReadByte(stream);
            Util.ReadByte(stream);
            Util.ReadByte(stream);
            Util.ReadByte(stream);
            Util.ReadByte(stream);
            Util.ReadByte(stream);
            Util.ReadByte(stream);
            i++;
            UnitInfo info = new UnitInfo();
            info.unit_id = type;
            info.x = x;
            info.y = y;
            info.player = player;
            units.Add(info);
        }
    }

    void readStrings(byte[] data) throws Exception {
        MemoryStream stream = new MemoryStream(data);
        int i = new int();
        int num_strings = Util.ReadWord(stream);
        int[] offsets = new int[num_strings];
        for (i = 0;i < num_strings;i++)
        {
            offsets[i] = Util.ReadWord(stream);
        }
        StreamReader tr = new StreamReader(stream);
        strings = new String[num_strings];
        for (i = 0;i < num_strings;i++)
        {
            if (tr.BaseStream.Position != offsets[i])
            {
                tr.BaseStream.Seek(offsets[i], SeekOrigin.Begin);
                tr.DiscardBufferedData();
            }
             
            strings[i] = Util.readUntilNull(tr);
        }
    }

    String[] strings = new String[]();
    public String getMapString(int idx) throws Exception {
        if (idx == 0)
            return "";
         
        return strings[idx - 1];
    }

    String mapName = new String();
    public String getName() throws Exception {
        return mapName;
    }

    String mapDescription = new String();
    public String getDescription() throws Exception {
        return mapDescription;
    }

    ushort scenarioType = new ushort();
    public ushort getScenarioType() throws Exception {
        return scenarioType;
    }

    Tileset tileSet = Tileset.Badlands;
    public Tileset getTileset() throws Exception {
        return tileSet;
    }

    ushort width = new ushort();
    public ushort getWidth() throws Exception {
        return width;
    }

    ushort height = new ushort();
    public ushort getHeight() throws Exception {
        return height;
    }

    ushort[][] mapTiles = new ushort[][]();
    public ushort[][] getMapTiles() throws Exception {
        if (mapTiles == null)
        {
            parseSection("MTXM");
            /* XXX put these here for now.. */
            /* tile info */
            if (sections.ContainsKey("TILE"))
                parseSection("TILE");
             
            /* isometric mapping */
            if (sections.ContainsKey("ISOM"))
                parseSection("ISOM");
             
        }
         
        return mapTiles;
    }

    byte[][] mapMask = new byte[][]();
    public byte[][] getMapMask() throws Exception {
        if (mapMask == null)
            parseSection("MASK");
         
        return mapMask;
    }

    int numComputerSlots = new int();
    public int getNumComputerSlots() throws Exception {
        return numComputerSlots;
    }

    int numHumanSlots = new int();
    public int getNumHumanSlots() throws Exception {
        return numHumanSlots;
    }

    int numPlayers = new int();
    public int getNumPlayers() throws Exception {
        return numPlayers;
    }

    TriggerData briefingData;
    public TriggerData getBriefingData() throws Exception {
        if (briefingData == null)
            parseSection("MBRF");
         
        return briefingData;
    }

    public List<UnitInfo> getUnits() throws Exception {
        if (units == null)
            /* initial units on the map */
            parseSection("UNIT");
         
        return units;
    }

}


