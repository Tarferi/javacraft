//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:06
//

package SCSharp;

import SCSharp.MpqResource;
import SCSharp.ParallaxLayer;
import SCSharp.Util;

public class Spk   implements MpqResource
{
    ParallaxLayer[] layers = new ParallaxLayer[]();
    public Spk() throws Exception {
    }

    public void readFromStream(Stream stream) throws Exception {
        layers = new ParallaxLayer[Util.readWord(stream)];
        for (int i = 0;i < layers.Length;i++)
        {
            int numobjects = Util.readWord(stream);
            layers[i] = new ParallaxLayer(numobjects);
            Console.WriteLine("Layer {0} has {1} objects", i, numobjects);
        }
        for (int i = 0;i < layers.Length;i++)
        {
            Console.WriteLine("Reading layer {0}", i);
            layers[i].ReadFromStream(stream);
        }
    }

    public ParallaxLayer[] getLayers() throws Exception {
        return layers;
    }

}


