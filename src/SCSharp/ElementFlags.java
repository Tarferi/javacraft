//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:01
//

package SCSharp;


public enum ElementFlags
{
    //
    // SCSharp.Mpq.Bin
    //
    // Authors:
    //	Chris Toshok (toshok@gmail.com)
    //
    // Copyright 2006-2010 Chris Toshok
    //
    //
    // Permission is hereby granted, free of charge, to any person obtaining
    // a copy of this software and associated documentation files (the
    // "Software"), to deal in the Software without restriction, including
    // without limitation the rights to use, copy, modify, merge, publish,
    // distribute, sublicense, and/or sell copies of the Software, and to
    // permit persons to whom the Software is furnished to do so, subject to
    // the following conditions:
    //
    // The above copyright notice and this permission notice shall be
    // included in all copies or substantial portions of the Software.
    //
    // THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    // EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
    // MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    // NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
    // LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
    // OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
    // WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
    //
    Unknown00000001,
    Unknown00000002,
    Unknown00000004,
    Visible,
    RespondToMouse,
    Unknown00000020,
    CancelButton,
    NoSoundOnMouseOvr,
    Unknown00000100,
    HasHotkey,
    FontSmallest,
    FontSmaller,
    Unknown00001000,
    Transparent,
    FontLargest,
    Unused00008000,
    FontLarger,
    Unused00020000,
    Translucent,
    DefaultButton,
    BringToFront,
    CenterTextHoriz,
    RightAlignText,
    CenterTextVert,
    TopAlignText,
    BottomAlignText,
    NoClickSound,
    Unused08000000,
    Unused10000000,
    Unused20000000,
    Unused40000000
}

