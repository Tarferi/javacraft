//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:04
//

package SCSharp;

import SCSharp.DatCollection;
import SCSharp.DatVariable;
import SCSharp.DatVariableType;
import SCSharp.MpqResource;

//
// SCSharp.Mpq.Dat
//
// Authors:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
public abstract class Dat   implements MpqResource
{
    protected byte[] buf = new byte[]();
    List<DatVariable> variables = new List<DatVariable>();
    Dictionary<int, DatCollection> collections = new Dictionary<int, DatCollection>();
    protected Dat() throws Exception {
        variables = new List<DatVariable>();
        collections = new Dictionary<int, DatCollection>();
    }

    public void readFromStream(Stream stream) throws Exception {
        buf = new byte[(int)stream.Length];
        stream.Read(buf, 0, buf.Length);
    }

    //Console.WriteLine ("buf size = {0}, offset = {0}", buf.Length, offset);
    int offset = 0;
    protected int addVariableBlock(int num_entries, DatVariableType var) throws Exception {
        DatVariable new_variable = new DatVariable(var,offset,num_entries);
        int rv = variables.Count;
        variables.Add(new_variable);
        offset += new_variable.blockSize();
        return rv;
    }

    protected void addUnknownBlock(int size) throws Exception {
        offset += size;
    }

    protected int addPlacedVariableBlock(int place, int num_entries, DatVariableType var) throws Exception {
        offset = place;
        DatVariable new_variable = new DatVariable(var,offset,num_entries);
        int rv = variables.Count;
        variables.Add(new_variable);
        offset += new_variable.blockSize();
        return rv;
    }

    public int getVariableOffset(int variableId) throws Exception {
        return variables[variableId].Offset;
    }

    protected DatCollection getCollection(int variableId) throws Exception {
        if (collections.ContainsKey(variableId))
            return collections[variableId];
         
        DatCollection rv = variables[variableId].CreateCollection(buf);
        collections[variableId] = rv;
        return rv;
    }

}


