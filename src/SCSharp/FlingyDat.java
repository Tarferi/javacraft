//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:04
//

package SCSharp;

import SCSharp.Dat;
import SCSharp.DatCollection;
import SCSharp.DatVariableType;

//
// SCSharp.Mpq.FlingyDat
//
// Authors:
//	Chris Toshok (toshok@gmail.com)
//
// Copyright 2006-2010 Chris Toshok
//
//
// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to
// the following conditions:
//
// The above copyright notice and this permission notice shall be
// included in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
// EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
// NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
// LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
// OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
/*
  sprite id      at 0x0000. WORD.
  speed          at 0x0170. DWORD.
  turn style     at 0x0450. WORD
  acceleration   at 0x05c0. DWORD.
  turning radius at 0x08a0. BYTE.
  move controls  at ?.. ?.
*/
public class FlingyDat  extends Dat 
{
    int spriteBlockId = new int();
    int speedBlockId = new int();
    int turnStyleBlockId = new int();
    int accelBlockId = new int();
    //int turnRadiusBlockId;
    static final int NUM_ENTRIES = 184;
    public FlingyDat() throws Exception {
        spriteBlockId = addVariableBlock(NUM_ENTRIES,DatVariableType.Word);
        speedBlockId = addVariableBlock(NUM_ENTRIES,DatVariableType.DWord);
        turnStyleBlockId = addVariableBlock(NUM_ENTRIES,DatVariableType.Word);
        accelBlockId = addVariableBlock(NUM_ENTRIES,DatVariableType.DWord);
    }

    /* XXX turnRadiusBlockId */
    public DatCollection<ushort> getSpriteIds() throws Exception {
        return (DatCollection<ushort>)getCollection(spriteBlockId);
    }

    public DatCollection<uint> getSpeeds() throws Exception {
        return (DatCollection<uint>)getCollection(speedBlockId);
    }

    public DatCollection<ushort> getTurnStyles() throws Exception {
        return (DatCollection<ushort>)getCollection(turnStyleBlockId);
    }

    public DatCollection<uint> getAccels() throws Exception {
        return (DatCollection<uint>)getCollection(accelBlockId);
    }

}


