//
// Translated by CS2J (http://www.cs2j.com): 3. 6. 2016 8:43:16
//



public class Brute   
{
    static final int MAX_COUNT = 45;
    static String allowable = "()\\/._0123456789abcdefghijklmnopqrstuvwxyz";
    static StreamWriter sw = new StreamWriter();
    static int entry_count = 0;
    static Object iolock = new Object();
    static Object consolelock = new Object();
    static void testStrings(MpqReader.MpqArchive mpq, String dot, int count, char[] test, int index) throws Exception {
        if (index == count)
        {
            entry_count++;
            if (entry_count % 10000 == 0)
            {
                synchronized (consolelock)
                {
                    {
                        Console.Write(dot);
                        Console.Out.Flush();
                    }
                }
            }
             
            String entry = new String(test, 0, count);
            if (mpq.fileExists(entry))
            {
                synchronized (iolock)
                {
                    {
                        sw.WriteLine(entry);
                    }
                }
            }
             
            return ;
        }
         
        for (int j = 0;j < allowable.Length;j++)
        {
            test[index] = allowable[j];
            TestStrings(mpq, dot, count, test, index + 1);
        }
    }

    public static void main(String[] args) throws Exception {
        Brute.Main(args);
    }

    public static void Main(String[] args) throws Exception {
        if (args.Length < 2)
        {
            Console.WriteLine("usage:  brute.exe <mpq-file> <output-list.txt>");
            Environment.Exit(0);
        }
         
        MpqReader.MpqArchive mpq = new MpqReader.MpqArchive(args[0]);
        sw = File.CreateText(args[1]);
        Thread t = new Thread();
        t.Start();
        for (int count = 8;count < MAX_COUNT;count++)
        {
            if (count % 2 == 1)
                continue;
             
            char[] test = new char[count];
            synchronized (consolelock)
            {
                {
                    Console.Write("M{0}", count);
                    Console.Out.Flush();
                }
            }
            TestStrings(mpq, "m", count, test, 0);
        }
    }

}


